<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyToken extends Model
{
    protected $fillable = [
        'property_id',
        'user_id',
        'token_name',
        'token_symbol',
        'token_value',
        'token_supply',
        'token_decimal',
        'token_image'
    ];

    public function Property(){
        return $this->belongsTo('App\Property', 'property_id', 'id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}

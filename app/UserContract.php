<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserContract extends Model
{ 


	protected $table = 'usercontract';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */
    protected $fillable = [
  		//'user_id',
    	'tokenname',
    	'tokensymbol',
    	'tokenvalue',
    	'tokensupply',
    	'contract_address',
        'bonus',
    	'acquistion',
    	'usage',
    	'redemption',
    	'decimal',
        'token_image',            
        'title',            
        'content',            
    	'banner_image',
        'status'            
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];//

    /**
     * Used to Get Property Details
     */
    public function property(){
        return $this->belongsTo('App\Property', 'property_id');
    }

    /**
     * Used to get UserContract
     */
    public function getUserContract($tokenId){
        $usercontract = UserContract::with('property')->findOrFail($tokenId);
        return $usercontract;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvestorBonusToken extends Model
{
    protected $table = 'investorbonustoken';
    
    protected $fillable = [
        'tokendividendid',
        'propertyid',
        'investor_id',
        'issuer_id',
        'total_tokens',
        'inves_holdingdays',
        'dividendtokens',
        'dividendapprovestatus',
        'approveddate',
        'total_holdingdays_req',
        'txnhash',
        'status',        
    ];
}

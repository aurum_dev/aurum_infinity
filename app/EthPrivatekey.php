<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EthPrivatekey extends Model
{
      protected $table='ethprivatekey';
    protected $fillable = ['user_email','privatekey'];
}

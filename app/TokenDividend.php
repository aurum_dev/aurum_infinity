<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TokenDividend extends Model
{
      protected $table='tokendividend';
    protected $fillable = ['propertyid','user_id','startdate','enddate','title','description','dividend','doc1','doc2','doc3','doc4','doc5','status'];
}
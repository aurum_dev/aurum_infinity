<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','last_name', 'email', 'password','user_type','dob','country_id','mobile','kyc','g2f_temp','eth_otp',
        'google2fa_secret','verified','email_token','issuer_pros_doc','approved','btc_address','eth_address','bank_approved','mobile',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function addressbook()
    {
        return $this->hasMany('App\Addressbook','user_id');
    }

    public function identity()
    {
        return $this->hasOne('App\UserIdentity','user_id');
    }

    public function finance()
    {
        return $this->hasOne('App\UserFinance','user_id');
    }

    public function background()
    {
        return $this->hasOne('App\UserBackground','user_id');
    }

    public static function getUser($id=0){
        $sql = User::orderBy('created_at' , 'desc');
        if($id != 0)
            $users = $sql->find($id);
        else
            $users = $sql->get();    
        return $users;
    }
}

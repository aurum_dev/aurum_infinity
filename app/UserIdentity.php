<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserIdentity extends Model
{
    protected $fillable=[
    	'user_id',
    	'first_name',
		'middle_name',
    	'last_name',
    	'dob',
    	'address',
    	'state',
    	'city',
    	'pincode',
    	'pan_number',
    	'documentType',
    	'document_number',
    	'pan',
    	'frontdocument',
    	'backdocument',
		'country_code',
        'primary_phone',
        'city_id',
        'eth_address',
    ]; 


    public function country()
    {
        return $this->hasOne('App\Country','code','country_code');
    }

    public function city()
    {
        return $this->hasOne('App\City','country_code','country_code');
    }
}

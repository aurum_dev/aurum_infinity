<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaticTransfer extends Model
{
      protected $table='matictransfer';
    protected $fillable = ['user_id','maticamt','transferdate'];
}
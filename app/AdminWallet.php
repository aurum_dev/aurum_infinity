<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminWallet extends Model
{
    protected $table='admin_wallet';
    protected $fillable = [
        'usertokentransid',
        'property_id',
        'issuer_id',
        'investor_id',
        'payment_type',
        'total_tokens',
        'paybyinvestor',
        'paytoissuer',
        'admincommission',
        'txnhash',
        'status',
        'razorpayhash',
        'razorpaytxnhash',

    ];

 /*   public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function property(){
        return $this->belongsTo('App\Property', 'user_contract_id');
    }

    public function propertyToken()
    {
        return $this->belongsTo('App\PropertyToken','user_contract_id');
    }

    public function usercontract()
    {
        return $this->belongsTo('App\UserContract','user_contract_id');
    }*/
}

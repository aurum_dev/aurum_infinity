<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileHelp extends Model
{
    protected $table = 'help';
    
    protected $fillable = [
        'user_id',
        'name',
        'email',
        'message',
        'priority',
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
     protected $fillable = [

       'user_id', 'user_type','log_message','log_time','ip_address'

   ];
}

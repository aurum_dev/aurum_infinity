<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProfileIdentity extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_code'  =>  'required',
            'first_name' => 'required|max:30',
              'middle_name' => '',
           // 'middle_name' => 'required|max:30',
            'last_name' => 'required|max:30',
            'dob' => 'required|max:20',
            'address'   => 'required|max:100',
            'city'      =>  'required|max:50',
            'state'     =>  'required|max:50',
            'pincode'   =>  'required|max:10',
            'pan_number'    =>  'required|max:10',
            'repan_number'  =>  'required|max:10|same:pan_number',
        //    'pan'           =>  'required',
         //   'document_number'   => 'required',
          //  'redocument_number' => 'required|same:document_number',
          //  'frontdocument'     =>  'required',
          //  'backdocument'      =>  'required',
        ];
    }
}

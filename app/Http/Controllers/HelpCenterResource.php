<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HelpCenter;
use App\ProfileHelp;
use Mail;
use App\Mail\HelpMail;

class HelpCenterResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $helpcenter = ProfileHelp::orderBy('created_at' , 'desc')->get();
        return view('admin.helpcenter.index', compact('helpcenter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function create()
    {
       return view('admin.helpcenter.create');
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'subject' => 'required',          
            'description' => 'required',
           
        ]);

        try{
            print_r($_REQUEST);

            echo $request->email;
            HelpCenter::create($request->all());
            $email_userdata=[
            'name' => $request->name,
            'email' => $request->email, 
            'subject' => $request->subject,  
             'description' => $request->description,            
            'email_token' => base64_encode($request->email)
        ];
            
            if($_SERVER['REMOTE_ADDR'] != '127.0.0.1')
            Mail::to($request->email)->send(new HelpMail($email_userdata));

            return back()->with('flash_success', 'HelpCenter Successfully Stored');

        } 

        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'something went wrong');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $helpcenter = HelpCenter::findOrFail($id);
            return view('admin.helpcenter.edit',compact('helpcenter'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'type' => 'required',
            'title' => 'required',
            'description' => 'required',
           
        ]);

        try{
        
           $helpcenter = HelpCenter::findOrFail($id);

            $helpcenter->type = $request->type;
            $helpcenter->title = $request->title;
            $helpcenter->description = $request->description;
          
            $helpcenter->save();
             return redirect()->route('admin.helpcenter.index')->with('flash_success', 'Updated successfully');   

        } 

        catch (ModelNotFoundException $e) {
            dd($e);
            return back()->with('flash_error', 'something went wrong');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         try {
            HelpCenter::find($id)->delete();
            return back()->with('message', 'HelpCenter Deleted Successfully');
        } 
        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'something went wrong');
        }
    }
}

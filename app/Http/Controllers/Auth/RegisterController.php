<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\User;
use App\Country;
use Mail;
use App\Mail\WelcomeMail;
use App\Mail\AdminWelcomeMail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {   
        $countries=Country::get();
        return view('auth.register',compact('countries'));
    }

    public function showIssuerRegistrationForm()
    {   
        $countries=Country::get();
        
        //echo public_path().'/issuer_doc/';
        //echo "Testing";
        return view('auth.issuerregister',compact('countries'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

     protected function validator(array $data)
    {
       
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'user_type' => ['required'],
            'issuer_pros_doc' => 'nullable|max:2048|mimes:doc,docx,pdf,jpg'
            
        ];       

        $validator = Validator::make($data, $rules);

        return $validator;
    }
  /*  protected function validator(array $data)
    {
       
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'user_type' => ['required'],
            'mobile' => ['required','max:11','unique:users'],
            'issuer_pros_doc' => 'nullable|max:2048|mimes:doc,docx,pdf,jpg'
            
        ];       

        $validator = Validator::make($data, $rules);
       
    if ($validator->fails()) {
    echo  response()->json($validator->errors(), 400);
  }
       //  return $validator;
    
    }*/

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {        
        $email=strtolower($data['email']);
        $email_token=base64_encode($email);
       
        $userdata=[    
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'user_type' => $data['user_type'],
            'email_token' =>$email_token,
            'mobile'=>$data['mobile'],
        ];

        $email_userdata=[
            'name' => $data['name'],
            'email' => $email,           
            'email_token' => base64_encode($email)
        ];

        if($_SERVER['REMOTE_ADDR'] != '127.0.0.1')
            Mail::to($email)->send(new WelcomeMail($email_userdata));
            ///$subject="Welcome to Direxct.";
           // $to=$email;
           // $this->emailSend($subject,$to);
           



        return User::create($userdata);
    }

public function emailSend($subject,$to){

$url = "https://api.sendgrid.com/v3/mail/send";
$curl = curl_init($url);
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

$headers = array(
   "Authorization: Bearer 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",
   "Content-Type: application/json",
);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

$data = '{"personalizations": [{"to": [{"email": "'.$to.'"}]}],"from": {"email": "shweta.arneja@aurumproptech.in"},"subject": "'.$subject.'","content": [{"type": "text/plain", "value": "and easy to do anywhere, even with cURL"}]}';
//$data1='';
curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

//for debug only!
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

$resp = curl_exec($curl);
curl_close($curl);
$rt=json_decode($resp,true);
}
    protected function create_issuer(array $data)
    {        
        $email=strtolower($data['email']);
        $email_token=base64_encode($email);

        $iss_doc_url = '';
        if(!empty($data['issuer_pros_doc'])){
            $iss_doc = $data['issuer_pros_doc'];
            $doc_filename = time() ."-doc." . $iss_doc->getClientOriginalExtension();

            $upload_path = public_path().'/issuer_doc/';
            $iss_doc_url = '/issuer_doc/'. $doc_filename;
            $success = $iss_doc->move($upload_path, $doc_filename);
        }
        
        $userdata=[    
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'user_type' => $data['user_type'],
            'email_token' =>$email_token,
            'issuer_pros_doc'=> $iss_doc_url,
        ];

        $email_userdata=[
            'name' => $data['name'],
            'email' => $email,  
            'user_type' => "Issuer",          
            'email_token' => base64_encode($email)
        ];

        // USER EMAIL
        if($_SERVER['REMOTE_ADDR'] != '127.0.0.1')
            Mail::to($email)->send(new WelcomeMail($email_userdata));

        // ADMIN EMAIL
        if($_SERVER['REMOTE_ADDR'] != '127.0.0.1')
            Mail::to(setting('support_mail'))->send(new AdminWelcomeMail($email_userdata));



        return User::create($userdata);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function validationErrorsToString($errArray) {
        $valArr = array();
        $errStrFinal='';
        foreach ($errArray->toArray() as $key => $value) { 
            $errStr = $value[0];
            array_push($valArr, $errStr);
        }
        if(!empty($valArr)){
            $errStrFinal = implode(",", $valArr);
        }
        return $errStrFinal;
    }
    public function register(Request $request)
    {
      

        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'user_type' => ['required'],
            'mobile' => ['required','max:11','unique:users'],
            'issuer_pros_doc' => 'nullable|max:2048|mimes:doc,docx,pdf,jpg'
            
        ];       

       $validator = Validator::make($request->all(), $rules);
       $result = $this->validationErrorsToString($validator->errors());

        // $this->validator($request->all())->validate();
    
        if(strlen($result)>0){
            return redirect('/register')->with(['flash_errorr'=>$result]);
        }
        else{
            event(new Registered($user = $this->create($request->all())));
               return redirect('/register')->with(['flash_success'=>'Account created successfully. Verify your account by clicking on the link sent in your welcome mail...']);
            }
  

    //    \Session::flash('flash_success','Account created successfully. Verify your account by clicking on the link sent in your welcome mail...');       
        //return redirect('/login');
       //  return redirect('/login')->with(['flash_success'=>'Account created successfully. Verify your account by clicking on the link sent in your welcome mail...']);
    
     
    }

    public function register_issuer(Request $request)
    {
        $this->validator($request->all())->validate();  
        event(new Registered($user = $this->create_issuer($request->all())));
        \Session::flash('flash_success','Account created successfully. Admin will approve your account');
        return redirect('/issuer/register');

    }


    public function verify($token)
    {
        $user = User::where('email_token',$token)->first();
        $user->verified = 1;
        $user->save();

        \Session::flash('flash_success','Your account has been verified successfully...');
        return redirect('/login');   
    }
}

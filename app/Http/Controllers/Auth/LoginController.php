<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Cache;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Authenticatable;
use App\Http\Requests\ValidateSecretRequest;
use App\Http\Controllers\CommonController;
use App\Property;
use App\Enquiry;
use App\AdminWallet;
use App\User;

use Mail;

use Carbon\Carbon;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function investor()
    {
        try {
           //  $property = Property::where('feature', 1)->where('status','<>','pending')->orderBy('created_at', 'desc')->get();
             //$wallet = AdminWallet::orderBy('created_at', 'desc')->get();
             return view('investor');
            //$user = Auth::user();
           // (new CommonController)->updateAddress($user);
           //$property = Property::where('feature', 1)->get();
           // $property = (new CommonController)->calculatePercentage($property);
           // return view('dashboard', compact('user', 'property'));
           
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get dashboard details. Please try again later');
        }
    }
     public function blog()
    {
        try {
           //  $property = Property::where('feature', 1)->where('status','<>','pending')->orderBy('created_at', 'desc')->get();
             //$wallet = AdminWallet::orderBy('created_at', 'desc')->get();
             return view('blog');
            //$user = Auth::user();
           // (new CommonController)->updateAddress($user);
           //$property = Property::where('feature', 1)->get();
           // $property = (new CommonController)->calculatePercentage($property);
           // return view('dashboard', compact('user', 'property'));
           
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get dashboard details. Please try again later');
        }
    }
    public function issuer()
    {
        try {
           //  $property = Property::where('feature', 1)->where('status','<>','pending')->orderBy('created_at', 'desc')->get();
             //$wallet = AdminWallet::orderBy('created_at', 'desc')->get();
             return view('listproperty');
            //$user = Auth::user();
           // (new CommonController)->updateAddress($user);
           //$property = Property::where('feature', 1)->get();
           // $property = (new CommonController)->calculatePercentage($property);
           // return view('dashboard', compact('user', 'property'));
           
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get dashboard details. Please try again later');
        }
    }
 public function getenquiryform(Request $request){        
    
    
        try {
           
             $enquiry=Enquiry::create([
                    'name'=>$request->name,
                    'email'=>$request->email,
                    'mobile'=>$request->mobile,
                    'message'=>$request->message,
                ]);
             $enquiry->name=$request->name;
             $enquiry->save();
             return response()->json(['status' => 'success']);
        }
         catch (Exception $e) {
            
            return response()->json(['status' => 'failed']);
        }
    }
    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        if (Auth::guard('admin')->check()) {
            return redirect('admin/home');
        }

        if (Auth::check()) {
            if(Auth::user()->user_type == 1)
                return redirect('/dashboard');
            else
                return view('issuer.dashboard');
        }

        return view('auth.login');
    }

    public function showLoginIssuerForm()
    {
        if (Auth::guard('admin')->check()) {
            return redirect('admin/home');
        }

        if (Auth::check()) {
            if(Auth::user()->user_type == 1)
                return redirect('/dashboard');
            else
                return view('issuer.dashboard');
        }

        return view('auth.issuerlogin');
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */


    protected function credentials(Request $request)
    {
        if (isset($request->email)) {
            $username = $request->email;
            return ['email' => $username, 'password' => $request->password,'user_type' => $request->user_type];
        }
    }


    protected function authenticated(Request $request, $user)
    {
        if ($user->google2fa_secret) {
           Auth::logout();

           $request->session()->put('2fa:user:id', $user->id);

           return redirect('2fa/validate');
        }

        if ($user->verified==0) {
           Auth::logout();
           \Session::flash('flash_error','Please verify your account by clicking on the link from your welcome mail...');
           return redirect('/login');
        }
      
        $user=User::where('email',$user->email)->first();
        $user->last_login_at = Carbon::now();
        $user->save();

        if(Auth::user()->user_type == 1)
            $this->redirectTo = '/dashboard';
        else
            $this->redirectTo = '/issuer/dashboard';

        return redirect()->intended($this->redirectTo);
    }

     /**
     *
     * @return \Illuminate\Http\Response
     */
    public function getValidateToken()
    {

        if (session('2fa:user:id')) {
            return view('2fa/validate');
        }

        return redirect('login');
    }

    /**
     *
     * @param  App\Http\Requests\ValidateSecretRequest $request
     * @return \Illuminate\Http\Response
     */

    public function validateG2fa(Request $request)
    {


        $user = $request->user();

        // // //encrypt and then save secret
        $user->google2fa_secret = $request->secret;
        $user->save();


        $this->postValidateToken($request);
    }

    public function postValidateToken(ValidateSecretRequest $request)
    {


        //get user id and create cache key
        $userId = $request->session()->pull('2fa:user:id');
        $key    = $userId . ':' . $request->totp;

        //use cache to store token to blacklist
        Cache::add($key, true, 4);

        //login and redirect user
        Auth::loginUsingId($userId);

        if($request->ajax()) {
            return response()->json(['message' =>"Success"], 200);
        }else{
            return redirect()->intended($this->redirectTo);
        }
    }

    /*
    * Used to Show Landing Page
    */
    public function welcome(){

      try {
        // return view('welcome');
         // $property = Property::where('feature', 1)->get();
         // $property = (new CommonController)->calculatePercentage($property);
         // return view('welcome',compact('property'));
           $property = Property::where('feature', 1)->where('status','<>','pending')->orderBy('created_at', 'desc')->get();
           $wallet = AdminWallet::orderBy('created_at', 'desc')->get();
           
           return view('welcome', compact("property" ,"wallet"));
      } catch (\Throwable $th) {
          return back()->with('flash_error', 'Unable to get dashboard details. Please try again later');
      }
    }
}

<?php

namespace App\Http\Controllers;

use App\Mail\Withdrawalotp;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Auth;
use Hash;
use Session;
use Exception;
use GuzzleHttp\Client;
use App\WithdrawEth;
use App\Property;
use App\UserContract;
use App\ProfileHelp;
use App\UserIdentity;
use App\UserFinance;
use App\UserBackground;
use App\UserToken;
use App\Coins;
use App\Mail\WelcomeMail;
use App\DepositHistory;
use App\UserTokenTransaction;
use App\Http\Controllers\CommonController;
use App\Support;
use App\Http\Requests\UpdatePassword;
use App\Http\Requests\ProfileIdentity;
use App\Http\Requests\InvestStore;
use App\PropertyToken;
use App\AdminWallet;
use App\Enquiry;
use Setting;
use App\IssuerUser;
use Mail;
use App\EthPrivatekey;
use App\InvestorBonusToken;
use App\TokenDividend;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('propertyDetail');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

       public function investor()
    {
        try {
             $property = Property::where('feature', 1)->where('status','<>','pending')->orderBy('created_at', 'desc')->get();
             $wallet = AdminWallet::orderBy('created_at', 'desc')->get();
             return view('investor', compact("property" ,"wallet"));
            //$user = Auth::user();
           // (new CommonController)->updateAddress($user);
           //$property = Property::where('feature', 1)->get();
           // $property = (new CommonController)->calculatePercentage($property);
           // return view('dashboard', compact('user', 'property'));
           
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get dashboard details. Please try again later');
        }
    }
    public function index()
    {
        try {
             $property = Property::where('feature', 1)->where('status','<>','pending')->orderBy('created_at', 'desc')->get();
             $wallet = AdminWallet::orderBy('created_at', 'desc')->get();
             return view('dashboard', compact("property" ,"wallet"));
            //$user = Auth::user();
           // (new CommonController)->updateAddress($user);
           //$property = Property::where('feature', 1)->get();
           // $property = (new CommonController)->calculatePercentage($property);
           // return view('dashboard', compact('user', 'property'));
           
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get dashboard details. Please try again later');
        }
    }

    /**
     * Used to show Property List
     */
    public function propertyList($type = 'single')
    {
        try {
            $user=Auth::user();
            //$token_type = ($type == 'asset') ? 2 : 1;
            $property = Property::where('status','<>','pending')->orderBy('created_at', 'desc')->get();
            //$property   = (new Property)->getProperty(0, 0, 0, $token_type);
           // $property   = (new CommonController)->calculatePercentage($property);
            //return view('propertyList',compact('user','property'));
             $wallet = AdminWallet::orderBy('created_at', 'desc')->get();
            return view('propertyList', compact('property','wallet'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to fetch property details. Please try again later');
        }
    }

    public function propertyAssetList($type)
    {
        try {
            //$user=Auth::user();
            $token_type = ($type == 'asset-fund') ? 2 : 1;
            $property   = (new Property)->getProperty(0, 0, 0, $token_type);
            $property   = (new CommonController)->calculatePercentage($property);
            return view('propertyAssetList', ["property" => $property]);
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to fetch property details. Please try again later');
        }
    }

  public function help()
    {
        try {
            $user=Auth::user();
            return view('support', compact('user'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to fetch property details. Please try again later');
        }
    }
    public function intel()
    {
        try {
            return view('intel');
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to fetch property details. Please try again later');
        }
    }

    /**
     * Used to show Property Details
     */
    public function propertyDetail($id)
    {   
        try {
        $user=Auth::user(); 
      
        if (isset($user->verified)) {    
                $property = Property::find($id);
               /* if (!is_null($property->userContract)) {
                    $tokens                          = (new UserToken)->getUserToken($property->userContract->id)->sum('token_acquire');
                    $usd_value                       = $property->userContract->tokenvalue * $tokens;
                    $property['accuired_usd']        = $usd_value;
                    $percentage                      = ($usd_value / $property->totalDealSize) * 100;
                    $property['accuired_percentage'] = $percentage;
                } else {
                    $property['accuired_usd']        = 0;
                    $property['accuired_percentage'] = 0;
                }*/
                //print_r($property->propertyImages);                 
                return view('propertyDetail', compact('user', 'property'));
            }else{
             
              return redirect('/login');
            }  
            
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to Get Property Details');
        }
    }

    /**
     * Used to Show Invest Details
     */
    public function applyInvest($id){
        try {
            $property = Property::where('id',$id)->first();
             $wallet = AdminWallet::where('property_id',$id)->get();
            return view('invest', compact('property','wallet'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get invest Details');
        }
    }

     public function applypayment(){
        try {
           // $property = Property::where('id',$id)->first();
          //   $wallet = AdminWallet::where('property_id',$id)->get();
            return view('payment');
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get invest Details');
        }
    }
    



     public function doPayment(Request $request)
    {
        /* Fetching all the testing credentials from .env file
         * payu_key,payu_salt etc
         */
         try {
             $transactionId = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
             $user = Auth::user();
           
            $history = new DepositHistory;
            $history->user_id = $user->id;
            $history->payment_id = $transactionId;
            $history->payment_mode = 'INR';
            $history->razorpaytxnhash='';    
            $history->amount = $request->payamount;
            $history->save();

            $token_equ_value = $bonus_token = $total_token = 0;
            $coin_price = 0;
            $user_contract = Property::where('id',$request->token_id)->first();

            $issuer_id = $user_contract->user_id;
            $issuer_user = User::where('id',$issuer_id)->first();

            $coins      = (new Coins)->getCoin($request->payby);
            $to_address = (!is_null($coins)) ? $coins->address : '';

            $bonus_token = (!is_null($user_contract) && $user_contract->bonus != 0) ? ($request->nooftoken * $user_contract->bonus) / 100 : 0;

            $total_token = $request->nooftoken + $bonus_token;

            $client    = new Client;
            $coinprice = $this->getCryptoPrices();
             $flag=0;
            
               
             // AdminWallet::
                $token_equ_value=$request->payamount;
                 if ($flag==0) {
                $details = '';
               
                $user_token_txn      =  UserTokenTransaction::create([
                    'user_id'=>$user->id,
                    'user_token_id'=>$user_contract->id,
                    'user_contract_id'=>$user_contract->id,
                    'payment_type'=>$request->payby,
                ]);
                $user_token_txn->user_id          = $user->id;
                $user_token_txn->user_token_id    = $user_contract->id;
                $user_token_txn->payment_type     = $request->payby;
                $user_token_txn->payment_amount   = $request->payamount;
                $user_token_txn->token_price      = $user_contract->tokenValue;
                $user_token_txn->number_of_token  = $request->nooftoken;
                $user_token_txn->txn_hash         = $details;
      
                $user_token_txn->total_token      = $request->nooftoken;
                $user_token_txn->status           = 2;
                $user_token_txn->coin_price       = $coin_price;
                $user_token_txn->razorpayhash=$transactionId;   
                $user_token_txn->razorpaytxnhash='';    
                $user_token_txn->save();

              $UserTokenTransactionLastId=UserTokenTransaction::all()->last()->id;
$tkcount=$request->tokensu+$request->nooftoken;

if($tkcount==$request->tokensupply){
  $admin_token_txn_wallet =  Property::where('id',$request->token_id)->update([
                'status' => 'soldout',  
               
            ]);
}
      


                $admin_token_txn_wallet =  AdminWallet::create([
                    'usertokentransid'=>$UserTokenTransactionLastId,
                    'property_id'=>$user_contract->id,
                    'issuer_id'=>$user_contract->user_id,
                    'investor_id'=>$user->id,
                    'payment_type'=>$request->payby,
                    'total_tokens'=>$request->nooftoken,
                    'paybyinvestor'=>$request->payamount,
                    'paytoissuer'=>$request->tokenissueramt,
                    'admincommission'=>$request->payamount-$request->tokenissueramt,
                    'txnhash'=>$details,
                    'status'=>2,
                    'razorpayhash'=>$transactionId,
                ]);
              
                $admin_token_txn_wallet->property_id=$user_contract->id;
                $admin_token_txn_wallet->issuer_id=$user_contract->user_id;
                $admin_token_txn_wallet->investor_id=$user->id;
                $admin_token_txn_wallet->payment_type='INR';
                $admin_token_txn_wallet->total_tokens=$request->nooftoken;
                $admin_token_txn_wallet->paybyinvestor=$request->payamount;
                $admin_token_txn_wallet->paytoissuer=$request->tokenissueramt;
            $admin_token_txn_wallet->admincommission=$request->payamount-$request->tokenissueramt;
                $admin_token_txn_wallet->txnhash=$details;    
                $admin_token_txn_wallet->status=2;  
                $admin_token_txn_wallet->razorpayhash=$transactionId; 
                $admin_token_txn_wallet->razorpaytxnhash='';              
                $admin_token_txn_wallet->save();
               // $this->doPayment();
 
              //  return response()->json(['status' => 'success']);
               // return back()->with('flash_success', "Token Purchased Successfully !");
            } else {
             //    return response()->json(['status' => 'failed']);
              //  return back()->with('flash_error', "Transaction Failed...");
            }

            
        } catch (\Throwable $th) {
         //  return response()->json(['status' => 'failed']);
        }
       // $dta=$this->storePayment($request);
        $payukey = "7rnFly";
        $payusalt = "pjVQAWpA";
        $success_url = 'https://localhost/aurum_laravel/paymentSuccess';
        $failure_url = 'https://localhost/aurum_laravel/paymentFailed';

       
        $action = 'https://test.payu.in/_payment';
        $amount = $request->payamount;
        $firstname = $user->name;
        $email = $user->email;
        $phone = 8010292168;
        $productinfo = 'No of Tokens-'.$request->nooftoken;

        $udf="payu_paisa";

         
        $hash = $this->generateHash($payukey, $transactionId, $amount , $productinfo , $firstname, $email , $payusalt,$udf);


       
        return view('sendToGateway')->with('salt', $payusalt)->with('action', $action)->with('key', $payukey)
            ->with('txnid', $transactionId)->with('amount', $amount)->with('productinfo' , $productinfo)
            ->with('firstname', $firstname)-> with('email', $email)-> with('phone', $phone)
            ->with('surl', $success_url)->with('furl', $failure_url)->with('hash', $hash);

    }

    public function generateHash($payukey, $transactionId, $amount , $productinfo , $firstname, $email , $payusalt,$udf5)
    { 
        $hash=strtolower(hash('sha512', $payukey.'|'.$transactionId.'|'.$amount.'|'.$productinfo.'|'.$firstname.'|'.$email.'|||||'.$udf5.'||||||'.$payusalt));
      
        return $hash;
    }
    function getCallbackUrl()
    {
        $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $uri = str_replace('/index.php','/',$_SERVER['REQUEST_URI']);
        return $protocol . $_SERVER['HTTP_HOST'] . $uri . 'response.php';
    }
    public function paymentFailed(){
           return redirect('/dashboard')->with(['flash_error'=>'Transaction Failed...','clearcookie'=>'1']);
    }
   public function paymentSuccess()
    {            
        $postdata = $_POST;
     
        $msg = '';
        $salt = "pjVQAWpA";
        if (isset($postdata ['key'])) {
            $key                =   $postdata['key'];
            $txnid              =   $postdata['txnid'];
            $amount             =   $postdata['amount'];
            $productInfo        =   $postdata['productinfo'];
            $firstname          =   $postdata['firstname'];
            $email              =   $postdata['email'];
            $udf5               =   $postdata['udf5'];  
            $status             =   $postdata['status'];
            $resphash           =   $postdata['hash'];
            //Calculate response hash to verify 
            $keyString          =   $key.'|'.$txnid.'|'.$amount.'|'.$productInfo.'|'.$firstname.'|'.$email.'|||||'.$udf5.'|||||';
            $keyArray           =   explode("|",$keyString);
            $reverseKeyArray    =   array_reverse($keyArray);
            $reverseKeyString   =   implode("|",$reverseKeyArray);
            $CalcHashString     =   strtolower(hash('sha512', $salt.'|'.$status.'|'.$reverseKeyString)); //hash without additionalcharges
            
            //check for presence of additionalcharges parameter in response.
            $additionalCharges  =   "";
            
            If (isset($postdata["additionalCharges"])) {
               $additionalCharges=$postdata["additionalCharges"];
               //hash with additionalcharges
               $CalcHashString  =   strtolower(hash('sha512', $additionalCharges.'|'.$salt.'|'.$status.'|'.$reverseKeyString));
            }
            //Comapre status and hash. Hash verification is mandatory.
            if ($status == 'success'  && $resphash == $CalcHashString) {
                $msg = "Transaction Successful, Hash Verified...<br />";
                //Do success order processing here...
                //Additional step - Use verify payment api to double check payment.
                if($this->verifyPayment($key,$salt,$txnid,$status)){

                    $history = DepositHistory::where('payment_id',$txnid)->update([
                        'razorpaytxnhash'=>$resphash,
                    ]);          

                    $user_token_txn =UserTokenTransaction::where('razorpayhash',$txnid)->update([
                        'razorpaytxnhash'=>$resphash,
                    ]);

                    $admin_token_txn_wallet =  AdminWallet::where('razorpayhash',$txnid)->update([
                        'razorpaytxnhash'=>$resphash,
                    ]);

                    $msg = "Token Purchased Successfully !";
                    return redirect('/dashboard')->with(['flash_success'=>'Token Purchased Successfully !','clearcookie'=>'1']);
                }
                else{
                    return redirect('/dashboard')->with(['flash_error'=>'Transaction Failed...','clearcookie'=>'1']);
                   
                }
            }
            else {
               
                return redirect('/dashboard')->with(['flash_error'=>'Transaction Failed...','clearcookie'=>'1']);
            } 
           
        }
        else exit(0);
    }
            
            
            public function verifyPayment($key,$salt,$txnid,$status)
        {
            $command = "verify_payment"; //mandatory parameter
            
            $hash_str = $key  . '|' . $command . '|' . $txnid . '|' . $salt ;
            $hash = strtolower(hash('sha512', $hash_str)); //generate hash for verify payment request

            $r = array('key' => $key , 'hash' =>$hash , 'var1' => $txnid, 'command' => $command);
                
            $qs= http_build_query($r);
            //for production
            //$wsUrl = "https://info.payu.in/merchant/postservice.php?form=2";
           
            //for test
            $wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
            
            try 
            {       
                $c = curl_init();
                curl_setopt($c, CURLOPT_URL, $wsUrl);
                curl_setopt($c, CURLOPT_POST, 1);
                curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
                curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
                curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($c, CURLOPT_SSLVERSION, 6); //TLS 1.2 mandatory
                curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
                $o = curl_exec($c);
                if (curl_errno($c)) {
                    $sad = curl_error($c);
                    throw new Exception($sad);
                }
                curl_close($c);
                
             
                $response = json_decode($o,true);
                
                if(isset($response['status']))
                {
                    // response is in Json format. Use the transaction_detailspart for status
                    $response = $response['transaction_details'];
                    $response = $response[$txnid];
                    
                    if($response['status'] == $status) //payment response status and verify status matched
                        return true;
                    else
                        return false;
                }
                else {
                    return false;
                }
            }
            catch (Exception $e){
                return false;   
            }
        }

    /**
     * Used to store Payment
     */
    public function storePayment(Request $request){
        try {

             $user = Auth::user();
           
            $history = new DepositHistory;
            $history->user_id = $user->id;
            $history->payment_id = $request->razorpay_payment_id;
            $history->payment_mode = 'INR';
            $history->amount = $request->totalAmount;
            $history->save();

            $token_equ_value = $bonus_token = $total_token = 0;
            $coin_price = 0;
            $user_contract = Property::where('id',$request->token_id)->first();

            $issuer_id = $user_contract->user_id;
            $issuer_user = User::where('id',$issuer_id)->first();

            $coins      = (new Coins)->getCoin($request->payby);
            $to_address = (!is_null($coins)) ? $coins->address : '';

            $bonus_token = (!is_null($user_contract) && $user_contract->bonus != 0) ? ($request->nooftoken * $user_contract->bonus) / 100 : 0;

            $total_token = $request->nooftoken + $bonus_token;

            $client    = new Client;
            $coinprice = $this->getCryptoPrices();
             $flag=0;
            
               
             // AdminWallet::
                $token_equ_value=$request->payamount;
                 if ($flag==0) {
                $details = '';
               
                $user_token_txn      =  UserTokenTransaction::create([
                    'user_id'=>$user->id,
                    'user_token_id'=>$user_contract->id,
                    'user_contract_id'=>$user_contract->id,
                    'payment_type'=>$request->payby,
                ]);
                $user_token_txn->user_id          = $user->id;
                $user_token_txn->user_token_id    = $user_contract->id;
                $user_token_txn->payment_type     = $request->payby;
                $user_token_txn->payment_amount   = $request->payamount;
                $user_token_txn->token_price      = $user_contract->tokenValue;
                $user_token_txn->number_of_token  = $request->nooftoken;
                $user_token_txn->txn_hash         = $details;
                //$user_token_txn->bonus_value      = $request->bonustoken*$user_contract->tokenValue;
               // $user_token_txn->bonus_token      = $request->bonustoken;
                $user_token_txn->total_token      = $request->nooftoken;
                $user_token_txn->status           = 2;
                $user_token_txn->coin_price       = $coin_price;
                $user_token_txn->razorpayhash=$request->razorpay_payment_id;       
                $user_token_txn->save();

              $UserTokenTransactionLastId=UserTokenTransaction::all()->last()->id;
$tkcount=$request->tokensu+$request->nooftoken;

if($tkcount==$request->tokensupply){
  $admin_token_txn_wallet =  Property::where('id',$request->token_id)->update([
                'status' => 'soldout',  
               
            ]);
}
              /*  $user->USD -= $request->payamount;
                $issuer_user->USD = $issuer_user->USD+$request->tokenissueramt;
                $user->save();
                $issuer_user->save();*/
                //  $log_message='Token Purchased At';
              //    activity_log($log_message);
                  


                $admin_token_txn_wallet =  AdminWallet::create([
                    'usertokentransid'=>$UserTokenTransactionLastId,
                    'property_id'=>$user_contract->id,
                    'issuer_id'=>$user_contract->user_id,
                    'investor_id'=>$user->id,
                    'payment_type'=>$request->payby,
                    'total_tokens'=>$request->nooftoken,
                    'paybyinvestor'=>$request->payamount,
                    'paytoissuer'=>$request->tokenissueramt,
                    'admincommission'=>$request->payamount-$request->tokenissueramt,
                    'txnhash'=>$details,
                    'status'=>2,
                    'razorpayhash'=>$request->razorpay_payment_id,
                ]);
              
                $admin_token_txn_wallet->property_id=$user_contract->id;
                $admin_token_txn_wallet->issuer_id=$user_contract->user_id;
                $admin_token_txn_wallet->investor_id=$user->id;
                $admin_token_txn_wallet->payment_type='INR';
                $admin_token_txn_wallet->total_tokens=$request->nooftoken;
                $admin_token_txn_wallet->paybyinvestor=$request->payamount;
                $admin_token_txn_wallet->paytoissuer=$request->tokenissueramt;
            $admin_token_txn_wallet->admincommission=$request->payamount-$request->tokenissueramt;
                $admin_token_txn_wallet->txnhash=$details;    
                $admin_token_txn_wallet->status=2;  
                $admin_token_txn_wallet->razorpayhash=$request->razorpay_payment_id;               
                $admin_token_txn_wallet->save();
               // $this->doPayment();
 
              //  return response()->json(['status' => 'success']);
               // return back()->with('flash_success', "Token Purchased Successfully !");
            } else {
             //    return response()->json(['status' => 'failed']);
              //  return back()->with('flash_error', "Transaction Failed...");
            }

            
        } catch (\Throwable $th) {
         //  return response()->json(['status' => 'failed']);
        }
    }
    public function getenquiryformhome(Request $request){        
    
    
        try {
           
             $enquiry=Enquiry::create([
                    'name'=>$request->name,
                    'email'=>$request->email,
                    'mobile'=>$request->mobile,
                    'message'=>$request->message,
                ]);
             $enquiry->name=$request->name;
             $enquiry->save();
             return response()->json(['status' => 'success']);
        }
         catch (Exception $e) {
            
            return response()->json(['status' => 'failed']);
        }
    }
 /**
     * Used to Invest Token
     */
    public function investstore(InvestStore $request)
    {
  
        try {
            $user            = Auth::user();
            $token_equ_value = $bonus_token = $total_token = 0;
            $coin_price = 0;

           // $from_address = ($request->payby == 'ETH') ? $user->eth_address : $user->btc_address;
            $user_contract = Property::find($request->token_id);

            $issuer_id = $user_contract->user_id;
            $issuer_user = User::where('id',$issuer_id)->first();

            $coins      = (new Coins)->getCoin($request->payby);
            $to_address = (!is_null($coins)) ? $coins->address : '';

            $bonus_token = (!is_null($user_contract) && $user_contract->bonus != 0) ? ($request->nooftoken * $user_contract->bonus) / 100 : 0;

            $total_token = $request->nooftoken + $bonus_token;

            $client    = new Client;
            $coinprice = $this->getCryptoPrices();


            // --------------- Balance of ETH -----------
            $flag=0;
            if ($request->payby == 'FIAT') {
               
                if ($user->USD < $token_equ_value) {
                    return back()->with('flash_error', "You have low balance in selected payment method...");
                    $flag=1;
                }
                $token_equ_value=$request->payamount;
                
               
             /*   $purchaseTokenByETH = $this->purchaseTokenByETH($issuer_user->eth_address,$user->email,$user->eth_address,$token_equ_value,$user_contract->contract_address);
               
                if(!$purchaseTokenByETH){
                   // return back()->with('flash_error', 'Transaction Failed...');
                }

                if (isset($purchaseTokenByETH['status']) && $purchaseTokenByETH['status'] == 'success') {
                    
                   
                    $curldata['result'] = $purchaseTokenByETH['txHash'];
                } else {
                    return back()->with('flash_error', 'Transaction Failed...');
                }*/
            }

            if ($flag==0) {
                $details = '';
               
                $user_token_txn      =  UserTokenTransaction::create([
                    'user_id'=>$user->id,
                    'user_token_id'=>$user_contract->id,
                    'user_contract_id'=>$user_contract->id,
                    'payment_type'=>$request->payby,
                ]);
                $user_token_txn->user_id          = $user->id;
                $user_token_txn->user_token_id    = $user_contract->id;
                $user_token_txn->payment_type     = $request->payby;
                $user_token_txn->payment_amount   = $request->payamount;
                $user_token_txn->token_price      = $user_contract->tokenValue;
                $user_token_txn->number_of_token  = $request->nooftoken;
                $user_token_txn->txn_hash         = $details;
            $user_token_txn->bonus_value      = $request->bonustoken*$user_contract->tokenValue;
                $user_token_txn->bonus_token      = $request->bonustoken;
                $user_token_txn->total_token      = $request->nooftoken;
                $user_token_txn->status           = 2;
                $user_token_txn->coin_price       = $coin_price;
                $user_token_txn->save();

$UserTokenTransactionLastId=UserTokenTransaction::all()->last()->id;

                $user->USD -= $request->payamount;
                $issuer_user->USD = $issuer_user->USD+$request->tokenissueramt;
                $user->save();
                $issuer_user->save();
                //  $log_message='Token Purchased At';
              //    activity_log($log_message);
                  


                $admin_token_txn_wallet =  AdminWallet::create([
                    'usertokentransid'=>$UserTokenTransactionLastId,
                    'property_id'=>$user_contract->id,
                    'issuer_id'=>$user_contract->user_id,
                    'investor_id'=>$user->id,
                    'payment_type'=>$request->payby,
                    'total_tokens'=>$request->nooftoken,
                    'paybyinvestor'=>$request->payamount,
                    'paytoissuer'=>$request->tokenissueramt,
                    'admincommission'=>$request->payamount-$request->tokenissueramt,
                    'txnhash'=>$details,
                    'status'=>2,
                ]);
              
                $admin_token_txn_wallet->property_id=$user_contract->id;
                $admin_token_txn_wallet->issuer_id=$user_contract->user_id;
                $admin_token_txn_wallet->investor_id=$user->id;
                $admin_token_txn_wallet->payment_type=$request->payby;
                $admin_token_txn_wallet->total_tokens=$request->nooftoken;
                $admin_token_txn_wallet->paybyinvestor=$request->payamount;
                $admin_token_txn_wallet->paytoissuer=$request->tokenissueramt;
            $admin_token_txn_wallet->admincommission=$request->payamount-$request->tokenissueramt;
                $admin_token_txn_wallet->txnhash=$details;    
                $admin_token_txn_wallet->status=2;              
                $admin_token_txn_wallet->save();

                return back()->with('flash_success', "Token Purchased Successfully !");
            } else {
                return back()->with('flash_error', "Transaction Failed...");
            }

        } catch (Exception $e) {
            return back()->with('flash_error', "Transaction Failed...");
        }

    }
    /**
     * Used to Invest Token
     */
    public function investstoreadmin(Request $request, $id)
    {
                   
        try {
            
         $admin_wallet = AdminWallet::where('wallet_id',$id)->get();        
         $issuer_user = User::where('id',$admin_wallet[0]['issuer_id'])->first();
         $investor_user = User::where('id',$admin_wallet[0]['investor_id'])->first();
         $user_contract = Property::where('id',$admin_wallet[0]['property_id'])->first();
         
    
            $token_equ_value = $bonus_token = $total_token = 0;
            $coin_price = 0;

       $token_equ_value=$admin_wallet[0]['total_tokens'];


                $purchaseTokenByETH = $this->purchaseTokenByETH($issuer_user->eth_address,$investor_user->email,$investor_user->eth_address,$token_equ_value,$user_contract->contract_address,$user_contract->tokenDecimal,$issuer_user->email);
            
                if(!$purchaseTokenByETH){
                    return back()->with('flash_error', 'Transaction Failed...');
                }

                if (isset($purchaseTokenByETH['status']) && $purchaseTokenByETH['status'] == 'success') {
                    
                  
                    $curldata['result'] = $purchaseTokenByETH['txHash'];
                } else {
                    return response()->json(['status' => 'failed']);
                }
            
 
            if (isset($curldata['result'])) {
                $details = $curldata['result'];

              $user_token_txn = UserTokenTransaction::where('id',$admin_wallet[0]['usertokentransid'])->update([
                'status' => 1,  
                'txn_hash' => $details,      
            ]);
              date_default_timezone_set('Asia/Calcutta'); 
$date=date("Y-m-d h:i:s");
               $admin_token_txn_wallet =  AdminWallet::where('wallet_id',$id)->update([
                'status' => 1,  
                'txnhash' => $details,  
                'updated_at'=>$date, 
                'approvedby'=>Auth::user()->name."-".Auth::user()->email,   
            ]);


                return response()->json(['status' => 'success']);
            } else {
                 return response()->json(['status' => 'failed']);
            }

        } catch (Exception $e) {
            return response()->json(['status' => 'failed']);
        }

    }


public function investbonusstoreadmin(Request $request, $id)
    {
                   
       try {
            
         $investorbonustoken = InvestorBonusToken::where('bonustokenid',$id)->first();    
         $issuer_user = User::where('id',$investorbonustoken->issuer_id)->first();          
         $investor_user = User::where('id',$investorbonustoken->investor_id)->first();
         $user_contract = Property::where('id',$investorbonustoken->propertyid)->first();
         
    
            $token_equ_value = $bonus_token = $total_token = 0;
            $coin_price = 0;

       $token_equ_value=$investorbonustoken->dividendtokens;


                $purchaseTokenByETH = $this->purchaseTokenByETH($issuer_user->eth_address,$investor_user->email,$investor_user->eth_address,$token_equ_value,$user_contract->contract_address,$user_contract->tokenDecimal,$issuer_user->email);
    

    if (isset($purchaseTokenByETH['status']) && $purchaseTokenByETH['status'] == 'success') {
                   $curldata['result'] = $purchaseTokenByETH['txHash'];
                } else {
                   return response()->json(['status' => 'failed2']);
                }
            
 
            if (isset($curldata['result'])) {
                $details = $curldata['result'];

              $InvestorBonusToken = InvestorBonusToken::where('bonustokenid',$id)->update([
                'status' => 1,  
                'txnhash' => $details,      
            ]);

               return response()->json(['status' => 'success']);
            } else {
                 return response()->json(['status' => 'failed2']);
            }

        } catch (Exception $e) {
            return response()->json(['status' => 'failed3']);
        }

    }
    public function purchaseTokenByETH($fromAddress,$fromEmail,$toAddress,$tokenValue,$contractaddress,$tokendecimal,$femail)
    {
        try {

         $prop=EthPrivatekey::where('user_email',$femail)->first(); 

         $client  = new Client();
                   $headers = ['Content-Type' => 'application/json',];
                    $url     = 'http://13.126.245.106:3000/getKey';
                      $body = [
                            'password' => $femail,  
                            'privateKey' =>$prop->privatekey,             
                        ];
                    $res = $client->post($url, [
                        'headers' => $headers,
                            'body' => json_encode($body),
                    ]);
             $res = json_decode($res->getBody(), true);
       
         //echo str_replace('0x', '', $res['privateKey']);
         
             
              // $eth_pvt_key ="d8dec2009137255316b48478978783520acfc7783797c3b4efb60beadf00234c";
            if (isset($res['status']) && $res['status'] == 'success') {
                $eth_pvt_key=str_replace('0x', '', $res['privateKey']);
                $url         = 'http://13.126.245.106:3000/transfer';
                $client      = new Client();
                $headers     = ['Content-Type' => 'application/json',];
            
                $body = [
                    'senderAddress' => $fromAddress,
                    'senderPrivateKey' => $eth_pvt_key,
                    'to' => $toAddress,
                    'amount' => $tokenValue,
                    'decimal' => $tokendecimal,
                    'contract_address'=>$contractaddress,
                ];

                $res = $client->post($url, [
                    'headers' => $headers,
                    'body' => json_encode($body),
                ]);

                $details = json_decode($res->getBody(), true);
                logger()->info($details);

                return $details;

            } else {
                return false;
            }

        } catch (Exception $e) {
            logger()->info($e->getMessage());
            return false;
        }
    }

    public function transferToken($fromAddress,$fromEmail, $toAddress)
    {
        try{
            $client  = new Client();
            $headers = ['Content-Type' => 'application/x-www-form-urlencoded',];
            $url     = 'http://localhost:1337/getkey';

            $res = $client->post($url, [
                'headers'     => $headers,
                'form_params' => [
                    'add'    => $fromAddress,
                    'string' => $fromEmail,
                ],
            ]);

        } catch (Exception $e) {
            return back()->with('flash_error', "Transaction Failed...");
        }
    }

    public function generateWithdrawOTP(Request $request){
        try {
            $user = Auth::user();
            $data = rand(100000,999999);
            $user->eth_otp = Hash::make($data);
            $user->save();

            $to_email = $user->email;
            //$to_email = 'test12345671@mailinator.com';

            $maildata = ['otp' => $data];
            \Illuminate\Support\Facades\Mail::to($to_email)->send(new Withdrawalotp($maildata));

            return response()->json(['success' => ['msg' => 'We have sent 6 digits OTP to your email address.']],200);
        }catch(Exception $e){
            \Log::critical($e->getMessage());
            return response()->json(['error' => ['msg' => 'Something Went Wrong']],500);
        }
    }

    //Change Password
    public function update_password(UpdatePassword $request)
    {

        $User = Auth::user();
        if (Hash::check($request->current_password, $User->password)) {
            if ($request->current_password != $request->password) {
                $User->password = bcrypt($request->password);
                $User->save();
                //Auth::logout();
                return back()->with('flash_success', trans('user.profiles.pass_updated'));
            } else
                return back()->with(['flash_error'=> trans('user.profiles.same'),'tab'=>'password']);
        } else
            return back()->with('flash_error', trans('user.profiles.current_wrong_pwd'));
    }


    public function supportstore(Request $request)
    {
        $this->validate($request, [
            'title'       => 'required',
            'description' => 'required',
        ]);

        try {
            $support              = new Support;
            $support->title       = $request->title;
            $support->description = $request->description;
            $support->status      = 0;
            $support->save();

            return back()->with('flash_success', "Support Created Successfully...");
        } catch (Exception $e) {
            return back()->with('flash_error', "Something went wrong...");
        }
    }

    public function blog()
    {
        $user = Auth::user();
        return view('blog', compact('user'));
    }

    public function blogDetail()
    {
        $user = Auth::user();
        return view('blogDetail', compact('user'));
    }

    public function profile()
    {
        $user = Auth::user();        
        $bankdetail=IssuerUser::where('user_id',$user->id)->get();
        return view('profile', compact('user','bankdetail'));
    }
 public function phelp(Request $request)
    {
        try {
            $user = Auth::user();
           // $data = $request->all(); 
           
             $profilehelp = ProfileHelp::create([
                'user_id'=>$user->id,
                'name'=>$request->name,
                'email'=>$request->email,             
                'message'=>$request->message,
                'priority'=>$request->priority,
            ]);    
            $profilehelp->user_id=$user->id;
            $profilehelp->save();    
                    return back()->with('flash_success', "Your request has been processed.");
               
          
        } catch (Exception $e) {
           return back()->with('flash_error', "Something went wrong...");
        }
    }
    public function profile_identity(ProfileIdentity $request)
    {
        try {
            $user = Auth::user();
            $data = $request->all();
             $client      = new Client(['http_errors' => false]);
            $headers     = [
                'Content-Type'  => 'application/json',
                'Authorization' => 'Bearer '.getenv('SUREPASS'),
            ];
            $body        = ["id_number" => $request->pan_number];
            $url         = 'https://kyc-api.aadhaarkyc.io/api/v1/pan/pan';
            $res         = $client->post($url, [
                'headers' => $headers,
                'body'    => json_encode($body),
            ]);
            $response = json_decode($res->getBody(), true);
           
$firstname=strtoupper($request->first_name);
$middlename=strtoupper($request->middle_name);
$lastname=" ".strtoupper($request->last_name);

if($middlename!=''){
    $middlename=" ".strtoupper($request->middle_name);
}else{
    $middlename="";
}

if($firstname!=''){
    $name=$firstname.$middlename.$lastname;
}

           if($response['data']['full_name']==$name){

                if(isset($response['success']) && isset($response['status_code']) && $response['success'] == true && $response['status_code'] == '200'){
                   
                    if ($request->hasFile('pan'))
                        $data['pan'] = $request->pan->store('pan');
                    if ($request->hasFile('frontdocument'))
                        $data['frontdocument'] = $request->frontdocument->store('frontdocument');
                    if ($request->hasFile('backdocument'))
                        $data['backdocument'] = $request->backdocument->store('backdocument');
                    $data['user_id'] = $user->id;
                    UserIdentity::updateOrCreate(['user_id' => $user->id], $data);
                    User::where('id',$user->id)->update(['kyc' => 1]);


                    return back()->with('flash_success', "KYC Details updated Successfully...");
                }
                else{                   
                    return back()->with('flash_error', 'Invalid Pan Number');
                }
          }else{
                 return back()->with('flash_error', 'Invalid Full Name Details.Please try again.');
          }
            //unset($data['document'], $data['photo'], $data['_token']);
        } catch (Exception $e) {
           // return back()->with('flash_error', "Something went wrong...");
        }
    }

    public function profile_finance(ProfileFinance $request)
    {
        Session::put('check_url', $_SERVER['REQUEST_URI']);

        try {
            $user = Auth::user();
            $data = $request->all();

            unset($data['e_signature'], $data['accreditation'], $data['_token']);
            if ($request->hasFile('e_signature'))
                $data['e_signature'] = $request->e_signature->store('userdata');
            $data['user_id'] = $user->id;
            if ($request->accreditation)
                $data['accreditation'] = json_encode($request->accreditation);

            UserFinance::updateOrCreate(['user_id' => $user->id], $data);

            return back()->with('flash_success', "Finance updated Successfully...");
        } catch (Exception $e) {
            dd($e);
            return back()->with('flash_error', "Something went wrong...");
        }
    }

    public function profile_background(Request $request)
    {
        Session::put('check_url', $_SERVER['REQUEST_URI']);
        $this->validate($request, [
            'investment_experience'           => 'required|max:10',
            'investment_size'                 => 'required|numeric|min:1',
            // 'investment_type' => 'required|max:10',
            'investment_objective'            => 'required|max:30',
            // 'previously_invested' => 'required|max:10',
            'property_type'                   => 'required|max:10',
            // 'geography' => 'required|max:30',
            'holding_period'                  => 'required|max:10',
            'expected_investment_nxt_1year'   => 'required|max:10',
            'expected_investment_per_project' => 'required|max:10',
            'risk_type'                       => 'required|max:30'
        ]);

        try {
            $user = Auth::user();
            $data = $request->all();

            unset($data['geography'], $data['previously_invested'], $data['investment_type'], $data['_token']);

            $data['user_id'] = $user->id;
            if ($request->previously_invested)
                $data['previously_invested'] = json_encode($request->previously_invested);
            if ($request->geography)
                $data['geography'] = json_encode($request->geography);
            if ($request->investment_type)
                $data['investment_type'] = json_encode($request->investment_type);

            UserBackground::updateOrCreate(['user_id' => $user->id], $data);

            return back()->with('flash_success', "Background updated Successfully...");
        } catch (Exception $e) {

            return back()->with('flash_error', "Something went wrong...");
        }
    }

    public function wallet()
    {
        $user    = Auth::user();

        $fiat_history = DepositHistory::where('user_id',$user->id)->where('payment_mode','FIAT')->orderBy('created_at', 'DESC')->get();
        return view('wallet', compact('user', 'fiat_history'));
    }
      public function paymenthistory()
    {
        $user    = Auth::user();

        $fiat_history = DepositHistory::where('user_id',$user->id)->join('admin_wallet', 'admin_wallet.razorpayhash', '=', 'payment_id')->get();
        return view('paymenthistory', compact('user', 'fiat_history'));
    }


    public function withdrawETHBalance()
    {
        $user = Auth::user();
        $history = WithdrawEth::whereuser_id($user->id)->orderBy('created_at', 'DESC')->get();

        $eth_address = $user->eth_address;

        $wallet = json_decode($this->getETHbalance($eth_address),TRUE);
        $ethbalance = $wallet['balance'];

        $gasPrice = $this->gasPrice();
        $gasPrice = json_decode($gasPrice, true);
        if(isset($gasPrice['result']) && $gasPrice['result']){
            $gas = $gasPrice['result'];
        }
        else{
            $gas = '0x5208';
        }
        $gwei = hexdec($gas) / 1000000000;
        $ether = $gwei / 1000000000;
        $gasinether = ($ether * 30400);
        $gasinether = ((30 / 100) * $gasinether) + $gasinether;

        return view('ethwithdraw',compact('ethbalance', 'gwei', 'gasinether', 'user','history'));
    }

public function notifications()
    {
        try {
            $user    = Auth::user();
          
    $investorbonustoken=InvestorBonusToken::where('investor_id', $user->id)->where('dividendapprovestatus','1')->join('tokendividend','tokendividend.dividend_id','=','tokendividendid')->join('users','users.id','=','issuer_id')->get();

         

            return view('notifications', compact('user','investorbonustoken'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get investment details. Please try again later');
        }
    }

public function notificationsdetail($id)
    {


        try {
            $user    = Auth::user();
          
    $investorbonustoken=InvestorBonusToken::where('investor_id', $user->id)->where('tokendividendid',$id)->join('tokendividend','tokendividend.dividend_id','=','tokendividendid')->join('users','users.id','=','issuer_id')->get();

         

            return view('bonus_detail', compact('user','investorbonustoken'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get investment details. Please try again later');
        }
    }

    public function investment()
    {
        try {
            $user    = Auth::user();
            $wallet=AdminWallet::where('investor_id', $user->id)->join('property', 'property.id', '=', 'property_id')->get();
            $investorbonustoken=InvestorBonusToken::where('investor_id', $user->id)->where('status','1')->get();

           // $tokens = UserTokenTransaction::where('user_id', $user->id)->get();   

            return view('investment', compact('user','wallet','investorbonustoken'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get investment details. Please try again later');
        }
    }

    private function _getCoinValues($Coin)
    {
        $value = 0;
        try {
            $client      = new Client();
            $requestdata = $client->get('https://min-api.cryptocompare.com/data/price?fsym=USD&tsyms=' . $Coin);
            $response    = json_decode($requestdata->getBody(), 1);
            $value       = $response[$Coin];
        } catch (\Throwable $throwable) {
            $value = 0;
        }

        return $value;
    }

    public function investDetail()
    {
        $user = Auth::user();
        return view('investDetail', compact('user'));
    }

    public function setting(Request $request)
    {
        try {
            $user = Auth::user();
            return view('setting', compact('user'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get Setting Data. Please try again later');
        }
    }

    public function activity()
    {
        $user = Auth::user();
        return view('activity', compact('user'));
    }

    public function sellToken()
    {
        $user = Auth::user();
        return view('sellToken', compact('user'));
    }

    public function buyToken()
    {
        $user = Auth::user();
        return view('buyToken', compact('user'));
    }

    /**
     * Generate a secret key in Base32 format
     *
     * @return string
     */
    private function generateSecret()
    {
        $randomBytes = random_bytes(10);
        return Base32::encodeUpper($randomBytes);
    }

    /**
     * Used to calculate Token value
     */

   

    public function gettokeninvestvalue(Request $request)
    {
  try {
            $token_equ_value = $bonus_token = $total_token = $price = 0;
            $user_contract = Property::find($request->token_id);
           
            $token_value   = $user_contract->tokenValue;
            $bonus         = $user_contract->Divident;

            // $coin_value = $this->getCryptoPrices();
            // if ($request->payment_id == 'BTC') {
            //     $price = $coin_value['BTC'];
            // }
            // if ($request->payment_id == 'ETH') {
            //     $price = $coin_value['ETH'];
            // }
            // if ($request->payment_id == 'XRP') {
            //     $price = $coin_value['XRP'];
            // }
            
          //  $price = $user_contract->tokenValue;
           // $token_equ_value=number_format($request->no_of_token * $token_value,10,'.','');
            /*$token_equ_value = number_format($request->no_of_token * $token_value / $price,10,'.','');
            $percentage = (Setting::get('purchase_commission', 0.3) / 100) * $token_equ_value;
            $token_equ_value += $percentage;
            if ($bonus == 0) {
                $bonus_token = 0;
            } else {
                $bonus_token = ($request->no_of_token * $bonus) / 100;
            }

            $total_token = $request->no_of_token + $bonus_token;*/


            $result = ['status' => 1, 'token_equ_value' => $token_equ_value, 'bonus_token' => $bonus_token, 'total_token' => $total_token];

            return $result;

        } catch (Exception $e) {
            
            $result = ['status' => 0];

            return $result;
        }

       

         
    }

    /**
     * Used to get Coin Price
     */
    public function getCryptoPrices()
    {
        $url = 'https://api.coingecko.com/api/v3/coins/markets?vs_currency=' . Setting::get('default_currency') . '&ids=bitcoin%2Cethereum%2Cripple&order=market_cap_desc&per_page=100&page=1&sparkline=false';

        $headers = [
            'Accepts: application/json',
        ];
        $request = "{$url}"; // create the request URL


        $curl = curl_init(); // Get cURL resource
        // Set cURL options
        curl_setopt_array($curl, array(
            CURLOPT_URL            => $request,            // set the request URL
            CURLOPT_HTTPHEADER     => $headers,     // set the headers
            CURLOPT_RETURNTRANSFER => 1         // ask for raw response instead of bool
        ));

        $response     = curl_exec($curl); // Send the request, save the response
        $cryptoprices = json_decode($response); // print json decoded response
        curl_close($curl); // Close request
        $coin_type        = [];
        $coin_type['BTC'] = 0;
        $coin_type['ETH'] = 0;
        $coin_type['XRP'] = 0;
        if (!empty($cryptoprices) && count($cryptoprices) > 0) {
            foreach ($cryptoprices as $price) {
                switch ($price->id) {
                    case 'bitcoin':
                        $coin_type['BTC'] = $price->current_price;
                        break;
                    case 'ethereum':
                        $coin_type['ETH'] = $price->current_price;
                        break;
                    case 'ripple':
                        $coin_type['XRP'] = $price->current_price;
                        break;
                    default:
                        # code...
                        break;
                }
            }
        }
        return $coin_type;
    }

    /**
     * Get ETH Balance
     */
    public function getETHbalance($address)
    {
        try {

            if(empty($address)){
                return json_encode(['msg' => 'failed', 'balance' => 0]);
            }

            $client     = new Client();
            $headers    = [
                'Content-Type' => 'application/json',
            ];
            $body       = ["jsonrpc" => "2.0", "method" => "eth_getBalance", "params" => [$address, 'latest'], "id" => 1];
            $url        = env('INFURA_URL');
            $res        = $client->post($url, [
                'headers' => $headers,
                'body'    => json_encode($body),
            ]);
            $getbalance = json_decode($res->getBody(), true);
            if (isset($getbalance['result'])) {
                $balance = hexdec($getbalance['result']) / 1000000000000000000;
                return json_encode(['msg' => 'success', 'balance' => $balance]);
            } else {
                return json_encode(['msg' => 'failed', 'balance' => 0]);
            }
        } catch (\Throwable $th) {
            return json_encode(['msg' => 'failed', 'balance' => 0]);
        }
    }

    public function updateDepositHistory($user_id, $eth_address)
    {
        try {
            if(empty($eth_address)) {
                return false;
            }

            $client      = new Client();
            $res         = $client->get('https://api-ropsten.etherscan.io/api?apikey='.env("ETHERSCANKEY").'&module=account&action=txlist&address=' . $eth_address . '&page=1&offset=10&sort=asc');
            $transaction = json_decode($res->getBody(), true);
            //dd($transaction);
            foreach ($transaction['result'] as $item) {
                $hash    = $item['hash'];
                $history = DepositHistory::wheretxn_hash($hash)->where('user_id',$user_id)->first();
                if (!$history) {
                    $eth_amount = $item['value'] / 1000000000000000000;

                    $Transaction           = new DepositHistory;
                    $Transaction->user_id  = $user_id;
                    $Transaction->amount   = $eth_amount;
                    $Transaction->type     = 'ETH';
                    $Transaction->address  = $item['to'];
                    $Transaction->txn_hash = $hash;
                    $Transaction->status   = 'success';
                    $Transaction->save();
                }
            }
        } catch (\Throwable $th) {
            logger()->info($th->getMessage());
        }
    }

    public function checkLastTransaction($eth_address)
    {
        try {
            if(empty($eth_address)) {
                return false;
            }

            $client      = new Client();
            $res         = $client->get('https://api-ropsten.etherscan.io/api?apikey='.env("ETHERSCANKEY").'&module=account&action=txlist&address=' . $eth_address . '&page=1&offset=1&sort=desc');
            $transaction = json_decode($res->getBody(), true);

            $status = false;

            if(!isset($transaction['result'])){
                $status = true;
            }

            if(count($transaction['result']) == 0){
                $status = true;
            }

            foreach ($transaction['result'] as $item) {
                if(isset($item['blockNumber']) && isset($item['blockHash'])){
                    if(!is_null($item['blockNumber']) && !is_null($item['blockHash'])){
                        $status = true;
                    }
                }
            }

            return $status;

        } catch (\Throwable $th) {
            logger()->info($th->getMessage());
        }
    }

    public function sendETH(Request $request){
        $this->validate($request, [
            'amount'    =>  'required|numeric|min:0',
            'address'   =>  'required',
            'withdrawotp' => 'required|digits:6',
        ]);

        try {
            $user = Auth::user();
            if(!Hash::check($request->withdrawotp, $user->eth_otp)){
                return back()->with('flash_error', 'Withdrawal otp is mismatched');
            }

            $wallet = json_decode($this->getETHbalance($user->eth_address),TRUE);
            $eth_balance = $wallet['balance'];
            if($eth_balance <= 0){
                return back()->with('flash_error', 'Not enough ETH available in your wallet to withdraw');
            }

            $gasPrice = $this->gasPrice();
            $gasPrice = json_decode($gasPrice, true);
            if(isset($gasPrice['result']) && $gasPrice['result']){
                $gas = $gasPrice['result'];
            }
            else{
                $gas = '0x5208';
            }
            $gwei = hexdec($gas) / 1000000000;
            $ether = $gwei / 1000000000;
            $gasinether = ($ether * 30400);
            $gasinether = ((30 / 100) * $gasinether) + $gasinether;

            $eth_balance = $eth_balance - $request->amount;
            $withdraw_amount = $eth_balance - $gasinether;

            if($eth_balance < $gasinether){
                return back()->with('flash_error', 'Insufficient balance to withdraw');
            }

            $client  = new Client();
            $headers = ['Content-Type' => 'application/x-www-form-urlencoded',];
            $url     = 'http://localhost:1337/getkey';

            $res = $client->post($url, [
                'headers'     => $headers,
                'form_params' => [
                    'add'    => $user->eth_address,
                    'string' => $user->email,
                ],
            ]);

            $res = json_decode($res->getBody(), true);
            if (isset($res['status']) && $res['status'] == true) {
                $eth_pvt_key = $res['key'];
                $url         = 'http://localhost:3000/eth_transfer';

                $client      = new Client();
                $headers     = ['Content-Type' => 'application/json',];
                $body = [
                    'from' => $user->eth_address,
                    'password' => $eth_pvt_key,
                    'to' => $request->address,
                    'value' => $request->amount,
                ];

                $res = $client->post($url, [
                    'headers' => $headers,
                    'body' => json_encode($body),
                ]);

                $details = json_decode($res->getBody(), true);
                \Log::info($details);
                if (isset($details['status']) && $details['status'] == 'success') {
                    $model = new WithdrawEth;
                    $model->user_id = $user->id;
                    $model->sender = $user->eth_address;
                    $model->receiver = $request->address;
                    $model->amount = $request->amount;
                    $model->tx_hash = $details['result'];
                    $model->save();

                    return back()->with('flash_success', 'ETH has been withdrawn successfully');
                } else {
                    return back()->with('flash_error', 'Unable to withdraw ETH. Please try again later');
                }

            } else {
                return back()->with('flash_error', 'Unable to withdraw ETH. Please try again later');
            }

            //dd($request->all());

        } catch (\Throwable $th){
            \Log::critical('withdrawETH', ['message' => $th->getMessage()]);
            return back()->with('flash_error', 'Unable to withdraw ETH. Please try again later');
        }


//        try {
//            $user = Auth::user();
//            if(Hash::check($request->withdrawotp, $user->otp)){
//                $port = $this->checkport('7329');
//                if($port != false){
//                    $unlock = $this->unlock($user->eth_address, $user->email);
//                    if(isset($unlock['result'])) {
//                        $gasPrice = $this->gasPrice();
//                        $gasPrice = json_decode($gasPrice, true);
//                        if(isset($gasPrice['result']) && $gasPrice['result']){
//                            $gas = $gasPrice['result'];
//                        }
//                        else{
//                            $gas = '0x5208';
//                        }
//                        $gwei = hexdec($gas) / 1000000000;
//                        $ether = $gwei / 1000000000;
//                        $gasinether = ($ether * 30400);
//                        $gasinether = ((30 / 100) * $gasinether) + $gasinether;
//
//                        $eth_balance = $this->eth_curl($user->eth_address);
//                        $eth_balance = json_decode($eth_balance, true);
//                        if(isset($eth_balance['result'])){
//                            $balance = hexdec($eth_balance['result']);
//                            $ethbalance = $balance/1000000000000000000;
//                        }
//                        else{
//                            $ethbalance = 0;
//                        }
//
//                        if($eth_balance < $gasinether){
//                            if($request->ajax())
//                                return response()->json(['error' => ['msg' => 'Not enough gas fee available in your wallet to withdraw']], 400);
//                            else
//                                return back()->with('flash_error', 'Not enough gas fee available in your wallet to withdraw');
//                        }
//
//                        $price = '0x'.dechex(($request->amount) * 1000000000000000000);
//                        $client = new Client();
//                        $headers = [
//                            'Content-Type' => 'application/json',
//                        ];
//                        $body = ["jsonrpc" => "2.0","method" => "eth_sendTransaction", "params" => array(["from" => $user->eth_address, "to" => $request->address, "value" => $price]), "id" => 1];
//                        $url ="http://54.209.140.176:7329";
//                        $res = $client->post($url, [
//                            'headers' => $headers,
//                            'body' => json_encode($body),
//                        ]);
//                        $txs = json_decode($res->getBody(),true);
//                        \Log::info($txs);
//                        if(isset($txs['result'])) {
//                            $model = new WithdrawEth;
//                            $model->user_id = $user->id;
//                            $model->sender = $user->eth_address;
//                            $model->receiver = $request->address;
//                            $model->amount = $request->amount;
//                            $model->tx_hash = $txs['result'];
//                            $model->save();
//                            if($request->ajax())
//                                return response()->json(['success' => ['msg' => 'ETH has been withdrawn successfully']], 200);
//                            else
//                                return back()->with('flash_success', 'ETH has been withdrawn successfully');
//                        }
//                        else{
//                            if($request->ajax())
//                                return response()->json(['error' => ['msg' => 'Gas fee too high. Please try again later']], 400);
//                            else
//                                return back()->with('flash_error', 'Gas fee too high. Please try again later');
//                        }
//                    }
//                    else{
//                        if($request->ajax())
//                            return response()->json(['error' => ['msg' => 'Unable to withdraw ETH. Please try again later']], 400);
//                        else
//                            return back()->with('flash_error', 'Unable to withdraw ETH. Please try again later');
//                    }
//                }
//                else{
//                    if($request->ajax())
//                        return response()->json(['error' => ['msg' => 'Unable to made transaction right now. Please try again later']], 400);
//                    else
//                        return back()->with('flash_error', 'Unable to made transaction right now. Please try again later');
//                }
//            }
//            else{
//                return back()->with('flash_error', 'Withdrawal otp is mismatched');
//            }
//        } catch (\Throwable $th) {
//            \Log::critical('withdrawETH', ['message' => $th->getMessage()]);
//            if($request->ajax())
//                return response()->json(['error' => ['msg' => 'Unable to withdraw ETH. Please try again later']], 400);
//            else
//                return back()->with('flash_error', 'Unable to withdraw ETH. Please try again later');
//        }
    }

    public function unlock($address, $email){
        try {
            $client = new Client();
            $headers = [
                'Content-Type' => 'application/json',
            ];
            $body = ["jsonrpc" => "2.0","method" => "personal_unlockAccount", "params" => [$address, $email, 3600], "id" => 1];
            $url ="http://54.209.140.176:7329";
            $res = $client->post($url, [
                'headers' => $headers,
                'body' => json_encode($body),
            ]);
            $unlock = json_decode($res->getBody(),true);
            return $unlock;
        } catch (\Throwable $th) {
            $unlock = ['status' => false];
            return $unlock;
        }
    }

    /**
     * Used to get ETH Balance
     */
    public function eth_curl($address){
        try {
            $curl = curl_init();
            $request = json_encode([
                'jsonrpc'   =>  '2.0',
                'method'    =>  'eth_getBalance',
                'params'    =>  [
                    $address,
                    'latest'
                ],
                'id'    =>  1
            ]);
            curl_setopt_array($curl, array(
                CURLOPT_URL => env('INFURA_URL'),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $request,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            return $response;
        } catch (\Throwable $th) {
            \Log::critical('eth_curl', ['message' => $th->getMessage()]);
            return back()->with('flash_error', 'Unable to fetch eth balance data. Please try again later');
        }
    }

    public function checkport($port){
        if($_SERVER['REMOTE_ADDR'] == '127.0.0.1')
            $host = 'localhost';
        else
            $host = '54.209.140.176';
        $connection = @fsockopen($host, $port);
        return $connection;
    }

    public function gasPrice(){
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => env('INFURA_URL'),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS =>"{\"jsonrpc\":\"2.0\",\"method\":\"eth_gasPrice\",\"params\":[],\"id\":1}",
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            return $response;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

}

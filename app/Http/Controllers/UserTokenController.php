<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\UserContract;
use App\TokenType;
use App\Coins;
use App\UserToken;
use App\UserTokenTransaction;
use App\AddressBook;
use App\PropertyToken;
use Auth;
use App\Property;
use App\TokenDividend;
use App\AdminWallet;
use App\InvestorBonusToken;
class UserTokenController extends Controller
{
    public function index(){
        try {
            $user_token = UserTokenTransaction::orderBy('created_at', 'desc')->get();
            return view('admin.usertoken.index',compact('user_token'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to Get User Token Details');
        }        
    }

    public function tokentransaction(){
        try {
            $tokens = PropertyToken::orderBy('created_at', 'desc')->get();
            return view('admin.usertoken.usertransaction',compact('tokens'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to Get User Token Details');
        }
    }

    public function tokenusertransaction($id){
        $user_token_txn=UserTokenTransaction::where('user_token_id',$id)->get();
        $page=1;
        return view('admin.usertoken.usertransaction',compact('user_token','page'));
    }    

    public function tokentransactionstatus($id){

		$user_token_txn=UserTokenTransaction::findorFail($id);
		
		$contract_id=$user_token_txn->user_contract_id;

		$user_contract=UserContract::findorFail($contract_id);

		$user_id=$user_token_txn->user_id;

		// $address_book=AddressBook::where('user_id',$user_id)->where('coin_symbol','ETH')->first();

		$coins=Coins::where('symbol','ETH')->first();


    	$address=$coins->address;
        $to_address='';
        $total_token=$user_token_txn->total_token;
        $contractABI=Setting::get('contract_abi');                
        $contract = $user_contract->contract_address;

    	$eth_pvt_key_temp=$coins['star_reference_counter'].$coins['port_reference_counter'];
    	$eth_pvt_key=$this->simple_crypt($eth_pvt_key_temp,'decrypt');

    	$url ="http://localhost:8082/senderc";
        
        $client = new Client();
        
        $headers = [
            'Content-Type' => 'application/json',
        ];

        $quorum_url="http://3.17.77.216:22000";

        $body = ["quorum_url"=>$quorum_url,"token_tag" => $eth_pvt_key,"from" => $address,"to" => $to_address,"total_token" => $total_token,"contract" => $contract,"contractABI" => $contractABI,"decimal"=>$user_contract->decimal];

        $res = $client->post($url, [
            'headers' => $headers,
            'body' => json_encode($body),
        ]);
        $details = json_decode($res->getBody(),true);

        //return $details;

        //\Log::info($details);                    
        
        if($details['result']==1){ 

            if($details['status']==1){ 

		        $user_token_txn->status=1;
		        $user_token_txn->save();

		        $user_token=UserToken::findorFail($user_token_txn->user_token_id);
		        $total=$user_token->token_acquire+$total_token;
		        $user_token->token_acquire=$total;
		        $user_token->save();

		        return back()->with('flash_success','Transaction updated successfully');
	    	}

    	}elseif($details['result']==0){ 
            return response()->json(['error' => $details['error']], 500); 
        }
        else{
            return response()->json(['error' => $details['msg']], 500); 
        }

    }

    public function dividend(){
    	$TokenDividend = TokenDividend::orderBy('created_at', 'desc')->get();      
        return view('admin.pages.dividend', compact('TokenDividend'));
    }
    public function dividentdetail($id)
    {
       
        try {        
            $dividend=TokenDividend::where('dividend_id',$id)->first();
            $property= Property::where('id',$dividend->propertyid)->first();
            $issuer= User::where('id',$dividend->user_id)->first();  
            return view('admin.pages.dividend_detail',compact('dividend','property','issuer'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get Token lists. Please try again later');
        }
    }
     public function dividenttokendetail($id)
    {
        try {        
            $tokenbonusdividend=InvestorBonusToken::where('tokendividendid',$id)->get();
            $propid=$tokenbonusdividend[0]['propertyid'];

            $issuerid=$tokenbonusdividend[0]['issuer_id'];
          
            $property= Property::where('id',$propid)->first();
            $issuer= User::where('id',$issuerid)->first(); 
            $tokenDividend= TokenDividend::where('dividend_id',$id)->first();  
            return view('admin.pages.dividend_token_detail',compact('tokenbonusdividend','property','issuer','tokenDividend'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get Token lists. Please try again later');
        }
    }

    public function investordividendnotification($id){        
        try{
            $dividend=TokenDividend::where('dividend_id',$id)->first();
            $property= Property::where('id',$dividend->propertyid)->first();
            $bonustoken=$dividend->dividend/$property->tokenValue;
          
            $noofdays = (strtotime($dividend->enddate) - strtotime($dividend->startdate)) / (60 * 60 * 24);            
            $tokensupply=$property->tokenSupply;
           
            $date1=date("Y-m-d",strtotime($dividend->startdate));
            $date2=date("Y-m-d",strtotime($dividend->enddate));
            $adminWallet=AdminWallet::where('property_id',$property->id)->where('status', '=', 1)->whereBetween('updated_at',[$date1, $date2])->get();
          

           $tokendivi=TokenDividend::where('dividend_id',$id)->update([
            'status'=>1]);

            foreach($adminWallet as $ty=>$val){
                 $invnoofdays = (strtotime($dividend->enddate) - strtotime($val->updated_at)) / (60 * 60 * 24);  
                 $d1=$val->total_tokens/$tokensupply;
                 $d2=$d1*$bonustoken*$invnoofdays;
                 $d3=$d2/$noofdays;


                $investorbonustoken=InvestorBonusToken::create([
                    'tokendividendid'=>$id,
                    'propertyid'=>$dividend->propertyid,
                    'investor_id'=>$val->investor_id,
                    'issuer_id'=>$val->issuer_id,
                    'total_tokens'=>$val->total_tokens,
                    'inves_holdingdays'=>$invnoofdays,
                    'total_holdingdays_req'=>$noofdays, 
                    'dividendapprovestatus'=>1,  
                    'dividendtokens'=>round($d3, 4),   
                    'txnhash'=>'',
                    'status'=>'0',
                ]);
            
               $investorbonustoken->save();
            }    
 
          




            return response()->json(['status' => 'success']);
        }catch (\Throwable $th) {
            return response()->json(['status' => 'failed']);
        }
    }

    public function simple_crypt($string, $action = 'encrypt'){
        
        $key = "YMoEtIgr#W&Ab7uu3mlZeanIMr";   

        $res = '';
        if($action !== 'encrypt'){
            $string = base64_decode($string);
        }
        for( $i = 0; $i < strlen($string); $i++){
            $c = ord(substr($string, $i));
            if($action == 'encrypt'){
                $c += ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr($c & 0xFF);
            }else{
                $c -= ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr(abs($c) & 0xFF);
            }
        }
        if($action == 'encrypt'){
            $res = base64_encode($res);
        }
        return $res;
    }

}

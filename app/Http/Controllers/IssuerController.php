<?php

namespace App\Http\Controllers;

use App\DepositHistory;
use App\Http\Controllers\HomeController;
use App\Mail\Withdrawalotp;
use App\ManagementMembers;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Property;
use App\IssuerTokenRequest;
use App\UserContract;
use App\IssuerUser;
use App\UserToken;
use App\AssetType;
use App\Amenity;
use App\UserIdentity;
use App\Document;
use App\WithdrawEth;
use App\KycDocument;
use App\PropertyLandmark;
use App\PropertyComparable;
use App\PropertyImage;
use App\Http\Requests\CreateProperty;
use App\IssuerKyc;
use Auth;
use App\TokenDividend;
use App\Http\Controllers\CommonController;
use App\PropertyDetail;
use App\PropertyDocument;
use App\PropertyInformation;
use App\PropertyTeam;
use App\PropertyToken;
use App\AdminWallet;
use App\User;
use App\InvestorBonusToken;
use Illuminate\Support\Facades\Hash;

class IssuerController extends Controller
{
    /**
     * Used to show Issuer Dashboard
     */
    public function dashboard()
    {
        try {
            $user = Auth::user();
            (new CommonController)->updateAddress($user);
            $tokens = PropertyToken::where('user_id', $user->id);
            $requested = $tokens->count();
            $created = $tokens->where('deploy_status', 1)->count();
            return view('issuer.dashboard', compact('requested', 'created'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get dashboard details. Please try again later');
        }
    }
    public function dividenttokendetail($id)
    {
        try {        
            $tokenbonusdividend=InvestorBonusToken::where('tokendividendid',$id)->get();
            $propid=$tokenbonusdividend[0]['propertyid'];

            $issuerid=$tokenbonusdividend[0]['issuer_id'];
          
            $property= Property::where('id',$propid)->first();
            $issuer= User::where('id',$issuerid)->first(); 
            $tokenDividend= TokenDividend::where('dividend_id',$id)->first();  
            return view('issuer.dividend_token_detail',compact('tokenbonusdividend','property','issuer','tokenDividend'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get Token lists. Please try again later');
        }
    }
    /**
     * Used to show Issuer KYC
     */
    public function kyc()
    {
        try {
            $documents = Document::with('kycdocument')->orderBy('order', 'asc')->get();
            return view('issuer.kyc', compact('documents'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get KYC details. Please try again later');
        }
    }

    /**
     * Used to update KYC
     */
    public function updateBankDetails(Request $request){
        
       /* $this->validate($request, [
            'accholname'     =>  'required|max:255',
            'accnumber'  =>  'required|max:400',
            'ifsccode'      =>  'required|max:100',
            'bankname'     =>  'required|max:255',
            
        ]);*/
        
        
        try{
            $user = Auth::user();

          $datas = $request->all();
            $datas['user_id'] = $user->id;
         $product=IssuerUser::where('user_id',$user->id)->get();
       
         if(count($product)==0){
            $bankdetail = IssuerUser::create([
                'user_id'=>$user->id,
                'accountholdername'=>$request->accountholdername,
                'accno'=>$request->accnumber,             
                'bankname'=>$request->bankname,
                'ifsccode'=>$request->ifsccode,
            ]);    
            
            $bankdetail->save();
             User::where('id',$user->id)->update(['bank_approved' => 1]);
       }else{
        $bankdetail = IssuerUser::where('user_id',$user->id)->update([                
                'accountholdername'=>$request->accountholdername,
                'accno'=>$request->accnumber,             
                'bankname'=>$request->bankname,
                'ifsccode'=>$request->ifsccode,
            ]);  
        User::where('id',$user->id)->update(['bank_approved' => 1]);
            return back()->with('flash_success', 'Bank Details has been updated successfully');
       }


        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to update Bank Details. Please try again later');
        }
    }
    public function updateKyc(Request $request)
    {
       $this->validate($request, [
            'cname'     =>  'required|max:255',
            'caddress'  =>  'required|max:255',
            'city'      =>  'required|max:255',
            'state'     =>  'required|max:255',
            'cpan'      =>  'required|max:10',
            'cgst'      =>  'required|max:50',
        ]);
        try {
            $user = Auth::user();
            
            $client      = new Client(['http_errors' => false]);
            $headers     = [
                'Content-Type'  => 'application/json',
                'Authorization' => 'Bearer '.getenv('SUREPASS'),
            ];
            $body        = ["id_number" => $request->cgst, 'filing_status_get' => true, 'hsn_info_get' => true];
            $url         = 'https://kyc-api.aadhaarkyc.io/api/v1/corporate/gstin';
            $res         = $client->post($url, [
                'headers' => $headers,
                'body'    => json_encode($body),
            ]);
            $response = json_decode($res->getBody(), true);
            if(isset($response['success']) && isset($response['status_code']) && $response['success'] == true && $response['status_code'] == '200'){
                if($response['data']['business_name'] == $request->cname){
                    if($response['data']['pan_number'] == $request->cpan){
                        $data = $request->all();
                        $data['user_id'] = $user->id;
                        IssuerKyc::updateOrCreate(['user_id' => $user->id], $data);
                         User::where('id',$user->id)->update(['kyc' => 1]);
                        return back()->with('flash_success', 'KYC Updated Successfully');
                    }else{
                        return back()->with('flash_error', 'Pan number is not matched with the GSTIN');
                    }
                }else{
                    return back()->with('flash_error', 'Company name is not matched with the GSTIN');
                }
            }else{
                return back()->with('flash_error', 'Please enter valid GST Number');
            }
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to update KYC. Please try again later');
        }
    }

    /**
     * Used to show Issuer Profile
     */
    public function profile()
    {
        $user = Auth::user();
        $kyc = IssuerKyc::where('user_id',$user->id)->first();
        return view('issuer.profile', compact('user', 'kyc'));
    }

    /**
     * Used to Update Payment Details
     */
    public function profile_update(Request $request)
    {

        try {
            $data=UserIdentity::where('user_id',Auth::user()->id)->first();

            if(isset($data->id)){
                $userIdentity =UserIdentity::where('user_id',Auth::user()->id)->first();
            }else{
                $userIdentity = new UserIdentity;
            }
            
            $userIdentity->user_id       = Auth::user()->id;
            $userIdentity->first_name    = $request->fname;
            $userIdentity->last_name     = $request->lname;
            $userIdentity->primary_phone = $request->mobileno;
            $userIdentity->city_id       = 0;
            $userIdentity->eth_address   = $request->eth_address;
            $userIdentity->save();

            return back()->with('flash_success', 'Profile has been updated successfully');
        } catch (\Throwable $th) {
            dd($th->getMessage());
            return back()->with('flash_error', 'Oops, Unable to update Profile');
        }
    }

    /**
     * Used to show Property
     */
    public function property() {

        $user         = Auth::user();
        $properties = Property::where('user_id', $user->id)->orderBy('created_at', 'desc')->get();
        return view('issuer.property', compact('properties'));
        $propertylist = (new CommonController)->calculatePercentage($propertylist);
        return view('issuer.property', ["properties" => $propertylist]);
    }

    public function propertyDetail($id)
    {
        try {
            $user     = Auth::user();
            $property = Property::find($id);
            if (!is_null($property->userContract)) {
                $tokens                          = (new UserToken)->getUserToken($property->userContract->id)->sum('token_acquire');
                $usd_value                       = $property->userContract->tokenvalue * $tokens;
                $property['accuired_usd']        = $usd_value;
                $percentage                      = ($usd_value / $property->totalDealSize) * 100;
                $property['accuired_percentage'] = $percentage;
            } else {
                $property['accuired_usd']        = 0;
                $property['accuired_percentage'] = 0;
            }
            return view('issuer.propertyDetail', compact('user', 'property'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to Get Property Details');
        }
    }


    /**
     * Used to show Token
     */
    public function token()
    {
        $assetType = AssetType::orderBy('type', 'asc')->get();
        $amenity = Amenity::where('status', '1')->orderBy('amenity_name', 'asc')->get();
        return view('issuer.token', compact('assetType','amenity'));
    }
     public function dividendlist()
    {
        $TokenDividend = TokenDividend::orderBy('created_at', 'desc')->get();
        return view('issuer.dividendlist',compact('TokenDividend'));
    }
public function dividend()
    {
    $user= Auth::user();
      //  $assetType = AssetType::orderBy('type', 'asc')->get();
    $property = Property::where('user_id', $user->id)->orderBy('created_at', 'desc')->get();
        return view('issuer.dividend', compact('property'));
    }
    public function asset_fund()
    {
        $assetType = AssetType::orderBy('type', 'asc')->get();
        return view('issuer.asset_fund', compact('assetType'));
    }

    /**
     * Used to show Token List
     */
    public function tokenList($id)
    {
       
        try {
         //  $tokens = UserContract::where('status', 1)->where('user_id',Auth::user()->id)->get();
            //return view('issuer.tokenlist', compact('tokens'));
            $property = Property::where('id',$id)->get();
            $wallet = AdminWallet::where('property_id',$id)->where('status', 1)->join('users', 'users.id', '=', 'investor_id')->get();

            return view('issuer.tokenlist',compact('property','wallet'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get Token lists. Please try again later');
        }
    }
 public function dividentdetail($id)
    {
       
        try {        
            $dividend=TokenDividend::where('dividend_id',$id)->first();
            $property= Property::where('id',$dividend->propertyid)->first();
            $issuer= User::where('id',$dividend->user_id)->first();  
            return view('issuer.dividend_detail',compact('dividend','property','issuer'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get Token lists. Please try again later');
        }
    }
    /**
     * Used to show Token Requests
     */
    public function tokenRequest()
    {
        try {
            $tokens = Property::where('user_id', Auth::user()->id)->get();
            return view('issuer.tokenrequest', compact('tokens'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get Token Request List. Please try again later');
        }
    }
     public function tokenApprove()
    {
        try {
            $tokens = Property::where('user_id', Auth::user()->id)->get();
            return view('issuer.tokenapprove', compact('tokens'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get Token Request List. Please try again later');
        }
    }
    public function investordividendnotification($id){        
        try{
            $dividend=TokenDividend::where('dividend_id',$id)->first();
            $property= Property::where('id',$dividend->propertyid)->first();


            $bonustoken=$dividend->dividend/$property->tokenValue;
            $noofdays = (strtotime($dividend->enddate) - strtotime($dividend->startdate)) / (60 * 60 * 24);
echo $noofdays;
            //return response()->json(['status' => 'success']);
        }catch (\Throwable $th) {
            //return response()->json(['status' => 'failed']);
        }
    }

    /**
     * Used to Store Dividend
     */
    public function dividendStore(Request $request)
    {
        try {
            $user = Auth::user();
            $datas = $request->all();
            $datas['user_id'] = $user->id;
            
            
            if($request->hasfile('document1')) {

                    $document1file = $request->file('document1');
                    $document1extension = $document1file->getClientOriginalExtension(); 
                  
                    $document1filename = time() . '.' . $document1extension;
                    $document1destination="public/uploads/dividenddoc1/".$document1filename;
                    $document1file->move('public/uploads/dividenddoc1/', $document1filename);                    
                    $doc1 =  $document1destination;
            } else {
                    return $request;
                    $doc1 = '';
            }

            if($request->hasfile('document2')) {

                    $document2file = $request->file('document2');
                    $document2extension = $document2file->getClientOriginalExtension(); 
                  
                    $document2filename = time() . '.' . $document2extension;
                    $document2destination="public/uploads/dividenddoc2/".$document2filename;
                    $document2file->move('public/uploads/dividenddoc2/', $document2filename);                    
                    $doc2 =  $document2destination;
            } else {
                    return $request;
                    $doc2 = '';
            }


            if($request->hasfile('document3')) {

                    $document3file = $request->file('document3');
                    $document3extension = $document3file->getClientOriginalExtension(); 
                  
                    $document3filename = time() . '.' . $document3extension;
                    $document3destination="public/uploads/dividenddoc3/".$document3filename;
                    $document3file->move('public/uploads/dividenddoc3/', $document3filename);
                    $doc3 =  $document3destination;
            } else {
                    return $request;
                    $doc3 = '';
            }

            if($request->hasfile('document4')) {

                    $document4file = $request->file('document4');
                    $document4extension = $document3file->getClientOriginalExtension(); 
                  
                    $document4filename = time() . '.' . $document4extension;
                    $document4destination="public/uploads/dividenddoc4/".$document4filename;
                    $document4file->move('public/uploads/dividenddoc4/', $document4filename);
                    $doc4 =  $document4destination;
            } else {
                    return $request;
                    $doc4 = '';
            }


             if($request->hasfile('document5')) {

                    $document5file = $request->file('document5');
                    $document5extension = $document3file->getClientOriginalExtension(); 
                  
                    $document5filename = time() . '.' . $document5extension;
                    $document5destination="public/uploads/dividenddoc5/".$document5filename;
                    $document5file->move('public/uploads/dividenddoc5/', $document5filename);
                    $doc5 =  $document5destination;
            } else {
                    return $request;
                    $doc5 = '';
            }

            $tokenDividend= TokenDividend::create([
                    'propertyid'=>$request->propertyname,
                    'user_id'=>$user->id,
                    'startdate'=>$request->fromdate,
                    'enddate'=>$request->enddate,
                    'title'=>$request->title,
                    'description'=>$request->description,
                    'dividend'=>$request->dividend,
                    'status'=>0,
                    'doc1'=>$doc1,
                    'doc2'=>$doc2,
                    'doc3'=>$doc3,
                    'doc4'=>$doc4,
                    'doc5'=>$doc5,
                ]);
                
                $tokenDividend->save();
        return redirect('/issuer/property')->with(['flash_success'=>'Dividend Request Generated Successfully','clearcookie'=>'1']);
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to Create Dividend Request');
        }
    }

    /**
     * Used to Store Property
     */
    public function storeProperty(Request $request)
    {
        try {
            $user = Auth::user();
            $datas = $request->all();
            $datas['user_id'] = $user->id;
           
           
            $property = Property::create($datas);            
            $property->user_id = $user->id;
            $property->propertyName = $request->propertyname;
            $property->propertyState=$request->state;
            $property->propertyCity=$request->city;
            $property->propertyDistrict=$request->propertydistrict;
            $property->propertyType=$request->propertytype;
            $property->tokenName=$request->tokenname;
            $property->tokenSymbol=$request->tokensymbol;
            $property->tokenValue=$request->tokenvalue;
            $property->tokenSupply=$request->tokensupply;
            $property->tokenDecimal=$request->tokendecimal;
           // $property->tokenLogo=$request->tokenlogo;


            if($request->hasfile('tokenlogo')) {

                    $tokenfile = $request->file('tokenlogo');
                    $tokenextension = $tokenfile->getClientOriginalExtension(); 
                  
                    $tokenfilename = time() . '.' . $tokenextension;
                    $tokendestination="public/uploads/tokenlogo/".$tokenfilename;
                    $tokenfile->move('public/uploads/tokenlogo/', $tokenfilename);                    
                    $property->tokenLogo =  $tokendestination;
            } else {
                    return $request;
                    $property->tokenLogo = '';
            }


            $property->Divident=$request->divident;
            $property->minimumInvest=$request->minimuminvestment;
            $property->propertySize=$request->propertysize;
            $property->targetIrr=$request->targetirr;
            $property->averageYield=$request->averageyield;
            $property->propertyDesc=$request->propdesc;
            $property->Proponent=$request->proponent;
            $property->Onmap=$request->onmap;
            $property->Address=$request->address;
            $property->propertyStatus=$request->propertystatus;
            $property->buildYear=$request->buildyear;
            $property->reraNumber=$request->reranumber;
            $property->propertyManager=$request->propertymanager;
            $property->Custodian=$request->custodian;
            $amty=implode("#",$request->amenities);
            $property->propertyAmenity=$amty;
          
            if($request->hasfile('techspecs')) {

                    $file = $request->file('techspecs');
                    $extension = $file->getClientOriginalExtension(); 
                  
                    $filename = time() . '.' . $extension;
                    $destination="public/uploads/techspecs/".$filename;
                    $file->move('public/uploads/techspecs/', $filename);                    
                    $property->propertyTechSpecs =  $destination;
            } else {
                    return $request;
                    $property->propertyTechSpecs = '';
            }

             $property->costOfPremise = $request->costofpremise;
             $property->stampDutyReg = $request->stampdutyregistration;
             $property->acquisitionFee = $request->acquisitionfee;
             $property->otherCharges = $request->othercharges;
             $property->legalFees=$request->legalfees;
             $property->maintenanceReserve=$request->maintenancereserve;
             $property->adminExpenses=$request->adminexpenses;
             $property->monthlyOpex=$request->monthlyopex;
             $property->monthlyPropertyTax=$request->monthlypropertytax;
             $property->regulatoryCharges=$request->monthlyregulatorycharges;
             $property->totalAcquisitionCost=$request->totalacquisitioncost;
             $property->carpetArea=$request->carpetarea;
             $property->builtUpArea=$request->builtuparea;
             $property->monthlyRent=$request->monthlyrent;
             $property->leasedPeriod=$request->leasedperiod;
             $property->securityDeposit=$request->securitydeposit;
             $property->escalation=$request->escalation;
             $property->tenants=$request->tenants;
             $property->fittedOut=$request->fittedout;
             $property->monthlyMaintenance=$request->monthlymaintenance;
             $property->leaseChart=$request->leasechart;
             $property->grossYield=$request->grossyield;
             $property->expectedIr=$request->expectedirr;
             $property->tokenLockinPeriod=$request->tokenlockinperiod;
             $property->aurumFacilationFees=$request->aurumfacilationfees;
             $property->performanceFees=$request->performancefees;
             if($request->hasfile('investmentdeck')) {

                    $investfile = $request->file('investmentdeck');
                    $investextension = $investfile->getClientOriginalExtension(); 
                  
                    $investfilename = time() . '.' . $investextension;
                    $investdestination="public/uploads/investmentdoc/".$investfilename;
                    $investfile->move('public/uploads/investmentdoc/', $investfilename);                    
                    $property->propertyInvestmentDeck =  $investdestination;
            } else {
                    return $request;
                    $property->propertyInvestmentDeck = '';
            }

            if($request->hasfile('titlereport')) {

                    $titlefile = $request->file('titlereport');
                    $titleextension = $titlefile->getClientOriginalExtension(); 
                  
                    $titlefilename = time() . '.' . $titleextension;
                    $titledestination="public/uploads/titlereport/".$titlefilename;
                    $titlefile->move('public/uploads/titlereport/', $titlefilename);                    
                    $property->propertytitleReport=$titledestination;
            } else {
                    return $request;
                    $property->propertytitleReport = '';
            }

             if($request->hasfile('propertyduediligence')) {

                    $duediligencefile = $request->file('propertyduediligence');
                    $duediligenceextension = $duediligencefile->getClientOriginalExtension();
                    $duediligencefilename = time() . '.' . $duediligenceextension;
                    $duediligencedestination="public/uploads/propertyduediligence/".$duediligencefilename;
                    $duediligencefile->move('public/uploads/propertyduediligence/', $duediligencefilename);                    
                    $property->propertyDueDiligence=$duediligencedestination;
            } else {
                    return $request;
                    $property->propertyDueDiligence = '';
            }

            if($request->hasfile('micromarketreport')) {

                    $micromarketfile = $request->file('micromarketreport');
                    $micromarketextension = $micromarketfile->getClientOriginalExtension();
                    $micromarketfilename = time() . '.' . $micromarketextension;
                    $micromarketdestination="public/uploads/micromarketreport/".$micromarketfilename;
                    $micromarketfile->move('public/uploads/micromarketreport/', $micromarketfilename);                    
                    $property->propertyMicroMarket=$micromarketdestination;
            } else {
                    return $request;
                    $property->propertyMicroMarket = '';
            }

             if($request->hasfile('propertymanagementagreement')) {

             $managementagreementfile = $request->file('propertymanagementagreement');
            $managementagreementextension = $micromarketfile->getClientOriginalExtension();
             $managementagreementfilename = time() . '.' . $managementagreementextension;
     $managementagreementdestination="public/uploads/propertymanagementagreement/".$managementagreementfilename;
     $managementagreementfile->move('public/uploads/propertymanagementagreement/', $managementagreementfilename);                    
                    $property->propertyMgmtAgrmt=$managementagreementdestination;
            } else {
                    return $request;
                    $property->propertyMgmtAgrmt = '';
            }
            if($request->hasfile('valuationreport')) {

             $valuationreportfile = $request->file('valuationreport');
            $valuationreportextension = $micromarketfile->getClientOriginalExtension();
             $valuationreportfilename = time() . '.' . $valuationreportextension;
             $valuationreportdestination="public/uploads/valuationreport/".$valuationreportfilename;
             $valuationreportfile->move('public/uploads/valuationreport/', $valuationreportfilename);                    
                    $property->propertyValReport=$valuationreportdestination;
            } else {
                    return $request;
                    $property->propertyValReport = '';
            }


            if($request->hasFile('gallery'))
            {
                foreach($request->file('gallery') as $image)
                {
                    $PropImgdestinationPath = 'uploads/propertyimages/';
                    $propfilename = $image->getClientOriginalName();
                    $image->move($PropImgdestinationPath, $propfilename);
                    $ty[]=$PropImgdestinationPath.$propfilename;

                }
               $rt=implode("#",$ty);
               echo $rt;
            }
            $property->galleryImage = $rt;
            $property->status ='pending';
            $property->tokenStatus ='pending';
             $property->save();
        
           return redirect('/issuer/property')->with(['flash_success'=>'Property created successfully','clearcookie'=>'1']);
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to Create Property');
        }
    }

    /**
     * Used to Create or Update Product
     */
    public function productcrud($property, $request)
    {
        try{
        if ($request->has('dividend')) {
            $property->dividend = $request->dividend;
        }

        $property->user_id                   = Auth::user()->id;
        $property->propertyName              = $request->propertyName;
        
        $property->expectedIrr               = $request->expectedIrr;
        // $property->fundedMembers             = $request->fundedMembers;
        $property->initialInvestment         = $request->initialInvestment;
        $property->propertyEquityMultiple    = $request->propertyEquityMultiple;
        
        $property->holdingPeriod             = $request->holdingPeriod;
        $property->propertyOverview          = $request->propertyOverview;
        $property->propertyHighlights        = $request->propertyHighlights;
        $property->token_type                = $request->token_type;
        
        $property->ManagementTeamDescription = $request->ManagementTeamDescription;

        //Newly added Fields
        $property->leased_period = $request->leased_period;
        $property->rera_number = $request->rera_number;
        $property->dev_name = $request->dev_name;
        $property->property_age = $request->property_age;
        $property->property_status = $request->property_status;
        $property->tenant_name = $request->tenant_name;
        $property->rent_escalation = $request->rent_escalation;
        $property->notice_period = $request->notice_period;
        $property->carpet_area = $request->carpet_area;
        $property->built_up_area = $request->built_up_area;
        $property->gross_yield = $request->gross_yield;
        $property->annual_rent = $request->annual_rent;
        $property->geo_location = $request->geo_location;
        $property->street_address = $request->street_address;
        $property->floor_number = $request->floor_number;
        $property->area_name = $request->area_name;
        $property->pincode = $request->pincode;
        
        $property->facilities = $request->facilities;
        $property->facilitation_fees = $request->facilitation_fees;
        $property->psf_rate = $request->psf_rate;
        $property->custodian = $request->custodian;
        $property->registration_fees = $request->registration_fees;
        $property->property_gross_yield = $request->property_gross_yield;
        $property->total_cost_psf = $request->total_cost_psf;
        $property->net_operating_yield_sale = $request->net_operating_yield_sale;
        $property->net_realizable_yield_sale = $request->net_realizable_yield_sale;
        $property->net_realizable_yield_purchase = $request->net_realizable_yield_purchase;
        $property->stamp_duty = $request->stamp_duty;
        $property->sale_price = $request->sale_price;



        if ($request->token_type == 1) {
            
            $property->propertyLocation = $request->propertyLocation;
            $property->propertyType     = $request->propertyType;
            if ($request->has('token_value')) {
                $property->totalDealSize = $request->total_sft / $request->token_value;  //$request->totalDealSize;
            }
            $property->total_sft                 = $request->total_sft;
            $property->propertyLocationOverview  = $request->propertyLocationOverview;
            // $property->locality                  = $request->locality;
            $property->yearOfConstruction        = $request->yearOfConstruction;
            $property->storeys                   = $request->storeys;
            $property->propertyParking           = $request->propertyParking;
            $property->floorforSale              = $request->floorforSale;
            $property->propertyParkingRatio      = $request->propertyParkingRatio;
            // $property->propertyTotalBuildingArea = $request->propertyTotalBuildingArea;
            $property->propertyDetailsHighlights = $request->propertyDetailsHighlights;
        } else {
            // dd('b');
            $property->totalDealSize = $request->totalDealSize;
        }

        if ($request->hasFile('propertyVideo'))
            $property->propertyVideo = $request->propertyVideo->store('propertyVideo');

        if ($request->hasFile('propertyLogo'))
            $property->propertyLogo = $request->propertyLogo->store('propertyLogo');

        if ($request->hasFile('floorplan'))
            $property->floorplan = $request->floorplan->store('floorplan');

        if ($request->hasFile('investor'))
            $property->investor = $request->investor->store('investor');

        if ($request->hasFile('titlereport'))
            $property->titlereport = $request->titlereport->store('titlereport');

        if ($request->hasFile('termsheet'))
            $property->termsheet = $request->termsheet->store('termsheet');

        if ($request->hasFile('due_diligence_report'))
            $property->due_diligence_report = $request->due_diligence_report->store('due_diligence_report');

        if ($request->hasFile('propertyManagementTeam')) {
            $property->propertyManagementTeam = $request->propertyManagementTeam->store('propertyManagementTeam');
        }

        if ($request->hasFile('propertyUpdatesDoc')) {
            $property->propertyUpdatesDoc = $request->propertyUpdatesDoc->store('propertyUpdatesDoc');
        }

        $property->save();

        // if(!empty($request['landmarks'])){
        // 	PropertyLandmark::where('property_id', $property->id)->delete();
        // 	foreach ($request['landmarks'] as $key => $value) {
        // 		$property_landmark=new PropertyLandmark;
        // 		$property_landmark->property_id=$property->id;
        // 		$property_landmark->landmarkName=$value['landmarkName'];
        // 		$property_landmark->landmarkDist=$value['landmarkDist'];
        // 		$property_landmark->save();
        // 	}
        // }

        if (!empty($request['comparables'])) {
            PropertyComparable::where('property_id', $property->id)->delete();
            foreach ($request['comparables'] as $key => $value) {
                $property_comparables                     = new PropertyComparable;
                $property_comparables->property_id        = $property->id;
                // $property_comparables->property           = $value['property'];
                $property_comparables->type               = $value['type'];
                // $property_comparables->location           = $value['location'];
                $property_comparables->distance           = $value['distance'];
                // $property_comparables->rent               = $value['rent'];
                $property_comparables->saleprice          = $value['saleprice'];
                $property_comparables->map                = $request->map->store('map');
                $property_comparables->comparable_details = $request->comparabledetails->store('comparabledetails');
                $property_comparables->save();
            }
        }

        if (!empty($request->member)) {
            foreach ($request->member as $key => $value) {
                $member                    = new ManagementMembers;
                $member->property_id       = $property->id;
                $member->memberName        = $value['name'];
                
                $member->memberPosition    = $value['position'];
                
                $member->memberDescription = $value['description'];
                $member->memberPic         = $value['pic']->store('memberPic');
                
                $member->save();
            
            }
        }

        if ($request->hasFile('propertyimages')) {
            $files = $request->file('propertyimages');
            foreach ($files as $key => $file) {
                $productImage              = new PropertyImage;
                $productImage->property_id = $property->id;
                $productImage->image       = $file->store('propertyImage');
                $productImage->save();
            }
        }

        return $property;
    }
    catch (\Throwable $th) {
       return back()->with('flash_error','Something went wrong');
    }
}
    public function wallet(){
        $user = Auth::user();

        $fiat_history = AdminWallet::orderBy('created_at', 'DESC')->get();
        return view('issuer.wallet', compact('user', 'fiat_history'));
    }
    //ETH WITHDRAWAL FLOW
    public function generateWithdrawOTP(Request $request){
        try {
            $user = Auth::user();
            $data = rand(100000,999999);
            $user->eth_otp = Hash::make($data);
            $user->save();

            $to_email = $user->email;
            //$to_email = 'test12345671@mailinator.com';

            logger()->info('otp: '. $data);

            $maildata = ['otp' => $data];
            \Illuminate\Support\Facades\Mail::to($to_email)->send(new Withdrawalotp($maildata));

            return response()->json(['success' => ['msg' => 'We have sent 6 digits OTP to your email address.']],200);
        }catch(Exception $e){
            \Log::critical($e->getMessage());
            return response()->json(['error' => ['msg' => 'Something Went Wrong']],500);
        }
    }
    public function withdrawETHBalance()
    {
        $user = Auth::user();
        $history = WithdrawEth::whereuser_id($user->id)->orderBy('created_at', 'DESC')->get();
        $eth_address = $user->eth_address;

        $wallet = json_decode((new HomeController)->getETHbalance($eth_address),TRUE);

        $ethbalance = $wallet['balance'];

        $gasPrice = $this->gasPrice();
        $gasPrice = json_decode($gasPrice, true);
        if(isset($gasPrice['result']) && $gasPrice['result']){
            $gas = $gasPrice['result'];
        }
        else{
            $gas = '0x5208';
        }
        $gwei = hexdec($gas) / 1000000000;
        $ether = $gwei / 1000000000;
        $gasinether = ($ether * 30400);
        $gasinether = ((30 / 100) * $gasinether) + $gasinether;

        return view('ethwithdraw_issuer',compact('ethbalance', 'gwei', 'gasinether', 'user','history'));
    }

    public function sendETH(Request $request){
        $this->validate($request, [
            'amount'    =>  'required|numeric|min:0',
            'address'   =>  'required',
            'withdrawotp' => 'required|digits:6',
        ]);

        try {

            $user = Auth::user();
            if(!Hash::check($request->withdrawotp, $user->eth_otp)){
                return back()->with('flash_error', 'Withdrawal otp is mismatched');
            }

            $wallet = json_decode((new HomeController)->getETHbalance($user->eth_address),TRUE);
            $eth_balance = $wallet['balance'];
            if($eth_balance <= 0){
                return back()->with('flash_error', 'Not enough ETH available in your wallet to withdraw');
            }
//dd($user->eth_address,$request->amount,$request->withdrawotp);
            $gasPrice = $this->gasPrice();
            $gasPrice = json_decode($gasPrice, true);
            if(isset($gasPrice['result']) && $gasPrice['result']){
                $gas = $gasPrice['result'];
            }
            else{
                $gas = '0x5208';
            }
            $gwei = hexdec($gas) / 1000000000;
            $ether = $gwei / 1000000000;
            $gasinether = ($ether * 30400);
            $gasinether = ((30 / 100) * $gasinether) + $gasinether;

            $eth_balance = $eth_balance - $request->amount;
            $withdraw_amount = $eth_balance - $gasinether;

            if($eth_balance < $gasinether){
                return back()->with('flash_error', 'Insufficient balance to withdraw');
            }

            $client  = new Client();
            $headers = ['Content-Type' => 'application/x-www-form-urlencoded',];
            $url     = 'http://localhost:1337/getkey';

            $res = $client->post($url, [
                'headers'     => $headers,
                'form_params' => [
                    'add'    => $user->eth_address,
                    'string' => $user->email,
                ],
            ]);

            $res = json_decode($res->getBody(), true);
            if (isset($res['status']) && $res['status'] == true) {
                $eth_pvt_key = $res['key'];
                $url         = 'http://localhost:3000/eth_transfer';

                $client      = new Client();
                $headers     = ['Content-Type' => 'application/json',];
                $body = [
                    'from' => $user->eth_address,
                    'password' => $eth_pvt_key,
                    'to' => $request->address,
                    'value' => $request->amount,
                ];

                $res = $client->post($url, [
                    'headers' => $headers,
                    'body' => json_encode($body),
                ]);

                $details = json_decode($res->getBody(), true);
                \Log::info($details);
                if (isset($details['status']) && $details['status'] == 'success') {
                    $model = new WithdrawEth;
                    $model->user_id = $user->id;
                    $model->sender = $user->eth_address;
                    $model->receiver = $request->address;
                    $model->amount = $request->amount;
                    $model->tx_hash = $details['result'];
                    $model->save();

                    return back()->with('flash_success', 'ETH has been withdrawn successfully');

                } else {
                    return back()->with('flash_error', 'Unable to withdraw ETH. Please try again later');
                }

            } else {
                return back()->with('flash_error', 'Unable to withdraw ETH. Please try again later');
            }

            //dd($request->all());

        } catch (\Throwable $th){
            \Log::critical('withdrawETH', ['message' => $th->getMessage()]);
            return back()->with('flash_error', 'Unable to withdraw ETH. Please try again later');
        }


//        try {
//            dd($request->all());
//            $user = Auth::user();
//            if(Hash::check($request->withdrawotp, $user->otp)){
//                $port = $this->checkport('7329');
//                if($port != false){
//                    $unlock = $this->unlock($user->eth_address, $user->email);
//                    if(isset($unlock['result'])) {
//                        $gasPrice = $this->gasPrice();
//                        $gasPrice = json_decode($gasPrice, true);
//                        if(isset($gasPrice['result']) && $gasPrice['result']){
//                            $gas = $gasPrice['result'];
//                        }
//                        else{
//                            $gas = '0x5208';
//                        }
//                        $gwei = hexdec($gas) / 1000000000;
//                        $ether = $gwei / 1000000000;
//                        $gasinether = ($ether * 30400);
//                        $gasinether = ((30 / 100) * $gasinether) + $gasinether;
//
//                        $eth_balance = $this->eth_curl($user->eth_address);
//                        $eth_balance = json_decode($eth_balance, true);
//                        if(isset($eth_balance['result'])){
//                            $balance = hexdec($eth_balance['result']);
//                            $ethbalance = $balance/1000000000000000000;
//                        }
//                        else{
//                            $ethbalance = 0;
//                        }
//
//                        if($eth_balance < $gasinether){
//                            if($request->ajax())
//                                return response()->json(['error' => ['msg' => 'Not enough gas fee available in your wallet to withdraw']], 400);
//                            else
//                                return back()->with('flash_error', 'Not enough gas fee available in your wallet to withdraw');
//                        }
//
//                        $price = '0x'.dechex(($request->amount) * 1000000000000000000);
//                        $client = new Client();
//                        $headers = [
//                            'Content-Type' => 'application/json',
//                        ];
//                        $body = ["jsonrpc" => "2.0","method" => "eth_sendTransaction", "params" => array(["from" => $user->eth_address, "to" => $request->address, "value" => $price]), "id" => 1];
//                        $url ="http://54.209.140.176:7329";
//                        $res = $client->post($url, [
//                            'headers' => $headers,
//                            'body' => json_encode($body),
//                        ]);
//                        $txs = json_decode($res->getBody(),true);
//                        \Log::info($txs);
//                        if(isset($txs['result'])) {
//                            $model = new WithdrawEth;
//                            $model->user_id = $user->id;
//                            $model->sender = $user->eth_address;
//                            $model->receiver = $request->address;
//                            $model->amount = $request->amount;
//                            $model->tx_hash = $txs['result'];
//                            $model->save();
//                            if($request->ajax())
//                                return response()->json(['success' => ['msg' => 'ETH has been withdrawn successfully']], 200);
//                            else
//                                return back()->with('flash_success', 'ETH has been withdrawn successfully');
//                        }
//                        else{
//                            if($request->ajax())
//                                return response()->json(['error' => ['msg' => 'Gas fee too high. Please try again later']], 400);
//                            else
//                                return back()->with('flash_error', 'Gas fee too high. Please try again later');
//                        }
//                    }
//                    else{
//                        if($request->ajax())
//                            return response()->json(['error' => ['msg' => 'Unable to withdraw ETH. Please try again later']], 400);
//                        else
//                            return back()->with('flash_error', 'Unable to withdraw ETH. Please try again later');
//                    }
//                }
//                else{
//                    if($request->ajax())
//                        return response()->json(['error' => ['msg' => 'Unable to made transaction right now. Please try again later']], 400);
//                    else
//                        return back()->with('flash_error', 'Unable to made transaction right now. Please try again later');
//                }
//            }
//            else{
//                return back()->with('flash_error', 'Withdrawal otp is mismatched');
//            }
//        } catch (\Throwable $th) {
//            \Log::critical('withdrawETH', ['message' => $th->getMessage()]);
//            if($request->ajax())
//                return response()->json(['error' => ['msg' => 'Unable to withdraw ETH. Please try again later']], 400);
//            else
//                return back()->with('flash_error', 'Unable to withdraw ETH. Please try again later');
//        }
    }
    /**
     * Used to store Payment
     */
    public function storePayment(Request $request){
        try {
           $user = Auth::user();
            $user->USD += $request->totalAmount;
            $user->save();

            $history = new DepositHistory;
            $history->user_id = $user->id;
            $history->payment_id = $request->razorpay_payment_id;
            $history->payment_mode = 'FIAT';
            $history->amount = $request->totalAmount;
            $history->save();

            return "success";
        } catch (\Throwable $th) {
            return "failed";
        }
    }
    public function unlock($address, $email){
        try {
            $client = new Client();
            $headers = [
                'Content-Type' => 'application/json',
            ];
            $body = ["jsonrpc" => "2.0","method" => "personal_unlockAccount", "params" => [$address, $email, 3600], "id" => 1];
            $url ="http://54.209.140.176:7329";
            $res = $client->post($url, [
                'headers' => $headers,
                'body' => json_encode($body),
            ]);
            $unlock = json_decode($res->getBody(),true);
            return $unlock;
        } catch (\Throwable $th) {
            $unlock = ['status' => false];
            return $unlock;
        }
    }

    /**
     * Used to get ETH Balance
     */
    public function eth_curl($address){      
        try {
            $curl = curl_init();
            $request = json_encode([
                'jsonrpc'   =>  '2.0',
                'method'    =>  'eth_getBalance',
                'params'    =>  [
                    $address,
                    'latest'
                ],
                'id'    =>  1
            ]);
            curl_setopt_array($curl, array(
                CURLOPT_URL => env('INFURA_URL'),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $request,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            return $response;
        } catch (\Throwable $th) {
            \Log::critical('eth_curl', ['message' => $th->getMessage()]);
            return back()->with('flash_error', 'Unable to fetch eth balance data. Please try again later');
        }          
    }

    public function checkport($port){
        if($_SERVER['REMOTE_ADDR'] == '127.0.0.1')
            $host = 'localhost';
        else
            $host = '54.209.140.176';    
        $connection = @fsockopen($host, $port);
        return $connection;
    }

    public function gasPrice(){
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => env('INFURA_URL'),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS =>"{\"jsonrpc\":\"2.0\",\"method\":\"eth_gasPrice\",\"params\":[],\"id\":1}",
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            return $response;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }        
    }
}

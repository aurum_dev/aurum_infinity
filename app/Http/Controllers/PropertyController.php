<?php

namespace App\Http\Controllers;

use App\ManagementMembers;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Property;
use App\PropertyLandmark;
use App\PropertyComparable;
use App\PropertyImage;
use App\AssetType;
use App\Amenity;
use App\IssuerTokenRequest;
use App\User;
use Exception;
use Auth;
use App\ProfileHelp;
use App\PropertyToken;
use App\PropertyDetail;
use App\PropertyDocument;
use App\PropertyInformation;
use App\PropertyTeam;
use App\EthPrivatekey;
class PropertyController extends Controller
{
    public function index()
    {
        try {
            $title="Property List";
            $property = Property::orderBy('created_at', 'desc')->get();
            return view('admin.property.index', compact('property','title'));
        } catch (Exception $e) {
            return back()->with('flash_error',
                                trans('api.something_went_wrong'));
        }
    }

   public function create()
    {
        try {
           
            $token_type = \request()->get('type', 'property');
            $users      = User::orderBy('name', 'asc')->where('user_type', 2)->get();
            $assetType  = AssetType::orderBy('type', 'asc')->get();
             $amenity = Amenity::where('status', '1')->orderBy('amenity_name', 'asc')->get();

            if ($token_type == 'asset-fund') {
                return view('admin.property.create_asset_fund', compact('assetType', 'users'));
            } else {
                return view('admin.property.create', compact('assetType', 'users','amenity'));
            }

        } catch (Exception $e) {
            return back()->with('flash_error',
                                trans('api.something_went_wrong'));
        }
    }

    public function store(Request $request)
    {
       
        try {
            $user = Auth::user();
            $datas = $request->all();
            $datas['user_id'] = $request->propertyclient;
           
           
            $property = Property::create($datas);            
            $property->user_id = $request->propertyclient;
            $property->propertyName = $request->propertyname;
            $property->propertyState=$request->state;
            $property->propertyCity=$request->city;
            $property->propertyDistrict=$request->propertydistrict;
            $property->propertyType=$request->propertytype;
            $property->tokenName=$request->tokenname;
            $property->tokenSymbol=$request->tokensymbol;
            $property->tokenValue=$request->tokenvalue;
            $property->tokenSupply=$request->tokensupply;
            $property->tokenDecimal=$request->tokendecimal;
           // $property->tokenLogo=$request->tokenlogo;


            if($request->hasfile('tokenlogo')) {

                    $tokenfile = $request->file('tokenlogo');
                    $tokenextension = $tokenfile->getClientOriginalExtension(); 
                  
                    $tokenfilename = time() . '.' . $tokenextension;
                    $tokendestination="public/uploads/tokenlogo/".$tokenfilename;
                    $tokenfile->move('public/uploads/tokenlogo/', $tokenfilename);                    
                    $property->tokenLogo =  $tokendestination;
            } else {
                    return $request;
                    $property->tokenLogo = '';
            }


            $property->Divident=$request->divident;
            $property->minimumInvest=$request->minimuminvestment;
            $property->propertySize=$request->propertysize;
            $property->targetIrr=$request->targetirr;
            $property->averageYield=$request->averageyield;
            $property->propertyDesc=$request->propdesc;
            $property->Proponent=$request->proponent;
            $property->Onmap=$request->onmap;
            $property->Address=$request->address;
            $property->propertyStatus=$request->propertystatus;
            $property->buildYear=$request->buildyear;
            $property->reraNumber=$request->reranumber;
            $property->propertyManager=$request->propertymanager;
            $property->Custodian=$request->custodian;
            $amty=implode("#",$request->amenities);
            $property->propertyAmenity=$amty;
          
            if($request->hasfile('techspecs')) {

                    $file = $request->file('techspecs');
                    $extension = $file->getClientOriginalExtension(); 
                  
                    $filename = time() . '.' . $extension;
                    $destination="public/uploads/techspecs/".$filename;
                    $file->move('public/uploads/techspecs/', $filename);                    
                    $property->propertyTechSpecs =  $destination;
            } else {
                    return $request;
                    $property->propertyTechSpecs = '';
            }

             $property->costOfPremise = $request->costofpremise;
             $property->stampDutyReg = $request->stampdutyregistration;
             $property->acquisitionFee = $request->acquisitionfee;
             $property->otherCharges = $request->othercharges;
             $property->legalFees=$request->legalfees;
             $property->maintenanceReserve=$request->maintenancereserve;
             $property->adminExpenses=$request->adminexpenses;
             $property->monthlyOpex=$request->monthlyopex;
             $property->monthlyPropertyTax=$request->monthlypropertytax;
             $property->regulatoryCharges=$request->monthlyregulatorycharges;
             $property->totalAcquisitionCost=$request->totalacquisitioncost;
             $property->carpetArea=$request->carpetarea;
             $property->builtUpArea=$request->builtuparea;
             $property->monthlyRent=$request->monthlyrent;
             $property->leasedPeriod=$request->leasedperiod;
             $property->securityDeposit=$request->securitydeposit;
             $property->escalation=$request->escalation;
             $property->tenants=$request->tenants;
             $property->fittedOut=$request->fittedout;
             $property->monthlyMaintenance=$request->monthlymaintenance;
             $property->leaseChart=$request->leasechart;
             $property->grossYield=$request->grossyield;
             $property->expectedIr=$request->expectedirr;
             $property->tokenLockinPeriod=$request->tokenlockinperiod;
             $property->aurumFacilationFees=$request->aurumfacilationfees;
             $property->performanceFees=$request->performancefees;
             if($request->hasfile('investmentdeck')) {

                    $investfile = $request->file('investmentdeck');
                    $investextension = $investfile->getClientOriginalExtension(); 
                  
                    $investfilename = time() . '.' . $investextension;
                    $investdestination="public/uploads/investmentdoc/".$investfilename;
                    $investfile->move('public/uploads/investmentdoc/', $investfilename);                    
                    $property->propertyInvestmentDeck =  $investdestination;
            } else {
                    return $request;
                    $property->propertyInvestmentDeck = '';
            }

            if($request->hasfile('titlereport')) {

                    $titlefile = $request->file('titlereport');
                    $titleextension = $titlefile->getClientOriginalExtension(); 
                  
                    $titlefilename = time() . '.' . $titleextension;
                    $titledestination="public/uploads/titlereport/".$titlefilename;
                    $titlefile->move('public/uploads/titlereport/', $titlefilename);                    
                    $property->propertytitleReport=$titledestination;
            } else {
                    return $request;
                    $property->propertytitleReport = '';
            }

             if($request->hasfile('propertyduediligence')) {

                    $duediligencefile = $request->file('propertyduediligence');
                    $duediligenceextension = $duediligencefile->getClientOriginalExtension();
                    $duediligencefilename = time() . '.' . $duediligenceextension;
                    $duediligencedestination="public/uploads/propertyduediligence/".$duediligencefilename;
                    $duediligencefile->move('public/uploads/propertyduediligence/', $duediligencefilename);                    
                    $property->propertyDueDiligence=$duediligencedestination;
            } else {
                    return $request;
                    $property->propertyDueDiligence = '';
            }

            if($request->hasfile('micromarketreport')) {

                    $micromarketfile = $request->file('micromarketreport');
                    $micromarketextension = $micromarketfile->getClientOriginalExtension();
                    $micromarketfilename = time() . '.' . $micromarketextension;
                    $micromarketdestination="public/uploads/micromarketreport/".$micromarketfilename;
                    $micromarketfile->move('public/uploads/micromarketreport/', $micromarketfilename);                    
                    $property->propertyMicroMarket=$micromarketdestination;
            } else {
                    return $request;
                    $property->propertyMicroMarket = '';
            }

             if($request->hasfile('propertymanagementagreement')) {

             $managementagreementfile = $request->file('propertymanagementagreement');
            $managementagreementextension = $micromarketfile->getClientOriginalExtension();
             $managementagreementfilename = time() . '.' . $managementagreementextension;
     $managementagreementdestination="public/uploads/propertymanagementagreement/".$managementagreementfilename;
     $managementagreementfile->move('public/uploads/propertymanagementagreement/', $managementagreementfilename);                    
                    $property->propertyMgmtAgrmt=$managementagreementdestination;
            } else {
                    return $request;
                    $property->propertyMgmtAgrmt = '';
            }
            if($request->hasfile('valuationreport')) {

             $valuationreportfile = $request->file('valuationreport');
            $valuationreportextension = $micromarketfile->getClientOriginalExtension();
             $valuationreportfilename = time() . '.' . $valuationreportextension;
             $valuationreportdestination="public/uploads/valuationreport/".$valuationreportfilename;
             $valuationreportfile->move('public/uploads/valuationreport/', $valuationreportfilename);                    
                    $property->propertyValReport=$valuationreportdestination;
            } else {
                    return $request;
                    $property->propertyValReport = '';
            }


            if($request->hasFile('gallery'))
            {
                foreach($request->file('gallery') as $image)
                {
                    $PropImgdestinationPath = 'uploads/propertyimages/';
                    $propfilename = $image->getClientOriginalName();
                    $image->move($PropImgdestinationPath, $propfilename);
                    $ty[]=$PropImgdestinationPath.$propfilename;

                }
               $rt=implode("#",$ty);
              
            }
            $property->galleryImage = $rt;
            $property->status ='pending';
             $property->tokenStatus ='pending';
             $property->save();

            /**/
            //
            
           // $property_tokens = PropertyToken::create($datas);
           

            return back()->with('flash_success',
                                'Property created successfully');
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to Create Property');
        }
    }

    public function edit($id)
    {
        try {
          /*  $property  = Property::with('propertyTeams')->where('id', $id)
                                 ->with('propertyImages', 'propertyDocuments',
                                        'propertyInformation','propertyDetails')->first();*/

            $property  =Property::where('id', $id)->get();
            $assetType = AssetType::orderBy('type', 'asc')->get();
            $amenity = Amenity::where('status', '1')->orderBy('amenity_name', 'asc')->get();
           // $users      = User::orderBy('name', 'asc')->where('user_type', 2)->get();
            // if ($property->token_type == 2) {
            //     return view('admin.property.edit_asset_fund',
            //                 compact('property', 'assetType'));
            // } else {
                return view('admin.property.edit',
                            compact('property', 'assetType','amenity'));
            // }

        } catch (Exception $e) {
            dd($e);
            return back()->with('flash_error',
                                trans('api.something_went_wrong'));
        }
    }
public function createhelp($id)
    {
        $helpcenter = ProfileHelp::where('help_id',$id)->get();
        return view('admin.helpcenter.create',compact('helpcenter'));
    }
    /**
     * Used to delete Property
     */
    public function destroy($id)
    {
        try {
            if (Auth::guard('admin')->user()->role == 1) {
                return view('errors.404');
            }

            Property::find($id)->delete();

            return back()->with('flash_success',
                                'Property deleted successfully');
        } catch (Exception $e) {
            return back()->with('flash_error',
                                trans('admin.property.not_found'));
        }
    }

    public function documentdelete(Request $request){
        try{
            if (Auth::guard('admin')->user()->role == 1) {
                return view('errors.404');
            }

            $property=Property::find($request->id);
            $updatedata= [];
            $updatedata[$request->columnname] = null; 
            if($property){
                $updatedata[$request->columnname] = null; 
                $property->update($updatedata);
                return 1;
            }
        }
        catch(Exception $e){
            return 0;
        }
    }


    public function update(Request $request, $id)
    {


    try {
        
            $user = Auth::user();
            $datas = $request->all();
  

             if($request->hasfile('tokenlogo')) {

                    $tokenfile = $request->file('tokenlogo');
                    $tokenextension = $tokenfile->getClientOriginalExtension(); 
                  
                    $tokenfilename = time() . '.' . $tokenextension;
                    $tokendestination="public/uploads/tokenlogo/".$tokenfilename;
                    $tokenfile->move('public/uploads/tokenlogo/', $tokenfilename);                    
                    $datas['tokenLogo'] =  $tokendestination;
            } else {
                    
                    $datas['tokenLogo'] = $request->tokenlogoold;
            }


          if($request->hasfile('techspecs')) {

                    $file = $request->file('techspecs');
                    $extension = $file->getClientOriginalExtension(); 
                  
                    $filename = time() . '.' . $extension;
                    $destination="public/uploads/techspecs/".$filename;
                    $file->move('public/uploads/techspecs/', $filename);                    
                    $datas['propertyTechSpecs'] =  $destination;
            } else {
                   // return $request;
                    $datas['propertyTechSpecs'] = $request->techspecsold;
            }
             
    if($request->hasfile('investmentdeck')) {

                    $investfile = $request->file('investmentdeck');
                    $investextension = $investfile->getClientOriginalExtension(); 
                  
                    $investfilename = time() . '.' . $investextension;
                    $investdestination="public/uploads/investmentdoc/".$investfilename;
                    $investfile->move('public/uploads/investmentdoc/', $investfilename);                    
                    $datas['propertyInvestmentDeck'] =  $investdestination;
            } else {
                   // return $request;
                    $datas['propertyInvestmentDeck'] = $request->investmentdeckold;
            }

                if($request->hasfile('titlereport')) {

                    $titlefile = $request->file('titlereport');
                    $titleextension = $titlefile->getClientOriginalExtension(); 
                  
                    $titlefilename = time() . '.' . $titleextension;
                    $titledestination="public/uploads/titlereport/".$titlefilename;
                    $titlefile->move('public/uploads/titlereport/', $titlefilename);                    
                    $datas['propertytitleReport']=$titledestination;
            } else {
                    //return $request;
                    $datas['propertytitleReport'] = $request->titlereportold;
            }
if($request->hasfile('propertyduediligence')) {

                    $duediligencefile = $request->file('propertyduediligence');
                    $duediligenceextension = $duediligencefile->getClientOriginalExtension();
                    $duediligencefilename = time() . '.' . $duediligenceextension;
                    $duediligencedestination="public/uploads/propertyduediligence/".$duediligencefilename;
                    $duediligencefile->move('public/uploads/propertyduediligence/', $duediligencefilename);                    
                    $datas['propertyDueDiligence']=$duediligencedestination;
            } else {
                   // return $request;
                    $datas['propertyDueDiligence'] = $request->propertyduediligenceold;
            }

           if($request->hasfile('micromarketreport')) {

                    $micromarketfile = $request->file('micromarketreport');
                    $micromarketextension = $micromarketfile->getClientOriginalExtension();
                    $micromarketfilename = time() . '.' . $micromarketextension;
                    $micromarketdestination="public/uploads/micromarketreport/".$micromarketfilename;
                    $micromarketfile->move('public/uploads/micromarketreport/', $micromarketfilename);                    
                    $datas['propertyMicroMarket']=$micromarketdestination;
            } else {
                   // return $request;
                    $datas['propertyMicroMarket'] = $request->micromarketreportold;
            }
             if($request->hasfile('propertymanagementagreement')) {

             $managementagreementfile = $request->file('propertymanagementagreement');
            $managementagreementextension = $micromarketfile->getClientOriginalExtension();
             $managementagreementfilename = time() . '.' . $managementagreementextension;
     $managementagreementdestination="public/uploads/propertymanagementagreement/".$managementagreementfilename;
     $managementagreementfile->move('public/uploads/propertymanagementagreement/', $managementagreementfilename);                    
                    $datas['propertyMgmtAgrmt']=$managementagreementdestination;
            } else {
                    //return $request;
                    $datas['propertyMgmtAgrmt'] = $request->propertymanagementagreementold;
            }


             if($request->hasfile('valuationreport')) {

             $valuationreportfile = $request->file('valuationreport');
            $valuationreportextension = $micromarketfile->getClientOriginalExtension();
             $valuationreportfilename = time() . '.' . $valuationreportextension;
             $valuationreportdestination="public/uploads/valuationreport/".$valuationreportfilename;
             $valuationreportfile->move('public/uploads/valuationreport/', $valuationreportfilename);                    
                    $datas['propertyValReport']=$valuationreportdestination;
            } else {
                   // return $request;
                    $datas['propertyValReport'] = $request->valuationreportold;
            }
            
          if($request->hasFile('gallery'))
            {
                foreach($request->file('gallery') as $image)
                {
                    $PropImgdestinationPath = 'uploads/propertyimages/';
                    $propfilename = $image->getClientOriginalName();
                    $image->move($PropImgdestinationPath, $propfilename);
                    $ty[]=$PropImgdestinationPath.$propfilename;

                }
               $rt=implode("#",$ty);
              $datas['galleryImage']=$rt;
            }else{
                 $datas['galleryImage'] = $request->galleryimageold;
            }
                $datas['amenity']=implode("#",$request->amenities);
                 $property = Property::where('id',$id)->update([
                'propertyName' => $datas['propertyname'],  
                'propertyState' => $datas['state'],  
                'propertyCity' => $datas['city'],  
                'propertyDistrict' => $datas['propertydistrict'],  
                'propertyType' => $datas['propertytype'],  
                'tokenName' => $datas['tokenname'],  
                'tokenSymbol' => $datas['tokensymbol'],  
                'tokenValue' => $datas['tokenvalue'],  
                'tokenSupply' => $datas['tokensupply'],  
                'tokenSupply' => $datas['tokendecimal'],  
                'tokenDecimal' => $datas['tokensupply'],  
               'tokenLogo' => $datas['tokenLogo'],  
                'Divident' => $datas['divident'],  
                'minimumInvest' => $datas['minimuminvestment'],  
                'propertySize' => $datas['propertysize'],  
                'targetIrr' => $datas['targetirr'],  
                'averageYield' => $datas['averageyield'],
               'propertyDesc' => $datas['propdesc'],  
                'Proponent' => $datas['proponent'],  
                'Onmap' => $datas['onmap'],  
              'Address' => $datas['address'],  
               'propertyStatus' => $datas['propertystatus'],  
                'buildYear'=>$datas['buildyear'], 
            'reraNumber'=>$datas['reranumber'], 
            'propertyManager'=>$datas['propertymanager'], 
            'Custodian'=>$datas['custodian'], 
                'propertyAmenity'=>$datas['amenity'],
            'propertyTechSpecs'=>$datas['propertyTechSpecs'],
             'costOfPremise'=>$datas['costofpremise'],
              'stampDutyReg'=>$datas['stampdutyregistration'],
              'acquisitionFee'=>$datas['acquisitionfee'],
              'otherCharges'=>$datas['othercharges'],
              'legalFees'=>$datas['legalfees'],
              'maintenanceReserve'=>$datas['maintenancereserve'],
              'adminExpenses'=>$datas['adminexpenses'],
              'monthlyOpex'=>$datas['monthlyopex'],
              'monthlyPropertyTax'=>$datas['monthlypropertytax'],
              'regulatoryCharges'=>$datas['monthlyregulatorycharges'],
              'totalAcquisitionCost'=>$datas['totalacquisitioncost'],
              'carpetArea'=>$datas['carpetarea'],
              'builtUpArea'=>$datas['builtuparea'],
              'monthlyRent'=>$datas['monthlyrent'],
              'leasedPeriod'=>$datas['leasedperiod'],
              'securityDeposit'=>$datas['securitydeposit'],
              'escalation'=>$datas['escalation'],
              'tenants'=>$datas['tenants'],
              'fittedOut'=>$datas['fittedout'],
              'monthlyMaintenance'=>$datas['monthlymaintenance'],
              'leaseChart'=>$datas['leasechart'],
              'grossYield'=>$datas['grossyield'],
              'expectedIr'=>$datas['expectedirr'],
              'tokenLockinPeriod'=>$datas['tokenlockinperiod'],
              'aurumFacilationFees'=>$datas['aurumfacilationfees'],
              'performanceFees'=>$datas['performancefees'],
                 'propertyInvestmentDeck'=> $datas['propertyInvestmentDeck'],
             'propertytitleReport'=> $datas['propertytitleReport'],
              'propertyDueDiligence'=> $datas['propertyDueDiligence'],
              'propertyMicroMarket'=> $datas['propertyMicroMarket'],
              'propertyMgmtAgrmt'=> $datas['propertyMgmtAgrmt'],
              'propertyValReport'=> $datas['propertyValReport'],
             'galleryImage'=>$datas['galleryImage'],         
            ]);
          
            return back()->with('flash_success', 'Property updated successfully');

        } catch (Exception $e) {
          
            return back()->with('flash_error',              trans('api.something_went_wrong'));
        } 
    }

    /**
     * Used to show Asset Types
     */
    public function ShowAssetType()
    {
        try {
            $assets = AssetType::orderBy('type', 'asc')->get();
            return view('admin.assettype.index', compact('assets'));
        } catch (Exception $e) {
            return back()->with('flash_error',
                                trans('api.something_went_wrong'));
        }
    }

    /**
     * Used to show Create Asset Types
     */
    public function CreateAssetType()
    {
        return view('admin.assettype.create');
    }

    /**
     * Used to store Asset Types
     */
    public function storeAssetType(Request $request)
    {
        $this->validate($request, [
            'propertytype' => 'required|max:255',
        ]);
        try {
            $model       = new AssetType;
            $model->type = $request->propertytype;
            $model->save();

            return redirect('/admin/property/show/assetType')->with('flash_success',
                                                                    'Asset type created successfully');
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to add asset type');
        }
    }

    /**
     * Used to show Edit Asset Types
     */
    public function showEdit($id)
    {
        try {
            $asset = AssetType::find($id);
            return view('admin.assettype.edit', compact('asset'));
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to get asset type');
        }
    }

    /**
     * Used to update Asset Type
     */
    public function updateAssetType(Request $request, $id)
    {
        try {
            $model       = AssetType::find($id);
            $model->type = $request->propertytype;
            $model->save();

            return redirect('/admin/property/show/assetType')->with('flash_success',
                                                                    'Asset type updated successfully');
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to update asset type');
        }
    }

    /**
     * Used to delete Asset Type
     */
    public function deleteAssetType($id)
    {
        try {
            AssetType::find($id)->delete();
            return back()->with('flash_success',
                                'Asset types deleted successfully');
        } catch (\Throwable $th) {
            return back()->with('flash_error', 'Unable to delete asset type');
        }
    }

    /**
     * Used to Create or Update Product
     */
    public function productcrud($property, $request)
    {
        if ($request->has('dividend')) {
            $property->dividend = $request->dividend;
        }

        $property->user_id                   = Auth::user()->id;
        $property->propertyName              = $request->propertyName;
        $property->expectedIrr               = $request->expectedIrr;
        $property->initialInvestment         = $request->initialInvestment;
        $property->fundedMembers             = $request->fundedMembers;
        $property->propertyEquityMultiple    = $request->propertyEquityMultiple;
        $property->holdingPeriod             = $request->holdingPeriod;
        $property->propertyOverview          = $request->propertyOverview;
        $property->propertyHighlights        = $request->propertyHighlights;
        $property->token_type                = $request->token_type;
        $property->ManagementTeamDescription = $request->ManagementTeamDescription;

        if ($request->token_type == 1) {
            $property->propertyLocation = $request->propertyLocation;
            $property->propertyType     = $request->propertyType;
            if ($request->has('token_value')) {
                $property->totalDealSize = $request->total_sft
                                           / $request->token_value;  //$request->totalDealSize;
            }
            $property->total_sft          = $request->total_sft;
            $property->propertyLocationOverview
                                          = $request->propertyLocationOverview;
            $property->locality           = $request->locality;
            $property->yearOfConstruction = $request->yearOfConstruction;
            $property->storeys            = $request->storeys;
            $property->propertyParking    = $request->propertyParking;
            $property->floorforSale       = $request->floorforSale;
            $property->propertyParkingRatio
                                          = $request->propertyParkingRatio;
            $property->propertyTotalBuildingArea
                                          = $request->propertyTotalBuildingArea;
            $property->propertyDetailsHighlights
                                          = $request->propertyDetailsHighlights;
        } else {
            $property->totalDealSize = $request->totalDealSize;
        }

        if ($request->hasFile('propertyVideo')){
            $property->propertyVideo = $request->propertyVideo->store('propertyVideo');
        }
        if ($request->hasFile('propertyLogo')) {
            $property->propertyLogo
                = $request->propertyLogo->store('propertyLogo');
        }

        if ($request->hasFile('floorplan')) {
            $property->floorplan = $request->floorplan->store('floorplan');
        }

        if ($request->hasFile('investor')) {
            $property->investor = $request->investor->store('investor');
        }

        if ($request->hasFile('titlereport')) {
            $property->titlereport
                = $request->titlereport->store('titlereport');
        }

        if ($request->hasFile('termsheet')) {
            $property->termsheet = $request->termsheet->store('termsheet');
        }

        if ($request->hasFile('propertyManagementTeam')) {
            $property->propertyManagementTeam = $request->propertyManagementTeam->store('propertyManagementTeam');
        }

        if ($request->hasFile('propertyUpdatesDoc')) {
            $property->propertyUpdatesDoc = $request->propertyUpdatesDoc->store('propertyUpdatesDoc');
        }

        $property->save();

        // if(!empty($request['landmarks'])){
        // 	PropertyLandmark::where('property_id', $property->id)->delete();
        // 	foreach ($request['landmarks'] as $key => $value) {
        // 		$property_landmark=new PropertyLandmark;
        // 		$property_landmark->property_id=$property->id;
        // 		$property_landmark->landmarkName=$value['landmarkName'];
        // 		$property_landmark->landmarkDist=$value['landmarkDist'];
        // 		$property_landmark->save();
        // 	}
        // }

        if (!empty($request['comparables'])) {
            //PropertyComparable::where('property_id', $property->id)->delete();

            $comparablesIds = [];
            foreach ($request->comparables as $key => $value) {
                if (isset($value['cid'])) {
                    $comparablesIds[] = $value['cid'];
                }
            }

            if (count($comparablesIds) > 0) {
                PropertyComparable::whereNotIn('id', $comparablesIds)->where('id', $property->id)->delete();
            }

            foreach ($request->comparables as $key => $value) {
                $property_comparables              = new PropertyComparable;
                if (isset($value['cid'])) {
                    $property_comparables = PropertyComparable::where('id', $value['cid'])->first();
                }

                $property_comparables->property_id = $property->id;
                $property_comparables->property    = $value['property'];
                $property_comparables->type        = $value['type'];
                $property_comparables->location    = $value['location'];
                $property_comparables->distance    = $value['distance'];
                $property_comparables->rent        = $value['rent'];
                $property_comparables->saleprice   = $value['saleprice'];

                if ($request->hasFile('map')) {
                    $property_comparables->map = $request->map->store('map');
                }
                if ($request->hasFile('comparabledetails')) {
                    $property_comparables->comparable_details = $request->comparabledetails->store('comparabledetails');
                }

                $property_comparables->save();
            }
        }

        if (!empty($request->member)) {

            $memberIds = [];
            foreach ($request->member as $key => $value) {
                if (isset($value['mid'])) {
                    $memberIds[] = $value['mid'];
                }
            }

            if (count($memberIds) > 0) {
                ManagementMembers::whereNotIn('id', $memberIds)->where('property_id', $property->id)->delete();
            }

            foreach ($request->member as $key => $value) {
                $member = new ManagementMembers;
                if (isset($value['mid'])) {
                    $member = ManagementMembers::where('id', $value['mid'])->first();
                }
                $member->property_id       = $property->id;
                $member->memberName        = $value['name'];
                $member->memberPosition    = $value['position'];
                $member->memberDescription = (isset($value['description']) ? $value['description'] : null);
                if (!empty($value['pic'])) {
                    $member->memberPic = $value['pic']->store('memberPic');
                }
                $member->save();
            }
        }

        if ($request->hasFile('propertyimages')) {
            $files = $request->file('propertyimages');
            foreach ($files as $key => $file) {
                $productImage              = new PropertyImage;
                $productImage->property_id = $property->id;
                $productImage->image       = $file->store('propertyImage');
                $productImage->save();
            }
        }

        return $property;
    }

    /**
     * Used to Add Feature
     */
    public function propertyFeature($id)
    {
        try {
            $property = Property::find($id);
            if ($property->feature == 0) {
                $property->feature = 1;
            } else {
                $property->feature = 0;
            }
            $property->save();

            return response()->json(['status' => 'success']);
        } catch (\Throwable $th) {
            return response()->json(['status' => 'failed']);
        }
    }
     public function propertyTrusted($id)
    {
        try {
            $property = Property::find($id);
            if ($property->trusted == 0 || $property->trusted == '') {
                $property->trusted = 1;
            } else {
                $property->trusted = 0;
            }
            $property->save();

            return response()->json(['status' => 'success']);
        } catch (\Throwable $th) {
            return response()->json(['status' => 'failed']);
        }
    }
    public function propertyEthereumAddress($id)
    {
        try {
            $user = User::where('id',$id)->first();
            $useremail=$user->email;          
                 
           $client  = new Client();
            $headers     = ['Content-Type' => 'application/json',];
            $url     = 'http://13.126.245.106:3000/createAccount';
             $body = [
                    'password' => $useremail,                   
                ];

             $res = $client->post($url, [
                    'headers' => $headers,
                    'body' => json_encode($body),
                ]);

            $res = json_decode($res->getBody(), true);                
           $user=User::where('id',$id)->update(['eth_address' => $res['address']]);
            $prop=EthPrivatekey::create($res);

            $prop->user_email=$useremail;
            $prop->privatekey=json_encode($res['privateKey']);
            $prop->save();
            return response()->json(['status' => 'success']);
        } catch (\Throwable $th) {
            return response()->json(['status' => 'failed']);
        }
    }
    
     public function propertyGrowth($id)
    {
        try {
            $property = Property::find($id);
            if ($property->highgrowth == 0 || $property->highgrowth == '') {
                $property->highgrowth = 1;
            } else {
                $property->highgrowth = 0;
            }
            $property->save();

            return response()->json(['status' => 'success']);
        } catch (\Throwable $th) {
            return response()->json(['status' => 'failed']);
        }
    }

  public function propertyOffice($id)
    {
        try {
            $property = Property::find($id);
            if ($property->office == 0 || $property->office == '') {
                $property->office = 1;
            } else {
                $property->office = 0;
            }
            $property->save();

            return response()->json(['status' => 'success']);
        } catch (\Throwable $th) {
            return response()->json(['status' => 'failed']);
        }
    }
    public function propertyGradea($id)
    {
        try {
            $property = Property::find($id);
            if ($property->gradea == 0 || $property->gradea == '') {
                $property->gradea = 1;
            } else {
                $property->gradea = 0;
            }
            $property->save();

            return response()->json(['status' => 'success']);
        } catch (\Throwable $th) {
            return response()->json(['status' => 'failed']);
        }
    }

    /**
     * Used to Update Property Status
     */
    public function propertyStatus(Request $request, $id)
    {
        try {
            $property         = Property::find($id);
            $property->status = $request->status;
            $property->save();

            return response()->json(['status' => 'success']);
        } catch (\Throwable $th) {
            return response()->json(['status' => 'failed']);
        }
    }
}
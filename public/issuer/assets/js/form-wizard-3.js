$((function() {
    "use strict";
    $("#wizard1").steps({
        headerTag: "h3",
        bodyTag: "section",
        autoFocus: true,
        titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>'
    });
var form = $(".form-horizontal").show();
	$("#wizard2").steps({
        headerTag: "h3",
        bodyTag: "section",
        autoFocus: true,
        titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
        onStepChanging: function(a, e, s) {
			if (e < s) {
				if(e===0){
                    var valid=true;					
					if(document.getElementById('propertyclient').value==''){
						document.getElementById('propertyclient-form').innerHTML='This value is required';		
                        valid=false;						
					}	
				    if(document.getElementById('propertyname').value==''){
						document.getElementById('propertyname-form').innerHTML='This value is required';		
                        valid=false;						
					}						
					if(document.getElementById('stateId').value==''){
						document.getElementById('stateId-form').innerHTML='This value is required';		
                         valid=false;							
					}
					if(document.getElementById('cityId').value==''){					
						document.getElementById('cityId-form').innerHTML='This value is required';		
                       valid=false;								
					}
					if(document.getElementById('propertydistrict').value==''){					
						document.getElementById('propertydistrict-form').innerHTML='This value is required';
                        valid=false;								
					}
					if(document.getElementById('propertytype').value==''){						
						document.getElementById('propertytype-form').innerHTML='This value is required';	
						 valid=false;		
					}
					if(document.getElementById('propertydistrict').value==''){						
						document.getElementById('propertydistrict-form').innerHTML='This value is required';	
							 valid=false;		
					}					
					
						if(document.getElementById('tokenname').value==''){						
							document.getElementById('tokenname-form').innerHTML='This value is required';		
								 valid=false;		
						}
					
					if(document.getElementById('tokensymbol').value==''){						
						document.getElementById('tokensymbol-form').innerHTML='This value is required';		
							 valid=false;		
					}
					if(document.getElementById('tokenvalue').value==''){						
						document.getElementById('tokenvalue-form').innerHTML='This value is required';		
							 valid=false;		
					}
					if(document.getElementById('tokensupply').value==''){						
						document.getElementById('tokensupply-form').innerHTML='This value is required';		
							 valid=false;		
					}
					if(document.getElementById('tokendecimal').value==''){						
						document.getElementById('tokendecimal-form').innerHTML='This value is required';		
							 valid=false;		
					}
					if(document.getElementById('tokenlogoold').value==""){
						if(document.getElementById('tokenlogo').value==''){						
							document.getElementById('tokenlogo-form').innerHTML='This value is required';		
								 valid=false;		
						}
					}
					
					if(document.getElementById('divident').value==''){						
						document.getElementById('divident-form').innerHTML='This value is required';		
							 valid=false;		
					}
					if(document.getElementById('minimuminvestment').value==''){						
						document.getElementById('minimuminvestment-form').innerHTML='This value is required';		
							 valid=false;		
					}
					
					if(document.getElementById('propertysize').value==''){						
						document.getElementById('propertysize-form').innerHTML='This value is required';	
							 valid=false;		
					}
					if(document.getElementById('targetirr').value==''){						
						document.getElementById('targetirr-form').innerHTML='This value is required';		
							 valid=false;		
					}
					if(document.getElementById('averageyield').value==''){						
						document.getElementById('averageyield-form').innerHTML='This value is required';	
							 valid=false;		
					}
					if(document.getElementById('propdesc').value==''){						
						document.getElementById('propdesc-form').innerHTML='This value is required';	
							 valid=false;		
					}
					return valid;
				}else if(e===1){
					 var valid2=true;		
	if(document.getElementById('proponent').value==''){						
						document.getElementById('proponent-form').innerHTML='This value is required';	
							 valid2=false;		
					}
					if(document.getElementById('onmap').value==''){						
						document.getElementById('onmap-form').innerHTML='This value is required';	
							 valid2=false;		
					}
					
					if(document.getElementById('address').value==''){						
						document.getElementById('address-form').innerHTML='This value is required';	
							 valid2=false;		
					}
					if(document.getElementById('propertystatus').value==''){						
						document.getElementById('propertystatus-form').innerHTML='This value is required';	
							 valid2=false;		
					}
					if(document.getElementById('buildyear').value==''){						
						document.getElementById('buildyear-form').innerHTML='This value is required';	
							 valid2=false;		
					}
					if(document.getElementById('reranumber').value==''){						
						document.getElementById('reranumber-form').innerHTML='This value is required';	
							 valid2=false;		
					}
					if(document.getElementById('propertymanager').value==''){						
						document.getElementById('propertymanager-form').innerHTML='This value is required';	
							 valid2=false;		
					}
					if(document.getElementById('custodian').value==''){						
						document.getElementById('custodian-form').innerHTML='This value is required';	
							 valid2=false;		
					}
					return valid2;
				}
				else if(e===2){
					var valid3=true;
					var chks = document.getElementsByName('amenities[]');
					  var i, count=0;
					  for (i = 0;i<chks.length;i++){						 
						if (chks[i].checked){
							count+=1;
						}
					  }
					
					  if(count>=1){
						   valid3=true;
					  }else{
						  	document.getElementById('amenities-form').innerHTML='Please must select atleast one amenity';	
							alert('Please must select atleast one amenity');
							valid3=false;
					  }
					   if(document.getElementById('techspecsold').value==''){
					  if(document.getElementById('techspecs').value==''){
						  document.getElementById('techspecs-form').innerHTML='Please upload property technical specification';	
						valid3=false;
					  }
					   }
					  
					  if(document.getElementById('techspecs').value!=''){
							var myFile="";							
							  myFile = $("#techspecs").val();							 
							  var upld = myFile.split('.').pop();							 
							  if(upld=='pdf'){
								valid3=true;
							  }else{
								alert("Only PDF are allowed");
								document.getElementById('techspecs-form').innerHTML="Only PDF Format are allowed";
								valid3=false;
							  }
							 
						
					  }
					
				
					return valid3;
				}
				else if(e===3){
					var valid4=true;
					if(document.getElementById('costofpremise').value==''){						
						document.getElementById('costofpremise-form').innerHTML='This value is required';	
							 valid4=false;		
					}
					if(document.getElementById('stampdutyregistration').value==''){						
						document.getElementById('stampdutyregistration-form').innerHTML='This value is required';	
							 valid4=false;		
					}
					if(document.getElementById('acquisitionfee').value==''){						
						document.getElementById('acquisitionfee-form').innerHTML='This value is required';	
							 valid4=false;		
					}
					if(document.getElementById('othercharges').value==''){						
						document.getElementById('othercharges-form').innerHTML='This value is required';	
							 valid4=false;		
					}
					if(document.getElementById('legalfees').value==''){						
						document.getElementById('legalfees-form').innerHTML='This value is required';	
							 valid4=false;		
					}
					if(document.getElementById('maintenancereserve').value==''){						
						document.getElementById('maintenancereserve-form').innerHTML='This value is required';	
							 valid4=false;		
					}
					if(document.getElementById('adminexpenses').value==''){						
						document.getElementById('adminexpenses-form').innerHTML='This value is required';	
							 valid4=false;		
					}
					if(document.getElementById('monthlyopex').value==''){						
						document.getElementById('monthlyopex-form').innerHTML='This value is required';	
							 valid4=false;		
					}
					if(document.getElementById('monthlypropertytax').value==''){						
						document.getElementById('monthlypropertytax-form').innerHTML='This value is required';	
							 valid4=false;		
					}
					if(document.getElementById('monthlyregulatorycharges').value==''){						
						document.getElementById('monthlyregulatorycharges-form').innerHTML='This value is required';	
							 valid4=false;		
					}
					if(document.getElementById('totalacquisitioncost').value==''){						
						document.getElementById('totalacquisitioncost-form').innerHTML='This value is required';	
							 valid4=false;		
					}
					
					
					return valid4;
				}
				else if(e===4){
					var valid5=true;
					if(document.getElementById('carpetarea').value==''){						
						document.getElementById('carpetarea-form').innerHTML='This value is required';	
							 valid5=false;		
					}
					if(document.getElementById('builtuparea').value==''){						
						document.getElementById('builtuparea-form').innerHTML='This value is required';	
							 valid5=false;		
					}
					if(document.getElementById('monthlyrent').value==''){						
						document.getElementById('monthlyrent-form').innerHTML='This value is required';	
							 valid5=false;		
					}
					if(document.getElementById('leasedperiod').value==''){						
						document.getElementById('leasedperiod-form').innerHTML='This value is required';	
							 valid5=false;		
					}
					if(document.getElementById('securitydeposit').value==''){						
						document.getElementById('securitydeposit-form').innerHTML='This value is required';	
							 valid5=false;		
					}
					if(document.getElementById('escalation').value==''){						
						document.getElementById('escalation-form').innerHTML='This value is required';	
							 valid5=false;		
					}
					if(document.getElementById('tenants').value==''){						
						document.getElementById('tenants-form').innerHTML='This value is required';	
							 valid5=false;		
					}
					if(document.getElementById('fittedout').value==''){						
						document.getElementById('fittedout-form').innerHTML='This value is required';	
							 valid5=false;		
					}
					if(document.getElementById('monthlymaintenance').value==''){						
						document.getElementById('monthlymaintenance-form').innerHTML='This value is required';	
							 valid5=false;		
					}
					if(document.getElementById('leasechart').value==''){						
						document.getElementById('leasechart-form').innerHTML='This value is required';	
							 valid5=false;		
					}
					
					return valid5;
				}
				else if(e===5){
					var valid6=true;
					if(document.getElementById('grossyield').value==''){						
						document.getElementById('grossyield-form').innerHTML='This value is required';	
							 valid6=false;		
					}
					if(document.getElementById('expectedirr').value==''){						
						document.getElementById('expectedirr-form').innerHTML='This value is required';	
							 valid6=false;		
					}
					if(document.getElementById('tokenlockinperiod').value==''){						
						document.getElementById('tokenlockinperiod-form').innerHTML='This value is required';	
							 valid6=false;		
					}
					if(document.getElementById('aurumfacilationfees').value==''){						
						document.getElementById('aurumfacilationfees-form').innerHTML='This value is required';	
							 valid6=false;		
					}
					if(document.getElementById('performancefees').value==''){						
						document.getElementById('performancefees-form').innerHTML='This value is required';	
							 valid6=false;		
					}
					  return valid6;
				}
				else if(e===6){
					var valid7=true;
					if(document.getElementById('investmentdeckold').value==''){
						if(document.getElementById('investmentdeck').value==''){
							  document.getElementById('investmentdeck-form').innerHTML='Please upload property technical specification';	
							valid7=false;
						  }					  
						  if(document.getElementById('investmentdeck').value!=''){
								var myFile="";							
								  myFile = $("#investmentdeck").val();							 
								  var upld = myFile.split('.').pop();							 
								  if(upld=='pdf'){
									valid7=true;
								  }else{
									alert("Only PDF are allowed");
									document.getElementById('investmentdeck-form').innerHTML="Only PDF Format are allowed";
									valid7=false;
								  }
						  }
					}
					if(document.getElementById('titlereportold').value==''){
					  if(document.getElementById('titlereport').value==''){
						  document.getElementById('titlereport-form').innerHTML='Please upload property technical specification';	
						  valid7=false;
					  }			
					  
					  if(document.getElementById('titlereport').value!=''){
							var myFile2="";							
							  myFile2 = $("#titlereport").val();							 
							  var upld = myFile2.split('.').pop();							 
							  if(upld=='pdf'){
								valid7=true;
							  }else{
								alert("Only PDF are allowed");
								document.getElementById('titlereport-form').innerHTML="Only PDF Format are allowed";
								valid7=false;
							  }
					  }
					}
					 if(document.getElementById('propertyduediligenceold').value==''){
					  if(document.getElementById('propertyduediligence').value==''){
						  document.getElementById('propertyduediligence-form').innerHTML='Please upload property technical specification';	
						  valid7=false;
					  }					  
					  if(document.getElementById('propertyduediligence').value!=''){
							var myFile3="";							
							  myFile3 = $("#propertyduediligence").val();							 
							  var upld = myFile3.split('.').pop();							 
							  if(upld=='pdf'){
								valid7=true;
							  }else{
								alert("Only PDF are allowed");
								document.getElementById('propertyduediligence-form').innerHTML="Only PDF Format are allowed";
								valid7=false;
							  }
					  }
					 }
					  if(document.getElementById('micromarketreportold').value==''){
					  if(document.getElementById('micromarketreport').value==''){
						  document.getElementById('micromarketreport-form').innerHTML='Please upload property technical specification';	
						  valid7=false;
					  }					  
					  if(document.getElementById('micromarketreport').value!=''){
							var myFile4="";							
							  myFile4 = $("#micromarketreport").val();							 
							  var upld = myFile4.split('.').pop();							 
							  if(upld=='pdf'){
								valid7=true;
							  }else{
								alert("Only PDF are allowed");
								document.getElementById('micromarketreport-form').innerHTML="Only PDF Format are allowed";
								valid7=false;
							  }
					  }
					  }
					  if(document.getElementById('propertymanagementagreementold').value==''){
					  if(document.getElementById('propertymanagementagreement').value==''){
						  document.getElementById('propertymanagementagreement-form').innerHTML='Please upload property technical specification';	
						  valid7=false;
					  }					  
					  if(document.getElementById('propertymanagementagreement').value!=''){
							var myFile5="";							
							  myFile5 = $("#propertymanagementagreement").val();							 
							  var upld = myFile5.split('.').pop();							 
							  if(upld=='pdf'){
								valid7=true;
							  }else{
								alert("Only PDF are allowed");
								document.getElementById('propertymanagementagreement-form').innerHTML="Only PDF Format are allowed";
								valid7=false;
							  }
					  }
					  }
					   if(document.getElementById('valuationreportold').value==''){
						  if(document.getElementById('valuationreport').value==''){
							  document.getElementById('valuationreport-form').innerHTML='Please upload property technical specification';	
							  valid7=false;
						  }					  
						  if(document.getElementById('valuationreport').value!=''){
								var myFile6="";							
								  myFile6 = $("#valuationreport").val();							 
								  var upld = myFile6.split('.').pop();							 
								  if(upld=='pdf'){
									valid7=true;
								  }else{
									alert("Only PDF are allowed");
									document.getElementById('valuationreport-form').innerHTML="Only PDF Format are allowed";
									valid7=false;
								  }
						  }
					   }
					  return valid7;
				}else if(e===7){
					var valid8=true;				
					if(document.getElementById('propgallery').value==''){
						alert('please enter');
						  //document.getElementById('propgallery-form').innerHTML='Please upload property technical specification';	
						  valid7=false;
					  }	
					return valid8;	
				
				}				
			}
			else {					  
				return true;
			}

           
        },

 onFinishing: function (event, currentIndex)
    {
     // return form.validate().settings.ignore = ":disabled",
        form.submit();
    },
    onFinished: function (event, currentIndex)
    {
        alert("Submitted!");
    }
});
		
    $("#wizard3").steps({
        headerTag: "h3",
        bodyTag: "section",
        autoFocus:true,
        titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
        stepsOrientation: 1
    })
}));
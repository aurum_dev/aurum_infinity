$(function() {
'use strict'
$('#wizard1').steps({
headerTag: 'h3',
bodyTag: 'section',
autoFocus: true,
titleTemplate: '<span class="number">#index#<\/span> <span class="title">#title#<\/span>'
});
var form = $(".form-horizontal").show();
$('#wizard2').steps({
headerTag: 'h3',
bodyTag: 'section',
autoFocus: true,
titleTemplate: '<span class="number">#index#<\/span> <span class="title">#title#<\/span>',
labels: {
        finish: "Submit"
    },
onStepChanging: function(event, currentIndex, newIndex) {
if (currentIndex < newIndex) {
// Step 1 form validation
if (currentIndex === 0) {
var fname = $('#firstname').parsley();
var lname = $('#lastname').parsley();
if (fname.isValid() && lname.isValid()) {
return true;
} else {
fname.validate();
lname.validate();
}
}
// Step 2 form validation
if (currentIndex === 1) {
var email = $('#email').parsley();
var namec = $('#name11').parsley();
if (email.isValid() && namec.isValid()) {
return true;
} else {
email.validate();
namec.validate();
}
}
// Always allow step back to the previous step even if the current step is not valid.
} else {
  
return true;
}
},
onFinishing: function(e, t) {
       // return form.validate().settings.ignore = ":disabled",
        form.submit();
    },
onFinished: function(e, t) {
        alert("Submitted!")
    }
});
$('#wizard3').steps({
headerTag: 'h3',
bodyTag: 'section',
autoFocus: true,
titleTemplate: '<span class="number">#index#<\/span> <span class="title">#title#<\/span>',
stepsOrientation: 1
});
});



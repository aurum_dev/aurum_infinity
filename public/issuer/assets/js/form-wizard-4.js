$((function() {
    "use strict";
    $("#wizard1").steps({
        headerTag: "h3",
        bodyTag: "section",
        autoFocus: true,
        titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>'
    });
var form = $(".form-horizontal").show();
	$("#wizard2").steps({
        headerTag: "h3",
        bodyTag: "section",
        autoFocus: true,
        titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
        onStepChanging: function(a, e, s) {
			if (e < s) {
				if(e===0){
                    var valid=true;					
					if(document.getElementById('propertyclient').value==''){
						document.getElementById('propertyclient-form').innerHTML='This value is required';		
                        valid=false;						
					}else{
						document.getElementById('propertyclient-form').innerHTML='';		                       
					}	
				    if(document.getElementById('propertyname').value==''){
						document.getElementById('propertyname-form').innerHTML='This value is required';		
                        valid=false;						
					}else{
						document.getElementById('propertyname-form').innerHTML='';
					}						
					if(document.getElementById('stateId').value==''){
						document.getElementById('stateId-form').innerHTML='This value is required';		
                         valid=false;							
					}else{
						document.getElementById('stateId-form').innerHTML='';
					}
					if(document.getElementById('cityId').value==''){					
						document.getElementById('cityId-form').innerHTML='This value is required';		
                       valid=false;								
					}else{
						document.getElementById('cityId-form').innerHTML='';
					}
					if(document.getElementById('propertydistrict').value==''){					
						document.getElementById('propertydistrict-form').innerHTML='This value is required';
                        valid=false;								
					}else{
						document.getElementById('propertydistrict-form').innerHTML='';
					}
					if(document.getElementById('propertytype').value==''){						
						document.getElementById('propertytype-form').innerHTML='This value is required';	
						 valid=false;		
					}else{
						document.getElementById('propertytype-form').innerHTML='';
					}
					if(document.getElementById('propertydistrict').value==''){						
						document.getElementById('propertydistrict-form').innerHTML='This value is required';	
							 valid=false;		
					}else{
						document.getElementById('propertydistrict-form').innerHTML='';
					}					
					
					if(document.getElementById('tokenname').value==''){						
						document.getElementById('tokenname-form').innerHTML='This value is required';		
							 valid=false;		
					}else{
						document.getElementById('tokenname-form').innerHTML='';
					}
					
					if(document.getElementById('tokensymbol').value==''){						
						document.getElementById('tokensymbol-form').innerHTML='This value is required';		
							 valid=false;		
					}else{
						document.getElementById('tokensymbol-form').innerHTML='';
					}
					
					
					if(document.getElementById('tokenvalue').value==''){						
						document.getElementById('tokenvalue-form').innerHTML='This value is required';		
							 valid=false;		
					}else{
						document.getElementById('tokenvalue-form').innerHTML='';
					}
					
					if(document.getElementById('tokensupply').value==''){						
						document.getElementById('tokensupply-form').innerHTML='This value is required';		
							 valid=false;		
					}else{
						document.getElementById('tokensupply-form').innerHTML='';
					}
					
					if(document.getElementById('tokendecimal').value==''){						
						document.getElementById('tokendecimal-form').innerHTML='This value is required';		
							 valid=false;		
					}else{
						document.getElementById('tokendecimal-form').innerHTML='';
					}
				/*	if(document.getElementById('tokenlogo-form').innerHTML>102400){
						alert('hi');
					}   */
					if(document.getElementById('tokenlogovalid').value==1){						
							document.getElementById('tokenlogo-form').innerHTML='Image is too big (max 2MB)';		
								 valid=false;		
					}else{
						document.getElementById('tokenlogo-form').innerHTML='';	
					}
					
					if(document.getElementById('tokenlogo').value==''){						
						document.getElementById('tokenlogo-form').innerHTML='This value is required';		
							 valid=false;		
					}else{
						document.getElementById('tokenlogo-form').innerHTML='';	
					}
					
					
					if(document.getElementById('divident').value==''){						
						document.getElementById('divident-form').innerHTML='This value is required';		
							 valid=false;		
					}else{
						document.getElementById('divident-form').innerHTML='';
					}
					if(document.getElementById('minimuminvestment').value==''){						
						document.getElementById('minimuminvestment-form').innerHTML='This value is required';		
							 valid=false;		
					}else{
						document.getElementById('minimuminvestment-form').innerHTML='';
					}
					
				if(parseInt(document.getElementById('minimuminvestment').value)>parseInt(document.getElementById('tokensupply').value)){	
						alert("Minimum Investment should be less than Token Supply");
							 valid=false;		
					}
					if(document.getElementById('propertysize').value==''){						
						document.getElementById('propertysize-form').innerHTML='This value is required';	
							 valid=false;		
					}else{
						document.getElementById('propertysize-form').innerHTML='';
					}
					if(document.getElementById('targetirr').value==''){						
						document.getElementById('targetirr-form').innerHTML='This value is required';		
							 valid=false;		
					}else{
						document.getElementById('targetirr-form').innerHTML='';
					}
					if(document.getElementById('averageyield').value==''){						
						document.getElementById('averageyield-form').innerHTML='This value is required';	
							 valid=false;		
					}else{
						document.getElementById('averageyield-form').innerHTML='';
					}
					if(document.getElementById('propdesc').value==''){						
						document.getElementById('propdesc-form').innerHTML='This value is required';	
							 valid=false;		
					}else{
						document.getElementById('propdesc-form').innerHTML='';
					}
					return valid;
				}else if(e===1){
					 var valid2=true;		
	                if(document.getElementById('proponent').value==''){						
						document.getElementById('proponent-form').innerHTML='This value is required';	
							 valid2=false;		
					}else{
						document.getElementById('proponent-form').innerHTML='';	
					}
					if(document.getElementById('onmap').value==''){						
						document.getElementById('onmap-form').innerHTML='This value is required';	
							 valid2=false;		
					}else{
						document.getElementById('onmap-form').innerHTML='';	
					}
					
					if(document.getElementById('address').value==''){						
						document.getElementById('address-form').innerHTML='This value is required';	
							 valid2=false;		
					}else{
						document.getElementById('address-form').innerHTML='';	
					}
					
					if(document.getElementById('propertystatus').value==''){						
						document.getElementById('propertystatus-form').innerHTML='This value is required';	
							 valid2=false;		
					}else{
						document.getElementById('propertystatus-form').innerHTML='';	
					}
					
					if(document.getElementById('buildyear').value==''){						
						document.getElementById('buildyear-form').innerHTML='This value is required';	
							 valid2=false;		
					}else{
						document.getElementById('buildyear-form').innerHTML='';	
					}
					
					if(document.getElementById('reranumber').value==''){						
						document.getElementById('reranumber-form').innerHTML='This value is required';	
							 valid2=false;		
					}else{
						document.getElementById('reranumber-form').innerHTML='';	
					}
					if(document.getElementById('propertymanager').value==''){						
						document.getElementById('propertymanager-form').innerHTML='This value is required';	
							 valid2=false;		
					}else{
						document.getElementById('propertymanager-form').innerHTML='';
					}
					if(document.getElementById('custodian').value==''){						
						document.getElementById('custodian-form').innerHTML='This value is required';	
							 valid2=false;		
					}else{
						document.getElementById('custodian-form').innerHTML='';
					}
					return valid2;
				}
				else if(e===2){
					var valid3=true;
					var chks = document.getElementsByName('amenities[]');
					  var i, count=0;
					  for (i = 0;i<chks.length;i++){						 
						if (chks[i].checked){
							count+=1;
						}
					  }
					
					  if(count>=1){
						   valid3=true;
						   document.getElementById('amenities-form').innerHTML='';
					  }else{
						  	document.getElementById('amenities-form').innerHTML='Please must select atleast one amenity';	
							alert('Please must select atleast one amenity');
							valid3=false;
					  }
					  
					 
					   
					  if(document.getElementById('techspecs').value==''){
						  document.getElementById('techspecs-form').innerHTML='Please upload property technical specification';	
						valid3=false;
					  }else{
						  document.getElementById('techspecs-form').innerHTML='';
					  }
					  
					   
					  
					  if(document.getElementById('techspecs').value!=''){
							var myFile="";							
							  myFile = $("#techspecs").val();							 
							  var upld = myFile.split('.').pop();							 
							  if(upld=='pdf'){
								valid3=true;
							  }else{
								alert("Only PDF are allowed");
								document.getElementById('techspecs-form').innerHTML="Only PDF Format are allowed";
								valid3=false;
							  }
							 
						
					  }
					   else{
						  document.getElementById('techspecs-form').innerHTML='';
					  }
					 if(document.getElementById('techspecsvalid').value==1){
						  document.getElementById('techspecs-form').innerHTML='File Size is too big (max 8MB) ';	
						  valid3=false;
					  }else{
						  document.getElementById('techspecs-form').innerHTML='';
					  }
				
					return valid3;
				}
				else if(e===3){
					var valid4=true;
					if(document.getElementById('costofpremise').value==''){						
						document.getElementById('costofpremise-form').innerHTML='This value is required';	
							 valid4=false;		
					}
					else{
						document.getElementById('costofpremise-form').innerHTML='';	
					}
					
					
					if(document.getElementById('stampdutyregistration').value==''){						
						document.getElementById('stampdutyregistration-form').innerHTML='This value is required';	
							 valid4=false;		
					}else{
						document.getElementById('stampdutyregistration-form').innerHTML='';
					}
					
					if(document.getElementById('acquisitionfee').value==''){						
						document.getElementById('acquisitionfee-form').innerHTML='This value is required';	
							 valid4=false;		
					}else{
						document.getElementById('acquisitionfee-form').innerHTML='';
					}
					
					
					if(document.getElementById('othercharges').value==''){						
						document.getElementById('othercharges-form').innerHTML='This value is required';	
							 valid4=false;		
					}else{
						document.getElementById('othercharges-form').innerHTML='';
					}
					
					
					if(document.getElementById('legalfees').value==''){						
						document.getElementById('legalfees-form').innerHTML='This value is required';	
							 valid4=false;		
					}else{
						document.getElementById('legalfees-form').innerHTML='';
					}
					
					if(document.getElementById('maintenancereserve').value==''){						
						document.getElementById('maintenancereserve-form').innerHTML='This value is required';	
							 valid4=false;		
					}else{
						document.getElementById('maintenancereserve-form').innerHTML='';
					}
					
					
					if(document.getElementById('adminexpenses').value==''){						
						document.getElementById('adminexpenses-form').innerHTML='This value is required';	
							 valid4=false;		
					}else{
						document.getElementById('adminexpenses-form').innerHTML='';
					}
					
					if(document.getElementById('monthlyopex').value==''){						
						document.getElementById('monthlyopex-form').innerHTML='This value is required';	
							 valid4=false;		
					}
					else{
						document.getElementById('monthlyopex-form').innerHTML='';
					}
					
					
					if(document.getElementById('monthlypropertytax').value==''){						
						document.getElementById('monthlypropertytax-form').innerHTML='This value is required';	
							 valid4=false;		
					}else{
						document.getElementById('monthlypropertytax-form').innerHTML='';
					}
					
					if(document.getElementById('monthlyregulatorycharges').value==''){						
						document.getElementById('monthlyregulatorycharges-form').innerHTML='This value is required';	
							 valid4=false;		
					}else{
						document.getElementById('monthlyregulatorycharges-form').innerHTML='';
					}
					
					if(document.getElementById('totalacquisitioncost').value==''){						
						document.getElementById('totalacquisitioncost-form').innerHTML='This value is required';	
							 valid4=false;		
					}else{
						document.getElementById('totalacquisitioncost-form').innerHTML='';
					}
					
					
					
					return valid4;
				}
				else if(e===4){
					var valid5=true;
					if(document.getElementById('carpetarea').value==''){						
						document.getElementById('carpetarea-form').innerHTML='This value is required';	
							 valid5=false;		
					}else{
						document.getElementById('carpetarea-form').innerHTML='';
					}
					
					
					if(document.getElementById('builtuparea').value==''){						
						document.getElementById('builtuparea-form').innerHTML='This value is required';	
							 valid5=false;		
					}else{
						document.getElementById('builtuparea-form').innerHTML='';
					}
					
					
					if(document.getElementById('monthlyrent').value==''){						
						document.getElementById('monthlyrent-form').innerHTML='This value is required';	
							 valid5=false;		
					}else{
						document.getElementById('monthlyrent-form').innerHTML='';
					}
					
					if(document.getElementById('leasedperiod').value==''){						
						document.getElementById('leasedperiod-form').innerHTML='This value is required';	
							 valid5=false;		
					}else{
						document.getElementById('leasedperiod-form').innerHTML='';
					}
					
					
					if(document.getElementById('securitydeposit').value==''){						
						document.getElementById('securitydeposit-form').innerHTML='This value is required';	
							 valid5=false;		
					}else{
						document.getElementById('securitydeposit-form').innerHTML='';
					}
					
					
					if(document.getElementById('escalation').value==''){						
						document.getElementById('escalation-form').innerHTML='This value is required';	
							 valid5=false;		
					}else{
						document.getElementById('escalation-form').innerHTML='';
					}
					
					if(document.getElementById('tenants').value==''){						
						document.getElementById('tenants-form').innerHTML='This value is required';	
							 valid5=false;		
					}else{
						document.getElementById('tenants-form').innerHTML='';
					}
					
					
					if(document.getElementById('fittedout').value==''){						
						document.getElementById('fittedout-form').innerHTML='This value is required';	
							 valid5=false;		
					}else{
						document.getElementById('fittedout-form').innerHTML='';
					}
					
					
					if(document.getElementById('monthlymaintenance').value==''){						
						document.getElementById('monthlymaintenance-form').innerHTML='This value is required';	
							 valid5=false;		
					}else{
						document.getElementById('monthlymaintenance-form').innerHTML='';
					}
					
					
					if(document.getElementById('leasechart').value==''){						
						document.getElementById('leasechart-form').innerHTML='This value is required';	
							 valid5=false;		
					}else{
						document.getElementById('leasechart-form').innerHTML='';
					}
					
					return valid5;
				}
				else if(e===5){
					var valid6=true;
					if(document.getElementById('grossyield').value==''){						
						document.getElementById('grossyield-form').innerHTML='This value is required';	
							 valid6=false;		
					}else{
						document.getElementById('grossyield-form').innerHTML='';
					}
					
					if(document.getElementById('expectedirr').value==''){						
						document.getElementById('expectedirr-form').innerHTML='This value is required';	
							 valid6=false;		
					}else{
						document.getElementById('expectedirr-form').innerHTML='';
					}
					
					if(document.getElementById('tokenlockinperiod').value==''){						
						document.getElementById('tokenlockinperiod-form').innerHTML='This value is required';	
							 valid6=false;		
					}else{
						document.getElementById('tokenlockinperiod-form').innerHTML='';
					}
					
					if(document.getElementById('aurumfacilationfees').value==''){						
						document.getElementById('aurumfacilationfees-form').innerHTML='This value is required';	
							 valid6=false;		
					}else{
						document.getElementById('aurumfacilationfees-form').innerHTML='';
					}
					
					if(document.getElementById('performancefees').value==''){						
						document.getElementById('performancefees-form').innerHTML='This value is required';	
							 valid6=false;		
					}else{
						document.getElementById('performancefees-form').innerHTML='';
					} 
					  return valid6;
				}
				else if(e===6){
					var valid72=true;
					var valid73=true;
					var valid71=true;
					var valid74=true;
					var valid75=true;
					var valid76=true;
					if(document.getElementById('investmentdeck').value==''){
						  document.getElementById('investmentdeck-form').innerHTML='Please upload property technical specification';	
						valid71=false;
					  }else{
						  document.getElementById('investmentdeck-form').innerHTML='';
					  }						  
					  if(document.getElementById('investmentdeck').value!=''){
							var myFile="";							
							  myFile = $("#investmentdeck").val();							 
							  var upld = myFile.split('.').pop();							 
							  if(upld=='pdf'){
								valid71=true;
							  }else{
								alert("Only PDF are allowed");
								document.getElementById('investmentdeck-form').innerHTML="Only PDF Format are allowed";
								valid71=false;
							  }
					  }else{
						  document.getElementById('investmentdeck-form').innerHTML='';
					  }	
					    if(document.getElementById('investmentdeckvalid').value==1){
						  document.getElementById('investmentdeck-form').innerHTML='File Size is too big (max 8 MB) ';	
						  valid71=false;
					  }else{
						  document.getElementById('investmentdeck-form').innerHTML='';
					  }	
					  if(document.getElementById('titlereport').value==''){
						  document.getElementById('titlereport-form').innerHTML='Please upload property technical specification';	
						  valid72=false;
					  }	else{
						  document.getElementById('titlereport-form').innerHTML='';
					  }					  
					  if(document.getElementById('titlereport').value!=''){
							var myFile2="";							
							  myFile2 = $("#titlereport").val();							 
							  var upld = myFile2.split('.').pop();							 
							  if(upld=='pdf'){
								valid72=true;
							  }else{
								alert("Only PDF are allowed");
								document.getElementById('titlereport-form').innerHTML="Only PDF Format are allowed";
								valid72=false;
							  }
					  }else{
						  document.getElementById('titlereport-form').innerHTML='';
					  }	
					  
					  if(document.getElementById('titlereportvalid').value==1){
						  document.getElementById('titlereport-form').innerHTML='File Size is too big (max 8 MB)';	
						  valid72=false;
					  }	else{
						  document.getElementById('titlereport-form').innerHTML='';
					  }	
					  
					  if(document.getElementById('propertyduediligence').value==''){
						  document.getElementById('propertyduediligence-form').innerHTML='Please upload property technical specification';	
						  valid73=false;
					  }	else{
						  document.getElementById('propertyduediligence-form').innerHTML='';
					  }	
					  
					  if(document.getElementById('propertyduediligence').value!=''){
							var myFile3="";							
							  myFile3 = $("#propertyduediligence").val();							 
							  var upld = myFile3.split('.').pop();							 
							  if(upld=='pdf'){
								valid73=true;
							  }else{
								alert("Only PDF are allowed");
								document.getElementById('propertyduediligence-form').innerHTML="Only PDF Format are allowed";
								valid73=false;
							  }
					  }else{
						  document.getElementById('propertyduediligence-form').innerHTML='';
					  }	
					  
					  
					   if(document.getElementById('propertyduediligencevalid').value==1){
						  document.getElementById('propertyduediligence-form').innerHTML='File Size is too big (max 8 MB)';	
						  valid73=false;
					  }	else{
						  document.getElementById('propertyduediligence-form').innerHTML='';
					  }	
					  
					  if(document.getElementById('micromarketreport').value==''){
						  document.getElementById('micromarketreport-form').innerHTML='Please upload property technical specification';	
						  valid74=false;
					  }else{
						  document.getElementById('micromarketreport-form').innerHTML='';	 
					  }
					  
					  if(document.getElementById('micromarketreport').value!=''){
							var myFile4="";							
							  myFile4 = $("#micromarketreport").val();							 
							  var upld = myFile4.split('.').pop();							 
							  if(upld=='pdf'){
								valid74=true;
							  }else{
								alert("Only PDF are allowed");
								document.getElementById('micromarketreport-form').innerHTML="Only PDF Format are allowed";
								valid74=false;
							  }
					  }else{
						  document.getElementById('micromarketreport-form').innerHTML='';	 
					  }
					  
					    if(document.getElementById('micromarketreportvalid').value==1){
						  document.getElementById('micromarketreport-form').innerHTML='File Size is too big (max 8 MB) ';	
						  valid74=false;
					  }else{
						  document.getElementById('micromarketreport-form').innerHTML='';	 
					  }	
					  
					  if(document.getElementById('propertymanagementagreement').value==''){
						  document.getElementById('propertymanagementagreement-form').innerHTML='Please upload property technical specification';	
						  valid75=false;
					  }else{
						  document.getElementById('micromarketreport-form').innerHTML='';
					  }					  
					  if(document.getElementById('propertymanagementagreement').value!=''){
							var myFile5="";							
							  myFile5 = $("#propertymanagementagreement").val();							 
							  var upld = myFile5.split('.').pop();							 
							  if(upld=='pdf'){
								valid75=true;
							  }else{
								alert("Only PDF are allowed");
								document.getElementById('propertymanagementagreement-form').innerHTML="Only PDF Format are allowed";
								valid75=false;
							  }
					  }else{
						  document.getElementById('micromarketreport-form').innerHTML='';
					  }	
					  
					    if(document.getElementById('propertymanagementagreementvalid').value==1){
						  document.getElementById('propertymanagementagreement-form').innerHTML='File Size is too big (max 8 MB) ';	
						  valid75=false;
					  }else{
						  document.getElementById('propertymanagementagreement-form').innerHTML='';
					  }	
					  
					  if(document.getElementById('valuationreport').value==''){
						  document.getElementById('valuationreport-form').innerHTML='Please upload property technical specification';	
						  valid76=false;
					  }	else{
						   document.getElementById('valuationreport-form').innerHTML='';
					  }					  
					  if(document.getElementById('valuationreport').value!=''){
							var myFile6="";							
							  myFile6 = $("#valuationreport").val();							 
							  var upld = myFile6.split('.').pop();							 
							  if(upld=='pdf'){
								valid76=true;
							  }else{
								alert("Only PDF are allowed");
								document.getElementById('valuationreport-form').innerHTML="Only PDF Format are allowed";
								valid76=false;
							  }
					  }else{
						   document.getElementById('valuationreport-form').innerHTML='';
					  }	
					    if(document.getElementById('valuationreportvalid').value==1){
						  document.getElementById('valuationreport-form').innerHTML='File Size is too big (max 8 MB) ';	
						  valid76=false;
					     }
                        else{
						   document.getElementById('valuationreport-form').innerHTML='';
					    }						 
						
						 if(valid71==true && valid72==true && valid73==true && valid74==true && valid75==true && valid76==true){
					  return true;
						 }
				}else if(e===7){
					var valid8=true;				
					if(document.getElementById('propgallery').value==''){
						alert('please enter');
						  //document.getElementById('propgallery-form').innerHTML='Please upload property technical specification';	
						  valid7=false;
					  }	
					return valid8;	
				
				}				
			}
			else {					  
				return true;
			}

           
        },

 onFinishing: function (event, currentIndex)
    {
     // return form.validate().settings.ignore = ":disabled",
        form.submit();
    },
    onFinished: function (event, currentIndex)
    {
        alert("Submitted!");
    }
});
		
    $("#wizard3").steps({
        headerTag: "h3",
        bodyTag: "section",
        autoFocus:true,
        titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
        stepsOrientation: 1
    })
}));
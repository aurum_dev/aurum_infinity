@extends('layout.appHome')

@section('content')
 <style>
                       .progress-bar-info{
                        background-color: #05bd8e !important;
                       }
.progress
{
   height: 10px !important;
}
.vc_custom_1565246688063{
   background-image: url(<?php echo url('/') ?>/public/asset/login/wp-content/uploads/2021/01/bann.jpg) !important;
}
                    </style> 
<div id="page" class="site">
            <!-- #content -->
            <div id="content" class="site-content">
               <div id="primary" class="content-area">
                  <main id="main" class="site-main">
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

                         
                        
                     <div class="container page-container">
                        <article id="post-3351" class="post-3351 page type-page status-publish hentry">
                           <header class="entry-header">
                           </header>
                           <!-- .entry-header -->
                           <div class="entry-content">
                          
                              <section data-vc-full-width="true" style="" id="tophomebannerclass" data-vc-full-width-init="false" class="vc_section visible-lg visible-md visible-sm hidden-xs vc_custom_1565246688063 vc_section-has-fill">
                                 <div class="vc_row wpb_row vc_row-fluid background-position-left-bottom home-thirteen-tab-banner vc_custom_1565260402984 vc_row-has-fill">
                                    <div class="wpb_column vc_column_container vc_col-sm-7">
                                       <div class="vc_column-inner vc_custom_1564979830197">
                                          <div class="wpb_wrapper">
                                             <h1 style="font-size: 36px;color: #172243;line-height: 51px;text-align: left;font-family: 'Public Sans', sans-serif;font-weight: bold;" class="vc_custom_heading font-weight-bold vc_custom_1565266792852" >Investing in Grade - A Properties,<br> now simple and secure</h1>
                                          
                                             <!-- radiantthemes-custom-button -->
                                             <div class="radiantthemes-custom-button hover-style-five rt810073130  display-inline-block"  data-button-direction="center" data-button-fullwidth="false" >
                                                <a style=" color: #ffffff; line-height: 25px; font-size: 14px;background:#05bd8e;border-radius:4px !important" class="radiantthemes-custom-button-main  vc_custom_1564647891751" href="{{url('/login')}}"  title="" target="_self">
                                                   <div class="placeholder" style="
                                                   font-size: 17px; text-transform: uppercase;
    font-family: 'Public Sans', sans-serif; font-weight: 600;">Explore Property</div>
                                                </a>
                                                
  
                                             </div>
                                             
                                             
                                             
                                             
                                             
 

                              
                                      
                                             
                                             
                                             
                                             
                                             
                                             
                                             <!-- radiantthemes-custom-button -->
                                          </div>
                                       </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-6">
                                       <div class="vc_column-inner vc_custom_1554094596602">
                                          <div class="wpb_wrapper"></div>
                                       </div>
                                    </div>
                                 </div>
                              </section>
                              <div class="vc_row-full-width vc_clearfix"></div>
                              
                              <div class="vc_row-full-width vc_clearfix"></div>
                              <section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section background-position-center-top position-relative home-thirteen-tablet-services vc_custom_1565259935698 vc_section-has-fill" >
                                 <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid home-thirteen-services-upper-space vc_custom_1565259781064 vc_row-has-fill vc_column-gap-30 vc_row-o-content-middle vc_row-flex" style="margin-top:101px !important;">
                                    
                                    <div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12 vc_col-xs-12">
                                       <div class="vc_column-inner vc_custom_1565260220932">
                                          <div class="wpb_wrapper">
                                             <h2 style="font-size: 25px !important;
    color: #232F3E !important;
    line-height:50px !important;
    text-align: left !important;
    text-transform: uppercase;
    font-family: 'Public Sans', sans-serif;
    font-weight: bold;letter-spacing: 0.03px;text-align:center !important;font-family: 'Public Sans', sans-serif; font-weight: 600;" class="vc_custom_heading font-weight-regular title-letter-spacing vc_custom_1564561171454" >Start Investing in 5 simple steps</h2>
                                            
                                           
                                            

                                        <!-- <div class="radiantthemes-custom-button hover-style-five rt537002223  display-inline-block"  data-button-direction="center" data-button-fullwidth="false" >
                                                <a style=" color: #ffffff; line-height: 25px; font-size: 14px;border: 2px solid #251d59 !important;" class="radiantthemes-custom-button-main  vc_custom_1564034074121" href="#"  title="" target="_self">
                                                   <div class="placeholder" style="font-size:16px;">Discover More&nbsp;&nbsp;<i class="fa fa-long-arrow-right" style="font-size:21px;vertical-align:middle"></i></div>
                                                </a>
                                                
                                             </div>-->
                                             <!-- radiantthemes-custom-button -->
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                     
                                 
                                 


<div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1554444973153" style="position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;">
   <div class="slide-up-hover-effect wpb_column vc_column_container vc_col-sm-4">
      <div class="vc_column-inner vc_custom_1579522094834">
         <div class="wpb_wrapper">
            <div class="rt-fancy-text-box element-fourteen  rt1436012728 ">
               <div class="" style="height: auto;">
                  <div class="gradient-overlay"></div>
                  <div class="placeholder">
              
                     <div class="heading" style="text-align:center;">
                        <p class="title"><a href="#" style="text-align:center;font-family: 'Public Sans', sans-serif;font-weight: bold;color:#232F3E !important;font-size:22px;line-height:50px;">Explore Properties</a></p>
                     </div>
                     <div class="content">
                        <p style="font-size:16px;font-weight: 500;text-align:center;color:#686F6F;font-family: 'Public Sans', sans-serif;">Picked by experts after thorough micro-market analysis & asset diligence.</p>
                     </div>
                    
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="slide-up-hover-effect wpb_column vc_column_container vc_col-sm-4">
      <div class="vc_column-inner vc_custom_1579522109424">
         <div class="wpb_wrapper">
            <div class="rt-fancy-text-box element-fourteen  rt2050145585 ">
               <div class="" style="height: auto;">
                  <div class="gradient-overlay"></div>
                  <div class="placeholder">
                        <div class="heading" style="text-align:center;">
                        <p class="title"><a href="#" style="text-align:center;font-family: 'Public Sans', sans-serif;font-weight: bold;color:#232F3E !important;font-size:22px;line-height:50px;">Analyse with Insights</a></p>
                     </div>
                     <div class="content">
                        <p style="font-size:16px;font-weight: 500;text-align:center;color:#686F6F;font-family: 'Public Sans', sans-serif;">From Micro-market & valuation reports and property management agreements.</p>
                     </div>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="slide-up-hover-effect wpb_column vc_column_container vc_col-sm-4">
      <div class="vc_column-inner vc_custom_1579522131920">
         <div class="wpb_wrapper">
            <div class="rt-fancy-text-box element-fourteen  rt949451357 ">
               <div class="" style="height:auto;">
                  <div class="gradient-overlay"></div>
                  <div class="placeholder">
                   
                     <div class="heading" style="text-align:center;">
                        <p class="title"><a href="#" style="text-align:center;font-family: 'Public Sans', sans-serif;font-weight: bold;color:#232F3E !important;font-size:22px;line-height:50px;">Monitor & Track</a></p>
                     </div>
                     <div class="content">
                        <p style="font-size:16px;font-weight: 500;text-align:center;color:#686F6F;font-family: 'Public Sans', sans-serif;">Your portfolio of asset tokens in real-time on your personalized Aurum Infinity dashboard.</p>
                     </div>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

    <div class="slide-up-hover-effect wpb_column vc_column_container vc_col-sm-12" class="video">
       <style>
          
video {
    clip-path: inset(2px 2px);
}

       </style>
       <center>
<video  id="video" style="height: 299px; padding-left: 0 !important;padding-right: 0 !important;" src="<?php echo url('/'); ?>/public/asset/login/wp-content/investor.mp4" muted autoplay></video></center>
  


<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
   <script>

$(function() {
  
  var $video = $('.video');
  var $window = $(window);

  $window.scroll(function() {

    var $topOfVideo = $video.offset().top;
    var $bottomOfVideo = $video.offset().top + $video.outerHeight();

    var $topOfScreen = $window.scrollTop();
    var $bottomOfScreen = $window.scrollTop() + $window.innerHeight();
    
    if(($bottomOfScreen > $bottomOfVideo) && ($topOfScreen < $topOfVideo)){
      $video[0].play();
    } else {
      $video[0].pause();
    }
    
  });
  
});

    // Pause video function
    const video = document.getElementById('video');

    function pauseVideo(event) {
        // Console test
        // console.log(video.duration)
        // console.log(event.currentTime)

        // Pause video 0.5 seconds before end time.
        if (event.currentTime > video.duration - 0.2) {
            video.pause();
        }
    }
</script>
    </div>
     <div class="slide-up-hover-effect wpb_column vc_column_container vc_col-sm-2"></div>
     <div class="slide-up-hover-effect wpb_column vc_column_container vc_col-sm-4">
      <div class="vc_column-inner vc_custom_1579522131920">
         <div class="wpb_wrapper">
            <div class="rt-fancy-text-box element-fourteen  rt949451357 ">
               <div class="" style="height:auto;">
                  <div class="gradient-overlay"></div>
                  <div class="placeholder">
                   
                     <div class="heading" style="text-align:center;">
                        <p class="title"><a href="#" style="text-align:center;font-family: 'Public Sans', sans-serif;font-weight: bold;color:#232F3E !important;font-size:22px;line-height:50px;">Validate Vital Asset Information</a></p>
                     </div>
                     <div class="content">
                        <p style="font-size:16px;font-weight: 500;text-align:center;color:#686F6F;font-family: 'Public Sans', sans-serif;">Like property features, financials, lease details, and return analysis.</p>
                     </div>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
     <div class="slide-up-hover-effect wpb_column vc_column_container vc_col-sm-4">
      <div class="vc_column-inner vc_custom_1579522131920">
         <div class="wpb_wrapper">
            <div class="rt-fancy-text-box element-fourteen  rt949451357 ">
               <div class="" style="height:auto;">
                  <div class="gradient-overlay"></div>
                  <div class="placeholder">
                   
                     <div class="heading" style="text-align:center;">
                        <p class="title"><a href="#" style="text-align:center;font-family: 'Public Sans', sans-serif;font-weight: bold;color:#232F3E !important;font-size:22px;line-height:50px;">Build Your Portfolio</a></p>
                     </div>
                     <div class="content">
                        <p style="font-size:16px;font-weight: 500;text-align:center;color:#686F6F;font-family: 'Public Sans', sans-serif;">After complete clarity; co-own and invest in a property of your choice.</p>
                     </div>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div><div class="slide-up-hover-effect wpb_column vc_column_container vc_col-sm-2"></div>
   
</div>



<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid home-thirteen-services-upper-space vc_custom_1565259781064 vc_row-has-fill vc_column-gap-30 vc_row-o-content-middle vc_row-flex">
                                    
                                    <div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12 vc_col-xs-12">
                                       <div class="vc_column-inner vc_custom_1565260220932">
                                          <div class="wpb_wrapper">
                                             <h2 style="font-size: 25px !important;
    color: #232F3E !important;
    line-height:50px !important;
    text-align: left !important;
    text-transform: uppercase;
    font-family: 'Public Sans', sans-serif;
    font-weight: bold;letter-spacing: 0.03px;text-align:center !important;font-family: 'Public Sans', sans-serif; font-weight: 600;" class="vc_custom_heading font-weight-regular title-letter-spacing vc_custom_1564561171454" >Strategy</h2>
                                            
                                           
                                            
 <p class="title" style="font-size:16px;font-weight: 500;text-align:center;color:#686F6F;font-family: 'Public Sans', sans-serif;">A 5-Layer Property Information-set disclosed by the Issuer makes it easier for you to choose <br>and add real estate tokens representing fractions of real estate. Here’s how it works:</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>



<div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1554444973153" style="padding-bottom:100px;  background-color: #ffffff; /* For browsers that do not support gradients */
  background-image: linear-gradient(#ffffff, #B9983D12);">
   <div class="slide-up-hover-effect wpb_column vc_column_container vc_col-sm-4">
      <div class="vc_column-inner vc_custom_1579522131920">
         <div class="wpb_wrapper">
            <div class="rt-fancy-text-box element-fourteen  rt949451357 ">
               <div class="holder matchHeight " style="height: 304.094px;">
                  <div class="gradient-overlay"></div>
                  <div class="placeholder">
                   <div class="icon"><img width="73" height="73" src="<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2021/01/Group 66796.png" class="attachment-full size-full" alt="Home-11-SEO-Consultancy-Icon" loading="lazy" data-no-retina=""></div>
                     <div class="heading">
                        <p class="title"><a href="#" style="text-align:center;font-family: 'Public Sans', sans-serif;font-weight: bold;color:#232F3E !important;font-size:22px;line-height:50px;">Due diligence</a></p>
                     </div>
                     <div class="content">
                        <p style="font-size:16px;font-weight: 500;text-align:left;color:#686F6F;font-family: 'Public Sans', sans-serif;">Asset-related risks are mitigated through due diligence reports uploaded by the issuers.

</p>
                     </div>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
      <div class="slide-up-hover-effect wpb_column vc_column_container vc_col-sm-4">
      <div class="vc_column-inner vc_custom_1579522131920">
         <div class="wpb_wrapper">
            <div class="rt-fancy-text-box element-fourteen  rt949451357 ">
               <div class="holder matchHeight " style="height: 304.094px;">
                  <div class="gradient-overlay"></div>
                  <div class="placeholder">
                   <div class="icon"><img width="73" height="73" src="<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2021/01/Group 66316.png" class="attachment-full size-full" alt="Home-11-SEO-Consultancy-Icon" loading="lazy" data-no-retina=""></div>
                     <div class="heading">
                        <p class="title"><a href="#" style="text-align:center;font-family: 'Public Sans', sans-serif;font-weight: bold;color:#232F3E !important;font-size:22px;line-height:50px;">In-depth financial analysis</a></p>
                     </div>
                     <div class="content">
                        <p style="font-size:16px;font-weight: 500;text-align:left;color:#686F6F;font-family: 'Public Sans', sans-serif;">Data-driven insights on asset growth are provided to help you make informed asset selections.</p>
                     </div>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   
  <div class="slide-up-hover-effect wpb_column vc_column_container vc_col-sm-4">
      <div class="vc_column-inner vc_custom_1579522131920">
         <div class="wpb_wrapper">
            <div class="rt-fancy-text-box element-fourteen  rt949451357 ">
               <div class="holder matchHeight " style="height: 304.094px;">
                  <div class="gradient-overlay"></div>
                  <div class="placeholder">
                   <div class="icon"><img width="73" height="73" src="<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2021/01/Group 66317.png" class="attachment-full size-full" alt="Home-11-SEO-Consultancy-Icon" loading="lazy" data-no-retina=""></div>
                     <div class="heading">
                        <p class="title"><a href="#" style="text-align:center;font-family: 'Public Sans', sans-serif;font-weight: bold;color:#232F3E !important;font-size:22px;line-height:50px;">Market insights</a></p>
                     </div>
                     <div class="content">
                        <p style="font-size:16px;font-weight: 500;text-align:left;color:#686F6F;font-family: 'Public Sans', sans-serif;">Get insights on micro & macro market factors that determine the asset valuation.</p>
                     </div>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

    <div class="slide-up-hover-effect wpb_column vc_column_container vc_col-sm-4">
      <div class="vc_column-inner vc_custom_1579522131920">
         <div class="wpb_wrapper">
            <div class="rt-fancy-text-box element-fourteen  rt949451357 ">
               <div class="holder matchHeight " style="height: 304.094px;">
                  <div class="gradient-overlay"></div>
                  <div class="placeholder">
                   <div class="icon"><img width="73" height="73" src="<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2021/01/Group 66311.png" class="attachment-full size-full" alt="Home-11-SEO-Consultancy-Icon" loading="lazy" data-no-retina=""></div>
                     <div class="heading">
                        <p class="title"><a href="#" style="text-align:center;font-family: 'Public Sans', sans-serif;font-weight: bold;color:#232F3E !important;font-size:22px;line-height:50px;">Occupancy insights</a></p>
                     </div>
                     <div class="content">
                        <p style="font-size:16px;font-weight: 500;text-align:left;color:#686F6F;font-family: 'Public Sans', sans-serif;">Expert analysis of property demands & tenant needs across different age groups.</p>
                     </div>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="slide-up-hover-effect wpb_column vc_column_container vc_col-sm-4">
      <div class="vc_column-inner vc_custom_1579522131920">
         <div class="wpb_wrapper">
            <div class="rt-fancy-text-box element-fourteen  rt949451357 ">
               <div class="holder matchHeight " style="height: 304.094px;">
                  <div class="gradient-overlay"></div>
                  <div class="placeholder">
                   <div class="icon"><img width="73" height="73" src="<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2021/01/Group 66794.png" class="attachment-full size-full" alt="Home-11-SEO-Consultancy-Icon" loading="lazy" data-no-retina=""></div>
                     <div class="heading">
                        <p class="title"><a href="#" style="text-align:center;font-family: 'Public Sans', sans-serif;font-weight: bold;color:#232F3E !important;font-size:22px;line-height:50px;">Graded properties</a></p>
                     </div>
                     <div class="content">
                        <p style="font-size:16px;font-weight: 500;text-align:left;color:#686F6F;font-family: 'Public Sans', sans-serif;">We only enlist graded properties on our platform with appropriate classifications.</p>
                     </div>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   
</div>



                                
                                 <div class="vc_row-full-width vc_clearfix"></div>
                              

<style>
   .vc_custom_1540799876678 {
    margin-top: 0px !important;
    margin-bottom: 0px !important;
    padding-top: 0px !important;
    padding-bottom: 0px !important;
    background-image: url(<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2018/07/back.jpg) !important;
    background-position: center !important;
    background-repeat: no-repeat !important;
    background-size: cover !important;
}

</style>
 

<div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid background-position-right home-thirteen-responsive home-thirteen-tablet-case-study vc_custom_1540799876678" style="position: relative; padding-bottom:52px !important;">
<div class="wpb_column vc_column_container vc_col-sm-12">
<center>   <h2 style="
   font-size: 27px;
   line-height: 48px;
   text-align: center;
   text-transform: uppercase;
   padding-top: 50px !important;
   padding-bottom: 50px !important;
   color: #FFFFFF !important;
   letter-spacing: 0.03px;
   font-family: 'Public Sans', sans-serif;
   font-weight: 600;

" class="vc_custom_heading font-weight-bold vc_custom_1564996682116">Understanding investment parameters</h2></center></div>
  
   <div class="wpb_column vc_column_container vc_col-sm-12" style="padding-bottom:0px;">
      <div class="vc_column-inner vc_custom_1540799834491">
         <div class="wpb_wrapper">
            

             <div class="wpb_column vc_column_container vc_col-sm-12" style="padding-bottom:20px;">
               
               <p style="text-align:center;font-family: 'Public Sans', sans-serif;font-weight: bold;color:#ffffff !important;font-size:22px;line-height:50px;">Tokenisation</p>
               <p style="text-align:center;font-family: 'Public Sans', sans-serif;font-weight: 600;color:#AAAFAF !important;font-size:16px;">of a real estate asset denotes converting fractions of the asset <br>into digital tokens via blockchain technology.</p>
            </div>

         </div></div></div>
 
   <div class="wpb_column vc_column_container vc_col-sm-4" style="padding-bottom:50px;">
      <div class="vc_column-inner vc_custom_1540799834491">
         <div class="wpb_wrapper">
          
            <div class="wpb_column vc_column_container vc_col-sm-12" style="padding-bottom:50px;">
               
               <p style="text-align:right;font-family: 'Public Sans', sans-serif;font-weight: bold;color:#ffffff !important;font-size:22px;line-height:50px;">Internal Rate of Return (IRR)</p>
               <p style="text-align:right;font-family: 'Public Sans', sans-serif;font-weight: 600;color:#AAAFAF !important;font-size:16px;">The Internal Rate of Return in real estate is the estimated value that a property generates during the period of its ownership.</p>
            </div>
             <div class="wpb_column vc_column_container vc_col-sm-12" style="padding-bottom:50px;">
               
              
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-12" style="padding-bottom:50px;">
                 <p style="text-align:right;font-family: 'Public Sans', sans-serif;font-weight: bold;color:#ffffff !important;font-size:22px;line-height:50px;">Net Asset Value (or NAV)</p>
               <p style="text-align:right;font-family: 'Public Sans', sans-serif;font-weight: 600;color:#AAAFAF !important;font-size:16px;">It refers to the overall worth of a property, minus any outstanding debt and the expense of any fixed or scheduled capital costs.</p>

                 
            </div>
           
         </div>
      </div>
   </div>


   <div class="wpb_column vc_column_container vc_col-sm-4" style="padding-bottom:50px;">
      <div class="vc_column-inner vc_custom_1540799834491">
         <div class="wpb_wrapper">
         <img src="<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2021/01/token.png"> 
            
           
         </div>
      </div>
   </div>

<div class="wpb_column vc_column_container vc_col-sm-4" style="padding-bottom:50px;">
      <div class="vc_column-inner vc_custom_1540799834491">
         <div class="wpb_wrapper">
          
                 
            <div class="wpb_column vc_column_container vc_col-sm-12" style="padding-bottom:50px;">
               
               <p style="text-align:left;font-family: 'Public Sans', sans-serif;font-weight: bold;color:#ffffff !important;font-size:22px;line-height:50px;">Fractional Ownership</p>
               <p style="text-align:left;font-family: 'Public Sans', sans-serif;font-weight: 600;color:#AAAFAF !important;font-size:16px;">It means bifurcating a real estate asset into multiple fractions for a larger set of investors to invest in & co-own the asset equivalent to their investment.</p>
            </div>
             <div class="wpb_column vc_column_container vc_col-sm-12" style="padding-bottom:20px;">
               
              
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-12" style="padding-bottom:50px;">
                 <p style="text-align:left;font-family: 'Public Sans', sans-serif;font-weight: bold;color:#ffffff !important;font-size:22px;line-height:50px;">Entry Yield</p>
               <p style="text-align:left;font-family: 'Public Sans', sans-serif;font-weight: 600;color:#AAAFAF !important;font-size:16px;">The entry yield is the capacity of a property to generate income from the moment of its purchase.</p>

                 
            </div>
           
         </div>
      </div>
   </div>







 

 



   


</div>







     
                                 <div class="vc_row-full-width vc_clearfix"></div>
                              

                                 
                            
                           
                                 
                                 
                               
                                <div class="vc_row-full-width vc_clearfix"></div>
                                 
                                 
                              
                                 
                              
                                   




                    
                    


<style>
   .vc_custom_1565269117522 {
    margin-top: 0px !important;
    margin-bottom: 0px !important;
    padding-top: 0px !important;
    padding-bottom: 0px !important;
    background-image: url(<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2021/01/propertyvideo.jpg) !important;
    background-position: center !important;
    background-repeat: no-repeat !important;
    background-size: cover !important;
}

</style>
   <section data-vc-full-width="true" style="padding-top:0px !important;height:571px;" data-vc-full-width-init="false" class="vc_section background-position-center-bottom home-thirteen-pricing-table-responsive vc_custom_1565269117522 vc_section-has-fill">
                                
                                 
                                 <div class="vc_row wpb_row vc_row-fluid vc_custom_1565173495650">
                                  





                                 </div>
                              </section>


 <div class="vc_row-full-width vc_clearfix"></div>



<div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid background-position-right home-thirteen-responsive home-thirteen-tablet-case-study vc_custom_1540799876670" style="position: relative; margin-bottom:-20px !important;margin-top: 50px !important;">
<div class="wpb_column vc_column_container vc_col-sm-12">
<center>   <h2 style="
   font-size: 27px;
   line-height: 48px;
   text-align: center;
   text-transform: uppercase;
   padding-top: 0px !important;
   padding-bottom: 0px !important;
   
   color: #232F3E !important;
   letter-spacing: 0.03px;
   font-family: 'Public Sans', sans-serif;
   font-weight: 600;

" class="vc_custom_heading font-weight-bold vc_custom_1564996682116">How to get started</h2>
<p style="text-align:center;font-family: 'Public Sans', sans-serif;font-weight: 600;color:#232F3E !important;font-size:16px;line-height:30px">Start your Investment in High Growth Potential, Grade-A <br>Real Estate Assets with Aurum Infinity in 5 Easy Steps</p>
</center></div>
    
</div>
                    
                    
<div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid background-position-right home-thirteen-responsive home-thirteen-tablet-case-study vc_custom_1540799876679" style="position: relative; padding-left: 0 !important;padding-right: 0 !important;margin-bottom:-26px !important;margin-top: 90px !important;margin-left:-90px !important;margin-right:-100px !important;background-color: #ffffff; /* For browsers that do not support gradients */
  background-image: linear-gradient(#ffffff, #B9983D12);">
<div class="wpb_column vc_column_container vc_col-sm-12" class="video">
<center>
<img src="<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2021/01/Group 67211.png">
 
</center>
 
</div>

 
</div>




<section data-vc-full-width="true" style="padding-top: 50px !important; position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;" data-vc-full-width-init="true" class="vc_section background-position-center-bottom home-thirteen-pricing-table-responsive vc_custom_1565269117521 vc_section-has-fill">
   <div class="vc_row wpb_row vc_row-fluid vc_custom_1565173495650">
      <div class="home-thirteen-responsive-get-in-touch wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12">
         <div class="vc_column-inner vc_custom_1565240932947">
            <div class="wpb_wrapper">
               <h2 style="font-size: 35px;color: #232F3E;line-height: 48px;font-weight:700 !important;text-align: center;color:#232F3E;letter-spacing:0.03px;font-family: 'Public Sans', sans-serif;
    font-weight: 600;" class="vc_custom_heading font-weight-regular title-letter-spacing vc_custom_1564573746979">FAQs:</h2>
               <div class="radiantthemes-accordion element-four   rt1460847459">
                  <div class="radiantthemes-accordion-item">
                     <div class="radiantthemes-accordion-item-title">
                        <div class="radiantthemes-accordion-item-title-icon"></div>
                        <h4 class="panel-title"><span class="panel-title-counter"></span>What is asset tokenization?</h4>
                     </div>
                     <div class="radiantthemes-accordion-item-body" style="display: none;">Asset Tokenization is the process by which the ownership of real estate assets is digitized and represented by tokens on the blockchain. This enables a universal, fractional form of real estate ownership to be carried out totally on-chain.</div>
                  </div>
                  <div class="radiantthemes-accordion-item">
                     <div class="radiantthemes-accordion-item-title">
                        <div class="radiantthemes-accordion-item-title-icon"></div>
                        <h4 class="panel-title"><span class="panel-title-counter"></span>How does asset tokenization work?</h4>
                     </div>
                     <div class="radiantthemes-accordion-item-body" style="display: none;">Issuers generate digital tokens representing fractions of real estate assets on the blockchain. All transactions are recorded on the chain and cash-flows are divided among investors in proportion to their holdings.</div>
                  </div>
                  <div class="radiantthemes-accordion-item">
                     <div class="radiantthemes-accordion-item-title">
                        <div class="radiantthemes-accordion-item-title-icon"></div>
                        <h4 class="panel-title"><span class="panel-title-counter"></span>How do I buy asset tokens?</h4>
                     </div>
                     <div class="radiantthemes-accordion-item-body" style="display: none;">Start by selecting the number of tokens corresponding to the fraction you want to hold in the property, pay the required amount and hold the tokens in your secure wallet. These tokens represent your proportion of ownership.</div>
                  </div>
                  <div class="radiantthemes-accordion-item">
                     <div class="radiantthemes-accordion-item-title">
                        <div class="radiantthemes-accordion-item-title-icon"></div>
                        <h4 class="panel-title"><span class="panel-title-counter"></span>Who will own the property?</h4>
                     </div>
                     <div class="radiantthemes-accordion-item-body" style="display: none;">All investors are the co-owners of the property, in proportion to the number of tokens held by them. Every transaction of asset tokens on the blockchain gets recorded and co-owner information gets updated with the appropriate registering authority.</div>
                  </div>
                  <div class="radiantthemes-accordion-item">
                     <div class="radiantthemes-accordion-item-title">
                        <div class="radiantthemes-accordion-item-title-icon"></div>
                        <h4 class="panel-title"><span class="panel-title-counter"></span>How are rental yields distributed?</h4>
                     </div>
                     <div class="radiantthemes-accordion-item-body" style="display: none;">Every month, rental income and other property cash flows are divided among Asset Token holders in proportion to their holdings on the property and delivered to their secure wallets.</div>
                  </div>
                  <div class="radiantthemes-accordion-item">
                     <div class="radiantthemes-accordion-item-title">
                        <div class="radiantthemes-accordion-item-title-icon"></div>
                        <h4 class="panel-title"><span class="panel-title-counter"></span>How do I sell my asset tokens?</h4>
                     </div>
                     <div class="radiantthemes-accordion-item-body" style="display: none;">You are free to list your asset tokens for sale on our platform after the lock-in period.</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>











                                   
                     </section>
                                







                              
                               
                                   <div class="vc_row-full-width vc_clearfix"></div>
                              <section data-vc-full-width="true" style="background-image: url(<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2021/01/banner90.jpg) !important;
    background-position: center !important;
    background-repeat: no-repeat !important;
    background-size: cover !important;
;padding-top:64px !important;display:block;" data-vc-full-width-init="false" class="vc_section background-position-center-bottom home-thirteen-pricing-table-responsive vc_custom_1565269117521 vc_section-has-fill">
                                
                                 
                                 <div class="vc_row wpb_row vc_row-fluid vc_custom_1565173495650">
                                    <div class="home-thirteen-responsive-get-in-touch wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12">
                                       <div class="vc_column-inner vc_custom_1565240932947">
                                          <div class="wpb_wrapper">
                                          
                                          
                                          
                                          <div class="wpb_wrapper">
                                              <h2 style="font-size: 35px;color: #232F3E !important;line-height: 48px;text-align: left;color:#251d59;letter-spacing:0.03px;font-family: 'Public Sans', sans-serif;font-weight:bold;margin-bottom:0 !important;" class="vc_custom_heading font-weight-bold vc_custom_1564998591449"> Have queries about <br>Aurum Infinity?</h2>
 
   <h2 style="font-size: 20px !important;
    color: #232F3E !important;
    line-height: 60px !important;
    text-align: left !important;
    font-weight: 600 !important;letter-spacing: 0.03px;margin-bottom: 0 !important;" class="vc_custom_heading font-weight-regular title-letter-spacing vc_custom_1564573746979">Get in touch with us today!</h2>
  



   <div class="vc_column-inner">
                                          <div class="wpb_wrapper">
                                          <h2 style="font-size: 24px !important;
    color: #232F3E !important;
    line-height: 60px !important;
    text-align: left !important;
    font-weight: 600 !important;letter-spacing: 0.03px;padding-top:0px !important;" class="vc_custom_heading font-weight-regular title-letter-spacing vc_custom_1564573746979" ></h2>
                                             <!-- rt-cf7 -->
                                             <div class="radiant-contact-form rt5d04c30e5c56e06 element-three home-thirteen-box-shadow vc_custom_1564636361306"  >
                                                <div role="form" class="wpcf7" id="wpcf7-f3578-p3351-o1" lang="en-US" dir="ltr">
                                                
                                                   <div class="screen-reader-response">
                                                      <p role="status" aria-live="polite" aria-atomic="true"></p>
                                                      <ul></ul>
                                                   </div>
                                                  
                                                   <div class="wpcf7-form init" novalidate="novalidate" data-status="init" onSubmit>
                                                       @csrf()
                                                      <div class="get-in_touch">
                                                          <div id="demo"></div>
                                                         <div class="row">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                                               <div class="form-row"> 
   
                                                                  <span class="wpcf7-form-control-wrap Name"><input type="text" name="Name" id="name" value="" size="40" required class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Full Name" /></span> 
                                                                  <span id="name-form" style="color:#ff0000;"></span></div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                                               <div class="form-row"> <span class="wpcf7-form-control-wrap email"><input type="email" required name="email" id="email" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Email" /></span>
                                                               <span id="email-form" style="color:#ff0000;"></span> </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                                               <div class="form-row"> <span class="wpcf7-form-control-wrap Subject"><input type="text" required name="Mobile" id="mobile" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Mobile" /></span> 
                                                                  <span id="mobile-form" style="color:#ff0000;"></span></div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                                               <div class="form-row"> <span class="wpcf7-form-control-wrap Message"><textarea name="Message" required cols="20" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" id="message" placeholder="Message" style="height: 74px;overflow:hidden"></textarea></span>
                                                               <span id="message-form" style="color:#ff0000;"></span> </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                                               <div class="form-row submit-btn"> <input style="background:#05bd8e;border-radius:4px !important" type="submit" id="enquirysubmit" value="SUBMIT" class="wpcf7-form-control wpcf7-submit" /> </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      
                                                   </div>


                                              
                                                </div>
                                                <div class="clearfix"></div>
                                             </div>
                                             <!-- rt-cf7 -->
                                          </div>
                                       </div>
   
</div>
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                        
                                             
                                            
                                             
                                          </div>
                                       </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12">
                                       
                                    </div>
                                 </div>
                              </section>
                 



   <div class="vc_row-full-width vc_clearfix"></div>
                        <!----      <section data-vc-full-width="true" style="padding-top:0 !important" data-vc-full-width-init="false" class="vc_section background-position-center-bottom home-thirteen-pricing-table-responsive vc_custom_1565269117521 vc_section-has-fill">
                                
                                 
                                 <div class="vc_row wpb_row vc_row-fluid vc_custom_1565173495650">
                                    <div class="home-thirteen-responsive-get-in-touch wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12">
                                       <div class="vc_column-inner vc_custom_1565240932947">
                                          <div class="wpb_wrapper">
                                          
                                                                                  
                                          
                                             <h2 style="font-size: 35px;color: #000000;line-height: 48px;font-weight:700 !important;text-align: center;color:#251d59;letter-spacing:0.03px" class="vc_custom_heading font-weight-regular title-letter-spacing vc_custom_1564573746979" >FAQ (FREQUENTLY ASKED QUESTION)</h2>
                                            
                                             
                                             <div class="radiantthemes-accordion element-four   rt1460847459">
   <div class="radiantthemes-accordion-item">
      <div class="radiantthemes-accordion-item-title">
         <div class="radiantthemes-accordion-item-title-icon"></div>
         <h4 class="panel-title"><span class="panel-title-counter"></span>What is asset tokenization?</h4>
      </div>
      <div class="radiantthemes-accordion-item-body" style="display: none;">Asset Tokenization is the process by which the ownership of real estate assets is digitized and represented by tokens on the blockchain. This enables a universal, fractional form of real estate ownership to be carried out totally on-chain.</div>
   </div>
   <div class="radiantthemes-accordion-item">
      <div class="radiantthemes-accordion-item-title">
         <div class="radiantthemes-accordion-item-title-icon"></div>
         <h4 class="panel-title"><span class="panel-title-counter"></span>How does asset tokenization work?</h4>
      </div>
      <div class="radiantthemes-accordion-item-body" style="display: none;">Issuers generate digital tokens representing fractions of real estate assets on the blockchain. All transactions are recorded on the chain and cash-flows are divided among investors in proportion to their holdings.</div>
   </div>
   <div class="radiantthemes-accordion-item">
      <div class="radiantthemes-accordion-item-title">
         <div class="radiantthemes-accordion-item-title-icon"></div>
         <h4 class="panel-title"><span class="panel-title-counter"></span>How do I buy asset tokens?</h4>
      </div>
      <div class="radiantthemes-accordion-item-body" style="display: none;">Start by selecting the number of tokens corresponding to the fraction you want to hold in the property, pay the required amount and hold the tokens in your secure wallet. These tokens represent your proportion of ownership.</div>
   </div>
   <div class="radiantthemes-accordion-item">
      <div class="radiantthemes-accordion-item-title">
         <div class="radiantthemes-accordion-item-title-icon"></div>
         <h4 class="panel-title"><span class="panel-title-counter"></span>Who will own the property?</h4>
      </div>
      <div class="radiantthemes-accordion-item-body" style="display: none;">All investors are the co-owners of the property, in proportion to the number of tokens held by them. Every transaction of asset tokens on the blockchain gets recorded and co-owner information gets updated with the appropriate registering authority.</div>
   </div>
   <div class="radiantthemes-accordion-item">
      <div class="radiantthemes-accordion-item-title">
         <div class="radiantthemes-accordion-item-title-icon"></div>
         <h4 class="panel-title"><span class="panel-title-counter"></span>How are rental yields distributed?</h4>
      </div>
      <div class="radiantthemes-accordion-item-body" style="display: none;">Every month, rental income and other property cash flows are divided among Asset Token holders in proportion to their holdings on the property and delivered to their secure wallets.</div>
   </div>
   <div class="radiantthemes-accordion-item">
      <div class="radiantthemes-accordion-item-title">
         <div class="radiantthemes-accordion-item-title-icon"></div>
         <h4 class="panel-title"><span class="panel-title-counter"></span>How do I sell my asset tokens?</h4>
      </div>
      <div class="radiantthemes-accordion-item-body" style="display: none;">You are free to list your asset tokens for sale on our platform after the lock-in period.</div>
   </div>

</div>
                                            
                                             
                                          </div>
                                       </div>
                                    </div>
                                    
                                 </div>
                              </section>
-->


                              <div class="vc_row-full-width vc_clearfix"></div>
                              
                              
                           </div>
                           <!-- .entry-content -->
                        </article>
                        <!-- #post-3351 -->             
                     </div>
                  </main>
                  <!-- #main -->
               </div>
               <!-- #primary -->
            </div>
            <!-- #content -->
         </div>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">


    $(document).on("ready", function () {
             $("#enquirysubmit").on("click", function () {
                var name  = $('#name').val();
      var mobile  = $('#mobile').val();
      var email  = $('#email').val();
      var message  = $('#message').val();

      var valid=0;

      var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
       var filter = /^\d*(?:\.\d{1,2})?$/;
 var nameregex = /^[A-Za-z]+$/;
      if(name==""){        
         $('#name-form').html("Please must enter a Name");
         valid=1;
      }else{
         $('#name-form').html("");
        
      }
      if(!nameregex.test(name)) {
          $('#name-form').html("Please enter only characters");
         valid=1;
      }else{
         $('#name-form').html("");         
      }
      if(email==""){        
         $('#email-form').html("Please must enter a Email");
         valid=1;
      }else{
         $('#email-form').html("");         
      }
      if(!regex.test(email)) {
          $('#email-form').html("Please must enter a Valid Email");
         valid=1;
      }else{
         $('#email-form').html("");        
      }

      if(mobile==""){        
         $('#mobile-form').html("Please must enter a Mobile");
         valid=1;
      }else{
         $('#mobile-form').html("");
         
      }


          if (filter.test(mobile)) {
            if(mobile.length==10){
               $('#mobile-form').html("");
               
             }
             else {
               $('#mobile-form').html("Please put 10 digit mobile number");
                valid=1;
              }
            }
            else {
             $('#mobile-form').html("Please enter valid number");
                valid=1;
           }
      if(message==""){        
         $('#message-form').html("Please must enter a Message");
         valid=1;
      }else{
         $('#message-form').html("");
        
      }

      if(valid==0){
                   $.ajax({
                       url    : "{{url('/getenquiryform')}}",
                       type   : "POST",
   data   : {'name': name,'email':email,'mobile':mobile,'message':message, '_token': '{{ csrf_token() }}'},
                       success: function (result) {
                            if (result.status == "success") {
                              $('#demo').html('<div class="alert alert-success">Your Enquiry has been submitted Successfully.<button type="button" class="close" data-dismiss="alert">×</button></div>');
                              $('#name').val('');
                              $('#mobile').val('');
                              $('#email').val('');
                              $('#message').val('');

                           } else {
                               $('#demo').html('<div class="alert alert-danger">Your Enquiry has not been submitted. Please Try Again<button type="button" class="close" data-dismiss="alert">×</button></div>');
                           }
                       }
                   });
                }
            });
        });

</script>
 <script>
   // Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 3000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });

</script>
@endsection

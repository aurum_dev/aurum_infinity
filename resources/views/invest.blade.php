@extends('layout.appHome')

@section('content')
<script>
    function inpNum(e) {
  e = e || window.event;
  var charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
  var charStr = String.fromCharCode(charCode);
  if (!charStr.match(/^[0-9]+$/))
    e.preventDefault();
}

</script>
<style>

    .wraper_inner_banner {
    background-color: #f2f2f2;
    background-position: center top;
    background-image: url('../public/asset/login/wp-content/uploads/2018/07/Blog-Banner-Background-Image.png');
    background-size: cover;
}
h3{
    letter-spacing: 0 !important;
}
.form-control {
    display: block;
    width: 100%;
    height: 45px;
    padding: 6px 3px;
    font-size: 1em;
    line-height: 1.42857143;
    color: #555;
  
  
    -webkit-box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%);
    box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
#invoice {
    padding: 0px;
}

.invoice {
    position: relative;
    background-color: #FFF;
    min-height: 680px;
    padding: 15px
}

.invoice header {
    padding: 10px 0;
    margin-bottom: 20px;
    border-bottom: 1px solid #251d59
}

.invoice .company-details {
    text-align: right
}

.invoice .company-details .name {
    margin-top: 0;
    margin-bottom: 0
}

.invoice .contacts {
    margin-bottom: 20px !important;
}

.invoice .invoice-to {
    text-align: left
}

.invoice .invoice-to .to {
    margin-top: 0;
    margin-bottom: 0
}

.invoice .invoice-details {
    text-align: right
}

.invoice .invoice-details .invoice-id {
    margin-top: 0;
    color: #251d59
}

.invoice {
    padding-bottom: 50px
}

.invoice .thanks {
    margin-top: -100px;
    font-size: 2em;
    margin-bottom: 50px
}

.invoice .notices {
    padding-left: 6px;
    border-left: 6px solid #251d59;
    background: #e1e1e1;
    padding: 10px;
}

.invoice .notices .notice {
    font-size: 15px
}

.invoice table {
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
    margin-bottom: 20px
}

.invoice table td,
.invoice table th {
    padding: 15px;
    background: #eee;
    border-bottom: 1px solid #fff
}

.invoice table th {
    white-space: nowrap;
    font-weight: 400;
    font-size: 16px
}

.invoice table td h3 {
    margin: 0;
    font-weight: 400;
    color: #251d59;
    font-size: 1.2em
}

.invoice table .qty,
.invoice table .total,
.invoice table .unit {
    text-align: right;
    font-size: 1.2em
}

.invoice table .no {
    color: #fff;
    font-size: 1.6em;
    background: #251d59
}

.invoice table .unit {
    background: #ddd
}

.invoice table .total {
    background: #251d59;
    color: #fff
}

.invoice table tbody tr:last-child td {
    border: none
}

.invoice table tfoot td {
    background: 0 0;
    border-bottom: none;
    white-space: nowrap;
    text-align: right;
    padding: 10px 20px;
    font-size: 1.2em;
    border-top: 1px solid #aaa !important;
}

.invoice table tfoot tr:first-child td {
    border-top: none
}
.card {
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 0px solid rgba(0, 0, 0, 0);
    border-radius: .25rem;
    margin-bottom: 1.5rem;
    box-shadow: 0 2px 6px 0 rgb(218 218 253 / 65%), 0 2px 6px 0 rgb(206 206 238 / 54%);
}

.invoice table tfoot tr:last-child td {
    color: #251d59;
    font-size: 1.4em;
    border-top: 1px solid #251d59
}

.invoice table tfoot tr td:first-child {
    border: none
}

.invoice footer {
    width: 100%;
    text-align: center;
    color: #777;
    border-top: 1px solid #aaa;
    padding: 8px 0
}

@media print {
    .invoice {
        font-size: 11px !important;
        overflow: hidden !important
    }
    .invoice footer {
        position: absolute;
        bottom: 10px;
        page-break-after: always
    }
    .invoice>div:last-child {
        page-break-before: always
    }
}

.invoice .notices {
    padding-left: 6px;
    border-left: 6px solid #251d59;
    background: #e1e1e1;
    padding: 10px;
}
</style>
<div id="page" class="site">
            <!-- #content -->
            <div id="content" class="site-content">
               <div id="primary" class="content-area">
                  <main id="main" class="site-main">
                
                         
                      <div class="wraper_inner_banner">
   <div class="wraper_inner_banner_main">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="inner_banner_main">
                  <p class="title" style="text-align:left;font-size:36px;">
                     Apply For Investment
                  </p>
                  <p class="subtitle" style="text-align:left;font-size:16px;color:#fff !important;text-align: left;">
                  
                                
                                    Welcome Member, you now have access to greater financial details related to our offerings,<br> so you can make an informed investment decision. 
                                
                   
                  </p>
                   <p class="subtitle" style="text-align:left;font-size:15px;color:#fff !important;text-align: left;">
                  
                           
                                
                       <a href="{{url('/dashboard')}}" class="pro-breadcrumbs-item" style="color:#cbc6c6;font-style:italic">Home</a>
                <span>/</span>
                <a href="{{url('/propertyList')}}" class="pro-breadcrumbs-item" style="color:#cbc6c6;font-style:italic">Property List</a>
                <span>/</span>
                 <a href="{{url('/propertyDetail')}}/{{@$property->id}}" class="pro-breadcrumbs-item" style="color:#cbc6c6;font-style:italic">{{@$property->propertyName}}</a>
                <span>/</span>
                 <a href="#" class="pro-breadcrumbs-item" style="color:#cbc6c6;font-style:italic">Apply For Investment</a>
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>  
                     <div class="container page-container">
                        <article id="post-3351" class="post-3351 page type-page status-publish hentry">
                           <header class="entry-header">
                           </header>
                           <!-- .entry-header -->
                           <div class="entry-content">
                          
                           
                             
                              <div class="vc_row-full-width vc_clearfix"></div>
                              <section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section background-position-center-top position-relative home-thirteen-tablet-services vc_custom_1565259935698 vc_section-has-fill">
                                
              
                                 
                                 
                                 <div class="vc_row-full-width vc_clearfix"></div>
                                 <div class="vc_row wpb_row vc_row-fluid background-position-right home-thirteen-responsive home-thirteen-responsive-tablet vc_custom_1565174860866 vc_row-has-fill">
                                    <div class="wpb_column vc_column_container vc_col-sm-2">
                                       <div class="vc_column-inner vc_custom_1565169620144">
                                          <div class="wpb_wrapper"></div>
                                       </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-8">
                                       <div class="vc_column-inner vc_custom_1565169606522">
                                          <div class="wpb_wrapper">
                                             <h2 style="font-size: 35px;
    color: #000000;
    line-height: 48px;
    text-align: center;
    color: #251d59;
    letter-spacing: 0.03px;
" class="vc_custom_heading font-weight-bold vc_custom_1564996682116" ></h2>
                                             <div style="font-size: 15px;color: #000000;line-height: 28px;text-align: center" class="vc_custom_heading font-weight-regular vc_custom_1564996711569" ></div>
                                          </div>
                                       </div>
                                    </div>
                                   
                                 </div>
                                 <div class="vc_row wpb_row vc_row-fluid vc_custom_1533531188784">
   <div class="wpb_column vc_column_container vc_col-sm-12">
      <div class="vc_column-inner vc_custom_1533531180754">
         <div class="wpb_wrapper">
            <div class="wpb_raw_code wpb_content_element wpb_raw_html vc_custom_1608822318395">
               <div class="wpb_wrapper">
                  <div id="content" class="site-content">
                     <div id="primary" class="content-area">
                        <main id="main" class="site-main post-410 post type-post status-publish format-standard has-post-thumbnail hentry category-blogging category-remarketing category-seo category-social-media-marketing tag-business tag-corporate tag-marketing tag-seo">
                           <!-- wraper_blog_main -->
                           <div class="wraper_blog_main style-two">
                              <div class="container" style="padding-top:0 !important;">
                                 <!-- row -->
                                 








                                 <!-- row -->
                              </div>
                           </div>
                           <!-- wraper_blog_main -->
                        </main>
                        <!-- #main -->
                     </div>
                     <!-- #primary -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
                                 
                                 
                            
                              </section>
                             
                           
                              
                           </div>
                           <!-- .entry-content -->
                        </article>
                        <!-- #post-3351 -->             
                     </div>
                  </main>
                  <!-- #main -->
               </div>
               <!-- #primary -->
            </div>
            <!-- #content -->
         </div>


    <!-- Breadcrumb -->
    <div class="page-content">
        
        <!-- Property Tab Starts -->
        <div class="property-tab">
            <!-- Tab panes -->
            <div class="pro-content-tab-wrap p40">

                <div class="container" style="box-shadow:0px 4px 24px -1px rgb(0 0 0 / 10%);padding: 34px 38px 61px 35px;margin-bottom: 50px;">
                    <div class="tab-content">
         <div id="demo"></div>
                    <!-- Identity Tab Starts -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="wizard wizard1">

                                    <form role="form" action="{{url('payment-gateway')}}" method="POST" enctype="multipart/form-data" id="identity-form">
    </div>                                    @csrf()
    <?php
$totalsu=0;
                                        foreach($wallet as $index => $val){
                                            $totalsu+=$val->total_tokens;
                                        }

                                    ?>
                                    
                                        <div class="tab-content">
                                            <div class="tab-pane active" role="tabpanel" id="step1">
                                    <input type="hidden" id="totalsu" value="{{$totalsu}}">
                                    

                        <input type="hidden" id="totalsupply" value="{{ @$property->tokenSupply }}">
                                                <input type="hidden" id="token_id" name="token_id" value="{{@$property->id}}">
                                                <input type="hidden" id="minimuminvestmentid" name="" value="{{@$property->minimumInvest}}">
                                                <div class="row">
                                                    <div class="col-xs-12 form-group">
                                                        
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        
<div class="col-xs-12 col-sm-2">
    <img src="../{{@$property->tokenLogo }}" style="height:180px;" class="thumbnail"/>

</div>
<div class="col-xs-12 col-sm-6">
<p class="title" style="text-align:left;font-size:18px;color:#251d59;margin-bottom:5px;text-transform: uppercase;">
          <span style="font-size:15px;color:#929195;">Token Name :</span>      {{ @$property->tokenName }} </p>
   <p class="title" style="text-align:left;font-size:18px;color:#251d59;margin-bottom:5px;text-transform: uppercase;">   <span style="font-size:15px;color:#929195;">Property Name :</span>    
{{@$property->propertyName}} </p>

  <p class="title" style="text-align:left;font-size:18px;color:#251d59;margin-bottom:5px;text-transform: uppercase;">       <span style="font-size:15px;color:#929195;">Token Symbol :</span>
{{ @$property->tokenSymbol }} </p>

<p class="title" style="text-align:left;font-size:18px;color:#251d59;margin-bottom:5px;text-transform: uppercase;">    <span style="font-size:15px;color:#929195;">PAY BY :</span>   
INR </p>
      
<p class="title" style="text-align:left;font-size:17px;color:#251d59;margin-bottom:5px;text-transform:uppercase;">    <span style="font-size:15px;color:#929195;">PROPERTY STATUS : </span>        
{{@$property->status}} </p>

              </div><div class="col-xs-12 col-sm-4">
                <p class="title" style="text-align:left;font-size:20px;color:#5cb85c;margin-bottom:5px;text-transform: uppercase;">    <span style="font-size:18px;color:#929195;">Total Tokens :</span>{{ @$property->tokenSupply }}   
 </p>

         <?php
         $totaltoken=0;
$tmp = \App\AdminWallet::where('property_id',$property->id)->get();
foreach($tmp as $index => $val){
    $totaltoken+=$val->total_tokens;
}
$totaltoken2=0;
$tmp2 = \App\InvestorBonusToken::where('propertyid',$property->id)->get();
foreach($tmp2 as $index2 => $val2){
    $totaltoken2+=$val2->total_tokens;
}

?>

 <p class="title" style="text-align:left;font-size:20px;color:#d9534f;margin-bottom:5px;text-transform: uppercase;">    <span style="font-size:18px;color:#929195;">Balance Tokens : </span>{{ (@$property->tokenSupply-$totaltoken)-$totaltoken2 }}   
 </p>
 <p class="title" style="text-align:left;font-size:20px;color:#251d59;margin-bottom:5px;text-transform: uppercase;">    <span style="font-size:18px;color:#929195;">Minimum Investment : </span>{{ @$property->minimumInvest }}   
 </p>
          
<p class="title" style="text-align:left;font-size:17px;color:#251d59;margin-bottom:5px;">  <span style="font-size:15px;color:#929195;">PROPERTY : </span>             

<?php echo substr($property->propertyDesc,0,90); ?>&nbsp;&nbsp;
<a href='<?php echo url('/')."/propertyDetail/".$property->id; ?>' style="color:#154cc7;">READ MORE</a>
 </p>



              </div>


                                                    </div>
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="form-group">
                                                           
                                                            <input type="hidden" class="form-control" placeholder="Enter Token Name" name="tokenname" value="{{ @$property->tokenName }}" readonly/>
                                                        </div>
                                                        <div class="form-group">
                                                           
                                                            <input type="hidden" class="form-control" placeholder="Enter Token Name" name="tokenvalue" id="tokenvalue" value="{{ @$property->tokenValue }}" readonly/>
                                                        </div>
                                                        <div class="form-group">
                                                            
                                                            <input type="hidden" class="form-control" placeholder="Enter Token Symbol" name="tokensymbol" value="{{ @$property->tokenSymbol }}" readonly/>
                                                        </div>
                                                   
                                                       

                                                    </div>
                                                    <div class="col-xs-12 col-sm-6">
                                                         

                                                        <div class="form-group">
                                                            
                                                            <select name="payby" style="display:none;"  class="form-control payby">
                                                               <option value="INR">INR</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            
                                                            <input type="hidden" class="form-control" placeholder="Token Dividend" name="tokendivident" id="tokendivident" value="{{ @$property->Divident }}" readonly/>
                                                        </div>
                                                        
                                                       <!-- <div class="form-group">
                                                            <label class="control-label">Bonus Token Dividend</label>
                                                            <input type="number" readonly class="form-control" id="bonus_token" placeholder="Bonus Token Dividend" name="bonustokendivident" value="0"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Total Token</label>
                                                            <input type="text" class="form-control" name="totaltoken" id="total_token" placeholder="Total Token" value="0" readonly/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label">Pay Value</label>
                                                            <input type="text" class="form-control" name="payvalue" placeholder="Pay Value" id="amount" value="" required readonly/>
                                                        </div>-->
                                                    </div>

                                                    <div class="col-xs-12 col-sm-12">
     <div class="invoice">   

<input type="hidden" id="bonustoken" name="bonustoken"> 
<input type="hidden" id="tokenissueramt" name="tokenissueramt"> 
 <input type="hidden" id="payamount" name="payamount">
<table>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th class="text-left">DESCRIPTION</th>
                                       
                                        <th class="text-right">TOTAL</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <tr>
                                        <td class="no">01</td>
                                        <td class="text-left">
                                            <h3>NO OF TOKENS</h3>Creating a recognizable design solution based on the company's existing visual identity</td>
                                      
                                        <td class="total">
<?php $check="";
if($property->status=="soldout"){
$check="readonly";
}

?>
                                            <input type="text" style="text-align:right;background:transparent;color:#fff !important" required="required" {{$check}} class="form-control no-of-token" placeholder="NO OF TOKENS" onkeypress="inpNum(event)" name="nooftoken" value=""/></td>
                                       
                                    </tr>
                                   <!-- <tr>
                                        <td class="no">02</td>
                                        <td class="text-left">
                                            <h3>BONUS TOKEN DIVIDEND</h3>Developing a Content Management System-based Website</td>
                                      
                                        <td class="total" id="bonus_token">0
                                      </td>
                                    </tr>-->
                                     <tr>
                                        <td class="no">03</td>
                                        <td class="text-left">
                                            <h3>TOTAL TOKENS</h3>Developing a Content Management System-based Website</td>
                                      
                                        <td class="total" id="total_token">0</td>
                                    </tr>
                                   
                                </tbody>
                                <tfoot>
                                    <tr><td colspan="2">TOKEN VALUE (1 TOKEN)</td><td>&#8377; {{ @$property->tokenValue }}</td></tr>
                                    <tr><td colspan="2">SUBTOTAL</td><td id="subtotalamt">0

       
                                    </td></tr>

                                    <tr><td colspan="2">FEES (3%)</td><td id="payfees">0</td></tr>
                                    <tr>
                                        
                                        <td colspan="2">PAY VALUE</td>
                                        <td id="payamt">0
                                              

                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                            
<div class="notices"><div>NOTICE:</div><div class="notice">There is a 3% fee included in your token purchase transaction to cover administrative costs.<br>
By clicking the 'Buy Token' button I confirm that I have thoroughly read and I am of sound mind to comprehend the terms and conditions of this token purchase agreement.</div></div><br>
<div id="demo2"></div>


                        </div>   

                                                    </div>
                                                    
                                                   <?php
                                                  
if($property->status=="live"){
                                                    ?>
                                                    <div class="col-xs-12">
                                                        <ul class="list-inline pull-right">
                                                            <li>

                                                                 
                                                                <input type="submit" style="padding: 10px 25px;background-color: #251d59;border-color: #251d59;margin-left:8px;font-size: 19px;margin-top: 20px;" class="btn btn-primary btn1 btn2 mr-1 pay" value="Buy Token">
                                                            </li>
                                                        </ul>
                                                    </div>
<?php } ?>

                                                </div>

                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                 </form>
                                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


<script type="text/javascript">
     
       $(document).ready(function(){
      $(".pay").on("click", function () {
  
                 if($("#payamount").val() == ''){
            alert("Please Enter No. of Tokens");
            return false;
        }
        if($(".no-of-token").val() == ''){
            alert("Please Enter No. of Tokens");
             $('.no-of-token').val('');
                    
                     $('#bonustoken').val(''); 
                     $('#total_token').html('');
                     $('#subtotalamt').html('');
                     $('#tokenissueramt').val('');
                     $('#payfees').html('');
                      $('#payamt').html('');
                      $('#payamount').val('');
            return false;
        }
        var amount = $("#payamount").val();
        var name = "{{ Auth::user()->name }}";
        var key = "{{ getenv('RAZORPAY_KEY') }}";
        var image = "{{ asset('logo.png') }}";
        var token = "{{ csrf_token() }}";
        var token_id    = $('#token_id').val();
         var no_of_token = $('.no-of-token').val();
        var payment_id  = $('.payby').val();
        var payamount=$("#payamount").val();
        var bonustoken=$('#bonustoken').val();
        var tokenissueramt=$('#tokenissueramt').val(); 
        var tokensu='{{$totalsu}}';
        var tokensupply='{{ @$property->tokenSupply }}';
              /*  $.ajax({
                    url    : '{{ url("/storePayment") }}',
                    type   : "POST",
                   data: "_token="+token+"&razorpay_payment_id=111&totalAmount="+amount+"&token_id="+token_id+"&nooftoken="+no_of_token+"&payby="+payment_id+"&payamount="+payamount+"&bonustoken="+bonustoken+"&tokenissueramt="+tokenissueramt+"&tokensu="+tokensu+"&tokensupply="+tokensupply, 
                    success: function (result) {
                       // if (msg['status']=="success") {
                          //  $('#amount').val(result.token_equ_value);
                          //  $('#bonus_token').val(result.bonus_token);
                          //  $('#total_token').val(result.total_token);
                      //  } else {
                         //   $('#amount').val(0);
                      //  }
                    }
                });*/
});
       
        });
    </script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.all.min.js"></script> <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.min.css'></link>  
<style>
    
    .swal2-popup {
  font-size: 1.6rem !important;
  font-family: Georgia, serif;
}
</style>
<script>
$(document).ready(function(){
  $("button").click(function(){
    $("p").slideToggle();
  });
});
</script>


<script type="text/javascript">
     
       $(document).ready(function(){
       $(".payby, .no-of-token").on("change", function () {
   if($(".no-of-token").val() == ''){
            alert("Please Enter No. of Tokens");
             $('.no-of-token').val('');
                    
                     $('#bonustoken').val(''); 
                     $('#total_token').html('');
                     $('#subtotalamt').html('');
                     $('#tokenissueramt').val('');
                     $('#payfees').html('');
                      $('#payamt').html('');
                      $('#payamount').val('');
            return false;
        }
                var token_id    = $('#token_id').val();
                 var minimuminvestmentid    = $('#minimuminvestmentid').val();
  var tokensu='{{$totalsu}}';
  var tokensupply='{{ @$property->tokenSupply }}';

                var no_of_token = $('.no-of-token').val();
                if(no_of_token!=''){
                no_of_token=parseInt(no_of_token);
            }
if(parseInt(tokensu)<parseInt(tokensupply)){
      var tokencont=parseInt(tokensupply)-parseInt(tokensu);
      
      if(no_of_token<=tokencont){
                if(no_of_token>=minimuminvestmentid){
                                var payment_id  = $('.payby').val();
                                var divident=$('#tokendivident').val();
                                var tokenvalue=$('#tokenvalue').val();

                 //$('#nooftokens').html(no_of_token);
                 var bonus=(no_of_token*divident)/100;
                
                 $('#bonustoken').val(bonus); 
                 $('#total_token').html(no_of_token);
                 var amt=tokenvalue*no_of_token;
                 $('#subtotalamt').html("&#8377; "+amt);
                 $('#tokenissueramt').val(amt); 
                 var payfees=(amt*3)/100;
                 $('#payfees').html("&#8377; "+payfees);
                 var payamt=amt+payfees;
                 $('#payamt').html("&#8377; "+payamt);
                  $('#payamount').val(payamt);
                 }else if(no_of_token<minimuminvestmentid && tokencont<=no_of_token){
                                var payment_id  = $('.payby').val();
                                var divident=$('#tokendivident').val();
                                var tokenvalue=$('#tokenvalue').val();

                                 //$('#nooftokens').html(no_of_token);
                                 var bonus=(no_of_token*divident)/100;
                                
                                 $('#bonustoken').val(bonus); 
                                 $('#total_token').html(no_of_token);
                                 var amt=tokenvalue*no_of_token;
                                 $('#subtotalamt').html("&#8377; "+amt);
                                 $('#tokenissueramt').val(amt); 
                                 var payfees=(amt*3)/100;
                                 $('#payfees').html("&#8377; "+payfees);
                                 var payamt=amt+payfees;
                                 $('#payamt').html("&#8377; "+payamt);
                                  $('#payamount').val(payamt);
                 }else{
                  
                    alert("Minimum Token Investment should be "+minimuminvestmentid);
                     $('.no-of-token').val('');
                    
                     $('#bonustoken').val(''); 
                     $('#total_token').html('');
                     $('#subtotalamt').html('');
                     $('#tokenissueramt').val('');
                     $('#payfees').html('');
                      $('#payamt').html('');
                      $('#payamount').val('');
                     $("#demo").html('<div class="alert alert-danger"><button type="button" class="close" data-bs-dismiss="alert" onclick="closecta()">×</button>Minimum Token Investment should be '+minimuminvestmentid+'</div>');
                     $("#demo2").html('<div class="alert alert-danger"><button type="button" class="close" data-bs-dismiss="alert" onclick="closecta()">×</button>Minimum Token Investment should be '+minimuminvestmentid+'</div>');
                 }
        }else{
            swal({
    title: "Oops",
    text: "Balance Tokens are :"+tokencont+" Please purchase token from the balance tokens.",
    type: "error"
}).then(function() {
  //  window.location = "{{url('/dashboard')}}";
});

          //  alert();
                    $('.no-of-token').val('');

                  
                     $('#bonustoken').val(''); 
                     $('#total_token').html('');
                     $('#subtotalamt').html('');
                     $('#tokenissueramt').val('');
                     $('#payfees').html('');
                      $('#payamt').html('');
                      $('#payamount').val('');
                    $("#demo").html('<div class="alert alert-danger"><button type="button" class="close" data-bs-dismiss="alert" onclick="closecta()">×</button>Balance Tokens are'+tokencont+' Please purchase token from the balance tokens.</div>');
                    $("#demo2").html('<div class="alert alert-danger"><button type="button" class="close" data-bs-dismiss="alert" onclick="closecta()">×</button>Balance Tokens are '+tokencont+' Please purchase token from the balance tokens.</div>');

        }
}else{
         alert("Property Tokens has been sold out.Please Try for another Property");
                    $('.no-of-token').val('');                    
                    
                     $('#bonustoken').val(''); 
                     $('#total_token').html('');
                     $('#subtotalamt').html('');
                     $('#tokenissueramt').val('');
                     $('#payfees').html('');
                      $('#payamt').html('');
                      $('#payamount').val('');
                    $("#demo2").html('<div class="alert alert-danger"><button type="button" class="close" data-bs-dismiss="alert" onclick="closecta()">×</button> Tokens has been sold out for this property.Please Try for another Property</div>');
                      $("#demo").html('<div class="alert alert-danger"><button type="button" class="close" data-bs-dismiss="alert" onclick="closecta()">×</button> Tokens has been sold out for this property.Please Try for another Property</div>');
}
                /*$.ajax({
                    url    : "{{url('/gettokeninvestvalue')}}",
                    type   : "POST",
                    data   : {'token_id': token_id, 'no_of_token': no_of_token, 'payment_id': payment_id, '_token': '{{ csrf_token() }}'},
                    success: function (result) {
                        if (result.status == 1) {
                            $('#amount').val(result.token_equ_value);
                            $('#bonus_token').val(result.bonus_token);
                            $('#total_token').val(result.total_token);
                        } else {
                            $('#amount').val(0);
                        }
                    }
                });*/
});
       
        });
    </script>
                                     
                                </div>
                            </div>
                        </div>
                        <!-- Identity Tab Ends -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Property Tab Ends -->
@endsection
@section('scripts')
 

    
@endsection

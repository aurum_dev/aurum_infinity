@extends('layout.appHome')

@section('content')
<style>
                       .progress-bar-info{
                        background-color:#05bd8e !important;
                       }
.progress
{
   height: 10px !important;
   margin-top: 0 !important;
}
.post.style-two .entry-main {
    padding: 10px !important;
}
.wraper_inner_banner_main > .container {
    padding-top: 90px;
    padding-bottom: 50px !important;
}
                    </style> 
                    <style>
 

/* ================== Badge Overlay CSS ========================*/
.badge-overlay {
    position: absolute;
    left: -10px;
    top: 1px;
    width: 100%;
    height: 100%;
    overflow: hidden;
    pointer-events: none;
    z-index: 100;
    -webkit-transition: width 1s ease, height 1s ease;
    -moz-transition: width 1s ease, height 1s ease;
    -o-transition: width 1s ease, height 1s ease;
    transition: width 0.4s ease, height 0.4s ease
}

/* ================== Badge CSS ========================*/
.badge {
    margin: 0;
    padding: 0;
    color: white;
    padding: 10px 53px;
    font-size: 15px;
    font-family: Arial, Helvetica, sans-serif;
    text-align: center;
    line-height: normal;
    text-transform: uppercase;
    background: #ed1b24;
}

.badge::before, .badge::after {
    content: '';
    position: absolute;
    top: 0;
    margin: 0 -1px;
    width: 100%;
    height: 100%;
    background: inherit;
    min-width: 55px
}

.badge::before {
    right: 100%
}

.badge::after {
    left: 100%
}

/* ================== Badge Position CSS ========================*/


.top-right {
    position: absolute;
    top: 0;
    right: 0;
    -ms-transform: translateX(30%) translateY(0%) rotate(45deg);
    -webkit-transform: translateX(30%) translateY(0%) rotate(45deg);
    transform: translateX(30%) translateY(0%) rotate(45deg);
    -ms-transform-origin: top left;
    -webkit-transform-origin: top left;
    transform-origin: top left;
}

.bottom-full {
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    text-align: center;
}

/* ================== Badge color CSS ========================*/
.badge.red {
    background: #ed1b24;
    border-radius: 0;
}

.badge.orange {
    background: #008800;
     border-radius: 0;
}

.badge.pink {
    background: #ee2b8b;
     border-radius: 0;
}

.badge.blue {
    background: #00adee;
     border-radius: 0;
}

.badge.green {
    background: #b4bd00;
}


</style>

<div id="page" class="site">
            <!-- #content -->
            <div id="content" class="site-content">
               <div id="primary" class="content-area">
                  <main id="main" class="site-main">
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

                         
                      <div class="">
   <div class="wraper_inner_banner_main">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="inner_banner_main">
                  <p class="title" style="text-align:center;color: #232F3E;font-size:36px;font-family: 'Public Sans', sans-serif;font-weight: bold;padding-bottom:30px;">
                     FEATURED PROPERTIES
                  </p>
             
               </div>
            </div>
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background:#FBFAF5;padding:40px;">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
               <div class="inner_banner_main">
                  
                  <div class="subtitle" style="">
         <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">         
<input id="City" style="font-family: 'Public Sans', sans-serif,FontAwesome;margin-top:0px;background-color: #f5f4f4 !important;     padding-top: 20px !important;padding-right: 40px !important; padding-bottom: 20px !important; padding-left: 10px !important;border-radius: 4px;" type="text"  placeholder="&#xf041 City..." class="form-control mainLoginInput" name="city" required>



                  </div>
               </div>
            </div>
               <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
               <div class="inner_banner_main">
                 
                  <div class="subtitle" style="">
               
<input id="Assets" style="font-family: 'Public Sans', sans-serif,FontAwesome;margin-top:0px;background-color: #f5f4f4 !important;     padding-top: 20px !important;padding-right: 40px !important; padding-bottom: 20px !important; padding-left: 10px !important;border-radius: 4px;" type="text"  placeholder="&#xf015 Assets..." class="form-control mainLoginInput" name="password" required>



                  </div>
               </div>
            </div>
               <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
               <div class="inner_banner_main">
                 
                  <div class="subtitle" >
         <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">         
<input id="password" style="font-family: 'Public Sans', sans-serif,FontAwesome;margin-top:0px;background-color: #f5f4f4 !important;     padding-top: 20px !important;padding-right: 40px !important; padding-bottom: 20px !important; padding-left: 10px !important;border-radius: 4px;" type="text"  placeholder="&#xf02b Price..." class="form-control mainLoginInput" name="password" required>



                  </div>
               </div>
            </div>
             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
               <div class="inner_banner_main">
                  <button style="border-radius:5px !important;color: #ffffff; line-height: 20px; font-size: 14px;margin-top:60px;width:100%;border:0;background: #05bd8e !important;" class="radiantthemes-custom-button-main  vc_custom_1564647891751">
                                                   <div class="placeholder" style="font-size:16px;text-transform: uppercase;">SEARCH</div>
                                              </button>

               </div></div>
</div>

         </div>
      </div>
   </div>
</div>  
                     <div class="container page-container">
                        <article id="post-3351" class="post-3351 page type-page status-publish hentry">
                           <header class="entry-header">
                           </header>
                           <!-- .entry-header -->
                           <div class="entry-content">
                          
                              
                              <section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section background-position-center-top position-relative home-thirteen-tablet-services vc_custom_1565259935698 vc_section-has-fill">
                                
                                 <div class="vc_row-full-width vc_clearfix"></div>
                                
                                
                                 
                                 
                                 
                                 
                                 
                                 <div class="vc_row-full-width vc_clearfix"></div>
                        
                                 <div class="vc_row wpb_row vc_row-fluid vc_custom_1533531188784">
   <div class="wpb_column vc_column_container vc_col-sm-12">
      <div class="vc_column-inner vc_custom_1533531180754">
         <div class="wpb_wrapper">
            <div class="wpb_raw_code wpb_content_element wpb_raw_html vc_custom_1608822318395">
               <div class="wpb_wrapper">
                  <div id="content" class="site-content">
                     <div id="primary" class="content-area">
                        <main id="main" class="site-main post-410 post type-post status-publish format-standard has-post-thumbnail hentry category-blogging category-remarketing category-seo category-social-media-marketing tag-business tag-corporate tag-marketing tag-seo">
                           <!-- wraper_blog_main -->
                           <div class="wraper_blog_main style-two">
                              <div class="container" style="padding-top:0 !important;">
                                 <!-- row -->
                                 <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <!-- blog_main -->
                                       <div class="blog_main">
                                          <div class="row isotope-blog-style" style="position: relative; height: 536.562px;">
                                          

 @foreach ($property as $key => $value)

<?php 
$totaltoken=0;
foreach ($wallet as $key => $value1){
   if($value1->property_id==$value->id && $value1->status==1){
      $totaltoken+=$value1->total_tokens;
   }
}

?>




     
                                             <div class="isotope-blog-style-item col-lg-6 col-md-5 col-sm-6 col-xs-12" style="position: absolute; left: 0px; top: 0px;">
                                                <article id="post-410" class="style-two post-410 post type-post status-publish format-standard has-post-thumbnail hentry category-blogging category-remarketing category-seo category-social-media-marketing tag-business tag-corporate tag-marketing tag-seo">
<?php 

$gallery=explode("#",$value->galleryImage);

?>  
                             
                                                   <div class="post-thumbnail"><a href="<?php echo "propertyDetail/".$value->id; ?>"><img src="{{$gallery[0]}}" class="attachment-full size-full wp-post-image" alt="Blog-Image-001"  sizes="(max-width: 1176px) 100vw, 1176px" style="height:360px;" data-no-retina="" width="1176" height="784">           </a></div>
                                                   <!-- .post-thumbnail -->
                                                   <div class="entry-main" style="height:479px;">
                                                      <header class="entry-header">
                                                     
                                                         <h4 style="font-size: 24px;
    color: #040B21; line-height: 48px; color: #232F3E !important; letter-spacing: 0.03px;
    font-family: 'Public Sans', sans-serif; font-weight: bold;margin-bottom:0"><a href="" style="font-size: 24px;
    color: #040B21; line-height: 48px; color: #232F3E !important; letter-spacing: 0.03px;
    font-family: 'Public Sans', sans-serif; font-weight: bold;">{{ @$value->propertyName }}</a></h4>
                                                       <p style="font-size:15px;font-family: 'Public Sans', sans-serif;color:#040B21;font-weight:600;">{{ @$value->propertyCity }}   </p>
                                                      
<span class="progress-txt" style="color:#172243"><b style="font-weight:500;" style="color:#172243">{{ ($totaltoken*100)/$value->tokenSupply}}% FUNDED</b>&nbsp;&nbsp;   {{$totaltoken}}/{{ @$value->tokenSupply }}</span>
<div class="progress">
    <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="{{ ($totaltoken*100)/$value->tokenSupply}}" aria-valuemin="0" aria-valuemax="100" style="
    width: <?php echo ($totaltoken*100)/$value->tokenSupply;?>%">
    
    </div>
  </div>
                                                     
                                                     
                                                      </header>
                                                      
                                                      <!-- .entry-header -->
                                                      <div class="entry-content">
                                                    <div style="width:100%;height:52px;">
@if($value->trusted==1)
   <a href="javascript:void(0);" class="btn btn-primary" style="background-color: #d9f4ed;border-color: #d9f4ed;color: #040B21;font-size:13px;padding:0 7px;margin-left:-1px;margin-top:20px;margin-bottom:20px;font-family: 'Public Sans', sans-serif;cursor:default !important; "><i class="fa fa-lock" style="color:#05bd8e;"></i> Trusted</a>
@endif
@if($value->highgrowth==1)
<a href="javascript:void(0);" class="btn btn-primary" style="background-color: #f5edd6;border-color: #f5edd6;font-family: 'Public Sans', sans-serif;color: #040B21;margin-left:-1px;margin-top:20px;margin-bottom:20px;font-size:13px;padding:0 7px;cursor:default !important;"><i class="fa fa-bar-chart" style="color:#05bd8e;"></i> High Growth</a>
@endif
@if($value->office==1)
<a href="javascript:void(0);" class="btn btn-primary" style="background-color: #d9f4ed;border-color: #d9f4ed;font-family: 'Public Sans', sans-serif;color: #040B21;margin-left:-1px;margin-top:20px;margin-bottom:20px;font-size:13px;padding:0 7px;cursor:default !important;"><i class="fa fa-building-o" style="color:#05bd8e;"></i> Office</a>
@endif
@if($value->gradea==1)
<a href="javascript:void(0);" class="btn btn-primary" style="background-color: #f5edd6;border-color: #f5edd6;font-family: 'Public Sans', sans-serif;color: #040B21;margin-top:20px;font-size:13px;margin-bottom:20px;padding:0 7px;margin-left:-1px;cursor:default !important;"><i class="fa fa-star" style="color:#05bd8e;"></i> Grade A</a>

@endif

<br>


<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script> 
<link rel="stylesheet" href="https://site-assets.fontawesome.com/releases/v6.1.1/css/all.css">
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:10px;padding:0;">
                  
                 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="padding:0;">                 
                    <i class="fa fa-building" style="color:#B9983D;font-size: 30px;padding-top: 0px;"></i>
                 </div>                  
                 <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" style="padding:0;">                 
                   <p style="font-family: 'Public Sans', sans-serif;color: #686F6F;font-size:13px;margin-bottom:0;padding-left:10px;margin-top:-5px;"> Asset Type</p>
                   <p style="font-family: 'Public Sans', sans-serif;color: #172243;font-size:15px;padding-left:10px;margin-top:-5px;font-weight:600;">{{ @$value->propertyType }}</p>
                 </div>      

            </div>
           
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:10px;padding:0;">
                  
                 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="padding:0;">                 
                    <i class="fas fa-coins" style="color:#B9983D;font-size: 30px;padding-top: 0px;"></i>
                 </div>                  
                 <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" style="padding:0;">                 
                   <p style="font-family: 'Public Sans', sans-serif;color: #686F6F;font-size:13px;margin-bottom:0;padding-left:10px;margin-top:-5px;"> Total Tokens</p>
                   <p style="font-family: 'Public Sans', sans-serif;color: #172243;font-size:15px;padding-left:10px;margin-top:-5px;font-weight:600;">{{ @$value->tokenSupply }} Tokens</p>
                 </div>      

            </div> 
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:10px;padding:0;">
                  
                 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="padding:0;">        
                    <i class="fas fa-sack" style="color:#B9983D;font-size: 30px;padding-top: 0px;"></i>


                 </div>                  
                 <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" style="padding:0;">                 
                   <p style="font-family: 'Public Sans', sans-serif;color: #686F6F;font-size:13px;margin-bottom:0;padding-left:10px;margin-top:-5px;"> Min Investment</p>
                   <p style="font-family: 'Public Sans', sans-serif;color: #172243;font-size:15px;padding-left:10px;margin-top:-5px;font-weight:600;">{{ @$value->minimumInvest }} (in Tokens)</p>
                 </div>      

            </div> 

           
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:10px;padding:0;">
                  
                 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="padding:0;">        
                    <i class="fas fa-badge-percent" style="color:#B9983D;font-size: 30px;padding-top: 0px;"></i>


                 </div>                  
                 <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" style="padding:0;">                 
                   <p style="font-family: 'Public Sans', sans-serif;color: #686F6F;font-size:13px;margin-bottom:0;padding-left:10px;margin-top:-5px;"> Expected IRR</p>
                   <p style="font-family: 'Public Sans', sans-serif;color: #172243;font-size:15px;padding-left:10px;margin-top:-5px;font-weight:600;">{{ @$value->expectedIr }}% IRR</p>
                 </div>      

            </div> 

             <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12" style="margin-top:10px;padding:0;">
                  
                 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="padding:0;">        
          <style>
             .fa-border{
               background:#B9983D;
               padding: 0.2em 9px 0.15em !important;
    border: solid 0.08em #B9983D !important;
    border-radius: 14px !important;
             }


          </style>         
                     <i class='fas fa-indian-rupee-sign fa-border' style="font-size: 20px;
    padding-top: 4px;
    color: #fff;"></i>


                 </div>                  
                 <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" style="padding:0;">                 
                   <p style="font-family: 'Public Sans', sans-serif;color: #686F6F;font-size:13px;margin-bottom:0;margin-top:-5px;"> Token Value</p>
                   <p style="font-family: 'Public Sans', sans-serif;color: #172243;font-size:15px;margin-top:-5px;font-weight:600;">&#8377; {{ @$value->tokenValue }} (1 Token Value)</p>
                 </div>      

            </div> 
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:10px;padding:0;">
               
               <div class="radiantthemes-custom-button hover-style-five rt810073130  display-inline-block" data-button-direction="center" data-button-fullwidth="false">
                                                <a style=" color: #ffffff; line-height: 25px; font-size: 14px;background:#05bd8e;padding:10px 27px 10px 27px !important;margin-right: 0 !important;border-radius:4px !important" class="radiantthemes-custom-button-main  vc_custom_1564647891751" href="" title="" target="_self">
                                                   <div class="placeholder" style="
                                                   font-size: 15px; text-transform: uppercase;
    font-family: 'Public Sans', sans-serif; font-weight: 600;">EXPLORE MORE</div>
                                                </a>
                                                
  
                                             </div>
            </div>
             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:10px;padding:0;">
               


<div class="radiantthemes-custom-button hover-style-five rt810073130  display-inline-block" data-button-direction="center" data-button-fullwidth="false">
                                                <a style=" color: #05bd8e;border: #05bd8e 1px solid; line-height: 25px; font-size: 14px;background:#ffffff !important;margin-right: 0 !important;border-radius:4px !important;padding:9px 40px 9px 40px !important;" class="radiantthemes-custom-button-main  vc_custom_1564647891751" href="" title="" target="_self">
                                                   <div class="placeholder" style="
                                                   font-size: 14px; text-transform: uppercase;
    font-family: 'Public Sans', sans-serif; font-weight: 600;">INVEST NOW</div>
                                                </a>
                                                
  
                                             </div>

            </div>




</div>

                                                        
                                                      </div>
                                                      <!-- .entry-content -->
                                                      
                                                      <!-- .post-meta -->
                                                   </div>
                                                   <!-- .entry-main -->
                                                </article>
                                                <!-- #post-## -->
                                             </div>
                                            
                                        
                                           @endforeach
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          </div>
                                          
                                       </div>
                                       <!-- blog_main -->
                                    </div>
                                 </div>
                                 <!-- row -->
                              </div>
                           </div>
                           <!-- wraper_blog_main -->
                        </main>
                        <!-- #main -->
                     </div>
                     <!-- #primary -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
                                 
                                 
                            
                              </section>
                             
                                 
                                 
                                 
                                  <div class="vc_row-full-width vc_clearfix"></div>
                                
                                 
                                
                                
                                

                    
                    
    

                                   
                                 
                              
                              
                              
                              
                           </div>
                           <!-- .entry-content -->
                        </article>
                        <!-- #post-3351 -->             
                     </div>
                  </main>
                  <!-- #main -->
               </div>
               <!-- #primary -->
            </div>
            <!-- #content -->
         </div>


@endsection

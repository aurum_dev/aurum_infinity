@extends('layout.appHome')

@section('content')
 <style>
                       .progress-bar-info{
                        background-color: #05bd8e !important;
                       }
.progress
{
   height: 10px !important;
}

                    </style> 
<div id="page" class="site">
            <!-- #content -->
            <div id="content" class="site-content">
               <div id="primary" class="content-area">
                  <main id="main" class="site-main">
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

                         
                        
                     <div class="container page-container">
                        <article id="post-3351" class="post-3351 page type-page status-publish hentry">
                           <header class="entry-header">
                           </header>
                           <!-- .entry-header -->
                           <div class="entry-content">
                          
                              <section data-vc-full-width="true" style="" id="tophomebannerclass" data-vc-full-width-init="false" class="vc_section visible-lg visible-md visible-sm hidden-xs vc_custom_1565246688063 vc_section-has-fill">
                                 <div class="vc_row wpb_row vc_row-fluid background-position-left-bottom home-thirteen-tab-banner vc_custom_1565260402984 vc_row-has-fill">
                                    <div class="wpb_column vc_column_container vc_col-sm-7">
                                       <div class="vc_column-inner vc_custom_1564979830197">
                                          <div class="wpb_wrapper">
                                             <h1 style="font-size: 36px;color: #172243;line-height: 51px;text-align: left;font-family: 'Public Sans', sans-serif;font-weight: bold;" class="vc_custom_heading font-weight-bold vc_custom_1565266792852" >Transforming real estate ownership through asset tokenisation.</h1>
                                             <div style="font-size: 28px;color: #555D5D;line-height: 41px;text-align: left;font-family: 'Public Sans', sans-serif;font-weight: 600;" class="vc_custom_heading font-weight-regular vc_custom_1565155090818" >Invest in fractional ownership for <br>as low as <i class="fa fa-rupee"></i>5 lakhs.</div>
                                             <!-- radiantthemes-custom-button -->
                                             <div class="radiantthemes-custom-button hover-style-five rt810073130  display-inline-block"  data-button-direction="center" data-button-fullwidth="false" >
                                                <a style=" color: #ffffff; line-height: 25px; font-size: 14px;background:#05bd8e;border-radius:4px !important" class="radiantthemes-custom-button-main  vc_custom_1564647891751" href="{{url('/login')}}"  title="" target="_self">
                                                   <div class="placeholder" style="
                                                   font-size: 17px; text-transform: uppercase;
    font-family: 'Public Sans', sans-serif; font-weight: 600;">Explore the Process</div>
                                                </a>
                                                
  
                                             </div>
                                             
                                             
                                             
                                             
                                             
 

                              
                                      
                                             
                                             
                                             
                                             
                                             
                                             
                                             <!-- radiantthemes-custom-button -->
                                          </div>
                                       </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-6">
                                       <div class="vc_column-inner vc_custom_1554094596602">
                                          <div class="wpb_wrapper"></div>
                                       </div>
                                    </div>
                                 </div>
                              </section>
                              <div class="vc_row-full-width vc_clearfix"></div>
                              <section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section hidden-lg hidden-md hidden-sm visible-xs vc_custom_1565247517594">
                                 <div class="vc_row wpb_row vc_row-fluid vc_custom_1565258221905 vc_row-has-fill">
                                    <div class="wpb_column vc_column_container vc_col-sm-5">
                                       <div class="vc_column-inner vc_custom_1564637164082">
                                          <div class="wpb_wrapper">
                                             <h1 style="font-size: 35px;color: #000000;line-height: 48px;text-align: left" class="vc_custom_heading font-weight-bold vc_custom_1565266816417" >Rank your Site at from other</h1>
                                             <div style="font-size: 15px;color: #767676;line-height: 24px;text-align: left" class="vc_custom_heading font-weight-regular vc_custom_1565155053399" >Aliquam at pretium eros. Vivamus placerat sem id mauris laoreet, vitae msan eros pulvinar. Duis aliquet est risus.</div>
                                             <!-- radiantthemes-custom-button -->
                                             <div class="radiantthemes-custom-button hover-style-five rt172900896  display-inline-block"  data-button-direction="center" data-button-fullwidth="false" >
                                               <!-- <a style=" color: #ffffff; line-height: 25px; font-size: 14px;" class="radiantthemes-custom-button-main  vc_custom_1564034074121" href="#"  title="" target="_self">
                                                   <div class="placeholder">Discover More</div>
                                                </a>-->
                                             </div>
                                             <!-- radiantthemes-custom-button -->
                                          </div>
                                       </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-7">
                                       <div class="vc_column-inner vc_custom_1554094596602">
                                          <div class="wpb_wrapper"></div>
                                       </div>
                                    </div>
                                 </div>
                              </section>
                              <div class="vc_row-full-width vc_clearfix"></div>
                              <section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section background-position-center-top position-relative home-thirteen-tablet-services vc_custom_1565259935698 vc_section-has-fill">
                                 <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid home-thirteen-services-upper-space vc_custom_1565259781064 vc_row-has-fill vc_column-gap-30 vc_row-o-content-middle vc_row-flex">
                                    
                                    <div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12 vc_col-xs-12">
                                       <div class="vc_column-inner vc_custom_1565260220932">
                                          <div class="wpb_wrapper">
                                             <h2 style="font-size: 25px !important;
    color: #232F3E !important;
    line-height:50px !important;
    text-align: left !important;
    text-transform: uppercase;
    font-family: 'Public Sans', sans-serif;
    font-weight: bold;letter-spacing: 0.03px;text-align:center !important;font-family: 'Public Sans', sans-serif; font-weight: 600;" class="vc_custom_heading font-weight-regular title-letter-spacing vc_custom_1564561171454" >Enter the next big market</h2>
                                            
                                             <div style="font-size: 16px;color: #000000;line-height: 28px;text-align: center;font-family: 'Public Sans', sans-serif;
    font-weight: 500;" class="vc_custom_heading font-weight-regular home-thirteen-para-right-align vc_custom_1565157773347" ><b>Aurum Infinity</b> brings to you the future of Real Estate ownership through blockchain-based asset tokenization. Experience the edge of tech-enabled real estate investment that empowers you with transparency, flexibility and diversification. Get high returns with the benefits of real estate asset ownership, rental yield, and capital appreciation.</div>
                                            

                                          <center>  <div class="radiantthemes-custom-button hover-style-five rt810073130  display-inline-block" data-button-direction="center" data-button-fullwidth="false">
                                                <a style=" color: #ffffff; line-height: 25px; font-size: 14px;background:#05bd8e;border-radius:4px !important" class="radiantthemes-custom-button-main  vc_custom_1564647891751" href="https://localhost/aurum_laravel/login" title="" target="_self">
                                                   <div class="placeholder" style="
                                                   font-size: 17px; text-transform: uppercase;
    font-family: 'Public Sans', sans-serif; font-weight: 600;">INVEST NOW</div>
                                                </a>
                                                
  
                                             </div></center>
                                        <!-- <div class="radiantthemes-custom-button hover-style-five rt537002223  display-inline-block"  data-button-direction="center" data-button-fullwidth="false" >
                                                <a style=" color: #ffffff; line-height: 25px; font-size: 14px;border: 2px solid #251d59 !important;" class="radiantthemes-custom-button-main  vc_custom_1564034074121" href="#"  title="" target="_self">
                                                   <div class="placeholder" style="font-size:16px;">Discover More&nbsp;&nbsp;<i class="fa fa-long-arrow-right" style="font-size:21px;vertical-align:middle"></i></div>
                                                </a>
                                                
                                             </div>-->
                                             <!-- radiantthemes-custom-button -->
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                     
                                 
                                 
                                 <div class="vc_row-full-width vc_clearfix"></div>
                              <div class="vc_row wpb_row vc_row-fluid background-position-right home-thirteen-responsive home-thirteen-tablet-case-study vc_custom_1565175457236 vc_row-has-fill" style="margin-top: 50px !important;">
                                 <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner vc_custom_1564128339714">
                                       <div class="wpb_wrapper">
                                        
                                          <h2 style="font-size: 25px;
    color: #000000;
    line-height: 48px;
    text-align: center;
    color: #232F3E !important;
    letter-spacing: 0.03px;
    font-family: 'Public Sans', sans-serif;
    font-weight: 600;
" class="vc_custom_heading font-weight-bold vc_custom_1564996682116">GRADED PROPERTY LISTING</h2>
                                          <div class="rt-case-studies-slider element-one owl-carousel " data-case-studies-loop="true" data-case-studies-autoplay="true" data-case-studies-autoplaytimeout="6000" data-case-studies-desktopitem="3" data-case-studies-tabitem="1" data-case-studies-mobileitem="1">




@foreach ($property->slice(0, 6) as $key => $value)


<?php 
$totaltoken=0;
foreach ($wallet as $key => $value1){
   if($value1->property_id==$value->id && $value1->status==1){
    //if($value1->property_id==$value->id){
      $totaltoken+=$value1->total_tokens;
   }
}

?>
 <?php 

$gallery=explode("#",$value->galleryImage);

?>   



                                             <div class="rt-case-studies-slider-item" style="border:#ebebeb 1px solid;padding:0 !important">
                                                <div class="holder" style="text-align: left !important;">
                                                   <div class="pic" style="margin-bottom:10px;">
                                                      <div class="holder" style="background-image:url('{{$gallery[0]}}')"></div>
                                                      <a class="overlay" href="{{url('/login')}}"></a>
                                                   </div>
                                                   <div class="data" style="    padding: 0 8px;">
                                                      <h4 style="font-size: 24px;
    color: #040B21; line-height: 48px; color: #232F3E !important; letter-spacing: 0.03px;
    font-family: 'Public Sans', sans-serif; font-weight: bold;margin-bottom:0"><a href="" style="font-size: 24px;
    color: #040B21; line-height: 48px; color: #232F3E !important; letter-spacing: 0.03px;
    font-family: 'Public Sans', sans-serif; font-weight: bold;">{{ @$value->propertyName }}</a></h4>
<p style="font-size:15px;font-family: 'Public Sans', sans-serif;color:#040B21;font-weight:600;">{{ @$value->propertyCity }}   </p>
 <span class="progress-txt" style="color:#172243"><b style="font-weight:500;" style="color:#172243">{{ ($totaltoken*100)/$value->tokenSupply}}% FUNDED</b>&nbsp;&nbsp;   {{$totaltoken}}/{{ @$value->tokenSupply }}</span>
 <div class="progress" style="margin-top:0 !important;">
    <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="{{ ($totaltoken*100)/$value->tokenSupply}}" style="
    width: <?php echo ($totaltoken*100)/$value->tokenSupply;?>%">
    
    </div>
  </div>

@if($value->trusted==1)
<a href="javascript:void(0);" class="btn btn-primary" style="background-color: #d9f4ed;border-color: #d9f4ed;color: #040B21;font-size:13px;padding:0 7px;margin-left:-1px;margin-top:20px;margin-bottom:20px;font-family: 'Public Sans', sans-serif;cursor:default !important; "><i class="fa fa-lock" style="color:#05bd8e;"></i> Trusted</a>
@endif
@if($value->highgrowth==1)
<a href="javascript:void(0);" class="btn btn-primary" style="background-color: #f5edd6;border-color: #f5edd6;font-family: 'Public Sans', sans-serif;color: #040B21;margin-left:-1px;margin-top:20px;margin-bottom:20px;font-size:13px;padding:0 7px;cursor:default !important;"><i class="fa fa-bar-chart" style="color:#05bd8e;"></i> High Growth</a>
@endif
@if($value->office==1)
<a href="javascript:void(0);" class="btn btn-primary" style="background-color: #d9f4ed;border-color: #d9f4ed;font-family: 'Public Sans', sans-serif;color: #040B21;margin-left:-1px;margin-top:20px;margin-bottom:20px;font-size:13px;padding:0 7px;cursor:default !important;"><i class="fa fa-building-o" style="color:#05bd8e;"></i> Office</a>
@endif
@if($value->gradea==1)
<a href="javascript:void(0);" class="btn btn-primary" style="background-color: #f5edd6;border-color: #f5edd6;font-family: 'Public Sans', sans-serif;color: #040B21;margin-top:20px;font-size:13px;margin-bottom:20px;padding:0 7px;margin-left:-1px;cursor:default !important;"><i class="fa fa-star" style="color:#05bd8e;"></i> Grade A</a>
@endif
<br>
 <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script> 
<link rel="stylesheet" href="https://site-assets.fontawesome.com/releases/v6.1.1/css/all.css">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:10px;padding:0;">
                  
                 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="padding:0;">                 
                    <i class="fa fa-building" style="color:#B9983D;font-size: 30px;padding-top: 0px;"></i>
                 </div>                  
                 <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" style="padding:0;">                 
                   <p style="font-family: 'Public Sans', sans-serif;color: #686F6F;font-size:13px;margin-bottom:0;padding-left:10px;margin-top:-5px;"> Asset Type</p>
                   <p style="font-family: 'Public Sans', sans-serif;color: #172243;font-size:15px;padding-left:10px;margin-top:-5px;font-weight:600;">{{ @$value->propertyType }}</p>
                 </div>      

            </div>
           
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:10px;padding:0;">
                  
                 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="padding:0;">                 
                    <i class="fas fa-coins" style="color:#B9983D;font-size: 30px;padding-top: 0px;"></i>
                 </div>                  
                 <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" style="padding:0;">                 
                   <p style="font-family: 'Public Sans', sans-serif;color: #686F6F;font-size:13px;margin-bottom:0;padding-left:10px;margin-top:-5px;"> Total Tokens</p>
                   <p style="font-family: 'Public Sans', sans-serif;color: #172243;font-size:15px;padding-left:10px;margin-top:-5px;font-weight:600;">{{ @$value->tokenSupply }} Tokens</p>
                 </div>      

            </div> 
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:10px;padding:0;">
                  
                 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="padding:0;">        
                    <i class="fas fa-sack" style="color:#B9983D;font-size: 30px;padding-top: 0px;"></i>


                 </div>                  
                 <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" style="padding:0;">                 
                   <p style="font-family: 'Public Sans', sans-serif;color: #686F6F;font-size:13px;margin-bottom:0;padding-left:10px;margin-top:-5px;"> Min Investment</p>
                   <p style="font-family: 'Public Sans', sans-serif;color: #172243;font-size:15px;padding-left:10px;margin-top:-5px;font-weight:600;">{{ @$value->minimumInvest }} (in Tokens)</p>
                 </div>      

            </div> 

           
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:10px;padding:0;">
                  
                 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="padding:0;">        
                    <i class="fas fa-badge-percent" style="color:#B9983D;font-size: 30px;padding-top: 0px;"></i>


                 </div>                  
                 <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" style="padding:0;">                 
                   <p style="font-family: 'Public Sans', sans-serif;color: #686F6F;font-size:13px;margin-bottom:0;padding-left:10px;margin-top:-5px;"> Expected IRR</p>
                   <p style="font-family: 'Public Sans', sans-serif;color: #172243;font-size:15px;padding-left:10px;margin-top:-5px;font-weight:600;">{{ @$value->expectedIr }}% IRR</p>
                 </div>      

            </div> 

            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12" style="margin-top:10px;padding:0;">
                  
                 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="padding:0;">        
          <style>
             .fa-border{
               background:#B9983D;
               padding: 0.2em 9px 0.15em !important;
    border: solid 0.08em #B9983D !important;
    border-radius: 14px !important;
             }


          </style>         
                     <i class='fas fa-indian-rupee-sign fa-border' style="font-size: 20px;
    padding-top: 4px;
    color: #fff;"></i>


                 </div>                  
                 <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" style="padding:0;">                 
                   <p style="font-family: 'Public Sans', sans-serif;color: #686F6F;font-size:13px;margin-bottom:0;margin-top:-5px;"> Token Value</p>
                   <p style="font-family: 'Public Sans', sans-serif;color: #172243;font-size:15px;margin-top:-5px;font-weight:600;">&#8377; {{ @$value->tokenValue }} (1 Token Value)</p>
                 </div>      

            </div> 

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:10px;padding:0;">
               
               <div class="radiantthemes-custom-button hover-style-five rt810073130  display-inline-block" data-button-direction="center" data-button-fullwidth="false">
                                                <a style=" color: #ffffff; line-height: 25px; font-size: 14px;background:#05bd8e;padding:10px 27px 10px 27px !important;margin-right: 0 !important;border-radius:4px !important" class="radiantthemes-custom-button-main  vc_custom_1564647891751" href="" title="" target="_self">
                                                   <div class="placeholder" style="
                                                   font-size: 15px; text-transform: uppercase;
    font-family: 'Public Sans', sans-serif; font-weight: 600;">EXPLORE MORE</div>
                                                </a>
                                                
  
                                             </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="margin-top:10px;padding:0;">
               


<div class="radiantthemes-custom-button hover-style-five rt810073130  display-inline-block" data-button-direction="center" data-button-fullwidth="false">
                                                <a style=" color: #05bd8e;border: #05bd8e 1px solid; line-height: 25px; font-size: 14px;background:#ffffff !important;margin-right: 0 !important;border-radius:4px !important;padding:9px 40px 9px 40px !important;" class="radiantthemes-custom-button-main  vc_custom_1564647891751" href="" title="" target="_self">
                                                   <div class="placeholder" style="
                                                   font-size: 15px; text-transform: uppercase;
    font-family: 'Public Sans', sans-serif; font-weight: 600;">INVEST NOW</div>
                                                </a>
                                                
  
                                             </div>

            </div>

                                                   
                                                   </div>
                                                </div>
                                             </div>


@endforeach






                                             
                                            
                                             
                                   
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>


                                
                                 <div class="vc_row-full-width vc_clearfix"></div>
                              

<style>
   .vc_custom_1540799876678 {
    margin-top: 0px !important;
    margin-bottom: 0px !important;
    padding-top: 0px !important;
    padding-bottom: 0px !important;
    background-image: url(<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2018/07/back.jpg) !important;
    background-position: center !important;
    background-repeat: no-repeat !important;
    background-size: cover !important;
}

</style>
 

<div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid background-position-right home-thirteen-responsive home-thirteen-tablet-case-study vc_custom_1540799876678" style="position: relative; padding-bottom:52px !important;">
<div class="wpb_column vc_column_container vc_col-sm-12">
<center>   <h2 style="
   font-size: 27px;
   line-height: 48px;
   text-align: center;
   text-transform: uppercase;
   padding-top: 50px !important;
   padding-bottom: 50px !important;
   color: #FFFFFF !important;
   letter-spacing: 0.03px;
   font-family: 'Public Sans', sans-serif;
   font-weight: 600;

" class="vc_custom_heading font-weight-bold vc_custom_1564996682116">Why invest with Aurum Infinity?</h2></center></div>
   <div class="wpb_column vc_column_container vc_col-sm-3" style="border-right:rgba(116,115,115,0.53) 1px solid;border-bottom:rgba(116,115,115,0.53) 1px solid;padding-bottom:50px;">
      <div class="vc_column-inner vc_custom_1540799834491">
         <div class="wpb_wrapper">
            <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1540799914669" style="margin-bottom: 10px;">
               <figure class="wpb_wrapper vc_figure">
                  <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="95" height="95" src="<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2018/07/icon1.png" class="vc_single_image-img attachment-full" alt="Home-7-Market-Research-Image" loading="lazy" data-no-retina="" style="width:58px"></div>
               </figure>
            </div>
            <h3 style="font-size: 18px;font-family: 'Public Sans', sans-serif;
    font-weight: 500;color: #ffffff;line-height: 26px;text-align: center" class="vc_custom_heading letter-spacing-0 vc_custom_1546860710219">Transparent Returns</h3>
            
           
         </div>
      </div>
   </div>
   
 
 <div class="wpb_column vc_column_container vc_col-sm-3" style="border-right:rgba(116,115,115,0.53) 1px solid;border-bottom:rgba(116,115,115,0.53) 1px solid;padding-bottom:50px;">
      <div class="vc_column-inner vc_custom_1540799834491">
         <div class="wpb_wrapper">
            <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1540799914669" style="margin-bottom: 10px;">
               <figure class="wpb_wrapper vc_figure">
                  <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="95" height="95" src="<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2018/07/icon2.png" class="vc_single_image-img attachment-full" alt="Home-7-Market-Research-Image" loading="lazy" data-no-retina="" style="width:65px"></div>
               </figure>
            </div>
            <h3 style="font-size: 18px;font-family: 'Public Sans', sans-serif;
    font-weight: 500;color: #ffffff;line-height: 26px;text-align: center" class="vc_custom_heading letter-spacing-0 vc_custom_1546860710219">Data-backed insights</h3>
            
           
         </div>
      </div>
   </div>

<div class="wpb_column vc_column_container vc_col-sm-3" style="border-right:rgba(116,115,115,0.53) 1px solid;border-bottom:rgba(116,115,115,0.53) 1px solid;padding-bottom:50px;">
      <div class="vc_column-inner vc_custom_1540799834491">
         <div class="wpb_wrapper">
            <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1540799914669" style="margin-bottom: 10px;">
               <figure class="wpb_wrapper vc_figure">
                  <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="95" height="95" src="<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2018/07/icon3.png" class="vc_single_image-img attachment-full" alt="Home-7-Market-Research-Image" loading="lazy" data-no-retina="" style="width:64px"></div>
               </figure>
            </div>
            <h3 style="font-size: 18px;font-family: 'Public Sans', sans-serif;
    font-weight: 500;color: #ffffff;line-height: 26px;text-align: center" class="vc_custom_heading letter-spacing-0 vc_custom_1546860710219">User-Friendly Platform</h3>
            
           
         </div>
      </div>
   </div>

   
<div class="wpb_column vc_column_container vc_col-sm-3" style="border-bottom:rgba(116,115,115,0.53) 1px solid;padding-bottom:50px;">
      <div class="vc_column-inner vc_custom_1540799834491">
         <div class="wpb_wrapper">
            <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1540799914669" style="margin-bottom: 10px;">
               <figure class="wpb_wrapper vc_figure">
                  <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="95" height="95" src="<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2018/07/icon4.png" class="vc_single_image-img attachment-full" alt="Home-7-Market-Research-Image" loading="lazy" data-no-retina="" style="width:57px"></div>
               </figure>
            </div>
            <h3 style="font-size: 18px;font-family: 'Public Sans', sans-serif;
    font-weight: 500;color: #ffffff;line-height: 26px;text-align: center" class="vc_custom_heading letter-spacing-0 vc_custom_1546860710219">Realtime Portfolio Tracking</h3>
            
           
         </div>
      </div>
   </div>

   <div class="wpb_column vc_column_container vc_col-sm-3" style="border-right:rgba(116,115,115,0.53) 1px solid;padding-top:33px;">
      <div class="vc_column-inner vc_custom_1540799834491">
         <div class="wpb_wrapper">
            <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1540799914669" style="margin-bottom: 10px;">
               <figure class="wpb_wrapper vc_figure">
                  <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="95" height="95" src="<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2018/07/icon5.png" class="vc_single_image-img attachment-full" alt="Home-7-Market-Research-Image" loading="lazy" data-no-retina="" style="width:55px"></div>
               </figure>
            </div>
            <h3 style="font-size: 18px;font-family: 'Public Sans', sans-serif;
    font-weight: 500;color: #ffffff;line-height: 26px;text-align: center" class="vc_custom_heading letter-spacing-0 vc_custom_1546860710219">Graded Real estate assets</h3>
            
           
         </div>
      </div>
   </div>


   <div class="wpb_column vc_column_container vc_col-sm-3" style="border-right:rgba(116,115,115,0.53) 1px solid;padding-top:33px;">
      <div class="vc_column-inner vc_custom_1540799834491">
         <div class="wpb_wrapper">
            <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1540799914669" style="margin-bottom: 10px;">
               <figure class="wpb_wrapper vc_figure">
                  <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="95" height="95" src="<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2018/07/icon6.png" class="vc_single_image-img attachment-full" alt="Home-7-Market-Research-Image" loading="lazy" data-no-retina="" style="width:55px"></div>
               </figure>
            </div>
            <h3 style="font-size: 18px;font-family: 'Public Sans', sans-serif;
    font-weight: 500;color: #ffffff;line-height: 26px;text-align: center" class="vc_custom_heading letter-spacing-0 vc_custom_1546860710219">Multiple exit options</h3>
            
           
         </div>
      </div>
   </div>

   <div class="wpb_column vc_column_container vc_col-sm-3" style="border-right:rgba(116,115,115,0.53) 1px solid;padding-top:33px;">
      <div class="vc_column-inner vc_custom_1540799834491">
         <div class="wpb_wrapper">
            <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1540799914669" style="margin-bottom: 10px;">
               <figure class="wpb_wrapper vc_figure">
                  <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="95" height="95" src="<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2018/07/icon7.png" class="vc_single_image-img attachment-full" alt="Home-7-Market-Research-Image" loading="lazy" data-no-retina="" style="width:55px"></div>
               </figure>
            </div>
            <h3 style="font-size: 18px;font-family: 'Public Sans', sans-serif;
    font-weight: 500;color: #ffffff;line-height: 26px;text-align: center" class="vc_custom_heading letter-spacing-0 vc_custom_1546860710219">Low entry barrier</h3>
            
           
         </div>
      </div>
   </div>
<div class="wpb_column vc_column_container vc_col-sm-3" style="padding-top:33px;">
      <div class="vc_column-inner vc_custom_1540799834491">
         <div class="wpb_wrapper">
            <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1540799914669" style="margin-bottom: 10px;">
               <figure class="wpb_wrapper vc_figure">
                  <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="95" height="95" src="<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2018/07/icon82.png" class="vc_single_image-img attachment-full" alt="Home-7-Market-Research-Image" loading="lazy" data-no-retina="" style="width:55px"></div>
               </figure>
            </div>
            <h3 style="font-size: 18px;font-family: 'Public Sans', sans-serif;
    font-weight: 500;color: #ffffff;line-height: 26px;text-align: center" class="vc_custom_heading letter-spacing-0 vc_custom_1546860710219">High Asset Liquidity</h3>
            
           
         </div>
      </div>
   </div>
<div class="wpb_column vc_column_container vc_col-sm-12" style="padding-top:70px;">
    
<center>  <div class="radiantthemes-custom-button hover-style-five rt810073130  display-inline-block" data-button-direction="center" data-button-fullwidth="false">
                                                <a style=" color: #ffffff; line-height: 25px; font-size: 14px;background:#05bd8e;border-radius:4px !important" class="radiantthemes-custom-button-main  vc_custom_1564647891751" href="<?php echo url('/') ?>/login" title="" target="_self">
                                                   <div class="placeholder" style="
                                                   font-size: 17px; text-transform: uppercase;
    font-family: 'Public Sans', sans-serif; font-weight: 600;">INVEST NOW</div>
                                                </a>
                                                
  
                                             </div></center>

   

  </div>

  


   


</div>







     
                                 <div class="vc_row-full-width vc_clearfix"></div>
                              

<style>
   .vc_custom_1540799876679 {
    margin-top: 0px !important;
    margin-bottom: 0px !important;
    padding-top: 0px !important;
    padding-bottom: 0px !important;
    background-image: url(<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2018/07/auruminfinity2.gif) !important;
    background-position: center !important;
    background-repeat: no-repeat !important;
    background-size: cover !important;
}

</style>
 

<div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid background-position-right home-thirteen-responsive home-thirteen-tablet-case-study vc_custom_1540799876670" style="position: relative; margin-bottom:-20px !important;margin-top: 104px !important;">
<div class="wpb_column vc_column_container vc_col-sm-12">
<center>   <h2 style="
   font-size: 27px;
   line-height: 48px;
   text-align: center;
   text-transform: uppercase;
   padding-top: 0px !important;
   padding-bottom: 50px !important;
   
   color: #232F3E !important;
   letter-spacing: 0.03px;
   font-family: 'Public Sans', sans-serif;
   font-weight: 600;

" class="vc_custom_heading font-weight-bold vc_custom_1564996682116">How does It work?</h2></center></div>

</div>

<div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid background-position-right home-thirteen-responsive home-thirteen-tablet-case-study vc_custom_1540799876679" style="position: relative; padding-left: 0 !important;padding-right: 0 !important;margin-bottom:-26px !important;height:580px;margin-top: 0px !important;margin-left:-90px !important;margin-right:-100px !important">
<div class="wpb_column vc_column_container vc_col-sm-12" class="video">
 <style>
          
video {
    clip-path: inset(2px 2px);
}

       </style>
  
 <video  id="video" style="width:100%; padding-left: 0 !important;padding-right: 0 !important;" src="<?php echo url('/'); ?>/public/asset/login/wp-content/auruminfinityvideo2.mp4" muted autoplay></video>
  

 
</div>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
   <script>

$(function() {
  
  var $video = $('.video');
  var $window = $(window);

  $window.scroll(function() {

    var $topOfVideo = $video.offset().top;
    var $bottomOfVideo = $video.offset().top + $video.outerHeight();

    var $topOfScreen = $window.scrollTop();
    var $bottomOfScreen = $window.scrollTop() + $window.innerHeight();
    
    if(($bottomOfScreen > $bottomOfVideo) && ($topOfScreen < $topOfVideo)){
      $video[0].play();
    } else {
      $video[0].pause();
    }
    
  });
  
});

    // Pause video function
    const video = document.getElementById('video');

    function pauseVideo(event) {
        // Console test
        // console.log(video.duration)
        // console.log(event.currentTime)

        // Pause video 0.5 seconds before end time.
        if (event.currentTime > video.duration - 0.2) {
            video.pause();
        }
    }
</script>
 
</div>





                                        <div class="vc_row-full-width vc_clearfix"></div>
                              
                                 
                            
                              </section>
                             
                                 
                                 
                               
                                <div class="vc_row-full-width vc_clearfix"></div>
                                 
                                 
                              
                                 
                              
                                   




                    
                    


<style>
   .vc_custom_1565269117522 {
    margin-top: 0px !important;
    margin-bottom: 0px !important;
    padding-top: 0px !important;
    padding-bottom: 0px !important;
    background-image: url(<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2018/07/rt.jpg) !important;
    background-position: center !important;
    background-repeat: no-repeat !important;
    background-size: cover !important;
}

</style>
   <section data-vc-full-width="true" style="padding-top:0px !important;height:571px;" data-vc-full-width-init="false" class="vc_section background-position-center-bottom home-thirteen-pricing-table-responsive vc_custom_1565269117522 vc_section-has-fill">
                                
                                 
                                 <div class="vc_row wpb_row vc_row-fluid vc_custom_1565173495650">
                                    <div class="home-thirteen-responsive-get-in-touch wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12">
                                       <div class="vc_column-inner vc_custom_1565240932947">
                                          <div class="wpb_wrapper">
<img style="height:500px;position: absolute;
    margin-left: -106px;margin-top: 30px !important; border-radius: 5px;" src="<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2018/07/Mask Group 5.png">
                                          </div>
                                       </div>
                                    </div>


<div class="home-thirteen-responsive-get-in-touch wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12">
                                       <div class="vc_column-inner vc_custom_1565240932947">
                                          <div class="wpb_wrapper">


<img style="width:309px;" src="<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2018/07/Group 66471.png">
           
<p style="color:#232F3E;font-size:16px;font-family: 'Public Sans', sans-serif;
    font-weight: 600;padding-bottom:0px;" >Aurum PropTech is an integrated proptech ecosystem that focuses on enabling data-driven real estate investments, increasing efficiency of real estate construction, sale, and lease, enhancing consumer experience of buying and renting properties, and promoting connected living by the way of social commerce around real estate. Aurum PropTech aims to transform the real estate industry with the power of technology through its core values of empowerment, transparency, speed and elevated customer experience.</p>


    <h2 style="
   font-size: 27px;
   line-height: 48px;
   text-align: left;
   text-transform: uppercase;
   padding-top: 0px !important;
   padding-bottom: 0px !important;   
   color: #232F3E !important;
   letter-spacing: 0.03px;
   font-family: 'Public Sans', sans-serif;
   font-weight: 600;" class="vc_custom_heading font-weight-bold vc_custom_1564996682116">Our Partners</h2>

 <div class="clients owl-carousel element-one owl-nav-style-one owl-dot-style-one" data-owl-nav="true" data-owl-dots="true" data-owl-loop="true" data-owl-autoplay="true" data-owl-autoplay-timeout="5" data-owl-mobile-items="1" data-owl-tab-items="5" data-owl-desktop-items="5" style="background:#F4FAEC;border-radius:5px">
               
                  <div class="clients-item">
                     <div class="holder matchHeight">
                        <div class="table">
                           <div class="table-cell">
                              <div class="pic radiantthemes-retina" 
                              style="border-right: #BFC5D67E 2px solid; padding-right: 15px;
"><img width="193" height="59" src="{{url('/')}}/public/our_partners/3.png" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="Client-Image-006" loading="lazy" style="width:200px;"/></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- clients-item --><!-- clients-item -->
                  <div class="clients-item"  >
                     <div class="holder matchHeight">
                        <div class="table">
                           <div class="table-cell">
                              <div class="pic radiantthemes-retina" style="border-right: #BFC5D67E 2px solid; padding-right: 15px;
"><img width="174" height="85" src="{{url('/')}}/public/our_partners/4.png" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="Client-Image-001" loading="lazy" style="width:200px;"/></div>
                           </div>
                        </div>
                     </div>
                  </div>
                
                  <div class="clients-item">
                     <div class="holder matchHeight">
                        <div class="table">
                           <div class="table-cell">
                              <div class="pic radiantthemes-retina" style="border-right: #BFC5D67E 2px solid; padding-right: 15px;
"><img width="182" height="57" src="{{url('/')}}/public/our_partners/6.png" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="Client-Image-003" loading="lazy" style="width:200px;"/></div>
                           </div>
                        </div>
                     </div>
                  </div>
                
              <div class="clients-item">
                     <div class="holder matchHeight">
                        <div class="table">
                           <div class="table-cell">
                              <div class="pic radiantthemes-retina" style="border-right: #BFC5D67E 2px solid; padding-right: 15px;
"><img width="182" height="57" src="{{url('/')}}/public/our_partners/7.png" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="Client-Image-003" loading="lazy" style="width:200px;"/></div>
                           </div>
                        </div>
                     </div>
                  </div>


               </div>
    



                                          </div>
                                       </div>
                                    </div>


                                 </div>
                              </section>





                    
                    

                                   
                           
                              
                               
                                   <div class="vc_row-full-width vc_clearfix"></div>
                              <section data-vc-full-width="true" style="padding-top:64px !important;display:none;" data-vc-full-width-init="false" class="vc_section background-position-center-bottom home-thirteen-pricing-table-responsive vc_custom_1565269117521 vc_section-has-fill">
                                
                                 
                                 <div class="vc_row wpb_row vc_row-fluid vc_custom_1565173495650">
                                    <div class="home-thirteen-responsive-get-in-touch wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12">
                                       <div class="vc_column-inner vc_custom_1565240932947">
                                          <div class="wpb_wrapper">
                                          
                                          
                                          
                                          <div class="wpb_wrapper">
   <h2 style="font-size: 24px !important;
    color: #232F3E !important;
    line-height: 60px !important;
    text-align: left !important;
    font-weight: 600 !important;letter-spacing: 0.03px;" class="vc_custom_heading font-weight-regular title-letter-spacing vc_custom_1564573746979">GET IN TOUCH</h2>
   <h2 style="font-size: 35px;color: #232F3E !important;line-height: 48px;text-align: left;color:#251d59;letter-spacing:0.03px" class="vc_custom_heading font-weight-bold vc_custom_1564998591449"> ENQUIRY NOW</h2>
   <div style="font-size: 15px;color: #232F3E !important;line-height: 28px;text-align: left" class="vc_custom_heading font-weight-regular visible-lg visible-md hidden-sm hidden-xs vc_custom_1564998575876">To learn more before investing with us, fill in the details and we'll reach out to you within 24 hours</div>
   
</div>
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                          
                                        
                                             
                                            
                                             
                                          </div>
                                       </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12">
                                       <div class="vc_column-inner">
                                          <div class="wpb_wrapper">
                                          <h2 style="font-size: 24px !important;
    color: #232F3E !important;
    line-height: 60px !important;
    text-align: left !important;
    font-weight: 600 !important;letter-spacing: 0.03px;padding-top:40px !important;" class="vc_custom_heading font-weight-regular title-letter-spacing vc_custom_1564573746979" ></h2>
                                             <!-- rt-cf7 -->
                                             <div class="radiant-contact-form rt5d04c30e5c56e06 element-three home-thirteen-box-shadow vc_custom_1564636361306"  >
                                                <div role="form" class="wpcf7" id="wpcf7-f3578-p3351-o1" lang="en-US" dir="ltr">
                                                
                                                   <div class="screen-reader-response">
                                                      <p role="status" aria-live="polite" aria-atomic="true"></p>
                                                      <ul></ul>
                                                   </div>
                                                  
                                                   <div class="wpcf7-form init" novalidate="novalidate" data-status="init" onSubmit>
                                                       @csrf()
                                                      <div class="get-in_touch">
                                                          <div id="demo"></div>
                                                         <div class="row">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                                               <div class="form-row"> 
   
                                                                  <span class="wpcf7-form-control-wrap Name"><input type="text" name="Name" id="name" value="" size="40" required class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Full Name" /></span> 
                                                                  <span id="name-form" style="color:#ff0000;"></span></div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                                               <div class="form-row"> <span class="wpcf7-form-control-wrap email"><input type="email" required name="email" id="email" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Email" /></span>
                                                               <span id="email-form" style="color:#ff0000;"></span> </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                                               <div class="form-row"> <span class="wpcf7-form-control-wrap Subject"><input type="text" required name="Mobile" id="mobile" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Mobile" /></span> 
                                                                  <span id="mobile-form" style="color:#ff0000;"></span></div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                                               <div class="form-row"> <span class="wpcf7-form-control-wrap Message"><textarea name="Message" required cols="20" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" id="message" placeholder="Message" style="height: 74px;overflow:hidden"></textarea></span>
                                                               <span id="message-form" style="color:#ff0000;"></span> </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                                               <div class="form-row submit-btn"> <input style="background:#05bd8e;border-radius:4px !important" type="submit" id="enquirysubmit" value="SUBMIT" class="wpcf7-form-control wpcf7-submit" /> </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      
                                                   </div>


                                              
                                                </div>
                                                <div class="clearfix"></div>
                                             </div>
                                             <!-- rt-cf7 -->
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </section>
                 



   <div class="vc_row-full-width vc_clearfix"></div>
                        <!----      <section data-vc-full-width="true" style="padding-top:0 !important" data-vc-full-width-init="false" class="vc_section background-position-center-bottom home-thirteen-pricing-table-responsive vc_custom_1565269117521 vc_section-has-fill">
                                
                                 
                                 <div class="vc_row wpb_row vc_row-fluid vc_custom_1565173495650">
                                    <div class="home-thirteen-responsive-get-in-touch wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12">
                                       <div class="vc_column-inner vc_custom_1565240932947">
                                          <div class="wpb_wrapper">
                                          
                                                                                  
                                          
                                             <h2 style="font-size: 35px;color: #000000;line-height: 48px;font-weight:700 !important;text-align: center;color:#251d59;letter-spacing:0.03px" class="vc_custom_heading font-weight-regular title-letter-spacing vc_custom_1564573746979" >FAQ (FREQUENTLY ASKED QUESTION)</h2>
                                            
                                             
                                             <div class="radiantthemes-accordion element-four   rt1460847459">
   <div class="radiantthemes-accordion-item">
      <div class="radiantthemes-accordion-item-title">
         <div class="radiantthemes-accordion-item-title-icon"></div>
         <h4 class="panel-title"><span class="panel-title-counter"></span>What is asset tokenization?</h4>
      </div>
      <div class="radiantthemes-accordion-item-body" style="display: none;">Asset Tokenization is the process by which the ownership of real estate assets is digitized and represented by tokens on the blockchain. This enables a universal, fractional form of real estate ownership to be carried out totally on-chain.</div>
   </div>
   <div class="radiantthemes-accordion-item">
      <div class="radiantthemes-accordion-item-title">
         <div class="radiantthemes-accordion-item-title-icon"></div>
         <h4 class="panel-title"><span class="panel-title-counter"></span>How does asset tokenization work?</h4>
      </div>
      <div class="radiantthemes-accordion-item-body" style="display: none;">Issuers generate digital tokens representing fractions of real estate assets on the blockchain. All transactions are recorded on the chain and cash-flows are divided among investors in proportion to their holdings.</div>
   </div>
   <div class="radiantthemes-accordion-item">
      <div class="radiantthemes-accordion-item-title">
         <div class="radiantthemes-accordion-item-title-icon"></div>
         <h4 class="panel-title"><span class="panel-title-counter"></span>How do I buy asset tokens?</h4>
      </div>
      <div class="radiantthemes-accordion-item-body" style="display: none;">Start by selecting the number of tokens corresponding to the fraction you want to hold in the property, pay the required amount and hold the tokens in your secure wallet. These tokens represent your proportion of ownership.</div>
   </div>
   <div class="radiantthemes-accordion-item">
      <div class="radiantthemes-accordion-item-title">
         <div class="radiantthemes-accordion-item-title-icon"></div>
         <h4 class="panel-title"><span class="panel-title-counter"></span>Who will own the property?</h4>
      </div>
      <div class="radiantthemes-accordion-item-body" style="display: none;">All investors are the co-owners of the property, in proportion to the number of tokens held by them. Every transaction of asset tokens on the blockchain gets recorded and co-owner information gets updated with the appropriate registering authority.</div>
   </div>
   <div class="radiantthemes-accordion-item">
      <div class="radiantthemes-accordion-item-title">
         <div class="radiantthemes-accordion-item-title-icon"></div>
         <h4 class="panel-title"><span class="panel-title-counter"></span>How are rental yields distributed?</h4>
      </div>
      <div class="radiantthemes-accordion-item-body" style="display: none;">Every month, rental income and other property cash flows are divided among Asset Token holders in proportion to their holdings on the property and delivered to their secure wallets.</div>
   </div>
   <div class="radiantthemes-accordion-item">
      <div class="radiantthemes-accordion-item-title">
         <div class="radiantthemes-accordion-item-title-icon"></div>
         <h4 class="panel-title"><span class="panel-title-counter"></span>How do I sell my asset tokens?</h4>
      </div>
      <div class="radiantthemes-accordion-item-body" style="display: none;">You are free to list your asset tokens for sale on our platform after the lock-in period.</div>
   </div>

</div>
                                            
                                             
                                          </div>
                                       </div>
                                    </div>
                                    
                                 </div>
                              </section>
-->


                              <div class="vc_row-full-width vc_clearfix"></div>
                              
                              
                           </div>
                           <!-- .entry-content -->
                        </article>
                        <!-- #post-3351 -->             
                     </div>
                  </main>
                  <!-- #main -->
               </div>
               <!-- #primary -->
            </div>
            <!-- #content -->
         </div>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">


    $(document).on("ready", function () {
             $("#enquirysubmit").on("click", function () {
                var name  = $('#name').val();
      var mobile  = $('#mobile').val();
      var email  = $('#email').val();
      var message  = $('#message').val();

      var valid=0;

      var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
       var filter = /^\d*(?:\.\d{1,2})?$/;
 var nameregex = /^[A-Za-z]+$/;
      if(name==""){        
         $('#name-form').html("Please must enter a Name");
         valid=1;
      }else{
         $('#name-form').html("");
        
      }
      if(!nameregex.test(name)) {
          $('#name-form').html("Please enter only characters");
         valid=1;
      }else{
         $('#name-form').html("");         
      }
      if(email==""){        
         $('#email-form').html("Please must enter a Email");
         valid=1;
      }else{
         $('#email-form').html("");         
      }
      if(!regex.test(email)) {
          $('#email-form').html("Please must enter a Valid Email");
         valid=1;
      }else{
         $('#email-form').html("");        
      }

      if(mobile==""){        
         $('#mobile-form').html("Please must enter a Mobile");
         valid=1;
      }else{
         $('#mobile-form').html("");
         
      }


          if (filter.test(mobile)) {
            if(mobile.length==10){
               $('#mobile-form').html("");
               
             }
             else {
               $('#mobile-form').html("Please put 10 digit mobile number");
                valid=1;
              }
            }
            else {
             $('#mobile-form').html("Please enter valid number");
                valid=1;
           }
      if(message==""){        
         $('#message-form').html("Please must enter a Message");
         valid=1;
      }else{
         $('#message-form').html("");
        
      }

      if(valid==0){
                   $.ajax({
                       url    : "{{url('/getenquiryform')}}",
                       type   : "POST",
   data   : {'name': name,'email':email,'mobile':mobile,'message':message, '_token': '{{ csrf_token() }}'},
                       success: function (result) {
                            if (result.status == "success") {
                              $('#demo').html('<div class="alert alert-success">Your Enquiry has been submitted Successfully.<button type="button" class="close" data-dismiss="alert">×</button></div>');
                              $('#name').val('');
                              $('#mobile').val('');
                              $('#email').val('');
                              $('#message').val('');

                           } else {
                               $('#demo').html('<div class="alert alert-danger">Your Enquiry has not been submitted. Please Try Again<button type="button" class="close" data-dismiss="alert">×</button></div>');
                           }
                       }
                   });
                }
            });
        });

</script>
 <script>
   // Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 3000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });

</script>
@endsection

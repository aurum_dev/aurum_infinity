@extends('layout.appHome')


<style type="text/css">
     .copye {cursor: copy;}
       .wraper_inner_banner {
    background-color: #f2f2f2;
    background-position: center top;
    background-image: url('asset/login/wp-content/uploads/2018/07/Blog-Banner-Background-Image.png');
    background-size: cover;
}
.form-control {
    display: block;
    width: 100%;
    height: 45px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
  
    border-top: 0px solid #f5f4f4;
    border-right: 0px solid #f5f4f4;
    border-bottom: 2px solid #f5f4f4;
    border-left: 0px solid #f5f4f4;
    -webkit-box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%);
    box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
.rt-fancy-text-box.element-one>.holder {
  
    padding: 15px 6px 4px 16px !important;
    }
    .rt-fancy-text-box.element-one>.holder>.data .subtitle:before{
        width: 0 !important;
    }
    .rt-fancy-text-box.element-one>.holder>.data .subtitle{
        font-size: 40px !important;
        line-height: 88px !important;
    }
button.dt-button:first-child, div.dt-button:first-child, a.dt-button:first-child, input.dt-button:first-child {
    color: #ffffff !important;
    line-height: 25px !important;
    font-size: 14px !important;
    border: 2px solid #251d59 !important;
    background: #251d59 !important;
    margin-left: 0 !important;
}
button.dt-button, div.dt-button, a.dt-button, input.dt-button{
    color: #ffffff !important;
    line-height: 25px !important;
    font-size: 14px !important;
    border: 2px solid #251d59 !important;
    background: #251d59 !important;
    margin-left: 0 !important;
}
button.dt-button span.dt-down-arrow, div.dt-button span.dt-down-arrow, a.dt-button span.dt-down-arrow, input.dt-button span.dt-down-arrow{
    color: #fff !important;
}
table.dataTable thead th, table.dataTable tfoot th {
    font-weight: 500 !important;
}
.dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
    cursor: default;
    color: #251d59 !important;
    border: 1px solid #251d59 !important;
    background: #ebebeb !important;
    box-shadow: none;
    font-family: 'Poppins' !important;
    margin-bottom: 10px !important;
}
</style>
@section('content')
    <!-- Breadcrumb -->
    <div class="page-content">
       
        <!-- End Breadcrumb -->
        <!-- Property Head Starts -->
       
        <!-- Property Head Ends -->
<div class="wraper_inner_banner">
   <div class="wraper_inner_banner_main">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="inner_banner_main">
                  <p class="title" style="text-align:left;font-size:36px;">
                    Payment History
                  </p>
                  <p class="subtitle" style="text-align:left;font-size:16px;color:#fff !important;text-align: left;">                                              
                                    <span style="font-size:25px;">Welcome {{@$user->name}}, </span> you now have access to greater financial details related<br> to our offerings, so you can make an informed investment decision.                                 
                   
                  </p>
                   <p class="subtitle" style="text-align:left;font-size:15px;color:#fff !important;text-align: left;">
                  
                            <a href="{{url('/home')}}" class="pro-breadcrumbs-item" style="color:#cbc6c6;font-style:italic">Home</a>
                <span>/</span>
                                
                       <a href="{{url('/dashboard')}}" class="pro-breadcrumbs-item" style="color:#cbc6c6;font-style:italic">Dashboard</a>
                <span>/</span>
                <a href="#" class="pro-breadcrumbs-item" style="color:#cbc6c6;font-style:italic">My Account</a>
                <span>/</span>
                 <a href="#" class="pro-breadcrumbs-item" style="color:#cbc6c6;font-style:italic">Payment History</a>
                
                  </p>
               </div>
            </div>



         </div>
      </div>
   </div>
</div> 
        
     
<section class="container spaceall wallet-full">
            <!-- Top Details -->

            <div class="container">

                <div class="panel panel-default">
                    <div class="panel-body panel-currencies">

                        <div class="row">

                            <!-- Left Side Box-->
                             <div class="col-xs-4" ></div>
                              <div class="col-xs-4" ></div>
                            <div class="col-xs-4" style="background:#251d59;">
                                <div class="currencies-container ">
 <?php
 $amtt=0;
foreach($fiat_history as $history){
        if($history->razorpaytxnhash!=''){
        $amtt+=$history->amount;
        }
 }
 ?>


                                    <a class="btn btn-danger" style="padding: 10px 10px 0px 24px;background-color: #251d59 !important;border-color: #251d59 !important;font-size:20px !important;text-align:center;">

Total Token Transaction Amount
                                  <br><br><span style="font-size:36px;"><i class="fa fa-rupee" style="font-size:31px;margin-right:3px;"></i>   {{$amtt}} </span></a>
                                   <br><br>
                                    <!-- End Left Side Widget -->
                                </div>
                            </div>
                            <!-- End Left Size Box -->

                            <!-- Right Size Box -->
                            <div class="col-xs-12" style="margin-top:30px;">
                                <div class="details-container tab-content">
                                    <!-- BTC Deposit Tab -->
                                    <div class="tab-pane active" id="fiat_deposit">
                                      
                                     
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.1.0/css/buttons.dataTables.min.css">
    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.html5.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.colVis.min.js"></script>
     <script>
    $(document).ready(function() {
    $('#example').DataTable( {
       "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
           dom: 'Bfrtip',
            "order": [[ 0, "asc" ]],
       buttons: [
            {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-file" style="font-size: 17px;"></i>',
                className: 'btn btn-primary',
                titleAttr: 'Copy'
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o" style="font-size: 17px;"></i>',
                titleAttr: 'Excel'
            },
            
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o" style="font-size: 17px;"></i>',
                titleAttr: 'PDF'
            }
        ],
    } );
} );

</script>
                                        <!-- Deposit History -->
                                        <table id="example" class="datatable-full table table-striped table-bordered custom-table-style" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>S.no</th>
                                                <th>Property Name</th>
                                                <th>Token Name</th>
                                                <th>Token Symbol</th>
                                                <th>Total Tokens Purchased</th>
                                                 <th>Transaction ID</th>
                                              
                                               
                                                <th>Amount</th>
                                                 <th>Transaction Date</th>

                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            @foreach($fiat_history as $history => $value)

                                           <?php
                                      
//$prop = \App\AdminWallet::where('razorpayhash',$value->payment_id)->first();
//print_r($prop);

$propdet=\App\Property::where('id',$value->property_id)->first();
?>

                                                <tr>
                                                    <td>{{$history+1}}</td>
                                                    <td>{{@$propdet->propertyName}}</td>
                                                     <td>{{@$propdet->tokenName}}</td>
                                                     <td>{{@$propdet->tokenSymbol}}</td>
                                                      <td>{{@$value->total_tokens}}</td>
                                                    <td>{{ @$value->payment_id }}</td>
                                                   
                                                     <td><i class="fa fa-rupee" style="font-size:16px;margin-right:3px;"></i>{{number_format($value->amount,3)}}</td>
                                                    <td>{{ date("d, M Y",strtotime(@$value->created_at)) }}</td>
                                                    
                                                   
                                                    <td>
                                                     @if($value->razorpaytxnhash!='')   
                                                   <a class="btn btn-success"  style="padding:0 10px;"> Success</a>
                                                   @else
                                                    <a class="btn btn-danger" style="padding:0 10px;"> Failed</a>
                                                    @endif
                                                </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                       
                                        <!-- End Deposit History -->

                                    </div>
                                    <!-- End BTC Deposit Tab -->
                                </div>
                            </div>
                        </div>
                        <!-- .row -->
                    </div>
                </div>
            </div>
        </section>
    </div>
   
   
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script type="text/javascript">
     $(document).ready(function(){
       $(".pay").on("click", function () {
        if($("#amount").val() == ''){
            alert("Please enter amount");
            return false;
        }
        var amount = $("#amount").val();
        var name = "{{ Auth::user()->name }}";
        var key = "{{ getenv('RAZORPAY_KEY') }}";
        var image = "{{ asset('logo.png') }}";
        var token = "{{ csrf_token() }}";
        var options = {
            "key": key,
            "amount": (amount * 100), // 2000 paise = INR 20
            "name": name,
            "description": "Deposit",
            "image": image,
            "handler": function (response){
                $.ajax({
                    url: '{{ url("/storePayment") }}',
                    type: 'post',
                    data: "_token="+token+"&razorpay_payment_id="+response.razorpay_payment_id+"&totalAmount="+amount, 
                    success: function (msg) {
                        alert("Payment has been done");
                        window.location.reload();
                    }
                });
            
            },

            "theme": {
                "color": "#528FF0"
            }
        };
        var rzp1 = new Razorpay(options);
        rzp1.open();
        e.preventDefault();
    });
     });
</script>
@endsection

@section('styles')
    <style type="text/css">
        .introactive {
            background: #232020 !important;
            background: #140749 !important;
            color: #FFF;
        }
        .introactive .nav-link {
            color: #FFF;
        }
        div#deposit_address {
            font-size: 13px;
            font-weight: 500;
            padding: 11px;
            color: #000;
        }
        .custom-table-style thead tr th {
            vertical-align: middle;
            font-size: 12px;
            padding: 8px !important;
        }
        .custom-table-style tbody tr td {
            vertical-align: middle;
            font-size: 12px;
            padding: 8px !important;
            letter-spacing: .1px;
        }
        .send-code-button button.btn.btn-primary {
            padding: 10px 20px;
            background: #5f56e0;
            border: 1px solid #5f56e0;
        }
        .selectable {
            -webkit-touch-callout: all; /* iOS Safari */
            -webkit-user-select: all; /* Safari */
            -khtml-user-select: all; /* Konqueror HTML */
            -moz-user-select: all; /* Firefox */
            -ms-user-select: all; /* Internet Explorer/Edge */
            user-select: all; /* Chrome and Opera */
        }
        .alert-container {
            position: fixed;
            bottom: 5px;
            left: 11%;
            width: 32%;
            margin: 0 25% 0 25%;
            background-color: green;
            z-index: 1;
        }
        .alert {
            text-align: center;
            padding: 17px 0 20px 0;
            margin: 0 25% 0 25%;
            height: 54px;
            font-size: 20px;
        }
        a.nav-item.nav-link {
            color: #8d8d8d !important;
        }
    </style>
     


@endsection

@section('scripts')

@endsection

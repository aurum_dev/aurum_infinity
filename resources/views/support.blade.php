@extends('layout.appHome')

@section('content')

<style>
        /* .nice-select .list { overflow-y: scroll; height: 200px; } */
        .form-section-div {
            display: block !important;
        }
        .form-section-div.current {
            display: block; 
        }
        .rt-tab.element-five>ul.nav-tabs>li>a:before {
            background-color: #251d59 !important;
        }
        .rt-tab.element-five>ul.nav-tabs>li.active>a>span {
    position: relative;
    color: #fff !important;
}
  .wraper_inner_banner {
    background-color: #f2f2f2;
    background-position: center top;
    background-image: url('public/asset/login/wp-content/uploads/2018/07/Blog-Banner-Background-Image.png');
    background-size: cover;
}
        .form-control {
    display: block;
    width: 100%;
    height: 45px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
  
    border-top: 1px solid #f5f4f4;
    border-right: 1px solid #f5f4f4;
    border-bottom: 1px solid #f5f4f4;
    border-left: 1px solid #f5f4f4;
    -webkit-box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%);
    box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
#regForm {
  background-color: #ffffff;
    margin: 0px auto;
    font-family: 'Poppins';
    padding: 40px;
    padding-top: 0px;
    width: 100%;
    min-width: 300px;
}

h1 {
  text-align: center;  
}

input {
  padding: 10px;
  width: 100%;
  font-size: 17px;
   font-family: 'Poppins';
  border: 1px solid #aaaaaa;
}

/* Mark input boxes that gets an error on validation: */
input.invalid {
  background-color: #ffdddd;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

button {
  background-color: #251d59;
  color: #ffffff;
  border: none;
  padding: 10px 20px;
  font-size: 17px;
  font-family: 'Poppins';
  cursor: pointer;
}

button:hover {
  opacity: 0.8;
}

#prevBtn {
  background-color: #bbbbbb;
}

.form-control{
    border: #d7d6dd 1px solid !important;
}
    </style>
     <div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1526899957936" style="position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;">
   <div class="wpb_column vc_column_container vc_col-sm-2">
      <div class="vc_column-inner vc_custom_1526899963628">
         <div class="wpb_wrapper"></div>
      </div>
   </div>
 
   <div class="wpb_column vc_column_container vc_col-sm-2">
      <div class="vc_column-inner">
         <div class="wpb_wrapper"></div>
      </div>
   </div>
</div>

 <div class="page-content">

        <!-- Property Head Ends -->
        @if(Session::has('tabName'))
            @php $tabName = Session::get('tabName'); @endphp
        @endif
        <!-- Property Tab Starts -->
<div class="wraper_inner_banner">
   <div class="wraper_inner_banner_main">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="inner_banner_main">
                  <p class="title" style="text-align:left;font-size:36px;">
                    Profile
                  </p>
                  <p class="subtitle" style="text-align:left;font-size:16px;color:#fff !important;text-align: left;">                                              
                                    <span style="font-size:25px;">Welcome {{@$user->name}},</span> you now have access to greater financial details related<br> to our offerings, so you can make an informed investment decision.                                 
                   
                  </p>
                   <p class="subtitle" style="text-align:left;font-size:15px;color:#fff !important;text-align: left;">
                  
                           
                                
                       <a href="{{url('/dashboard')}}" class="pro-breadcrumbs-item" style="color:#cbc6c6;font-style:italic">Home</a>
                <span>/</span>
                <a href="#" class="pro-breadcrumbs-item" style="color:#cbc6c6;font-style:italic">Settings</a>
                <span>/</span>
                 <a href="#" class="pro-breadcrumbs-item" style="color:#cbc6c6;font-style:italic">Help</a>
                
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div> 

 <div class="wpb_column vc_column_container vc_col-sm-12" style="padding:0 100px;margin-top:90px;">
      <div class="vc_column-inner">
         <div class="wpb_wrapper">
               
               
                 @include('common.notify')
                 
                  <div id="65882d53-4a58-7" class="tab-pane">

<form method="POST" action="{{ url('/phelp') }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="userName">Name<span class="text-danger">*</span></label>
                                        <input type="text" name="name" required="" placeholder="" class="form-control" id="name">
                                    </div>
                                     <div class="col-md-6 form-group">
                                        <label for="userName">Email<span class="text-danger">*</span></label>
                                        <input type="email" value="{{Auth::user()->email}}" name="email" readonly placeholder="" class="form-control" id="email" data-parsley-minlength="8">
                                    </div>
                                </div>
                                <div class="row">
                                   
                                    <div class="col-md-6 form-group">
                                        <label for="userName">Message<span class="text-danger">*</span></label>
                                        <textarea name="message" required="" placeholder="" class="form-control" id="message"></textarea>
                                       
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="userName">Select Priority<span class="text-danger">*</span></label>
                                        <select class="form-control" id="priority" name="priority">
                                            <option>Select</option>
                                            <option>Low</option>
                                            <option>Medium</option>
                                            <option>High</option>
                                        </select>
                                         <input class="btn btn-primary mr-1" type="submit" style="margin-top: 30px !important;    background-color: #251d59;
    color: #ffffff;
    border: none;
    padding: 10px 20px;
    font-size: 17px;
    font-family: 'Poppins';
    cursor: pointer;width:40%;float:right;" value="Submit"/>
                                    </div>
                                </div>
                               
                                </div>
                            </form>

                  </div>
                  
                  
           
         </div>
      </div>
   </div>

  


        <div class="property-tab">
             <div class="pro-tab-wrap">
                <div class="container">
                    <!-- Nav tabs -->
                   
                </div>
            </div>
            <!-- Tab panes -->


           
        </div>
    </div>

@endsection
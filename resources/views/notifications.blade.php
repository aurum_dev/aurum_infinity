@extends('layout.appHome')

@section('content')

<style>
   table.dataTable tbody th, table.dataTable tbody td {
    padding: 32px 10px 8px 10px !important;
}
    .rt-fancy-text-box.element-one>.holder>.icon i{
        font-size: 55px !important;
    }
   .wraper_inner_banner {
    background-color: #f2f2f2;
    background-position: center top;
    background-image: url('public/asset/login/wp-content/uploads/2018/07/Blog-Banner-Background-Image.png');
    background-size: cover;
}
.rt-fancy-text-box.element-one>.holder {
  
    padding: 15px 6px 4px 16px !important;
    }
    .rt-fancy-text-box.element-one>.holder>.data .subtitle:before{
        width: 0 !important;
    }
    .rt-fancy-text-box.element-one>.holder>.data .subtitle{
        font-size: 40px !important;
        line-height: 88px !important;
    }
button.dt-button:first-child, div.dt-button:first-child, a.dt-button:first-child, input.dt-button:first-child {
    color: #ffffff !important;
    line-height: 25px !important;
    font-size: 14px !important;
    border: 2px solid #251d59 !important;
    background: #251d59 !important;
    margin-left: 0 !important;
}
button.dt-button, div.dt-button, a.dt-button, input.dt-button{
    color: #ffffff !important;
    line-height: 25px !important;
    font-size: 14px !important;
    border: 2px solid #251d59 !important;
    background: #251d59 !important;
    margin-left: 0 !important;
}
button.dt-button span.dt-down-arrow, div.dt-button span.dt-down-arrow, a.dt-button span.dt-down-arrow, input.dt-button span.dt-down-arrow{
    color: #fff !important;
}
table.dataTable thead th, table.dataTable tfoot th {
    font-weight: 500 !important;
}
.dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
    cursor: default;
    color: #251d59 !important;
    border: 1px solid #251d59 !important;
    background: #ebebeb !important;
    box-shadow: none;
    font-family: 'Poppins' !important;
    margin-bottom: 10px !important;
}
.rt-fancy-text-box.element-one>.holder{
   background-color: #dfdfdf !important;
}

</style>
 <style>
                                .neclass .rt-fancy-text-box.element-one>.holder:before{
    width: 80px !important;
    height: 109px !important;
    border-bottom-left-radius: 0 !important;
}

.section-50 {
    padding: 50px 0;
}

.m-b-50 {
    margin-bottom: 50px;
}

.dark-link {
    color: #333;
}

.heading-line {
    position: relative;
    padding-bottom: 5px;
}

.heading-line:after {
    content: "";
    height: 4px;
    width: 75px;
    background-color: #29B6F6;
    position: absolute;
    bottom: 0;
    left: 0;
}

.notification-ui_dd-content {
    margin-bottom: 30px;
}

.notification-list {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
    padding: 20px;
    margin-bottom: 7px;
    background: #fff;
    -webkit-box-shadow: 0 3px 10px rgba(0, 0, 0, 0.06);
    box-shadow: 0 3px 10px rgba(0, 0, 0, 0.06);
}

.notification-list--unread {
    border-left: 13px solid #251d59;
}

.notification-list .notification-list_content {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
}

.notification-list .notification-list_content .notification-list_img img {
    height: 48px;
    width: 48px;
    border-radius: 50px;
    margin-right: 20px;
}

.notification-list .notification-list_content .notification-list_detail p {
    margin-bottom: 5px;
    line-height: 1.2;
}

.notification-list .notification-list_feature-img img {
    height: 48px;
    width: 48px;
    border-radius: 5px;
    margin-left: 20px;
}
                            </style>


             
           


          <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">     
<!-- Breadcrumb -->
<div class="page-content">


<div class="wraper_inner_banner">
   <div class="wraper_inner_banner_main">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="inner_banner_main">
                  <p class="title" style="text-align:left;font-size:36px;">
                    Notifications
                  </p>
                  <p class="subtitle" style="text-align:left;font-size:16px;color:#fff !important;text-align: left;">                                              
                                    <span style="font-size:25px;">Welcome {{@$user->name}}, </span> you now have access to greater financial details related<br> to our offerings, so you can make an informed investment decision.                                 
                   
                  </p>
                   <p class="subtitle" style="text-align:left;font-size:15px;color:#fff !important;text-align: left;">
                  
                           
                                
                       <a href="{{url('/')}}" class="pro-breadcrumbs-item" style="color:#cbc6c6;font-style:italic">Home</a>
                <span>/</span>
                <a href="#" class="pro-breadcrumbs-item" style="color:#cbc6c6;font-style:italic">Notifications</a>
               
                
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div> 
    



    <!-- Property Tab Starts -->
  
</div>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.1.0/css/buttons.dataTables.min.css">
    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <!--<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.html5.min.js"></script>-->
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.colVis.min.js"></script>

 <section class="container table-property">
  <p class="title" style="text-align:center;font-size:30px;padding-bottom:50px;color:#251d59;">
                   
                  </p>
                            <table id="example2" class="datatable-full table table-striped table-bordered custom-table-style" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Recent</th>
                                        
                                    </tr>
                                </thead>



                                <tr>
                                    
                                    <td>
                                         <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12 vc_col-xs-12" style="margin-bottom:30px;">
                  <div class="vc_column-inner vc_custom_1528279952057">
                     <div class="wpb_wrapper neclass">



            <div class="notification-ui_dd-content">

            @foreach($investorbonustoken as $index => $val)
            <div class="notification-list notification-list--unread">
                <div class="notification-list_content" style="margin:0 !important">
                    <div class="notification-list_img">
                       <i class='fa fa-user-circle' style="font-size: 56px;
    margin-right: 12px;
    color: #d1cccc;"></i>
                    </div>
                    <div class="notification-list_detail">
                        <p><b>{{$val->name}}</b> Giving the Bonus Token Dividend</p>
                        <p class="text-muted">{{$val->title}}</p>
                        <p class="text-muted"><small>

                           <?php
$to_time = time();

$from_time = strtotime($val->approveddate);
$minutes = round(abs($to_time - $from_time) / 60,2);
if($minutes<60){
   echo $minutes." Minutes";
}else if($minutes>60 && $minutes<1440){
   $seconds = round(abs($to_time - $from_time));
   $hours = round($seconds / 60 / 60);
   echo $hours." Hours";
}else{
  
   echo round(abs($to_time - $from_time) / (60 * 60 * 24))." Days";
}
?>

                         

                        ago


                     </small>

                     <a href="{{ url('bonusdetail/'.$val->tokendividendid) }}" style="margin-left:30px;">Read More</a></p>
                    </div>
                </div>
               
            </div>
          @endforeach
           
           
           
        </div>
                       
                     </div>
                  </div>
               </div>
                                    </td>
                                </tr>


                                                          
                            </table>
<script>
    $(document).ready(function() {
    $('#example2').DataTable( {
        dom: 'Bfrtip',
       
    } );
} );

</script>
                        </section>
<!-- Property Tab Ends -->
@endsection


@section('scripts')

@endsection

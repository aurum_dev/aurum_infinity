@extends('layout.appHome')

@section('content')

<div id="page" class="site">
            <!-- #content -->
            <div id="content" class="site-content">
               <div id="primary" class="content-area">
                  <main id="main" class="site-main">
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                     <div class="container page-container">
                        <article id="post-3351" class="post-3351 page type-page status-publish hentry">
                          
                           <!-- .entry-header -->
                           <div class="entry-content">
                          
                               
                         <div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1531892317465 vc_row-has-fill" style="position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;">
   <div class="wpb_column vc_column_container vc_col-sm-12">
      <div class="vc_column-inner vc_custom_1531823802046">
         <div class="wpb_wrapper">
            <h6 style="font-size: 16px;color: #181616;line-height: 26px;text-align: center" class="vc_custom_heading font-weight-medium vc_custom_1531892110977">OUR SPECIALISTS</h6>
            <h3 style="font-size: 35px;color: #181616;line-height: 41px;text-align: center" class="vc_custom_heading font-weight-bold vc_custom_1531892291170">Our Executive Team Member</h3>
            <div class="wpb_text_column wpb_content_element  vc_custom_1531907390931">
               <div class="wpb_wrapper">
                  <p style="text-align: center;">There are many variations of passages of Lorem Ipsum available, but the majority have suffered<br>
                     alteration in some form, by injected humour, or randomised words.
                  </p>
               </div>
            </div>
            <div class="team element-seven owl-carousel owl-nav-style-one owl-dot-style-two owl-loaded owl-drag" data-owl-nav="false" data-owl-dots="false" data-owl-loop="true" data-owl-autoplay="true" data-owl-autoplay-timeout="6000" data-owl-mobile-items="1" data-owl-tab-items="3" data-owl-desktop-items="4" style="">
               <div class="owl-stage-outer">
                  <div class="owl-stage" style="transform: translate3d(-3162px, 0px, 0px); transition: all 0.25s ease 0s; width: 4600px;">
                     <div class="owl-item cloned" style="width: 287.5px;">
                        <div class="team-item matchHeight " style="height: 550.938px;">
                           <div class="holder">
                              <div class="pic">
                                 <div class="pic-background"></div>
                                 <div class="pic-main" style="background-image:url(https://seolounge.radiantthemes.com/wp-content/uploads/2018/07/SEO-Team-Member-Image-004-888x1024.png);"></div>
                              </div>
                              <div class="data">
                                 <p class="title">Scott Chasin</p>
                                 <p class="designation">Co-Founder &amp; CEO</p>
                                 <div class="socal-section">
                                    <ul></ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="owl-item cloned" style="width: 287.5px;">
                        <div class="team-item matchHeight " style="height: 550.938px;">
                           <div class="holder">
                              <div class="pic">
                                 <div class="pic-background"></div>
                                 <div class="pic-main" style="background-image:url(https://seolounge.radiantthemes.com/wp-content/uploads/2018/07/SEO-Team-Member-Image-003-888x1024.png);"></div>
                              </div>
                              <div class="data">
                                 <p class="title">Gene Stevens</p>
                                 <p class="designation">Co-Founder &amp; CTO</p>
                                 <div class="socal-section">
                                    <ul></ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="owl-item cloned" style="width: 287.5px;">
                        <div class="team-item matchHeight " style="height: 550.938px;">
                           <div class="holder">
                              <div class="pic">
                                 <div class="pic-background"></div>
                                 <div class="pic-main" style="background-image:url(https://seolounge.radiantthemes.com/wp-content/uploads/2018/07/SEO-Team-Member-Image-002-888x1024.png);"></div>
                              </div>
                              <div class="data">
                                 <p class="title">Ramon Peypoch</p>
                                 <p class="designation">Chief Product Officer</p>
                                 <div class="socal-section">
                                    <ul></ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="owl-item cloned" style="width: 287.5px;">
                        <div class="team-item matchHeight " style="height: 550.938px;">
                           <div class="holder">
                              <div class="pic">
                                 <div class="pic-background"></div>
                                 <div class="pic-main" style="background-image:url(https://seolounge.radiantthemes.com/wp-content/uploads/2018/07/SEO-Team-Member-Image-001-888x1024.png);"></div>
                              </div>
                              <div class="data">
                                 <p class="title">Jon Paterson</p>
                                 <p class="designation">VP. Solution Architecture</p>
                                 <div class="socal-section">
                                    <ul></ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="owl-item" style="width: 287.5px;">
                        <div class="team-item matchHeight " style="height: 550.938px;">
                           <div class="holder">
                              <div class="pic">
                                 <div class="pic-background"></div>
                                 <div class="pic-main" style="background-image:url(https://seolounge.radiantthemes.com/wp-content/uploads/2021/01/home-16-team-04.jpg);"></div>
                              </div>
                              <div class="data">
                                 <p class="title">Rosana Davio</p>
                                 <p class="designation">Chief Product Officer</p>
                                 <div class="socal-section">
                                    <ul>
                                       <li class="facebook"><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                       <li class="twitter"><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                       <li class="google-plus"><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                       <li class="linkedin"><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="owl-item" style="width: 287.5px;">
                        <div class="team-item matchHeight " style="height: 550.938px;">
                           <div class="holder">
                              <div class="pic">
                                 <div class="pic-background"></div>
                                 <div class="pic-main" style="background-image:url(https://seolounge.radiantthemes.com/wp-content/uploads/2021/01/home-17-team-04.jpg);"></div>
                              </div>
                              <div class="data">
                                 <p class="title">Rosana Davio</p>
                                 <p class="designation">Co-Founder &amp; CTO</p>
                                 <div class="socal-section">
                                    <ul>
                                       <li class="facebook"><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                       <li class="twitter"><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                       <li class="google-plus"><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                       <li class="linkedin"><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="owl-item" style="width: 287.5px;">
                        <div class="team-item matchHeight " style="height: 550.938px;">
                           <div class="holder">
                              <div class="pic">
                                 <div class="pic-background"></div>
                                 <div class="pic-main" style="background-image:url(https://seolounge.radiantthemes.com/wp-content/uploads/2021/01/home-16-team-02.jpg);"></div>
                              </div>
                              <div class="data">
                                 <p class="title">John Doe</p>
                                 <p class="designation">VP. Solution Architecture</p>
                                 <div class="socal-section">
                                    <ul>
                                       <li class="facebook"><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                       <li class="twitter"><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                       <li class="google-plus"><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                       <li class="linkedin"><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="owl-item" style="width: 287.5px;">
                        <div class="team-item matchHeight " style="height: 550.938px;">
                           <div class="holder">
                              <div class="pic">
                                 <div class="pic-background"></div>
                                 <div class="pic-main" style="background-image:url(https://seolounge.radiantthemes.com/wp-content/uploads/2021/01/home-16-team-01.jpg);"></div>
                              </div>
                              <div class="data">
                                 <p class="title">Zalina Thomas</p>
                                 <p class="designation">Technological Expert</p>
                                 <div class="socal-section">
                                    <ul>
                                       <li class="facebook"><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                       <li class="twitter"><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                       <li class="google-plus"><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                       <li class="linkedin"><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="owl-item" style="width: 287.5px;">
                        <div class="team-item matchHeight " style="height: 550.938px;">
                           <div class="holder">
                              <div class="pic">
                                 <div class="pic-background"></div>
                                 <div class="pic-main" style="background-image:url(https://seolounge.radiantthemes.com/wp-content/uploads/2018/07/SEO-Team-Member-Image-004-888x1024.png);"></div>
                              </div>
                              <div class="data">
                                 <p class="title">Scott Chasin</p>
                                 <p class="designation">Co-Founder &amp; CEO</p>
                                 <div class="socal-section">
                                    <ul></ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="owl-item" style="width: 287.5px;">
                        <div class="team-item matchHeight " style="height: 550.938px;">
                           <div class="holder">
                              <div class="pic">
                                 <div class="pic-background"></div>
                                 <div class="pic-main" style="background-image:url(https://seolounge.radiantthemes.com/wp-content/uploads/2018/07/SEO-Team-Member-Image-003-888x1024.png);"></div>
                              </div>
                              <div class="data">
                                 <p class="title">Gene Stevens</p>
                                 <p class="designation">Co-Founder &amp; CTO</p>
                                 <div class="socal-section">
                                    <ul></ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="owl-item" style="width: 287.5px;">
                        <div class="team-item matchHeight " style="height: 550.938px;">
                           <div class="holder">
                              <div class="pic">
                                 <div class="pic-background"></div>
                                 <div class="pic-main" style="background-image:url(https://seolounge.radiantthemes.com/wp-content/uploads/2018/07/SEO-Team-Member-Image-002-888x1024.png);"></div>
                              </div>
                              <div class="data">
                                 <p class="title">Ramon Peypoch</p>
                                 <p class="designation">Chief Product Officer</p>
                                 <div class="socal-section">
                                    <ul></ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="owl-item active" style="width: 287.5px;">
                        <div class="team-item matchHeight " style="height: 550.938px;">
                           <div class="holder">
                              <div class="pic">
                                 <div class="pic-background"></div>
                                 <div class="pic-main" style="background-image:url(https://seolounge.radiantthemes.com/wp-content/uploads/2018/07/SEO-Team-Member-Image-001-888x1024.png);"></div>
                              </div>
                              <div class="data">
                                 <p class="title">Jon Paterson</p>
                                 <p class="designation">VP. Solution Architecture</p>
                                 <div class="socal-section">
                                    <ul></ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="owl-item cloned active" style="width: 287.5px;">
                        <div class="team-item matchHeight " style="height: 550.938px;">
                           <div class="holder">
                              <div class="pic">
                                 <div class="pic-background"></div>
                                 <div class="pic-main" style="background-image:url(https://seolounge.radiantthemes.com/wp-content/uploads/2021/01/home-16-team-04.jpg);"></div>
                              </div>
                              <div class="data">
                                 <p class="title">Rosana Davio</p>
                                 <p class="designation">Chief Product Officer</p>
                                 <div class="socal-section">
                                    <ul>
                                       <li class="facebook"><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                       <li class="twitter"><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                       <li class="google-plus"><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                       <li class="linkedin"><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="owl-item cloned active" style="width: 287.5px;">
                        <div class="team-item matchHeight " style="height: 550.938px;">
                           <div class="holder">
                              <div class="pic">
                                 <div class="pic-background"></div>
                                 <div class="pic-main" style="background-image:url(https://seolounge.radiantthemes.com/wp-content/uploads/2021/01/home-17-team-04.jpg);"></div>
                              </div>
                              <div class="data">
                                 <p class="title">Rosana Davio</p>
                                 <p class="designation">Co-Founder &amp; CTO</p>
                                 <div class="socal-section">
                                    <ul>
                                       <li class="facebook"><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                       <li class="twitter"><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                       <li class="google-plus"><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                       <li class="linkedin"><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="owl-item cloned active" style="width: 287.5px;">
                        <div class="team-item matchHeight " style="height: 550.938px;">
                           <div class="holder">
                              <div class="pic">
                                 <div class="pic-background"></div>
                                 <div class="pic-main" style="background-image:url(https://seolounge.radiantthemes.com/wp-content/uploads/2021/01/home-16-team-02.jpg);"></div>
                              </div>
                              <div class="data">
                                 <p class="title">John Doe</p>
                                 <p class="designation">VP. Solution Architecture</p>
                                 <div class="socal-section">
                                    <ul>
                                       <li class="facebook"><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                       <li class="twitter"><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                       <li class="google-plus"><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                       <li class="linkedin"><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="owl-item cloned" style="width: 287.5px;">
                        <div class="team-item matchHeight " style="height: 550.938px;">
                           <div class="holder">
                              <div class="pic">
                                 <div class="pic-background"></div>
                                 <div class="pic-main" style="background-image:url(https://seolounge.radiantthemes.com/wp-content/uploads/2021/01/home-16-team-01.jpg);"></div>
                              </div>
                              <div class="data">
                                 <p class="title">Zalina Thomas</p>
                                 <p class="designation">Technological Expert</p>
                                 <div class="socal-section">
                                    <ul>
                                       <li class="facebook"><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                       <li class="twitter"><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                       <li class="google-plus"><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                       <li class="linkedin"><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="owl-nav disabled">
                  <div class="owl-prev">prev</div>
                  <div class="owl-next">next</div>
               </div>
               <div class="owl-dots disabled"></div>
               <div class="owl-thumbs"></div>
            </div>
         </div>
      </div>
   </div>
</div>
                            
                              
                           </div>
                           <!-- .entry-content -->
                        </article>
                        <!-- #post-3351 -->             
                     </div>
                  </main>
                  <!-- #main -->
               </div>
               <!-- #primary -->
            </div>
            <!-- #content -->
         </div>


@endsection

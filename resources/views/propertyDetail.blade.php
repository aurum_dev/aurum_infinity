@extends('layout.appHome')

@section('content')
<style>
   
body{
   background-image: linear-gradient(rgb(255, 255, 255), rgba(185, 152, 61, 0.07));
}

</style>
<div id="page" class="site">
            <!-- #content -->
            <div id="content" class="site-content">
               <div id="primary" class="content-area">
                  <main id="main" class="site-main">
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

                       
                     <div class="container page-container">
                        <article id="post-3351" class="post-3351 page type-page status-publish hentry">
                           <header class="entry-header">
                           </header>
                           <!-- .entry-header -->
                           <div class="entry-content">
                          
                          
                    <style>
                    .product-slider { padding: 0; }

.product-slider #carousel { border: 1px solid #ebebeb; margin: 0; }

.product-slider #thumbcarousel { margin: 12px 0 0; padding: 0 0; }

.product-slider #thumbcarousel .item { text-align: left; }

.product-slider #thumbcarousel .item .thumb { border: 4px solid #cecece; width: 20%; margin: 0 0 0 3px; display: inline-block; vertical-align: middle; cursor: pointer; max-width: 98px; }

.product-slider #thumbcarousel .item .thumb:hover { border-color: #1089c0; }

.product-slider .item img { width: 100%; height: auto; }

.carousel-control { color: #0284b8; text-align: center; text-shadow: none; font-size: 30px; width: 30px; height: 30px; line-height: 20px; top: 23%; }

.carousel-control:hover, .carousel-control:focus, .carousel-control:active { color: #333; }

.carousel-caption, .carousel-control .fa { font: normal normal normal 30px/26px FontAwesome; }
.carousel-control { background-color: rgba(0, 0, 0, 0); bottom: auto; font-size: 20px; left: 0; position: absolute; top: 30%; width: auto; }

.carousel-control.right, .carousel-control.left { background-color: rgba(0, 0, 0, 0); background-image: none; }
                    </style>      
                          <div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid home-thirteen-services-upper-space vc_custom_1565259781064 vc_row-has-fill vc_column-gap-30 vc_row-o-content-middle vc_row-flex" style="opacity:9;position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;padding-top:20px !important;">
   <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12" style="padding:0 !important;">
      <div class="vc_column-inner vc_custom_1565260206953">
         <div class="wpb_wrapper">
        



<div class="product-slider">
  <div id="carousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" >

<?php 

$gallery=explode("#",$property->galleryImage);
?>

     
      <div class="item active"> <img src="../<?php echo $gallery[0]; ?>"> </div>
   
 <?php 
                  
              for($i=1;$i<count($gallery);$i++){
                ?>
              
                <div class="item"> <img src="../<?php echo $gallery[$i]; ?>"> </div>

                <?php
            
              }
             ?>     




    </div>
  </div>
  <div class="clearfix">
    <div id="thumbcarousel" class="carousel slide" data-interval="false">
      <div class="carousel-inner" style="margin-left:16px;">
   <?php 

$gallery=explode("#",$property->galleryImage);

$numb =1;
   $flag =1;

?>
      <div class="item">
         <?php for($i=0;$i<count($gallery);$i++){ ?>
         <div data-target="#carousel" data-slide-to="<?php echo $i ?>" class="thumb"><img src="../<?php echo $gallery[$i] ?>"></div>
         <?php
$tt=count($gallery);

if($numb==5){ 

            ?>
                </div><div class="item">

             <?php
              }
              
               
            $numb++;
            $flag++;
           } ?>
      
      </div></div>
      <!-- /carousel-inner --> 
      <a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i> </a> <a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next"><i class="fa fa-angle-right" aria-hidden="true"></i> </a> </div>
    <!-- /thumbcarousel --> 
    
  </div>
</div>

    <script>
  
$('.carousel-inner>div:first-child').addClass('active');
       $('.carousel-inner>div:first-child').addClass('active');
    </script>    
            
         </div>
      </div>
   </div>
   <div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
      <div class="vc_column-inner vc_custom_1565260220932">
         <div class="wpb_wrapper">
            <h2 style="font-size: 26px !important;
               color: #232F3E !important;
               line-height: 0px !important;
               text-align: left !important;
               font-weight: 600 !important;letter-spacing: 0.03px;font-family: 'Public Sans', sans-serif;
    font-weight: 600;" class="vc_custom_heading font-weight-regular title-letter-spacing vc_custom_1564561171454">{{ @$property->propertyName }} <span style="color:#232F3E;font-size:16px;font-family: 'Public Sans', sans-serif;
    font-weight: 600;">, {{ @$property->propertyCity }}</span></h2>
           <p style="font-family: 'Public Sans', sans-serif;
    font-weight: 600;color:#05BD8E;line-height:40px;"><img src="<?php echo url('/'); ?>/public/asset/login/wp-content/pointimage.png">   Property Status Live</p>
            <div style="font-size: 16px;color:#686F6F;font-family: 'Public Sans', sans-serif;line-height: 28px;text-align: left" class="vc_custom_heading font-weight-regular home-thirteen-para-right-align vc_custom_1565157773347">{{ @$property->propertyDesc }}</div>
           <div class="home-thirteen-pricing-table wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12">
   <div class="vc_column-inner vc_custom_1565159240643" style="padding-left:0 !important;padding-right:0 !important">
      <div class="wpb_wrapper">
         

          <a target="_blank" href="https://mumbai.polygonscan.com/address/{{$property->contract_address}}" class="btn btn-primary" style="background-color: #05BD8E;font-family: 'Public Sans', sans-serif;border-color: #05BD8E;font-size:14px;margin-top:20px;padding:6px 10px;"><i class="fa fa-credit-card"></i>  View on Polygon Scan</a>



                     <?php if($property->status=="live"){ ?>
                             @if (Auth::check())

                                 <a href="{{ url('applyInvest/'.$property->id) }}" class="btn btn-primary" style="background-color: #fff;border-color: #05BD8E;margin-left:8px;font-size:14px;padding: 6px 10px;margin-top:20px;color: #05BD8E;font-family: 'Public Sans', sans-serif;"><i class="fa fa-money"></i>  Ready to Invest</a>
                             @else
                                <a href="{{('../profile')}}" class="btn btn-primary" style="padding: 6px 10px;background-color: #fff;border-color: #05BD8E;margin-left:8px;font-family: 'Public Sans', sans-serif;font-size:14px;margin-top:20px;color: #05BD8E;"><i class="fa fa-user"></i>  Become a Member</a>
                                @endif
                                </br>
                                @if (Auth::check())
                                    <small style="color: #686F6F;font-size:12px;font-family: 'Public Sans', sans-serif;"><b>Note:</b> Please update the profile information before investing.</small>
                                @endif
                        <?php } ?>


      </div>
   </div>
</div>  
            <!-- radiantthemes-custom-button -->
         </div>
      </div>
   </div>
</div>
                          
                         <section data-vc-full-width="true" data-vc-full-width-init="true" class="vc_section vc_custom_1525849326254 vc_section-has-fill" style="opacity:9;position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;">
  
   <div class="vc_row-full-width vc_clearfix"></div>
   <div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1526899957936" style="margin-bottom:100px;">
      <div class="wpb_column vc_column_container vc_col-sm-12">
         <div class="vc_column-inner vc_custom_1526899963628">
            <div class="wpb_wrapper">


<div class="wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill">
   <div class="vc_column-inner vc_custom_1527051906539">
      <div class="wpb_wrapper">
         <div class="rt-tab element-ten rt1980285457 ">
            <ul class="nav-tabs" style="position: relative;background-color: #fff;border: none;
    border-radius: 3px;box-shadow: 0px 2px 10px rgb(0 0 0 / 10%);text-align: left;">
               <li data-tab-icon="false" class="matchHeight active" style=""><a data-toggle="tab" href="#522d08bf-5c34-2" aria-expanded="false"><span><i class="fa fa-home"></i> Property Details</span></a></li>
               <li data-tab-icon="false" class="matchHeight" style=""><a data-toggle="tab" href="#d49252cd-c5c1-6" aria-expanded="false"><span><i class="fa fa-search"></i> Features</span></a></li>
               <li data-tab-icon="false" class="matchHeight " style=""><a data-toggle="tab" href="#c9018b76-49fa-11" aria-expanded="true"><span><i class="fa fa-credit-card"></i> Financials</span></a></li>
               <li data-tab-icon="false" class="matchHeight " style=""><a data-toggle="tab" href="#77adc051-9c2a-12" aria-expanded="true"><span><i class="fa fa-file-text-o"></i> Lease Details</span></a></li>
                <li data-tab-icon="false" class="matchHeight " style=""><a data-toggle="tab" href="#77adc051-9c2a-13" aria-expanded="true"><span><i class="fa fa-bar-chart"></i> Return Analysis</span></a></li>
                 <li data-tab-icon="false" class="matchHeight " style=""><a data-toggle="tab" href="#77adc051-9c2a-14" aria-expanded="true"><span><i class="fa fa-file"></i> Reports</span></a></li>
            </ul>
            <div class="tab-content" style="position: relative;
    padding: 20px 20px 15px 20px;
    background-color: #fff;
    border: none;
    border-radius: 3px;
    box-shadow: 0px 2px 10px rgb(0 0 0 / 10%);
    text-align: left;margin-left:20px;">
               <div id="522d08bf-5c34-2" class="tab-pane fade active in">
 <h4 class="title" style="font-weight: bold;color:#232F3E;font-size:18px;font-family: 'Public Sans', sans-serif;">Property Overview</h4>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Property Name</div>


</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->propertyName }}</div>
   
</div>

<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Property State / City / District</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->propertyState }} / {{ @$property->propertyCity }} / {{ @$property->propertyDistrict }}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Property Type</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->propertyType }}</div>   
</div>

<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Token Name</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->tokenName }}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Token Value</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->tokenValue }}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Minimum Investment</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->minimumInvest }}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Property Size</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->propertySize }}</div>   
</div>
        

<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Target IRR</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->targetIrr }}</div>   
</div> 


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Average Yield</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->averageYield }}</div>   
</div>


                   

               </div>
               <div id="d49252cd-c5c1-6" class="tab-pane fade">
                  


<h4 class="title" style="font-weight: bold;color:#232F3E;font-size:18px;font-family: 'Public Sans', sans-serif;">Features</h4>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Proponent</div>


</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->Proponent }}</div>
   
</div>

<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Property On Map</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;"><a style="color:#ee335e;" href="{{ @$property->Onmap }}" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i> Map Link</a></div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Address</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->Address }}</div>   
</div>

<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Property Status</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->propertyStatus }}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Build Year</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->buildYear }}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">RERA Number</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->reraNumber }}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Property Manager</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->propertyManager }}</div>   
</div>
        

<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Custodian</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->Custodian }}</div>   
</div> 


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Property Amenities</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;"><?php $amenity=explode("#",$property->propertyAmenity);
$amty=implode(",",$amenity);

?>
{{$amty}}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Property Technical Specification</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;"><a style="color:#ee335e;" href='../{{ @$property->propertyTechSpecs }}' target="_blank"><i class="fa fa-file-pdf-o"></i> Download</a>
</div>   
</div>



               </div>
               <div id="c9018b76-49fa-11" class="tab-pane fade">
                  



<h4 class="title" style="font-weight: bold;color:#232F3E;font-size:18px;font-family: 'Public Sans', sans-serif;">Financials</h4>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Cost of Premise</div>


</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->costOfPremise }}</div>
   
</div>

<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Stamp Duty Registration</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->stampDutyReg }}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Acquisition Fee</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->acquisitionFee }}</div>   
</div>

<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Other Charges</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->otherCharges }}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Legal Fees</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->legalFees }}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Maintenance Reserve</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->maintenanceReserve }}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Admin Expenses</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">&#8377; {{ @$property->adminExpenses }}</div>   
</div>
        

<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Monthly Opex</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->monthlyOpex }}</div>   
</div> 


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Monthly Property Tax</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->monthlyPropertyTax }}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Other Monthly Regulatory Charges</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">
   {{ @$property->regulatoryCharges }}
</div>   
</div>

<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Total Acquisition cost</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">
   &#8377;  {{ @$property->totalAcquisitionCost }} 
</div>   
</div>



               </div>



                <div id="77adc051-9c2a-12" class="tab-pane fade">
                  
               


<h4 class="title" style="font-weight: bold;color:#232F3E;font-size:18px;font-family: 'Public Sans', sans-serif;">Lease Details</h4>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Carpet Area (Sq Ft)</div>


</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->carpetArea }}</div>
   
</div>

<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Built Up Area (Sq Ft)</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->builtUpArea }}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Monthly Rent</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">&#8377; {{ @$property->monthlyRent }} </div>   
</div>

<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Leased Period</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->leasedPeriod }}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Security Deposit</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->securityDeposit }}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Escalation</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->escalation }}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Tenants</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->tenants }}</div>   
</div>
        

<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Fitted out</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->fittedOut }}</div>   
</div> 


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Monthly Maintenance</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->monthlyMaintenance }}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Lease Chart</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->leaseChart }}
</div>   
</div>



                  

               </div>


                <div id="77adc051-9c2a-13" class="tab-pane fade">
                  
               


<h4 class="title" style="font-weight: bold;color:#232F3E;font-size:18px;font-family: 'Public Sans', sans-serif;">Return Analysis</h4>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Gross Yield(%)</div>


</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->grossYield }}</div>
   
</div>

<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Expected IRR (%)</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->expectedIr }}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Token Lockin Period (Months)</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->tokenLockinPeriod }} </div>   
</div>

<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Aurum Facilation fees (%)</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">{{ @$property->aurumFacilationFees }}</div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Performance Fees</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;">&#8377; {{ @$property->performanceFees }}</div>   
</div>


               </div>
               <div id="77adc051-9c2a-14" class="tab-pane fade">
                  
<h4 class="title" style="font-weight: bold;color:#232F3E;font-size:18px;font-family: 'Public Sans', sans-serif;">Reports</h4>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Investment Deck</div>


</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;"><a style="color:#05BD8E;" href='../{{ @$property->propertyInvestmentDeck }}' target="_blank"><i class="fa fa-file-pdf-o"></i> Download</a></div>
   
</div>

<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Title Report</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;"><a style="color:#05BD8E;" href='../{{ @$property->propertytitleReport }}' target="_blank"><i class="fa fa-file-pdf-o"></i> Download</a></div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Property Due Diligence</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;"><a style="color:#05BD8E;" href='../{{ @$property->propertyDueDiligence }}' target="_blank"><i class="fa fa-file-pdf-o"></i> Download</a> </div>   
</div>

<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Micro Market Report</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;"><a style="color:#05BD8E;" href='../{{ @$property->propertyMicroMarket }}' target="_blank"><i class="fa fa-file-pdf-o"></i> Download</a></div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Property Management Agreement</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;"><a style="color:#05BD8E;" href='../{{ @$property->propertyMgmtAgrmt }}' target="_blank"><i class="fa fa-file-pdf-o"></i> Download</a></div>   
</div>


<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">
   
  <div style="border:#e7dfdf 1px solid; padding: 5px 10px; text-align: left; font-size: 15px; color: #686F6F; font-weight: 500;">Valuation Report</div>
</div>
<div class="home-thirteen-responsive-services-tablet wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12 vc_col-xs-12">  

<div style="border:#e7dfdf 1px solid;padding: 5px 10px;text-align:left;color:#232F3E;font-weight:bold;"><a style="color:#05BD8E;" href='../{{ @$property->propertyValReport }}' target="_blank"><i class="fa fa-file-pdf-o"></i> Download</a></div>   
</div>

                  

               </div>
            </div>
         </div>
      </div>
   </div>
</div>



             
            </div>
         </div>
      </div>
   </div>
   <div class="vc_row-full-width vc_clearfix"></div>
</section>
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                           </div>
                           <!-- .entry-content -->
                        </article>
                        <!-- #post-3351 -->             
                     </div>
                  </main>
                  <!-- #main -->
               </div>
               <!-- #primary -->
            </div>
            <!-- #content -->
         </div>

@endsection

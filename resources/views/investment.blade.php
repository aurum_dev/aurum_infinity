@extends('issuer.layout.baseinvestor')

@section('content')

<style>
   table.dataTable tbody th, table.dataTable tbody td {
    padding: 32px 10px 8px 10px !important;
}
    .rt-fancy-text-box.element-one>.holder>.icon i{
        font-size: 55px !important;
    }
   .wraper_inner_banner {
    background-color: #f2f2f2;
    background-position: center top;
    background-image: url('public/asset/login/wp-content/uploads/2018/07/Blog-Banner-Background-Image.png');
    background-size: cover;
}
.rt-fancy-text-box.element-one>.holder {
  
    padding: 15px 6px 4px 16px !important;
    }
    .rt-fancy-text-box.element-one>.holder>.data .subtitle:before{
        width: 0 !important;
    }
    .rt-fancy-text-box.element-one>.holder>.data .subtitle{
        font-size: 40px !important;
        line-height: 88px !important;
    }
button.dt-button:first-child, div.dt-button:first-child, a.dt-button:first-child, input.dt-button:first-child {
    color: #ffffff !important;
    line-height: 25px !important;
    font-size: 14px !important;
    border: 2px solid #251d59 !important;
    background: #251d59 !important;
    margin-left: 0 !important;
}
button.dt-button, div.dt-button, a.dt-button, input.dt-button{
    color: #ffffff !important;
    line-height: 25px !important;
    font-size: 14px !important;
    border: 2px solid #251d59 !important;
    background: #251d59 !important;
    margin-left: 0 !important;
}
button.dt-button span.dt-down-arrow, div.dt-button span.dt-down-arrow, a.dt-button span.dt-down-arrow, input.dt-button span.dt-down-arrow{
    color: #fff !important;
}
table.dataTable thead th, table.dataTable tfoot th {
    font-weight: 500 !important;
}
.dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
    cursor: default;
    color: #251d59 !important;
    border: 1px solid #251d59 !important;
    background: #ebebeb !important;
    box-shadow: none;
    font-family: 'Poppins' !important;
    margin-bottom: 10px !important;
}
.rt-fancy-text-box.element-one>.holder{
   background-color: #dfdfdf !important;
}

</style>
 <style>
                                .neclass .rt-fancy-text-box.element-one>.holder:before{
    width: 80px !important;
    height: 109px !important;
    border-bottom-left-radius: 0 !important;
}

                            </style>
  <?php
$tokenCount=0;
$tokenAmt=0;
$property=array();
$tokenactive=0;
$tokenpending=0;
$propertyName=array();
foreach($wallet as $index => $value){
$tokenCount+=$value->total_tokens;
$tokenAmt+=$value->paybyinvestor;
$property[]=$value->property_id;

$propertyName[]=$value->propertyName."#".$value->property_id;
if($value->txnhash!=''){
   $tokenactive+=$value->total_tokens;
    
}
if($value->txnhash==''){
   $tokenpending+=$value->total_tokens;
    
}
}

$dividendtoken=0;
foreach($investorbonustoken as $in =>$va){
   $dividendtoken+=$va->dividendtokens;
}


        /*  Property ID Uniqueness*/ 
        if(count($propertyName)!=0){
              $finlProp=array_unique($propertyName);  
              $finlimplode=implode(",",$finlProp);
              $finlExp=explode(",",$finlimplode); 
        }else{
               $finlExp=array();
        }  
       

             ?> 

             
           


               
<!-- Breadcrumb -->
<div class="page-content">
  <div class="breadcrumb-header justify-content-between">
            <div class="left-content">
              <div>
              <h2 class="main-content-title tx-24 mg-b-1 mg-b-lg-1" style="text-transform: uppercase;font-family: 'Public Sans', sans-serif;color:#232F3E;font-size:20px;font-weight:600;">Hi {{Auth::user()->name}}, Welcome Back!</h2>
              <p class="mg-b-0" style="text-transform: uppercase;font-family: 'Public Sans', sans-serif;color:#232F3E;font-size:18px;font-weight:500;line-height:50px;">Dashboard</p>
              </div>
            </div>
            
          </div>

 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
   
                <div class="col-xl-3 col-lg-6 col-md-6 col-xm-12" style="float:left;">
                     <div class="card overflow-hidden sales-card btn-light btn-block">
                       <div class="ps-3 pt-3 pe-3 pb-2 pt-0">
                           <div class="col-xl-2" style="float:left;">
                              <h6 class="mb-3 tx-20 text-white" style="color:#232F3E !important;font-size:28px;font-family: 'Public Sans', sans-serif;"><i class="fas fa-building" style="color:#B9983D !important;font-size:30px;"></i>  </h6>
                           </div>
                           <div class="col-xl-1" style="float:left;"></div>
                           <div class="col-xl-9" style="float:left;">
                              <div class="d-flex">
                                 <div class="">
                              <h6 class=" tx-20 text-white" style="color:#232F3E !important;font-size:28px !important;font-family: 'Public Sans', sans-serif;margin-top:-4px;margin-bottom:0;">  {{count(array_unique($property))}}</h6>
                                    <p class="mb-0 tx-15 text-white op-7" style="color:#686F6F !important;font-size:16px;font-family: 'Public Sans', sans-serif;">ASSETS</p>
                                 </div>
                                 
                              </div>
                           </div>
                        </div>
                     
                     </div>
                  </div>
                    <div class="col-xl-3 col-lg-6 col-md-6 col-xm-12" style="float:left;">
                     <div class="card overflow-hidden sales-card btn-light btn-block">
                       <div class="ps-3 pt-3 pe-3 pb-2 pt-0">
                           <div class="col-xl-2" style="float:left;padding:0">
                              <h6 class="mb-3 tx-20 text-white" style="color:#232F3E !important;font-size:28px;font-family: 'Public Sans', sans-serif;"><i class="fas fa-rupee-sign" style="color:#B9983D !important;font-size:30px;"></i>  </h6>
                           </div>
                         
                           <div class="col-xl-9" style="float:left;padding:0">
                              <div class="d-flex">
                                 <div class="">
                              <h6 class=" tx-20 text-white" style="color:#232F3E !important;font-size:28px !important;font-family: 'Public Sans', sans-serif;margin-top:-4px;margin-bottom:0;">{{$tokenAmt}}</h6>
                                    <p class="mb-0 tx-15 text-white op-7" style="color:#686F6F !important;font-size:16px;font-family: 'Public Sans', sans-serif;">NET INVESTMENT</p>
                                 </div>
                                 
                              </div>
                           </div>
                        </div>
                     
                     </div>
                  </div>
                  <div class="col-xl-3 col-lg-6 col-md-6 col-xm-12" style="float:left;">
                     <div class="card overflow-hidden sales-card btn-light btn-block">
                       <div class="ps-3 pt-3 pe-3 pb-2 pt-0">
                           <div class="col-xl-2" style="float:left;padding:0">
                              <h6 class="mb-3 tx-20 text-white" style="color:#232F3E !important;font-size:28px;font-family: 'Public Sans', sans-serif;"><i class="fas fa-coins" style="color:#B9983D !important;font-size:30px;"></i>  </h6>
                           </div>
                         
                           <div class="col-xl-9" style="float:left;padding:0">
                              <div class="d-flex">
                                 <div class="">
                              <h6 class=" tx-20 text-white" style="color:#232F3E !important;font-size:28px !important;font-family: 'Public Sans', sans-serif;margin-top:-4px;margin-bottom:0;">{{$tokenCount}}</h6>
                                    <p class="mb-0 tx-15 text-white op-7" style="color:#686F6F !important;font-size:16px;font-family: 'Public Sans', sans-serif;">TOTAL TOKENS</p>
                                 </div>
                                 
                              </div>
                           </div>
                        </div>
                     
                     </div>
                  </div>
                   <div class="col-xl-3 col-lg-6 col-md-6 col-xm-12" style="float:left;">
                     <div class="card overflow-hidden sales-card btn-light btn-block">
                       <div class="ps-3 pt-3 pe-3 pb-2 pt-0">
                           <div class="col-xl-2" style="float:left;padding:0">
                              <h6 class="mb-3 tx-20 text-white" style="color:#232F3E !important;font-size:28px;font-family: 'Public Sans', sans-serif;"><i class="fas fa-coins" style="color:#B9983D !important;font-size:30px;"></i>  </h6>
                           </div>
                         
                           <div class="col-xl-9" style="float:left;padding:0">
                              <div class="d-flex">
                                 <div class="">
                              <h6 class=" tx-20 text-white" style="color:#232F3E !important;font-size:28px !important;font-family: 'Public Sans', sans-serif;margin-top:-4px;margin-bottom:0;">{{$tokenactive}}</h6>
                                    <p class="mb-0 tx-15 text-white op-7" style="color:#686F6F !important;font-size:16px;font-family: 'Public Sans', sans-serif;">ACTIVE TOKENS</p>
                                 </div>
                                 
                              </div>
                           </div>
                        </div>
                     
                     </div>
                  </div>

<div class="wpb_column vc_column_container vc_col-sm-12" style="padding:0 100px;margin-top:90px;">
      <div class="vc_column-inner">
         <div class="wpb_wrapper">



                                    @if(Session::has('flash_error'))
   <div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
  </button>
   {{ Session::get('flash_error') }}
</div>
@endif


@if(Session::has('flash_success'))
  <div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
  </button>
   {{ Session::get('flash_success') }}
</div>
@endif
            
 
            <div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1528367210886" style="opacity:1 !important;">
               <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4 vc_col-xs-12" style="margin-bottom:30px;">
                  <div class="vc_column-inner vc_custom_1528279916690">
                     <div class="wpb_wrapper">
                        <div class="rt-fancy-text-box element-one  rt1743060351 ">
                           <div class="holder matchHeight  vc_custom_1528785688438" style="height: 140px !important;">
                              <div class="icon"><i class="fas fa-building"></i> </div>
                              <div class="data">
                                 <p class="title"><a href="#" style="font-weight:500;">ASSETS</a></p>
                                 <p class="subtitle">{{count(array_unique($property))}}</p>
                                 
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4 vc_col-xs-12" style="margin-bottom:30px;">
                  <div class="vc_column-inner vc_custom_1528279934387">
                     <div class="wpb_wrapper">
                        <div class="rt-fancy-text-box element-one  rt1904688430 ">
                           <div class="holder matchHeight  vc_custom_1528785734363" style="height: 140px !important;">
                              <div class="icon"><i class="fa fa-money"></i> </div>
                              <div class="data">
                                 <p class="title"><a href="#" style="font-weight:500;">NET INVESTMENT</a></p>
                                 <p class="subtitle"><i class="fas fa-rupee-sign" style="margin-right:5px;"></i>{{$tokenAmt}}</p>
                                 
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               
               <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4 vc_col-xs-12" style="margin-bottom:30px;">
                  <div class="vc_column-inner vc_custom_1528279952057">
                     <div class="wpb_wrapper">
                        <div class="rt-fancy-text-box element-one  rt105464081 ">
                           <div class="holder matchHeight  vc_custom_1528785722203" style="height: 140px !important;">
                              <div class="icon"><i class="fas fa-coins"></i> </div>
                              <div class="data">
                                 <p class="title"><a href="#" style="font-weight:500;">TOTAL TOKENS</a></p>
                                 <p class="subtitle">{{$tokenCount}}</p>

                                
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
              
               
                              <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4 vc_col-xs-12" style="margin-bottom:30px;">
                  <div class="vc_column-inner vc_custom_1528279952057">
                     <div class="wpb_wrapper">
                        <div class="rt-fancy-text-box element-one  rt105464081 ">
                           <div class="holder matchHeight  vc_custom_1528785722203" style="height: 140px !important;">
                              <div class="icon"><i class="fas fa-coins"></i> </div>
                              <div class="data">
                                 <p class="title"><a href="#" style="font-weight:500;">ACTIVE TOKENS</a></p>
                                 <p class="subtitle">{{$tokenactive}}</p>

                                
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
                          <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4 vc_col-xs-12" style="margin-bottom:30px;">
                  <div class="vc_column-inner vc_custom_1528279952057">
                     <div class="wpb_wrapper">
                        <div class="rt-fancy-text-box element-one  rt105464081 ">
                           <div class="holder matchHeight  vc_custom_1528785722203" style="height: 140px !important;">
                              <div class="icon"><i class="fas fa-coins"></i> </div>
                              <div class="data">
                                 <p class="title"><a href="#" style="font-weight:500;">PENDING TOKENS</a></p>
                                 <p class="subtitle">{{$tokenpending}}</p>

                                
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4 vc_col-xs-12" style="margin-bottom:30px;">
                  <div class="vc_column-inner vc_custom_1528279952057">
                     <div class="wpb_wrapper">
                        <div class="rt-fancy-text-box element-one  rt105464081 ">
                           <div class="holder matchHeight  vc_custom_1528785722203" style="height: 140px !important;">
                              <div class="icon"><i class="fas fa-coins"></i> </div>
                              <div class="data">
                                 <p class="title"><a href="#" style="font-weight:500;">DIVIDEND TOKENS</a></p>
                                 <p class="subtitle">{{$dividendtoken}}</p>

                                
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

            </div>
        </div></div></div>

    <!-- Property Tab Starts -->
  
</div>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.1.0/css/buttons.dataTables.min.css">
    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <!--<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.html5.min.js"></script>-->
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.colVis.min.js"></script>

 <section class="container table-property">
  <p class="title" style="text-align:center;font-size:30px;padding-bottom:50px;color:#251d59;">
                   YOUR PORTFOLIO
                  </p>
                            <table id="example2" class="datatable-full table table-striped table-bordered custom-table-style" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>PORTFOLIO DETAILS</th>
                                        
                                    </tr>
                                </thead>


<?php 


for($i=0;$i<count($finlExp);$i++){

$propname=explode("#",$finlExp[$i]);
$totalT=0;
$totalA=0;
$totalAct=0;
$totalPending=0;
foreach($wallet as $index => $value){
            if($propname[1]==$value->property_id){
                $totalT+=$value->total_tokens;
                $totalA+=$value->paybyinvestor;
                if($value->txnhash!=''){
                    $totalAct+=$value->total_tokens;
                }else{
                    $totalPending+=$value->total_tokens;
                }
               
            }
    }

$dividendtokens=0;
    foreach($investorbonustoken as $in => $vv){
       if($propname[1]==$vv->propertyid){
         $dividendtokens+=$vv->dividendtokens;
       }
    }
 
 ?>
                                <tr>
                                    
                                    <td>
                                         <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12 vc_col-xs-12" style="margin-bottom:30px;">
                  <div class="vc_column-inner vc_custom_1528279952057">
                     <div class="wpb_wrapper neclass">
                        <div class="rt-fancy-text-box element-one  rt105464081 ">
                           
                           <div class="holder matchHeight  vc_custom_1528785722203" style="height: 140px !important;">
                              <div class="icon"><a href="<?php echo "propertyDetail/".$propname[1]; ?>"><i class="fas fa-angle-double-right" style="font-size: 32px !important;
    padding-top: 15px;"></i> </a></div>
                              <div class="data">
                                 <p class="title" style="margin-bottom:10px"><a href="<?php echo "propertyDetail/".$propname[1]; ?>" style="font-weight:500;text-transform:uppercase;">{{$propname[0]}}</a></p>
                               <p class="title" style="text-align:left;font-size:18px;color:#251d59;margin-bottom:5px;text-transform: uppercase;">    <span style="font-size:15px;color:#6e04cf;">Total Tokens : </span>  {{$totalT}}  

                             &nbsp;&nbsp;  | &nbsp;&nbsp;<span style="font-size:15px;color:#6e04cf;">Active Tokens : </span>{{ $totalAct}}  

@if($dividendtokens>0)

                              &nbsp;&nbsp;|&nbsp;&nbsp;
                                <span style="font-size:15px;color:#6e04cf;">Pending Tokens : </span>{{ $totalPending}} &nbsp;&nbsp;  | &nbsp;&nbsp;<span style="font-size:15px;color:#6e04cf;">Dividend Tokens : </span>{{$dividendtokens}}
                                @endif

 </p>

  <p class="title" style="text-align:left;font-size:18px;color:#251d59;margin-bottom:5px;text-transform: uppercase;">    <span style="font-size:15px;color:#6e04cf;">Total Amount :</span> <i class="fas fa-rupee-sign"></i> {{$totalA}}
 </p>
 
                                               </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
                                    </td>
                                </tr>

<?php } ?>
                                                          
                            </table>
<script>
    $(document).ready(function() {
    $('#example2').DataTable( {
        dom: 'Bfrtip',
       
    } );
} );

</script>
                        </section>
<!-- Property Tab Ends -->
@endsection


@section('scripts')

@endsection

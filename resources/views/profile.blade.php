@extends('layout.appHome')

@section('content')
<script>
    function RestrictSpace() {
    if (event.keyCode == 32) {
        return false;
    }
}

</script>
    <style>
        /* .nice-select .list { overflow-y: scroll; height: 200px; } */
        .form-section-div {
            display: block !important;
        }
        .form-section-div.current {
            display: block; 
        }
        .rt-tab.element-five>ul.nav-tabs>li>a:before {
            background-color: #251d59 !important;
        }
        .rt-tab.element-five>ul.nav-tabs>li.active>a>span {
    position: relative;
    color: #fff !important;
}
  .wraper_inner_banner {
    background-color: #f2f2f2;
    background-position: center top;
    background-image: url('public/asset/login/wp-content/uploads/2018/07/Blog-Banner-Background-Image.png');
    background-size: cover;
}
        .form-control {
    display: block;
    width: 100%;
    height: 45px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
  
    border-top: 1px solid #f5f4f4;
    border-right: 1px solid #f5f4f4;
    border-bottom: 1px solid #f5f4f4;
    border-left: 1px solid #f5f4f4;
    -webkit-box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%);
    box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
#regForm {
  background-color: #ffffff;
    margin: 0px auto;
    font-family: 'Poppins';
    padding: 40px;
    padding-top: 0px;
    width: 100%;
    min-width: 300px;
}

h1 {
  text-align: center;  
}

input {
  padding: 10px;
  width: 100%;
  font-size: 17px;
   font-family: 'Poppins';
  border: 1px solid #aaaaaa;
}

/* Mark input boxes that gets an error on validation: */
input.invalid {
  background-color: #ffdddd;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

button {
  background-color: #251d59;
  color: #ffffff;
  border: none;
  padding: 10px 20px;
  font-size: 17px;
  font-family: 'Poppins';
  cursor: pointer;
}

button:hover {
  opacity: 0.8;
}

#prevBtn {
  background-color: #bbbbbb;
}

/* Make circles that indicate the steps of the form: */
.step {
  height: 26px;
  width: 27px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none;  
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
  background-color: #251d59;
    color: #fff;
}
    </style>
   

    <div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid vc_custom_1526899957936" style="position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 89.5px; padding-right: 89.5px;">
   <div class="wpb_column vc_column_container vc_col-sm-2">
      <div class="vc_column-inner vc_custom_1526899963628">
         <div class="wpb_wrapper"></div>
      </div>
   </div>
 
   <div class="wpb_column vc_column_container vc_col-sm-2">
      <div class="vc_column-inner">
         <div class="wpb_wrapper"></div>
      </div>
   </div>
</div>
       <!-- Breadcrumb -->
    <div class="page-content">

        <!-- Property Head Ends -->
        @if(Session::has('tabName'))
            @php $tabName = Session::get('tabName'); @endphp
        @endif
        <!-- Property Tab Starts -->
<div class="wraper_inner_banner">
   <div class="wraper_inner_banner_main">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="inner_banner_main">
                  <p class="title" style="text-align:left;font-size:36px;">
                    Profile
                  </p>
                  <p class="subtitle" style="text-align:left;font-size:16px;color:#fff !important;text-align: left;">                                              
                                    <span style="font-size:25px;">Welcome {{@$user->name}},</span> you now have access to greater financial details related<br> to our offerings, so you can make an informed investment decision.                                 
                   
                  </p>
                   <p class="subtitle" style="text-align:left;font-size:15px;color:#fff !important;text-align: left;">
                  
                           
                                
                       <a href="{{url('/dashboard')}}" class="pro-breadcrumbs-item" style="color:#cbc6c6;font-style:italic">Home</a>
                <span>/</span>
                <a href="#" class="pro-breadcrumbs-item" style="color:#cbc6c6;font-style:italic">Settings</a>
                <span>/</span>
                 <a href="#" class="pro-breadcrumbs-item" style="color:#cbc6c6;font-style:italic">Profile</a>
                
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div> 

 <div class="wpb_column vc_column_container vc_col-sm-12" style="padding:0 100px;margin-top:90px;">
      <div class="vc_column-inner">
         <div class="wpb_wrapper">
            <div class="rt-tab element-five rt1780428986 ">
               <ul class="nav-tabs">
                  <li data-tab-icon="false" class="matchHeight" style=""><a data-toggle="tab" href="#e262714b-64bd-6" aria-expanded="false"><span>KYC</span></a></li>
                  <li data-tab-icon="false" class="matchHeight" style=""><a data-toggle="tab" href="#65882d53-4a58-7" aria-expanded="false"><span>Change Password</span></a></li>
                   <?php if(Auth::user()->kyc==1){ ?>
                  <li data-tab-icon="false" class="matchHeight" style=""><a data-toggle="tab" href="#65882d53-4a58-8" aria-expanded="false"><span>Bank Details</span></a></li>
                 <?php  } ?>
               </ul>
               <div class="tab-content">
                 @include('common.notify')
                  <div id="e262714b-64bd-6" class="tab-pane fade">

 <form role="form" id="regForm" action="{{route('profile.identity')}}" method="POST" enctype="multipart/form-data" id="identity-form">
 @csrf()
  <div class="tab">
        <div class="col-xs-6 col-sm-6">
            <p>
                <label class="control-label">Country</label>     
                <select required="required"  class="form-control selectpicker" id="select-country" placeholder="Country" name="country_code"/>
                    <option value="">Select Country</option>
                    @foreach(countries() as $country)
                        <option value="{{$country->code}}" @if(@$user->identity->country_code == $country->code) selected @endif>{{$country->countryname}}</option>
                    @endforeach
                </select>  
                <span id="country_code_form" style="color:#ff0000;"></span>
            </p>
           <p>  
            <label class="control-label">Type</label>
            <select required="required" class="form-control kyc-type selectpicker" placeholder="Kyc Type" name="type"/>
            <option value="personal">Personal</option>
            <option value="company">Company</option>
            </select> </p></div>

            <div class="col-xs-6 col-sm-6">
                <div style="height:200px;"></div>
            </div>
        
  </div><!--Tab Closing--> 
  <div class="tab">  <div class="col-xs-6 col-sm-6">
<p>
<label class="control-label">First Name</label>
<input maxlength="30" type="text" required class="form-control personal" id="first_name" placeholder="Enter First Name" name="first_name" value="{{ (!empty($user->identity->first_name)) ? @$user->identity->first_name : old('first_name') }}"/>
<span id="first_name_form" style="color:#ff0000;"></span>
</p>
 <p>
<label class="control-label">Middle Name</label>
<input maxlength="30" type="text" class="form-control personal"  placeholder="Enter Middle Name" name="middle_name" value="{{ (!empty($user->identity->middle_name)) ? @$user->identity->middle_name : old('middle_name') }}"/>
</p>
<p>
<label class="control-label">Last Name</label>
<input maxlength="30" type="text" required class="form-control personal" id="last_name" placeholder="Enter Last Name" name="last_name" value="{{ (!empty($user->identity->last_name)) ? @$user->identity->last_name : old('last_name') }}"/>
<span id="last_name_form" style="color:#ff0000;"></span>
</p>
<p>
    <label class="control-label">Date of Birth</label>
    <input maxlength="20" type="date" required="required" id="dob" class="form-control personal" placeholder="Date of Birth" name="dob" @if(@$user->identity->dob) value="{{date('Y-m-d',strtotime(@$user->identity->dob))}}" @endif max="{{date('Y-m-d')}}"/>
    <span id="dob_form" style="color:#ff0000;"></span>
</p>

 </div>
   <div class="col-xs-6 col-sm-6">
    <p>
        <label class="control-label">Address</label>
        <input maxlength="100" type="text" required="required" id="address" class="form-control personal" placeholder="Address" name="address" value="{{ (!empty($user->identity->address)) ? @$user->identity->address : old('address') }}"/>
        <span id="address_form" style="color:#ff0000;"></span>
    </p>
    <p>
        <label class="control-label">City</label>
        <input maxlength="50" type="text" required="required" id="city" class="form-control personal" placeholder="City" name="city" value="{{ (!empty($user->identity->city)) ? @$user->identity->city : old('city') }}"/>
        <span id="city_form" style="color:#ff0000;"></span>
    </p>
    <p>
        <label class="control-label">State</label>
        <input maxlength="50" type="text" required="required" id="state" class="form-control personal" placeholder="State" name="state" value="{{ (!empty($user->identity->state)) ? @$user->identity->state : old('state') }}"/>
        <span id="state_form" style="color:#ff0000;"></span>
    </p>
    <p>
        <label class="control-label">Pincode</label>
        <input maxlength="50" type="text" required="required" id="pincode" class="form-control personal" placeholder="Pincode" name="pincode" value="{{ (!empty($user->identity->pincode)) ? @$user->identity->pincode : old('pincode') }}"/>
          <span id="pincode_form" style="color:#ff0000;"></span>
    </p>

 </div>
  </div>
  <div class="tab">

  <div class="col-xs-6 col-sm-6">
      
 <p>
        <label class="control-label">PAN Number</label>
        <input maxlength="50" type="text" class="form-control" id="pan_number" name="pan_number" value="{{ (!empty($user->identity->pan_number)) ? @$user->identity->pan_number : old('pan_number') }}" placeholder="Enter PAN Number" required/>
        <span id="pan_number_form" style="color:#ff0000;"></span>
    </p>
    <p>
        <label class="control-label">Re-enter PAN Number</label>
        <input maxlength="40" type="text" id="repan_number" class="form-control" placeholder="Enter Re-enter PAN Number"  name="repan_number" value="{{ (!empty($user->identity->pan_number)) ? @$user->identity->pan_number : old('repan_number') }}" required/>
        <span id="repan_number_form" style="color:#ff0000;"></span>
    </p>
   <!-- <p>
        <label class="control-label">Upload Pan Card</label>
        @if(@$user->identity->pan)
            <img src="{{img(@$user->identity->pan)}}" width="50" class="popImage">
        @endif
        <input type="file" name="pan" class="form-control" id="" required>
    </p>-->
  

  </div>
<div class="col-xs-6 col-sm-6">
    <div style="height:300px"></div></div>

   
  </div>
  <!--<div class="tab">
    <div class="col-xs-6 col-sm-6">
        

<p>
<label class="control-label">Document Type</label>
<select name="documentType" id="documentType" class="form-control">
    <option value="aadhar">Aadhar</option>
    <option value="passport">Passport</option>
    <option value="driving">Driving License</option>
</select>
</p>
<p>
<label class="control-label">Document Number</label>
<input maxlength="40" type="text" class="form-control" placeholder="Enter Document Number" required  name="document_number" value="{{ (!empty($user->identity->document_number)) ? @$user->identity->document_number : old('document_number') }}"/>
</p>
<p>
<label class="control-label">Re-enter Document Number</label>
<input maxlength="40" required type="text" class="form-control" id="redocument_number" placeholder="Enter Re-enter Document Number"  name="redocument_number" value="{{ (!empty($user->identity->document_number)) ? @$user->identity->document_number : old('redocument_number') }}"/>
</p>
<p>
  <div class="form-group">
                                                            <label class="control-label">Upload Front of Document</label>
                                                            @if(@$user->identity->frontdocument)
                                                                <img src="{{img(@$user->identity->frontdocument)}}" width="50" class="popImage">
                                                            @endif
                                                            <input type="file" name="frontdocument" class="form-control" id="" required>
                                                        </div></p>-->

<!--<p>
<label class="control-label">Upload Back of Document</label>
@if(@$user->identity->backdocument)
    <img src="{{img(@$user->identity->backdocument)}}" width="50" class="popImage">
@endif
<input type="file" name="backdocument" class="form-control" id="" required>
</p>

       
    </div>
    <div class="col-xs-6 col-sm-6"><div style="height:500px"></div></div>

  </div>-->

 <!-- <div class="tab"><div class="col-xs-6 col-sm-6"><div style="height:500px">
      
<div class="form-group">
                                                            <label class="control-label">Terms & Condition</label>
                                                            <input type="checkbox" name="terms" id="">
                                                        </div>

  </div></div></div>-->
  <div style="">
    <div style="float:right;">
      <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
      <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
    </div>
  </div>
  <!-- Circles which indicates the steps of the form: -->
  <div style="text-align:center;margin-top:40px;">
    <span class="step">1</span>
    <span class="step">2</span>
    <span class="step">3</span>
   
  </div>
</form>






                  </div>
                  <div id="65882d53-4a58-7" class="tab-pane fade">

<form method="POST" action="{{ route('change.password') }}" data-parsley-validate="">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="userName">Current Password<span class="text-danger">*</span></label>
                                        <input type="password" name="current_password" required="" placeholder="" class="form-control" id="current_password" data-parsley-minlength="8" onkeypress="return RestrictSpace()">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="userName">New Password<span class="text-danger">*</span></label>
                                        <input type="password" name="password" required="" placeholder="" class="form-control" id="newpassword" data-parsley-minlength="8" onkeypress="return RestrictSpace()">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="userName">Confirm Password<span class="text-danger">*</span></label>
                                        <input type="password" name="password_confirmation" required="" data-parsley-equalto-message="The confirm password is not match with password" data-parsley-equalto="#newpassword" placeholder="" class="form-control" id="password_confirmation" data-parsley-minlength="8" onkeypress="return RestrictSpace()">
                                        <input class="btn btn-primary mr-1" type="submit" style="margin-top: 30px !important;    background-color: #251d59;
    color: #ffffff;
    border: none;
    padding: 10px 20px;
    font-size: 17px;
    font-family: 'Poppins';
    cursor: pointer;width:40%;float:right;" value="Update Password"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 form-group text-left mb-0">
                                        
                                    </div>
                                </div>
                            </form>

                  </div>
                   <div id="65882d53-4a58-8" class="tab-pane fade">
                       
                    <form method="POST" action="{{ url('issuer/updateBankDetails') }}" data-parsley-validate="">
                                                    @csrf

 <div class="row">
                     <?php
                   $accholdername='';$accno='';$bankname='';$ifsccode='';
                   foreach($bankdetail as $index=>$val){
                   $accholdername=$val->accountholdername;
                   $accno=$val->accno;
                   $bankname=$val->bankname;
                  $ifsccode=$val->ifsccode;
}
?>

                        <div class="col-sm-6">
                            <div class="form-group">
                            <label for="userName">Account Holder Name<span class="text-danger">*</span></label>
                            <input type="text" name="accountholdername" title="Please enter only characters" pattern="[a-zA-Z'-'\s]*" parsley-trigger="change" required="" placeholder="" class="form-control" id="accountholdername" value="{{$accholdername}}" required>
                             </div>
                             <div class="form-group">
                                <label for="userName">Account Number<span class="text-danger">*</span></label>
                                <input type="password" name="accnumber" parsley-trigger="change" required="" placeholder="" class="form-control" id="accnumber" value="{{$accno}}" required>
                            </div>
                             <div class="form-group">
                                <label for="userName">IFSC CODE<span class="text-danger">*</span></label>
                                <input type="text" name="ifsccode" parsley-trigger="change" required="" placeholder="" class="form-control" id="ifsccode" value="{{$ifsccode}}" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label for="userName">Bank Name<span class="text-danger">*</span></label>
                            <input type="text" name="bankname" title="Please enter only characters" pattern="[a-zA-Z'-'\s]*" parsley-trigger="change" required="" placeholder="" class="form-control" id="bankname" value="{{$bankname}}" required>
                             </div>
                            
                            <div class="form-group">
                                <label for="userName">Retype Account Number<span class="text-danger">*</span></label>
                                <input type="password" name="retypeaccno" parsley-trigger="change" required="" placeholder="" value="{{$accno}}" class="form-control" onchange="accountnumber();" id="retypeaccno" value="" required>
                            </div>
                            </div>
                       
                        </div>
                         <div class="row next-btn" style="padding:25px 0px;">
                                <div class="col-sm-12">
                                    <div class="form-group text-center mb-0">
                                        <input class="btn btn-primary waves-effect waves-light mr-1" type="submit" value="Update Bank Details" onsubmit="return bankdetails();" style="margin-top: 30px !important;    background-color: #251d59;
    color: #ffffff;
    border: none;
    padding: 10px 20px;
    font-size: 17px;
    font-family: 'Poppins';
    cursor: pointer;width:40%;float:right;">
                                      
                                    </div>
                                </div>
                            </div>
                    </form>

                   </div>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
<script>
   function accountnumber(){
    var accnum=document.getElementById('accnumber').value;
    var retypeaccno=document.getElementById('retypeaccno').value;
    if(accnum!=retypeaccno){
        alert('Please enter the correct account number.');
    }
   }
</script>
   <script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");

if(document.getElementById("select-country").value==""){
    document.getElementById("country_code_form").innerHTML="Please must select country";
    valid=false;
}
if(document.getElementById("first_name").value==""){
    document.getElementById("first_name_form").innerHTML="Please must enter First Name";
    valid=false;
}
if(document.getElementById("last_name").value==""){
    document.getElementById("last_name_form").innerHTML="Please must enter Last Name";
    valid=false;
}
if(document.getElementById("dob").value==""){
    document.getElementById("dob_form").innerHTML="Please must enter Date Of Birth";
    valid=false;
}
if(document.getElementById("address").value==""){
    document.getElementById("address_form").innerHTML="Please must enter Address";
    valid=false;
}
if(document.getElementById("city").value==""){
    document.getElementById("city_form").innerHTML="Please must enter city";
    valid=false;
}
if(document.getElementById("state").value==""){
    document.getElementById("state_form").innerHTML="Please must enter state";
    valid=false;
}
if(document.getElementById("pincode").value==""){
    document.getElementById("pincode_form").innerHTML="Please must enter pincode";
    valid=false;
}
if(document.getElementById("pan_number").value==""){
    document.getElementById("pan_number_form").innerHTML="Please must enter PAN Number";
    valid=false;
}


  // A loop that checks every input field in the current tab:
  /*for (i = 0; i < y.length; i++) {
    // If a field is empty...
    alert(y[i]);
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = true;
    }
  }*/
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}
</script>


        <div class="property-tab">
             <div class="pro-tab-wrap">
                <div class="container">
                    <!-- Nav tabs -->
                   
                </div>
            </div>
            <!-- Tab panes -->


           
        </div>
    </div>
    <!-- Property Tab Ends -->
 
@endsection


@section('scripts')
     <script src="https://auruminfinity.com/js/parsley.js"></script>
      
    <script type="text/javascript">
        $(function () {
          var $sections = $('.form-section-div');
          function navigateTo(index) {
            // Mark the current section with the class 'current'
            $sections
              .removeClass('current')
              .eq(index)
                .addClass('current');
            // Show only the navigation buttons that make sense for the current section:
            $('.prev-step').toggle(index > 0);
            var atTheEnd = index >= $sections.length - 1;
            $('.next-step').toggle(!atTheEnd);
            $('.form-sub').toggle(atTheEnd);
          }
        
          function curIndex() {
            // Return the current index by looking at which section has the class 'current'
            return $sections.index($sections.filter('.current'));
          }
        
          // Previous button is easy, just go back
          $('.prev-step').click(function() {
            navigateTo(curIndex() - 1);
          });
        
          // Next button goes forward iff current block validates
          $('.next-step').click(function() {
            $('#identity-form').parsley().whenValidate({
              group: 'block-' + curIndex()
            }).done(function() {
              navigateTo(curIndex() + 1);
            });
          });
        
          // Prepare sections by setting the `data-parsley-group` attribute to 'block-0', 'block-1', etc.
          $sections.each(function(index, section) {
            $(section).find(':input').attr('data-parsley-group', 'block-' + index);
          });
          navigateTo(0); // Start at the beginning
        });
    </script>        
    <script type="text/javascript">
        /* Signature Code */
        function readURL(input) {
            if (input.files && input.files[0]) {

                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.image-upload-wrap').hide();

                    $('.file-upload-image').attr('src', e.target.result);
                    $('.file-upload-content').show();

                    $('.image-title').html(input.files[0].name);
                };

                reader.readAsDataURL(input.files[0]);

            } else {
                removeUpload();
            }
        }

        function removeUpload() {
            $('.file-upload-input').replaceWith($('.file-upload-input').clone());
            $('.file-upload-content').hide();
            $('.image-upload-wrap').show();
        }
        $('.image-upload-wrap').bind('dragover', function () {
            $('.image-upload-wrap').addClass('image-dropping');
        });
        $('.image-upload-wrap').bind('dragleave', function () {
            $('.image-upload-wrap').removeClass('image-dropping');
        });
        /*End Signature Code */

        $(function () {
            $("#map").googleMap({
                                    zoom  : 15, // Initial zoom level (optional)
                                    coords: [17.438136, 78.395246], // Map center (optional)
                                    type  : "ROADMAP" // Map type (optional)
                                });
        })
        var cities = '';
        $('.country').on('change', function () {
            $('.city').html('');
            $.get("{{url('countrycity')}}/" + $(this).val())
             .done(function (data) {
                 $('.city').html('');
                 $.each(data, function (index, value) {
                     cities += '<option value=' + value.id + '>' + value.name + '</option>';
                 });
                 $('.city').html(cities);
             });
        });
    </script>
    <script type="text/javascript">
        $(document).on("ready", function(){
            $(".personal").keyup(function(){
                var empty = false;
                $(".personal").each(function(){
                    if ($(this).val() == '') {
                        empty = true;
                    }
                });
                if (empty) {
                    $('.pan-card-btn').attr('disabled', 'disabled');
                } else {
                    $('.pan-card-btn').removeAttr('disabled');
                }
            });
        });
    </script>
    <style>
        form .error {
            color: #ff0000;
        }

    </style>
@endsection

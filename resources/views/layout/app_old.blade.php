<!DOCTYPE html>
<html>

<head>
    <title>{{Setting::get('site_title')}}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="asset/img/fav.png" type="image/png" sizes="16x16">

    <!-- fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC:300,400,500,700" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="{{asset('public/asset/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome CSS -->
    <link href="{{asset('public/asset/css/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/asset/css/fontawesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/asset/css/slick.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('public/asset/css/slick-theme.css')}}">

    <!-- Data Table CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/asset/css/datatables.min.css')}}"/>
    <!-- <link rel="stylesheet" type="text/css" href="{{asset('public/asset/css/jquery.dataTables.min.css')}}" /> -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/asset/css/responsive.dataTables.min.css')}}" />
      
    
    <!-- Style CSS -->
    <link href="{{asset('public/asset/css/style.css')}}" rel="stylesheet">
    <style type="text/css">
        .errors{
            color: #f00;
        }
    </style>
</head>

<body>
    @if(Auth::check())
        @include('include.header')
        <main class="content-wrapper">
    @else
        <main class="content-wrapper login">
    @endif

    @if(Auth::check())
        @include('common.notify')
    @endif
    
    @yield('content')    
        
    </main>
    @include('include.footer-new')

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <!-- jquery -->
    <script src="{{asset('public/asset/js/jquery.min.js')}}"></script>
    <!-- bootstrap-4 -->
    <script src="{{asset('public/asset/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('public/asset/js/bootstrap-popper.min.js')}}"></script>
    <!-- <script src="{{asset('public/asset/js/bootstrap-slim.min.js')}}"></script> -->
    <!-- Fontawesome -->
    <script type="text/javascript" src="{{asset('public/asset/js/all.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/asset/js/fontawesome.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/asset/js/slick.min.js')}}"></script>

    <!-- Data Table CSS -->
   
   
    <script type="text/javascript" src="{{asset('public/asset/js/datatables.min.js')}} "></script>
    <!-- <script type="text/javascript" src="{{asset('public/asset/js/dataTables.bootstrap4.min.js')}} "></script> -->
    <script type="text/javascript" src="{{asset('public/asset/js/dataTables.responsive.min.js')}}"></script>

    <!-- Google chart -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- Scripts -->
    <script type="text/javascript" src="{{asset('public/asset/js/script.js')}}"></script>

    @yield('scripts')

</body>
</html>
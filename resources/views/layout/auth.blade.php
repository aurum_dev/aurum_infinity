<!DOCTYPE html>
<html>
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="keywords" content="Real Estate" />
    <meta name="description" content="Real Estate">
    <meta name="author" content="STO">
    <title>{{Setting::get('site_title')}}</title>
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Google Webfonts -->

    <!-- Style -->
     <link href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500&display=swap" rel="stylesheet">
     <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/3.0/css/font-awesome.css">
    @include('include.styles')
    <!-- End Style -->

</head>

 <body  class="page-template-default page page-id-3351 theme-seolounge woocommerce-no-js wpb-js-composer js-comp-ver-6.5.0 vc_responsive" data-page-transition="0" data-header-style="header-style-twelve" data-nicescroll-cursorcolor="#fb0000" data-nicescroll-cursorwidth="8px">
      <!-- preloader -->
      <div class="preloader" data-preloader-timeout="500">
         <div class="table">
            <div class="table-cell">
               <div class="sk-wave">
                  <div class="sk-rect sk-rect1"></div>
                  <div class="sk-rect sk-rect2"></div>
                  <div class="sk-rect sk-rect3"></div>
                  <div class="sk-rect sk-rect4"></div>
                  <div class="sk-rect sk-rect5"></div>
               </div>
            </div>
         </div>
      </div>
      <!-- preloader -->
      <!-- overlay -->
      <div class="overlay"></div>
      <!-- overlay -->
      <!-- scrollup -->
      <!-- scrollup -->
      <!-- radiantthemes-website-layout -->
      <div class="radiantthemes-website-layout full-width">

    <!-- Header -->
    @include('include.header')
    <!-- End Header -->

    @yield('content')  
    <!-- Footer -->
    @include('include.footer-new')

    <!-- End Footer -->
  </div>

  <!-- Scripts -->
  @include('include.scripts')
 
</body>
</html>
@php
    $currentRoute = (\Request::getRequestUri() == "/issuer/wallet"? true : false);
@endphp
@extends('issuer.layout.base')
@section('content')
<style>
     #example_length{
margin-top: 5px !important;
    float: right !important;
    margin-right: 250px !important;
}

    .copye {cursor: copy;}

    .introactive
  {
      background: #232020 !important;
    background: #140749 !important;
      color: #FFF;
    }
    .introactive .nav-link
  {
      color: #FFF;
    }
  .currencies-container .currency-balance
  {
    font-size: 20px;
    font-weight: bold;
    font-family: monospace;
    margin: 0;
    text-align: left;
    color: #fff;
  }
  .withdraw-instruction .steps
  {
    line-height: 30px;
  }
  .table-currencies
  {
    border: none !important;
    border-radius: 10px;
  }
  .currency-symbol
  {
    color: #fff;
  }
  .input-group-addon
  {
    padding: 6px 12px;




    
    font-size: 14px;
  }
  .details-container
  {
    border-radius: 10px;
  }
  .table td
  {
    border:none !important;
  }
  
</style>
   <!-- breadcrumb -->
                    <div class="breadcrumb-header justify-content-between">
                        <div class="my-auto">
                            <div class="d-flex">
                                <h4 class="content-title mb-0 my-auto"> Payment History</h4><span class="text-muted mt-1 tx-13 ms-2 mb-0"></span>
                            </div>
                        </div>
                        <div class="d-flex my-xl-auto right-content">
                            <a href="javascript:void(0);" style="margin-left: 1em;float: right;margin-bottom: 20px;cursor: default !important;font-size:25px;" class="btn btn-primary pull-right">TOTAL AMOUNT : &nbsp;&nbsp;
 <?php
$amtt=0;
  foreach($fiat_history as $history){
    $amtt+=$history->paytoissuer;
  }

  echo '<i class="fas fa-rupee-sign"></i> '.$amtt;
                                           ?>
                            </a>
                           
                        </div>
                    </div>
                    <!-- breadcrumb -->

<div class="content-page" style='word-wrap: break-word;  background: #fff!important;
    background-clip: border-box; border: 1px solid #deebfd; border-radius: 5px;
    box-shadow: -8px 12px 18px 0 #dadee8; display: flex; flex-direction: column;
    margin-bottom: 1.3rem;  min-width: 0; position: relative;'>
  


<div class="content">
    <!-- Start container-fluid -->
    <div class="container-fluid wizard-border">
        <section class="container spaceall wallet-full">
        <!-- Top Details -->

        <div class="container">

            <div class="panel panel-default">
                <div class="panel-body panel-currencies">

                    <div class="row">

                        <!-- Left Side Box-->
                        
                        <!-- End Left Size Box -->

                        <!-- Right Size Box -->
                        <div class="col-sm-12">
                            <div class="details-container tab-content">
                                <!-- ETH Deposit Tab -->
                                <div class="tab-pane active" id="fiat_deposit">
                                   
                                 

                                    <!-- Deposit History -->
                                    <table id="example" class="datatable-full table table-striped table-bordered custom-table-style" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                             <th>ID</th>                                           
                                           <th>Property</th>
                                           <th>Investor</th>
                                           <th>Total Tokens Purchased</th>
                                            <th>Amount</th>
                                            <th>Token Purchased Date</th>
                                            <th>Token Approved Date</th>
                                            <th>Token Transfer Status</th>
                                        </tr>
                                        </thead>
                                        @foreach($fiat_history as $history=>$val)
                                            <tr>
                                                <td>{{ $history + 1 }}</td>
                                                 <td><?php
$tmp = \App\Property::where('id',$val->property_id)->first();
echo $tmp->propertyName;
                                                 ?>
                                      </td>   
                                                <td><?php
$tmp = \App\User::where('id',$val->investor_id)->first();
echo $tmp->name."-".$tmp->email;
                                                 ?></td>                                            
                                                <td>{{ @$val->total_tokens }}</td>
                                                <td><i class="fas fa-rupee-sign"></i>  {{number_format($val->paytoissuer,3)}}</td>
                                                <td>{{ date("d, M Y",strtotime(@$val->created_at)) }}</td>
                                                <td>
                                                <?php    if($val->txnhash!=''){
                                                     echo date("d, M Y",strtotime($val->updated_at));
                                                 }

                                                    ?>
                                                </td>
                                                <td>
                                                    @if(@$val->txnhash=='')
                                                    <a href="javascript:void(0);"class="btn btn-danger btn-block" style="padding:9px 10px;float:left;display:block;margin-top:0;cursor:default !important" title="Edit">Pending</i>
                                </a>
                                                   
                                                    @else
                                                    <a href="javascript:void(0);"class="btn btn-success btn-block" style="padding:9px 10px;float:left;display:block;margin-top:0;cursor:default !important" title="Edit">Success</i>
                                </a>
                                                    @endif

                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                     <!-- End Deposit History -->

                                </div>
                                <!-- End ETH Deposit Tab -->
                            </div>
                        </div>

                    </div>
                    <!-- .row -->

                </div>
            </div>

        </div>

        </section>
      
    </div>
</div>
<script type="text/javascript" src="https://auruminfinity.com/js/jquery.qrcode.js"></script>
<script type="text/javascript" src="https://auruminfinity.com/js/qrcode.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
 $(document).ready(function(){
    $(".table-currencies").click(function() {
       $('.table-currencies').removeClass('introactive');
      $(this).addClass('introactive');
    });
});
</script>
<script type="text/javascript">
    var btc_address = $('#qrcode-BTC').data('text');
    $('#qrcode-BTC').qrcode(btc_address);
    var eth_address = $('#qrcode-ETH').data('text');
    $('#qrcode-ETH').qrcode(eth_address);


    var alert = $(".alert-container");
    alert.hide();

    function copyToClipboard(element) {
      
      var $temp = $("<input>");
      $("body").append($temp);
      $temp.val($(element).text()).select();
      document.execCommand("copy");
      $temp.remove();
      alert.slideDown();
        window.setTimeout(function() {
          alert.slideUp();
        }, 2500);
    }

</script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script type="text/javascript">
     $(document).ready(function(){
       $(".pay").on("click", function () {
        if($("#amount").val() == ''){
            alert("Please enter amount");
            return false;
        }
        var amount = $("#amount").val();
        var name = "{{ Auth::user()->name }}";
        var key = "{{ getenv('RAZORPAY_KEY') }}";
        var image = "{{ asset('logo.png') }}";
        var token = "{{ csrf_token() }}";
        var options = {
            "key": key,
            "amount": (amount * 100), // 2000 paise = INR 20
            "name": name,
            "description": "Deposit",
            "image": image,
            "handler": function (response){
                $.ajax({
                    url: '{{ url("/issuer/storePayment") }}',
                    type: 'post',
                    data: "_token="+token+"&razorpay_payment_id="+response.razorpay_payment_id+"&totalAmount="+amount, 
                    success: function (msg) {
                        alert("Payment has been done");
                        window.location.reload();
                    }
                });
            
            },

            "theme": {
                "color": "#528FF0"
            }
        };
        var rzp1 = new Razorpay(options);
        rzp1.open();
        e.preventDefault();
    });
     });

</script>
@endsection

@section('styles')
<style type="text/css">
    .introactive {
        background: #232020 !important;
            background: #140749 !important;
        color: #FFF;
    }
    .introactive .nav-link {
        color: #FFF;
    }



div#deposit_address {
    font-size: 13px;
    font-weight: 500;
    padding: 11px;
    color: #000;
}
.custom-table-style thead tr th {
    vertical-align: middle;
    font-size: 12px;
    padding: 8px !important;
}
.custom-table-style tbody tr td {
    vertical-align: middle;
    font-size: 12px;
    padding: 8px !important;
    letter-spacing: .1px;
}
.send-code-button button.btn.btn-primary {
    padding: 10px 20px;
    background: #5f56e0;
    border: 1px solid #5f56e0;
}
.selectable{
    -webkit-touch-callout: all; /* iOS Safari */
    -webkit-user-select: all; /* Safari */
    -khtml-user-select: all; /* Konqueror HTML */
    -moz-user-select: all; /* Firefox */
    -ms-user-select: all; /* Internet Explorer/Edge */
    user-select: all; /* Chrome and Opera */

}
.alert-container {
  position: fixed;
    bottom: 5px;
    left: 11%;
    width: 32%;
    margin: 0 25% 0 25%;
    background-color: #e0afaf;
    z-index: 1;
}

  .alert {
    text-align: center;
    padding: 17px 0 20px 0;
    margin: 0 25% 0 25%;
    height: 54px;
    font-size: 20px;
  }
</style>
@endsection

@section('scripts')


@endsection

@extends('issuer.layout.base')
@section('content')
<style>
    .tooltip {
  position: relative;
  display: inline-block;
 
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 349px;
    background-color: #ebebeb;
    color: #333;
    text-align: center;
    border-radius: 6px;
    padding: 10px 10px;
    position: absolute;
    font-size: 14px;
    font-weight: 500;
    z-index: 1;
    margin-left: -180px;
    margin-top: 20px;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
}</style>
<script>
function checkPhoto(target,id,id2) {
    if(target.files[0].size > 2000000) {
        alert("Image is too big (max 2MB)");
        document.getElementById(id).value =1;
         document.getElementById(id2).value='';
        return false;
    }    else{
        document.getElementById(id).value =0;
    }
}
function checkPhotoFile(target,id) {
    if(target.files[0].size > 8000000) {
        alert("File is too big (max 8 MB)");
        document.getElementById(id).value =1;
        document.getElementById(id).src='';
        return false;
    }   
     else{
        document.getElementById(id).value =0;
    } 
}

</script>
                <!-- breadcrumb -->
                    <div class="breadcrumb-header justify-content-between">
                        <div class="my-auto">
                            <div class="d-flex">
                                <h4 class="content-title mb-0 my-auto"> Create Token & Property</h4><span class="text-muted mt-1 tx-13 ms-2 mb-0">/ Add New Property</span>
                            </div>
                        </div>
                        <div class="d-flex my-xl-auto right-content">
                          
                           
                        </div>
                    </div>
                    <!-- breadcrumb -->

<div class="row">
                        
                        <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    
                                    <form class="form-horizontal" action="{{action('IssuerController@storeProperty')}}" method="post" enctype="multipart/form-data">
                                  @csrf
                                <input type="hidden" name="token_type" value="1">
                                    <div id="wizard2">
                                        <h3>Property Overview Details</h3>
                                        
                                        <section>
                                            <p class="mg-b-20">Try the keyboard navigation by clicking arrow left or right!</p>
                                            <p style="color:#000;"><b>Note :</b> <i><span class="tx-danger">*</span> Fields are Mandatory</i></p>
                                            <div class="row row-sm">
                                                <div class="col-md-5 col-lg-4">
                                                    <label class="form-control-label">Property Name: <span class="tx-danger">*</span></label>
                                                    <input class="form-control" id="propertyname" name="propertyname" placeholder="Enter Property Name" required="" type="text">
                                                    <div class="tx-danger" id="propertyname-form" style="padding-top:10px;"></div>
                                                </div>
                                                
                                                <div class="col-lg-4 mg-t-20 mg-lg-t-0">
                                            <label class="form-control-label">Property State: <span class="tx-danger">*</span></label>
                                            
                                           
                                            <input type="hidden" name="country" id="countryId" value="IN"/>
<select name="state" class="states order-alpha form-control select2" id="stateId">
    <option value="">Select State</option>
</select>

<div class="tx-danger" id="stateId-form" style="padding-top:10px;"></div>
                                        
  <script src="https://geodata.solutions/includes/statecity.js"></script>                                          
                                            
                                        </div><!-- col-4 -->
                                        <div class="col-lg-4 mg-t-20 mg-lg-t-0">
                                            <label class="form-control-label">Property City: <span class="tx-danger">*</span></label>
                                        
<select name="city" class="cities order-alpha form-control select2" id="cityId">
    <option value="">Select City</option>
</select>

<div class="tx-danger" id="cityId-form" style="padding-top:10px;"></div>
   

                                            
                                        </div><!-- col-4 -->
                                        
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Property District/Town: <span class="tx-danger">*</span></label> 
                                                    <input class="form-control" id="propertydistrict" name="propertydistrict" placeholder="Enter Property District/Town" required="" type="text">
                                            <div class="tx-danger" id="propertydistrict-form" style="padding-top:10px;"></div>
                                            </div>
                                                <div class="col-lg-4 mg-t-20 mg-md-t-30">
                                            
                                            <label class="form-control-label">Property Type: <span class="tx-danger">*</span></label>
                                            <select name="propertytype" id="propertytype" class="form-control select2">
                                                <option value="">Select Property Type</option>
   
                                                @foreach ($assetType as $value)
                                                    <option value="{{ @$value->type }}">{{ @$value->type }}</option>
                                                @endforeach
</select>
<div class="tx-danger" id="propertytype-form" style="padding-top:10px;"></div>


                                            
                                            
                                        </div><!-- col-4 -->
                                        <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Token Name: <span class="tx-danger">*</span></label> 
                                                    <input class="form-control" id="tokenname" name="tokenname" placeholder="Enter Token Name" required="" type="text">
                                                <div class="tx-danger" id="tokenname-form" style="padding-top:10px;"></div>
                                                </div>
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Token Symbol : <span class="tx-danger">*</span></label> 
                                                    <input class="form-control" id="tokensymbol" name="tokensymbol" placeholder="Enter Token Symbol" required="" type="text">
                                                <div class="tx-danger" id="tokensymbol-form" style="padding-top:10px;"></div>
                                                </div>
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Token Value (INR): <span class="tx-danger">*</span>

<div class="tooltip" style="color:#ee335e;font-weight:bold;margin-left:10px;font-size:16px;"><i class="fa fa-info-circle" aria-hidden="true"></i>
  <span class="tooltiptext">Token value will be the 1 token value. <br>for e.g. 1 Token=2000</span>
</div>
                                                    </label> 
                                                    <input class="form-control" id="tokenvalue" name="tokenvalue" oninput="this.value = 
 !!this.value && Math.abs(this.value) > 0 ? Math.abs(this.value) : null" placeholder="Enter Token Value (INR)" required="" type="number" onkeypress="inpNum(event)">
                                                <div class="tx-danger" id="tokenvalue-form" style="padding-top:10px;"></div>
                                                </div>
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Token Supply (INR): <span class="tx-danger">*</span>
<div class="tooltip" style="color:#ee335e;font-weight:bold;margin-left:10px;font-size:16px;"><i class="fa fa-info-circle" aria-hidden="true"></i>
  <span class="tooltiptext">Token supply is the total no of tokens of your property.</span>
</div>
                                                    </label> 
                                                    <input class="form-control" id="tokensupply" name="tokensupply" placeholder="Enter Token Supply (INR)" oninput="this.value = 
 !!this.value && Math.abs(this.value) > 0 ? Math.abs(this.value) : null" required="" type="number" onkeypress="inpNum(event)">
                                                <div class="tx-danger" id="tokensupply-form" style="padding-top:10px;"></div>
                                                </div>
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Token Decimal : <span class="tx-danger">*</span></label> 
                                                    <input class="form-control" id="tokendecimal" oninput="this.value = 
 !!this.value && Math.abs(this.value) > 0 ? Math.abs(this.value) : null" name="tokendecimal" placeholder="Enter Token Decimal" required="" type="text">
                                                <div class="tx-danger" id="tokendecimal-form" style="padding-top:10px;"></div>
                                                </div>
                                                 <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30" style="float: left;display: inline-block;margin-top:32px;">
                                      <h3 style="font-size:17px;"><label class="form-control-label">Token Logo: <span class="tx-danger">*</span></label> </h3>
                                     
                                        
                                       
                                                        <input type="file" onchange="checkPhoto(this,'tokenlogovalid','tokenlogo')" class="" id="tokenlogo" name="tokenlogo" accept="image/jpg, image/jpeg, image/png" style="margin-top:-6px;"/>
                                                         <input type="hidden" id="tokenlogovalid" value="0">
                                                        <div class="tx-danger" id="tokenlogo-form" style="padding-top:10px;"></div>
                                                    </div>
                                                    
                                                 <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Divident : <span class="tx-danger">*</span>
<div class="tooltip" style="color:#ee335e;font-weight:bold;margin-left:10px;font-size:16px;"><i class="fa fa-info-circle" aria-hidden="true"></i>
  <span class="tooltiptext">Is that for bonus token</span>
</div>

                                                    </label> 
                                                    <input class="form-control" id="divident" name="divident" placeholder="Enter Divident" required="" type="text">
                                                <div class="tx-danger" id="divident-form" style="padding-top:10px;"></div>
                                                </div>
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Minimum Investment (Tokens): <span class="tx-danger">*</span></label> 
                                                    <input class="form-control" id="minimuminvestment" oninput="this.value = 
 !!this.value && Math.abs(this.value) > 0 ? Math.abs(this.value) : null" name="minimuminvestment" placeholder="Enter Minimum Investment (Tokens)" required="" type="text">
                                                <div class="tx-danger" id="minimuminvestment-form" style="padding-top:10px;"></div>
                                                </div>
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Property Size: <span class="tx-danger">*</span></label> 
                                                    <input class="form-control" id="propertysize" name="propertysize" placeholder="Property Size" required="" type="text">
                                                    <div class="tx-danger" id="propertysize-form" style="padding-top:10px;"></div>
                                                </div>
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Target IRR: <span class="tx-danger">*</span></label> 
                                                    <input class="form-control" id="targetirr" name="targetirr" placeholder="Target IRR" required="" type="text">
                                                    <div class="tx-danger" id="targetirr-form" style="padding-top:10px;"></div>
                                                </div>
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Average Yield(%): <span class="tx-danger">*</span></label>
                                                    <input class="form-control" id="averageyield" name="averageyield" placeholder="Average Yield(%)" required="" type="text">
                                                <div class="tx-danger" id="averageyield-form" style="padding-top:10px;"></div>
                                                </div>
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30" style="float: left;display: inline-block;">
                                                 <label class="form-control-label">Property Description: <span class="tx-danger">*</span></label> 
                                                <textarea class="form-control" id="propdesc" name="propdesc" placeholder="" required="" rows="3"></textarea>
                                            <div class="tx-danger" id="propdesc-form" style="padding-top:10px;"></div>
                                            </div>
                                            </div>
                                        </section>
                                        <h3>Property Details</h3>
                                        <section>
                                            <p>Please add the property details.</p>
                                            
                                            <p style="color:#000;"><b>Note :</b> <i><span class="tx-danger">*</span> Fields are Mandatory</i></p>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Proponent: <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="proponent" name="proponent" placeholder="Enter Proponent" required="" type="text">
                                            <div class="tx-danger" id="proponent-form" style="padding-top:10px;"></div>
                                            </div>
                                            
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Map Link: please click on the <a href="https://media.star-telegram.com/static/labs/GoogleMapLink2/index.html" target="_blank">Link</a><span class="tx-danger">*</span> 

<div class="tooltip" style="color:#ee335e;font-weight:bold;margin-left:10px;font-size:16px;"><i class="fa fa-info-circle" aria-hidden="true"></i>
  <span class="tooltiptext">Please click on the link and to enter your address it will provide you map link.</span>
</div>
                                                </label> 
                                                <input class="form-control" id="onmap" name="onmap" placeholder="Enter Map Link" required="" onchange="validation()" type="text">
                                                <div class="tx-danger" id="onmap-form" style="padding-top:10px;"></div>
                                            </div>
                                           <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                 <label class="form-control-label">Address: <span class="tx-danger">*</span></label> 
                                                <textarea class="form-control" id="address" name="address" placeholder="Address" required="" rows="3"></textarea>
                                            <div class="tx-danger" id="address-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                            <label class="form-control-label">Property Status: <span class="tx-danger">*</span></label>
                                            
<select name="propertystatus" class="form-control select2" id="propertystatus">
    <option value="">Select Property Status</option>
     <option value="Ready to Move">Ready to Move</option>
 <option value="Under Construction">Under Construction</option>
</select>
<div class="tx-danger" id="propertystatus-form" style="padding-top:10px;"></div>
                                        </div><!-- col-4 -->
                                        <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Year of Build: <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="buildyear" name="buildyear" placeholder="Enter Year Of Build" required="" type="text">
                                            <div class="tx-danger" id="buildyear-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">RERA Number: <span class="tx-danger">*</span></label> 
                                                <input class="form-control" id="reranumber" name="reranumber" placeholder="Enter RERA Number" required="" type="text">
                                            <div class="tx-danger" id="reranumber-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Property Manager: <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="propertymanager" name="propertymanager" placeholder="Enter Property Manager" required="" type="text">
                                            <div class="tx-danger" id="propertymanager-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Custodian: <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="custodian" name="custodian" placeholder="Enter Custodian Person Name" required="" type="text">
                                            <div class="tx-danger" id="custodian-form" style="padding-top:10px;"></div>
                                            </div>
                                        </section>
                                        <h3>Property Features</h3>
                                        <section>
                                        <p>Please select the property amenities and upload technical specification.</p>
                                        <p style="color:#000;"><b>Note :</b> <i><span class="tx-danger">*</span> Fields are Mandatory</i></p>
                                        
                                        <h3 style="font-size:17px;"><label class="form-control-label">Property Amenities: <span class="tx-danger">*</span></label> </h3>
                                        <div class="tx-danger" id="amenities-form" style="padding-top:10px;padding-bottom:10px;"></div>
                                        
                                        <div class="col-lg-12" style="float: left;display: inline-block;margin-top:10px;">  


                                            @foreach ($amenity as $value)
                                            <div class="col-lg-2" style="float: left;display: inline-block;margin-top:10px;">                                           
                                                <label class="ckbox"><input type="checkbox" name="amenities[]" value="{{ @$value->amenity_name }}"><span>{{ @$value->amenity_name }}</span></label>
                                            </div>
                                            @endforeach
                                           
                                            
                                            </div>
                                      <div class="col-lg-12" style="float: left;display: inline-block;margin-top:20px;">
                                      <h3 style="font-size:17px;"><label class="form-control-label">Property Technical Specification: <span class="tx-danger">*</span></label> <span style="font-size:13px;">(Tech Specs Sample Format <a href="<?php echo url('/') ?>/public/issuer/assets/pdf/Tech_Specs_Template.xlsx">Download</a>)</span></h3>
                                        <p style="color:#000;"><b>Note :</b> <i>Only PDF Format are allowed</i></p>
                                        
                                        </div>
                                        <div class="col-sm-12 col-md-4" style="float: left;display: inline-block;">
                                            <input type="file" class="" id="techspecs" name="techspecs" accept="application/pdf" onchange="checkPhotoFile(this,'techspecsvalid')"/>
                                            <div class="tx-danger" id="techspecs-form" style="padding-top:10px;"></div>
                                             <input type="hidden" id="techspecsvalid" value="0">
                                        </div>
                                        
                                        

                                        
                                        
                                        
                                        
                                        </section>
                                        
                                        
                                        <h3>Financials Details</h3>
                                        <section>
                                            <p>Please add the financial details.</p>
                                            <p style="color:#000;"><b>Note :</b> <i><span class="tx-danger">*</span> Fields are Mandatory</i></p>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Cost of Premise: <span class="tx-danger">*</span></label> 
                                                <input class="form-control" id="costofpremise" name="costofpremise" placeholder="Enter Cost of Premise" required="" type="text">
                                            <div class="tx-danger" id="costofpremise-form" style="padding-top:10px;"></div>
                                            </div>
                                            
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Stamp Duty Registration: <span class="tx-danger">*</span></label> 
                                                <input class="form-control" id="stampdutyregistration" name="stampdutyregistration" placeholder="Enter Stamp Duty Registration" required="" type="text">
                                        <div class="tx-danger" id="stampdutyregistration-form" style="padding-top:10px;"></div>
                                        </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Acquisition Fee: <span class="tx-danger">*</span></label> 
                                                
                                                <div class="input-group">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">&#8377;</span>
                                                </div><input aria-label="Amount (to the nearest dollar)" class="form-control" id="acquisitionfee" name="acquisitionfee" type="number" onkeypress="inpNum(event)">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">.00</span>
                                                </div>
                                            </div><!-- input-group -->
                                            <div class="tx-danger" id="acquisitionfee-form" style="padding-top:10px;"></div>
                                            </div>
                                            
                                        <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Other Charges: <span class="tx-danger">*</span></label> 
                                            
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">&#8377;</span>
                                                </div><input aria-label="Amount (to the nearest dollar)" class="form-control" id="othercharges" name="othercharges" type="number" onkeypress="inpNum(event)">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">.00</span>
                                                </div>
                                            </div><!-- input-group -->
                                        <div class="tx-danger" id="othercharges-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Legal Fees: <span class="tx-danger">*</span></label> 
                                            
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">&#8377;</span>
                                                </div><input aria-label="Amount (to the nearest dollar)" class="form-control" name="legalfees" id="legalfees" type="number" onkeypress="inpNum(event)">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">.00</span>
                                                </div>
                                            </div><!-- input-group -->
                                        <div class="tx-danger" id="legalfees-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Maintenance Reserve: <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="maintenancereserve" name="maintenancereserve" placeholder="Enter Maintenance Reserve" required="" type="text">
                                            <div class="tx-danger" id="maintenancereserve-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Admin Expenses: <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="adminexpenses" name="adminexpenses" placeholder="Enter Admin Expenses" required="" type="text">
                                                <div class="tx-danger" id="adminexpenses-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Monthly OPEX: <span class="tx-danger">*</span></label> 
                                                <input class="form-control" id="monthlyopex" name="monthlyopex" placeholder="Enter Monthly OPEX" required="" type="text">
                                                <div class="tx-danger" id="monthlyopex-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Monthly Property Tax: <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="monthlypropertytax" name="monthlypropertytax" placeholder="Enter Monthly Property Tax" required="" type="text">
                                                <div class="tx-danger" id="monthlypropertytax-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Other Monthly Regulatory Charges: <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="monthlyregulatorycharges" name="monthlyregulatorycharges" placeholder="Enter Other Monthly Regulatory Charges" required="" type="text">
                                                <div class="tx-danger" id="monthlyregulatorycharges-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Total Acquisition cost: <span class="tx-danger">*</span></label> 
                                            
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">&#8377;</span>
                                                </div><input aria-label="Amount (to the nearest dollar)" id="totalacquisitioncost" name="totalacquisitioncost" class="form-control" type="number" onkeypress="inpNum(event)">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">.00</span>
                                                </div>
                                            </div><!-- input-group -->
                                        <div class="tx-danger" id="totalacquisitioncost-form" style="padding-top:10px;"></div>
                                            </div>
                                        </section>
                                        
                                        <h3>Lease Details</h3>
                                        <section>
                                            <p>Please add the Lease details.</p>
                                            <p style="color:#000;"><b>Note :</b> <i><span class="tx-danger">*</span> Fields are Mandatory</i></p>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Carpet Area (sq. ft): <span class="tx-danger">*</span></label> 
                                                <input class="form-control" id="carpetarea" name="carpetarea" placeholder="Enter Carpet Area" required="" type="text">
                                            <div class="tx-danger" id="carpetarea-form" style="padding-top:10px;"></div>
                                            </div>
                                            
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Built Up Area (sq. ft): <span class="tx-danger">*</span></label> 
                                                <input class="form-control" id="builtuparea" name="builtuparea" placeholder="Enter Built Up Area" required="" type="text">
                                                <div class="tx-danger" id="builtuparea-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Monthly Rent: <span class="tx-danger">*</span></label> 
                                                
                                                <div class="input-group">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">&#8377;</span>
                                                </div><input aria-label="Amount (to the nearest dollar)" id="monthlyrent" name="monthlyrent" class="form-control" type="number" onkeypress="inpNum(event)">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">.00</span>
                                                </div>
                                            </div><!-- input-group -->
                                            <div class="tx-danger" id="monthlyrent-form" style="padding-top:10px;"></div>
                                            </div>
                                            
                                    
                                        
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Leased Period (Months): <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="leasedperiod" name="leasedperiod" placeholder="Enter Leased Period" required="" type="text">
                                            <div class="tx-danger" id="leasedperiod-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Security Deposit: <span class="tx-danger">*</span></label> 
                                                <input class="form-control" id="securitydeposit" name="securitydeposit" placeholder="Enter Security Deposit" required="" type="text">
                                            <div class="tx-danger" id="securitydeposit-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Escalation: <span class="tx-danger">*</span></label> 
                                                <input class="form-control" id="escalation" name="escalation" placeholder="Enter Escalation" required="" type="text">
                                            <div class="tx-danger" id="escalation-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Tenants: <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="tenants" name="tenants" placeholder="Enter Tenants" required="" type="text">
                                            <div class="tx-danger" id="tenants-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Fitted out : <span class="tx-danger">*</span></label> 
                                                <input class="form-control" id="fittedout" name="fittedout" placeholder="Enter Fitted out" required="" type="text">
                                            <div class="tx-danger" id="fittedout-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Monthly Maintenance : <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="monthlymaintenance" name="monthlymaintenance" placeholder="Enter Monthly Maintenance" required="" type="text">
                                                <div class="tx-danger" id="monthlymaintenance-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Lease Chart : <span class="tx-danger">*</span></label> 
                                                <input class="form-control" id="leasechart" name="leasechart" placeholder="Enter Lease Chart" required="" type="text">
                                            <div class="tx-danger" id="leasechart-form" style="padding-top:10px;"></div>
                                            </div>
                                            
                                        </section>
                                        <h3>Return Analysis</h3>
                                        <section>
                                            <p>Please add the Lease details.</p>
                                            <p style="color:#000;"><b>Note :</b> <i><span class="tx-danger">*</span> Fields are Mandatory</i></p>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Gross Yield (%): <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="grossyield" name="grossyield" placeholder="Enter Gross Yield (%)" required="" type="text">
                                            <div class="tx-danger" id="grossyield-form" style="padding-top:10px;"></div>
                                            </div>
                                            
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Expected IRR (%): <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="expectedirr" name="expectedirr" placeholder="Enter Expected IRR (%)" required="" type="text">
                                            <div class="tx-danger" id="expectedirr-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Token Lockin Period (months): <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="tokenlockinperiod" name="tokenlockinperiod" placeholder="Enter Token Lockin Period" required="" type="text">
                                            <div class="tx-danger" id="tokenlockinperiod-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Aurum Facilation fees (%): <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="aurumfacilationfees" name="aurumfacilationfees" placeholder="Enter Aurum Facilation fees" required="" type="text">
                                            <div class="tx-danger" id="aurumfacilationfees-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Performance Fees: <span class="tx-danger">*</span></label> 
                                                <input class="form-control" id="performancefees" name="performancefees" placeholder="Enter Performance Fees" required="" type="text">
                                            <div class="tx-danger" id="performancefees-form" style="padding-top:10px;"></div>
                                            </div>
                                        
                                            
                                        </section>
                                        
                                        <h3>Property Reports</h3>
                                        <section>
                                        <p>Please upload the reports and documents.</p>
                                        <p style="color:#000;"><b>Note :</b> <i><span class="tx-danger">*</span> Fields are Mandatory</i></p>
                                    <p style="color:#000;"><b>Note :</b> <i>Only PDF Format are allowed</i></p>
                                        <div class="col-sm-6 col-md-4" style="float: left;display: inline-block;">
                                        
                                        <label class="form-control-label">Investment Deck : <span class="tx-danger">*</span></label>
                                            <input type="file" class="" id="investmentdeck" name="investmentdeck" accept="application/pdf" onchange="checkPhotoFile(this,'investmentdeckvalid')"/>
                                            <div class="tx-danger" id="investmentdeck-form" style="padding-top:10px;"></div>
                                            <input type="hidden" id="investmentdeckvalid" value="">
                                        </div>
                                        <div class="col-sm-6 col-md-4 " style="float: left;display: inline-block;">
                                        <label class="form-control-label">Title Report : <span class="tx-danger">*</span></label>
                                            <input type="file" class="" id="titlereport" name="titlereport"  accept="application/pdf" onchange="checkPhotoFile(this,'titlereportvalid')"/>
                                            <div class="tx-danger" id="titlereport-form" style="padding-top:10px;"></div>
                                             <input type="hidden" id="titlereportvalid" value="0">
                                        </div>
                                        <div class="col-sm-6 col-md-4" style="float: left;display: inline-block;">
                                        <label class="form-control-label">Property Due Diligence : <span class="tx-danger">*</span></label>
                                            <input type="file" class="" id="propertyduediligence" name="propertyduediligence" accept="application/pdf" onchange="checkPhotoFile(this,'propertyduediligencevalid')"/>
                                            <div class="tx-danger" id="propertyduediligence-form" style="padding-top:10px;"></div>
                                             <input type="hidden" id="propertyduediligencevalid" value="0">
                                        </div>
                                        <div class="col-sm-6 col-md-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                        <label class="form-control-label">Micro market Report : <span class="tx-danger">*</span></label>
                                            <input type="file" class="" id="micromarketreport" name="micromarketreport" accept="application/pdf" onchange="checkPhotoFile(this,'micromarketreportvalid')"/>
                                            <div class="tx-danger" id="micromarketreport-form" style="padding-top:10px;"></div>
                                            <input type="hidden" id="micromarketreportvalid" value="0">
                                        </div>
                                        <div class="col-sm-6 col-md-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                        <label class="form-control-label">Property Management Agreement : <span class="tx-danger">*</span></label>
                                            <input type="file" class="" id="propertymanagementagreement" name="propertymanagementagreement" onchange="checkPhotoFile(this,'propertymanagementagreementvalid')" accept="application/pdf"/>
                                            <div class="tx-danger" id="propertymanagementagreement-form" style="padding-top:10px;"></div>
                                             <input type="hidden" id="propertymanagementagreementvalid" value="0">
                                        </div>
                                        <div class="col-sm-6 col-md-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                        <label class="form-control-label">Valuation Report : <span class="tx-danger">*</span></label>
                                            <input type="file" class="" id="valuationreport" name="valuationreport" accept="application/pdf" onchange="checkPhotoFile(this,'valuationreportvalid')"/>
                                            <div class="tx-danger" id="valuationreport-form" style="padding-top:10px;"></div>
                                              <input type="hidden" id="valuationreportvalid" value="0">
                                        </div>
                                        
                                        

                                        
                                        
                                        
                                        
                                        </section>
                                        
                                        <h3>Property Gallery</h3>
                                        <section>
                                        <p>Please upload the property images.</p>
                                    
                                        
                                        
                                        
                                    
                                        
                                        
                                
                                    

<div class="form-group fieldGroup">
        <div class="input-group">
           <div class="col-sm-6 col-md-4" style="float: left;display: inline-block;">
                                        
                                            <input type="file" class="" id="propgallery" name="gallery[]" accept="image/gif, image/jpg, image/jpeg, image/png"/>
                                        </div>
                                        
            <div class="input-group-addon"> 
                <a href="javascript:void(0)" class="btn btn-success addMore" style="background:#251d59;padding: 9px 32px;"><span class="glyphicon glyphicon glyphicon-plus"  aria-hidden="true"></span> Add</a>
            </div>
        </div>
    </div>

<!-- copy of input fields group -->
<div class="form-group fieldGroupCopy" style="display: none;">
    <div class="input-group">
        <div class="col-sm-6 col-md-4" style="float: left;display: inline-block;">
                                        
                                            <input type="file" class="" name="gallery[]" accept="image/gif, image/jpg, image/jpeg, image/png"/>

                                        </div>
                                                                                
        <div class="input-group-addon"> 
            <a href="javascript:void(0)" class="btn btn-danger remove" style="background:#875e8c;"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> Remove</a>
        </div>
    </div>
</div>



<!-- jQuery library -->


 <script>
$(document).ready(function(){
    //group add limit
    var maxGroup = 20;
    
    //add more fields group
    $(".addMore").click(function(){
        if($('body').find('.fieldGroup').length < maxGroup){
            var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
            $('body').find('.fieldGroup:last').after(fieldHTML);
        }else{
            alert('Maximum '+maxGroup+' Images are allowed.');
        }
    });
    
    //remove fields group
    $("body").on("click",".remove",function(){ 
        $(this).parents(".fieldGroup").remove();
    });
});

</script>   
        
                            
                                        
                                        </section>
                                        
                                        
                                    
                                </div></form>
                            </div>
                        </div>
                    </div>
                    <!-- /row -->

                
                    <!-- row closed -->


            </div>
            <script src="<?php echo url('/'); ?>/public/issuer/assets/plugins/jquery/jquery.min.js"></script>
            
        <!-- Internal Select2.min js -->
        <script src="<?php echo url('/'); ?>/public/issuer/assets/plugins/select2/js/select2.min.js"></script>        
        <script src="<?php echo url('/'); ?>/public/issuer/assets/js/index.js"></script>       
        <script src="<?php echo url('/'); ?>/public/issuer/assets/js/form-elements.js"></script>

      
<script>
    function inpNum(e) {
  e = e || window.event;
  var charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
  var charStr = String.fromCharCode(charCode);
  if (!charStr.match(/^[0-9]+$/))
    e.preventDefault();
}

</script>
<script>


function validation() {
    
url=document.getElementById("onmap").value;
    Reg = /^https?\:\/\/(www\.)?google\.[a-z]+\/maps\b/;
  var match = url.match(Reg);
 
  if (match){
    //alert("<span>correct</span>");
    }else{
    alert("Please must enter the valid URL.Click on the link you will get Map Link");
    document.getElementById("onmap").value='';
    }
}



</script>
   
@endsection

        

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Issuer Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Responsive bootstrap 4 admin template" name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
 

 <!-- Favicon -->
        <link rel="icon" href="{{ asset('public/issuer/assets/img/brand/favicon.png') }}" type="image/x-icon"/>

        <!-- Icons css -->
        <link href="{{ asset('public/issuer/assets/plugins/icons/icons.css') }}" rel="stylesheet">

        <!-- Bootstrap css -->
        <link href="{{ asset('public/issuer/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

        <!--  Right-sidemenu css -->
        <link href="{{ asset('public/issuer/assets/plugins/sidebar/sidebar.css') }}" rel="stylesheet">

        <!-- P-scroll bar css-->
        <link href="{{ asset('public/issuer/assets/plugins/perfect-scrollbar/p-scrollbar.css') }}" rel="stylesheet" />

        <!-- Sidemenu css -->
        <link id="theme" rel="stylesheet" href="{{ asset('public/issuer/assets/css/sidemenu.css') }}">

        <!--- Style css --->
        <link href="{{ asset('public/issuer/assets/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('public/issuer/assets/css/boxed.css') }}" rel="stylesheet">
        <link href="{{ asset('public/issuer/assets/css/dark-boxed.css') }}" rel="stylesheet">

        <!--- Dark-mode css --->
        <link href="{{ asset('public/issuer/assets/css/style-dark.css') }}" rel="stylesheet">

        <!---Skinmodes css-->
        <link href="{{ asset('public/issuer/assets/css/skin-modes.css') }}" rel="stylesheet" />

        
      <!-- Internal Select2 css -->
        <link href="{{ asset('public/issuer/assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet">

        <!--- Animations css-->
        <link href="{{ asset('public/issuer/assets/css/animate.css') }}" rel="stylesheet">

        <!---Switcher css-->
        <link href="{{ asset('public/issuer/assets/switcher/css/switcher.css') }}" rel="stylesheet">
        <link href="{{ asset('public/issuer/assets/switcher/demo.css') }}" rel="stylesheet">
        <link href="{{ asset('public/issuer/assets/plugins/datatable/datatables.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('public/issuer/assets/plugins/datatable/responsive.dataTables.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/issuer/assets/plugins/datatable/responsive.bootstrap5.css') }}" rel="stylesheet">
        <link href="{{ asset('public/issuer/assets/plugins/datatable/css/buttons.bootstrap5.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/issuer/assets/plugins/datatable/css/dataTables.bootstrap5.min.css') }}" rel="stylesheet">
        <link href="{{ asset('public/issuer/assets/plugins/datatable/css/jquery.dataTables.min.css') }}" rel="stylesheet">
<script async src="https://www.googletagmanager.com/gtag/js?id=G-05P584BCNZ"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'G-05P584BCNZ');
</script>
    @yield('style')
</head>
<body class="main-body app sidebar-mini">
    
 <!-- Loader -->
        <div id="global-loader">
            <img src="../public/issuer/assets/img/loader.svg" class="loader-img" alt="Loader">
        </div>
        <!-- /Loader -->
<div class="page">

        <!-- main-sidebar -->
        <!-- main-sidebar -->
            <div class="app-sidebar__overlay" data-bs-toggle="sidebar"></div>
            @include('issuer.layout.sidebar')
            <!-- main-sidebar -->        <!-- main-sidebar -->

        <!-- main-content -->
        <div class='main-content app-content'>

            <!-- main-header -->
            <!-- main-header -->
             @include('issuer.layout.header')
                <!-- /main-header -->            <!--/main-header -->

            <div class="container-fluid">

                 <div class="content-page">
            @include('common.notify')
        </div>
        @yield('content')
                    

            </div>
            <!-- Container closed -->
        </div>
     
        <!-- Footer opened -->
           @include('issuer.layout.footer')
            <!-- Footer closed -->
        
        </div>

</body>

  

        
        <!-- Right bar overlay-->
   <!-- Back-to-top -->
        <a href="#top" id="back-to-top"><i class="las la-angle-double-up"></i></a>

        <!-- Jquery js-->
        <script src="{{ asset('public/issuer/assets/plugins/jquery/jquery.min.js') }}"></script>

        <!-- Bootstrap js-->
        <script src="{{ asset('public/issuer/assets/plugins/bootstrap/js/popper.min.js') }}"></script>
        <script src="{{ asset('public/issuer/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

        <!-- Ionicons js-->
        <script src="{{ asset('public/issuer/assets/plugins/ionicons/ionicons.js') }}"></script>

        <!-- Moment js -->
        <script src="{{ asset('public/issuer/assets/plugins/moment/moment.js') }}"></script>

        <!-- P-scroll js -->
       <!-- <script src="{{ asset('issuer/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>-->
        <!-- <script src="{{ asset('issuer/assets/plugins/perfect-scrollbar/p-scroll.js') }}"></script>-->

        <!-- Rating js-->
        <script src="{{ asset('public/issuer/assets/plugins/rating/jquery.rating-stars.js') }}"></script>
        <script src="{{ asset('public/issuer/assets/plugins/rating/jquery.barrating.js') }}"></script>

        <!-- Sticky js -->
        <script src="{{ asset('public/issuer/assets/js/sticky.js') }}"></script>

        <!-- Sidebar js -->
        <script id="sidemenu" src="{{ asset('public/issuer/assets/plugins/side-menu/sidemenu.js') }}"></script>

        <!-- Right-sidebar js -->
        <script src="{{ asset('public/issuer/assets/plugins/sidebar/sidebar.js') }}"></script>
        <script src="{{ asset('public/issuer/assets/plugins/sidebar/sidebar-custom.js') }}"></script>

        <!-- eva-icons js -->
        <script src="{{ asset('public/issuer/assets/plugins/eva-icons/eva-icons.min.js') }}"></script>

        
        <!-- Moment js -->
        <script src="{{ asset('public/issuer/assets/plugins/raphael/raphael.min.js') }}"></script>

     

        <!--Internal  Perfect-scrollbar js -->
       <!-- <script src="{{ asset('issuer/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
        <script src="{{ asset('issuer/assets/plugins/perfect-scrollbar/p-scroll.js') }}"></script>
-->
        <!-- Internal Map -->
       <!--  <script src="{{ asset('issuer/assets/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
        <script src="{{ asset('issuer/assets/plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
-->
        <!--Internal  index js -->
     <!--   <script src="{{ asset('issuer/assets/js/index.js') }}"></script>-->


      
        <!-- Switcher js -->
        <script src="{{ asset('public/issuer/assets/switcher/js/switcher.js') }}"></script>

     <!--Internal  Datepicker js -->
        <script src="{{ asset('public/issuer/assets/plugins/jquery-ui/ui/widgets/datepicker.js') }}"></script>
        
        <!--Internal  jquery.maskedinput js -->
        <script src="{{ asset('public/issuer/assets/plugins/jquery.maskedinput/jquery.maskedinput.js') }}"></script>

        <!--Internal  spectrum-colorpicker js -->
        <script src="{{ asset('public/issuer/assets/plugins/spectrum-colorpicker/spectrum.js') }}"></script>

        <!-- Internal Select2.min js -->
        <script src="{{ asset('public/issuer/assets/plugins/select2/js/select2.min.js') }}"></script>
        
      
        <script src="{{ asset('public/issuer/assets/plugins/jquery-steps/jquery.steps.min.js') }}"></script>
        <script src="{{ asset('public/issuer/assets/plugins/parsleyjs/parsley.min.js') }}"></script>
        
        <!--Internal  Form-wizard js -->
        <script src="{{ asset('public/issuer/assets/js/form-wizard.js') }}"></script>
        <!-- custom js -->
        <script src="{{ asset('public/issuer/assets/js/custom.js') }}"></script>

        <!--Internal Ion.rangeSlider.min js -->
        <script src="{{ asset('public/issuer/assets/plugins/ion-rangeslider/js/ion.rangeSlider.min.js') }}"></script>

    <!--Internal  jquery-simple-date time picker js -->
        <script src="{{ asset('public/issuer/assets/plugins/amazeui-datetimepicker/js/amazeui.datetimepicker.min.js') }}"></script>
        <script src="{{ asset('public/issuer/assets/plugins/jquery-simple-datetimepicker/jquery.simple-dtpicker.js') }}"></script>

        <!--Internal  pickerjs js -->
        <script src="{{ asset('public/issuer/assets/plugins/pickerjs/picker.min.js') }}"></script>

     <!--   <script src="{{ asset('asset/js/jquery.min.js') }}"></script>
        <script src="{{ asset('issuer/assets/js/vendor.min.js') }}"></script>
        <script src="{{ asset('issuer/assets/js/app.min.js') }}"></script>
        <script src="{{ asset('issuer/assets/js/datatables.min.js') }}"></script>
       <script src="{{ asset('issuer/assets/js/data-table.js') }}"></script> -->

    
       

    <script src="{{ asset('public/issuer/assets/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('public/issuer/assets/plugins/datatable/datatables.min.js') }}"></script>
        <script src="{{ asset('public/issuer/assets/plugins/datatable/js/dataTables.bootstrap5.js') }}"></script>
        <script src="{{ asset('public/issuer/assets/plugins/datatable/js/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('public/issuer/assets/plugins/datatable/js/buttons.bootstrap5.min.js') }}"></script>
        <script src="{{ asset('public/issuer/assets/plugins/datatable/js/jszip.min.js') }}"></script>
        <script src="{{ asset('public/issuer/assets/plugins/datatable/js/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('public/issuer/assets/plugins/datatable/js/buttons.print.min.js') }}"></script>
        <script src="{{ asset('public/issuer/assets/plugins/datatable/js/buttons.colVis.min.js') }}"></script>
        <script src="{{ asset('public/issuer/assets/plugins/datatable/pdfmake/pdfmake.min.js') }}"></script>
        <script src="{{ asset('public/issuer/assets/plugins/datatable/pdfmake/vfs_fonts.js') }}"></script>
        <!--Internal  Datatable js -->
        <script src="{{ asset('public/issuer/assets/js/table-data.js') }}"></script>
    
    </body>
</html>

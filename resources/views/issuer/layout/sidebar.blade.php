	<aside class="app-sidebar sidebar-scroll">
				<div class="main-sidebar-header active">
					<a class="desktop-logo logo-light active" href="{{url('/')}}/issuer/dashboard"><img src="{{url('/')}}/public/issuer/assets/img/brand/logo.png" class="main-logo" alt="logo"></a>
					<a class="desktop-logo logo-dark active" href="{{url('/')}}/issuer/dashboard"><img src="{{url('/')}}/public/issuer/assets/img/brand/logo-white.png" class="main-logo dark-theme" alt="logo"></a>
					<a class="logo-icon mobile-logo icon-light active" href="{{url('/')}}/issuer/dashboard"><img src="{{url('/')}}/public/issuer/assets/img/brand/favicon.png" class="logo-icon" alt="logo"></a>
					<a class="logo-icon mobile-logo icon-dark active" href="{{url('/')}}/issuer/dashboard"><img src="{{url('/')}}/public/issuer/assets/img/brand/favicon-white.png" class="logo-icon dark-theme" alt="logo"></a>
				</div>
				<div class="main-sidemenu">
					<div class="app-sidebar__user clearfix">
						<div class="dropdown user-pro-body">
							<div class="">
								<img alt="user-img" class="avatar avatar-xl brround" src="{{url('/')}}/public/issuer/assets/img/faces/6.jpg"><span class="avatar-status profile-status bg-green"></span>
							</div>
							<div class="user-info">
								<h4 class="fw-semibold mt-3 mb-0">{{Auth::user()->name}} </h4>

    <span class="mb-0 text-muted">{{Auth::user()->email}}    </span> 
							</div>
						</div>
					</div>
					<ul class="side-menu">
						<li class="side-item side-item-category">Main</li>
						<li class="slide">
<li class="slide"><a class="side-menu__item" data-bs-toggle="slide" href="{{ route('dashboard') }}"><i class="la la-tachometer" style="font-size:22.5px;color:#5b6e88;padding: 1px 14px 1px 1px;"></i><span class="side-menu__label">Dashboard</span></a>
						</li>
							
						<li class="side-item side-item-category">General</li>
						
<li class="side-item side-item-category">PROPERTY</li>
						<li class="slide"><a class="side-menu__item" data-bs-toggle="slide" href="{{ route('property') }}"><i class="fas fa-home" style="font-size:16.5px;color:#5b6e88;padding: 1px 17px 1px 1px;"></i><span class="side-menu__label">Property</span><i class="angle fe fe-chevron-down"></i></a>
							<ul class="slide-menu">
								<li><a class="slide-item" href="{{ route('token') }}">Create Token & Property</a></li>
								<li><a class="slide-item"  href="{{ route('property') }}">Property List</a></li>
								<li><a class="slide-item"  href="{{route('dividend')}}">Create Dividend</a></li>
								<li><a class="slide-item"  href="{{route('dividendlist')}}">Dividend </a></li>
							</ul>
						</li>

					<!--<li class="slide"><a class="side-menu__item" data-bs-toggle="slide" href="{{ route('token') }}"><i class="fas fa-home" style="font-size:16.5px;color:#5b6e88;padding: 1px 18px 1px 1px;"></i><span class="side-menu__label">Create Token & Property</span></a>
						</li>-->
						<li class="slide"><a class="side-menu__item" data-bs-toggle="slide" href="{{ route('tokenRequest') }}"><i class="fas fa-coins" style="font-size:16.5px;color:#5b6e88;padding: 1px 19px 1px 3px;"></i><span class="side-menu__label">Token Requested</span></a>
						</li>
					<!--	<li class="slide"><a class="side-menu__item" data-bs-toggle="slide" href="{{ route('property') }}"><i class="fas fa-home" style="font-size:16.5px;color:#5b6e88;padding: 1px 17px 1px 1px;"></i><span class="side-menu__label">Property</span></a>
						</li>-->
					<li class="slide"><a class="side-menu__item" data-bs-toggle="slide" href="{{ route('issuer_paymenthistory') }}"><i class="fas fa-wallet" style="font-size:16.5px;color:#5b6e88;padding: 1px 18px 1px 1px;"></i><span class="side-menu__label">Payment History</span></a>
						</li>
						<!--<li class="slide"><a class="side-menu__item" data-bs-toggle="slide" href="#"><i class="fas fa-cogs" style="font-size:16.5px;color:#5b6e88;padding: 1px 16px 1px 1px;"></i><span class="side-menu__label">Logs</span></a>
						</li>-->
						  <li class="slide"><a class="side-menu__item" data-bs-toggle="slide" href="{{ route('logout') }}" onclick="event.preventDefault();
						document.getElementById('logout-form').submit();"><i class="fas fa-power-off" style="font-size:16.5px;color:#5b6e88;padding: 1px 16px 1px 1px;"></i><span class="side-menu__label">Logout</span></a>
						   	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
					   </li>

						
						
						
					
					</ul>
				</div>
			</aside>
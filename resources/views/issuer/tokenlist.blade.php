@extends('issuer.layout.base')
@section('content')
<!-- Start Page Content here -->
<style>
    #example_length{
margin-top: 5px !important;
    float: right !important;
    margin-right: 266px !important;
}
table.dataTable.dtr-inline.collapsed > tbody > tr > td.dtr-control:before, table.dataTable.dtr-inline.collapsed > tbody > tr > th.dtr-control:before{
    top: 20% !important;
}

</style>
<div class="content-page">

    <!-- Header Banner Start -->
     <div class="breadcrumb-header justify-content-between">
                        <div class="my-auto">
                            <div class="d-flex">
                                <h4 class="content-title mb-0 my-auto">  Token Acquired</h4><span class="text-muted mt-1 tx-13 ms-2 mb-0">/ Token List</span>
                            </div>
                        </div>
                        <div class="d-flex my-xl-auto right-content">
                            
                            
                        </div>
                    </div>
    <!-- Header Banner Start -->


     <div class="content-area py-1" style="background:#fff;padding-top: 40px !important;">
        <!-- Start container-fluid -->
        <div class="container-fluid wizard-border">
            <!-- start  -->
            <div class="row">
                <div class="col-12 table-responsive">
                    <div>

                        <div class="col-xs-12 col-sm-2" style="float:left;">
    <img src="{{url('/')."/".$property[0]['tokenLogo'] }}" style="height:100px;" class="thumbnail"/>

</div>
<?php
$tokens=0;
 foreach ($wallet as $key => $item){
    $tokens+=$item->total_tokens;
 
}

?>
<div class="col-xs-12 col-sm-8" style="float:left;">
                        <h4 class="content-title mb-0 my-auto"></h4>
                     <p class="title" style="text-align:left;font-size:18px;color:#251d59;margin-bottom:5px;text-transform: uppercase;">
          <span style="font-size:15px;color:#929195;">Token Name :</span>    
          {{$property[0]['tokenName']}}  </p>
   <p class="title" style="text-align:left;font-size:18px;color:#251d59;margin-bottom:5px;text-transform: uppercase;">   <span style="font-size:15px;color:#929195;">Property Name :</span>    
{{$property[0]['propertyName']}}</p>

  <p class="title" style="text-align:left;font-size:18px;color:#251d59;margin-bottom:5px;text-transform: uppercase;">       <span style="font-size:15px;color:#929195;">Token Symbol :</span>
{{ $property[0]['tokenSymbol'] }} </p>
<p class="title" style="text-align:left;font-size:18px;color:#251d59;margin-bottom:5px;text-transform: uppercase;">       <span style="font-size:15px;color:#929195;">Total Tokens :</span>
{{ $property[0]['tokenSupply'] }} </p>
<p class="title" style="text-align:left;font-size:18px;color:#251d59;margin-bottom:5px;text-transform: uppercase;">       <span style="font-size:15px;color:#929195;">Contract Address :</span>
{{ $property[0]['contract_address'] }} </p>
<p class="title" style="text-align:left;font-size:18px;color:#251d59;margin-bottom:5px;text-transform: uppercase;">       <span style="font-size:15px;color:#929195;">Sold Tokens :</span> {{$tokens}}
 </p>

 <?php
 $totaltoken2=0;
$tmp2 = \App\InvestorBonusToken::where('propertyid',$property[0]['id'])->get();
foreach($tmp2 as $index2 => $val2){
    $totaltoken2+=$val2->total_tokens;
}
//echo $totaltoken2;
if($totaltoken2>0){
 ?>

<p class="title" style="text-align:left;font-size:18px;color:#251d59;margin-bottom:5px;text-transform: uppercase;">       <span style="font-size:15px;color:#929195;">DIVIDEND (BONUS TOKENS) :</span> {{$totaltoken2}}
 </p>
<?php } ?>

 <p class="title" style="text-align:left;font-size:18px;color:#251d59;margin-bottom:5px;text-transform: uppercase;">       <span style="font-size:15px;color:#929195;">Balance Tokens :</span> {{($property[0]['tokenSupply']-$tokens)-$totaltoken2}}
 </p>

    </div>
                    <table id="example" class="table table-bordered" style="margin-top:30px;">
                    <thead>
                        <tr>
                        <th style="width:20px;">S.N0</th>
                       
                        <th style="width:30px;">Investor </th>
                        <th style="width:20px;">Total Token</th>
                        <th style="width:20px;">Total Amount(Pay By Investor)</th>
                        <th style="width:50px;">Hash</th>                        
                       
                        </tr>
                    </thead>
                    <tbody>
                         @foreach ($wallet as $key => $item)
                       <tr>
                           <td>{{$item->wallet_id}}</td>
                           <td>
                               <table style="width:100% !important;">
                                   <tr>
                                       <td><b>Name</b></td> <td><b>Email</b></td>
                                       
                                   </tr>
<tr>
                                      <td>{{$item->name}}</td>
                                       <td>{{$item->email}}</td>
                                   </tr>
                                 

                               </table>


                           </td>
                           
                           <td>{{$item->total_tokens}}</td>
                           <td>{{$item->paytoissuer}}</td>
                           <td>{{$item->txnhash}}</td>


                       </tr>
                        @endforeach


                        
                    </tbody>
                   
                    </table>
                    </div>
                </div>
            </div>
            <!-- end row -->


        </div>
        <!-- end container-fluid -->

    </div>
  

</div>
<!-- END content-page -->
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).on("ready", function(){
        $("#example1").DataTable();
    });
</script>
@endsection
@extends('issuer.layout.base')
@section('content')
<style>
    #example_length{
margin-top: 5px !important;
    float: right !important;
    margin-right: 266px !important;
}
.copyButton {
background-color: red !important;
}
.excelButton{
background-color: red
}
    
</style>

<!-- Start Page Content here -->
<div class="content-page">

    <!-- Header Banner Start -->
      <!-- breadcrumb -->
                    <div class="breadcrumb-header justify-content-between">
                        <div class="my-auto">
                            <div class="d-flex">
                                <h4 class="content-title mb-0 my-auto"> Approved Tokens</h4><span class="text-muted mt-1 tx-13 ms-2 mb-0">/ Token List</span>
                            </div>
                        </div>
                        <div class="d-flex my-xl-auto right-content">
                            
                            
                        </div>
                    </div>
                    <!-- breadcrumb -->
    <!-- Header Banner Start -->


      <div class="content-area py-1" style="background:#fff;padding-top: 40px !important;">
            <!-- Start container-fluid -->
            <div class="container-fluid wizard-border">
                <!-- start  -->
                <div class="row">
                    <div class="col-12 table-responsive">
                    <div>
                        <table id="example" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>S.N0</th>
                            <th>Token Name</th>
                            <th>Token Symbol</th>
                            <th>Token Value</th>
                            <th>Token Supply</th>
                            <th>Contract Address</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($tokens as $key => $item)
                            @if($item->tokenStatus=="live")
                                <tr>
                                    <td>{{ @$key + 1}}</td>
                                    <td>{{ @$item->tokenName }}</td>
                                    <td>{{ @$item->tokenSymbol }}</td>
                                    <td>{{ @$item->tokenValue }}</td>
                                    <td>{{ @$item->tokenSupply }}</td>
                                    <td>{{ @$item->contract_address }}</td>
                                    <td>
                            
                                        
<?php
if($item->tokenStatus=="live"){
    ?>
<button class="btn btn-success">Live</button>
    <?php

}else if($item->tokenStatus=="pending"){
 ?>
<button class="btn btn-warning">Pending</button>
    <?php
}else if($item->tokenStatus=="rejected"){
 ?>
<button class="btn btn-danger">Rejected</button>
    <?php
}


?>

                                    </td>
                                </tr>
                                @endif
                            @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>S.N0</th>
                            <th>Token Name</th>
                            <th>Token Symbol</th>
                            <th>Token Value</th>
                            <th>Token Supply</th>
                             <th>Contract Address</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                        </table>
                    </div>
                    </div>
                </div>
                <!-- end row -->


            </div>
            <!-- end container-fluid -->

            
        </div>
        <!-- end content -->

       
        <!-- end Footer -->
    </div>
    <!-- END content-page -->
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).on("ready", function(){
        $("#example1").DataTable();
    });
</script>
@endsection

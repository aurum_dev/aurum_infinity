@extends('issuer.layout.base')
@section('content')
<script>
    function RestrictSpace() {
    if (event.keyCode == 32) {
        return false;
    }
}

</script>
    <!-- <style>
.nice-select .list
{
  overflow-y: scroll;
  height: 200px;
}
</style> -->

    
   <div class="breadcrumb-header justify-content-between">
                        <div class="my-auto">
                            <div class="d-flex">
                                <h4 class="content-title mb-0 my-auto"> Profile</h4><span class="text-muted mt-1 tx-13 ms-2 mb-0"></span>
                            </div>
                        </div>
                        <div class="d-flex my-xl-auto right-content">
                          
                           
                        </div>
                    </div>





   <div class="content-page">       
        <div class="content">
            <!-- Start container-fluid -->
            <div class="container-fluid wizard-border">

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">

<div class="panel panel-primary tabs-style-2">
        <div class=" tab-menu-heading">
            <div class="tabs-menu1">
                <!-- Tabs -->
                <ul class="nav panel-tabs main-nav-line">
                    <li><a href="#tab4" class="nav-link active" data-bs-toggle="tab">KYC</a></li>
                    <li><a href="#tab5" class="nav-link" data-bs-toggle="tab">Profile Update</a></li>
                    <li><a href="#tab6" class="nav-link" data-bs-toggle="tab">Change Password</a></li>
                   <?php if(Auth::user()->kyc==1){ ?>
                     <li><a href="#tab7" class="nav-link" data-bs-toggle="tab">Bank Details</a></li>
                    
                  <?php  } ?>
                </ul>
            </div>
        </div>
        <div class="panel-body tabs-menu-body main-content-body-right border">
            <div class="tab-content">
                <div class="tab-pane active" id="tab4">
                  
                        <form action="{{ url('issuer/kyc') }}" method="POST" class="form-validation" enctype="multipart/form-data">
                                            @csrf
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="userName">Company Name<span class="text-danger">*</span></label>
                                        <input type="text" name="cname" parsley-trigger="change" required="" placeholder="" class="form-control" id="userName" value="{{ @$kyc->cname }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="userName">Company Address<span class="text-danger">*</span></label>
                                        <input type="text" name="caddress" parsley-trigger="change" required="" placeholder="" class="form-control" id="userName" value="{{ @$kyc->caddress }}" required>
                                    </div>            
                                    <div class="form-group">
                                        <label for="userName">City<span class="text-danger">*</span></label>
                                        <input type="text" name="city" parsley-trigger="change" required="" placeholder="" class="form-control" id="Email" value="{{ @$kyc->city }}" required>
                                    </div>                                       
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="userName">State<span class="text-danger">*</span></label>
                                        <input type="text" name="state" parsley-trigger="change" required="" placeholder="" class="form-control" id="userName" value="{{ @$kyc->state }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="userName">Company PAN<span class="text-danger">*</span></label>
                                        <input type="text" name="cpan" parsley-trigger="change" required="" placeholder="" class="form-control" id="userName" value="{{ @$kyc->cpan }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="userName">GSTIN<span class="text-danger">*</span></label>
                                        <input type="text" name="cgst" parsley-trigger="change" required="" placeholder="" class="form-control" id="userName" value="{{ @$kyc->cgst }}" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row next-btn" style="padding:25px 0px;">
                                <div class="col-sm-12">
                                    <div class="form-group text-center mb-0">
                                        <input class="btn btn-primary waves-effect waves-light mr-1" type="submit" value="Update KYC">
                                        {{-- <button class="btn btn-primary waves-effect waves-light mr-1" type="submit">Update Profile</button> --}}
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
                <div class="tab-pane" id="tab5">
                    
                 <form action="{{ url('issuer/profile_update') }}" method="POST" class="form-validation" enctype="multipart/form-data">
                                @csrf

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="userName">First Name<span class="text-danger">*</span></label>
                            <input type="text" name="fname" parsley-trigger="change" required="" placeholder="" class="form-control" id="userName" value="{{ @$user->name }}" required>
                        </div>
                        <div class="form-group">
                            <label for="userName">Email<span class="text-danger">*</span></label>
                            <input type="text" name="email" parsley-trigger="change" required="" placeholder="" class="form-control" id="userName" value="{{ @$user->email }}" required>
                        </div>
                        {{-- <div class="form-group">
                            <label for="userName">Gender<span class="text-danger">*</span></label>
                            <select class="form-control">
                              <option>Male</option>
                              <option>Female</option>
                            </select>
                        </div>                                                       --}}
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="userName">Last Name<span class="text-danger">*</span></label>
                            <input type="text" name="lname" parsley-trigger="change" required="" placeholder="" class="form-control" id="Email" value="{{ @$user->identity->last_name }}" required>
                        </div>
                        <div class="form-group">
                            <label for="userName">Mobile No<span class="text-danger">*</span></label>
                            <input type="number" name="mobileno" parsley-trigger="change" required="" placeholder="" class="form-control" id="userName" value="{{ @$user->identity->primary_phone }}" required>
                        </div>
                        <!-- <div class="form-group">
                            <label for="userName">ETH Address<span class="text-danger">*</span></label>
                            <input type="text" name="eth_address" parsley-trigger="change" required="" placeholder="" class="form-control" id="userName" value="{{ @$user->eth_address }}" required>
                        </div> -->
                        {{-- <div class="form-group mb-0">
                            <label for="userName">Avatar<span class="text-danger">*</span></label>
                            <input type="file" class="filestyle" data-btnClass="btn-primary">
                        </div> --}}
                    </div>
                </div>

                <div class="row next-btn" style="padding:25px 0px;">
                    <div class="col-sm-12">
                        <div class="form-group text-center mb-0">
                            <input class="btn btn-primary waves-effect waves-light mr-1" type="submit" value="Update Profile">
                            {{-- <button class="btn btn-primary waves-effect waves-light mr-1" type="submit">Update Profile</button> --}}
                        </div>
                    </div>
                </div>
            </form>


                </div>
                <div class="tab-pane" id="tab6">
                    <form method="POST" action="{{ route('change.password') }}">
                        @csrf
                        <div class="form-group">
                            <label for="userName">Current Password<span class="text-danger">*</span></label>
                            <input type="password" name="current_password" required="" placeholder="" class="form-control" id="current_password" onkeypress="return RestrictSpace()">
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="userName">New Password<span class="text-danger">*</span></label>
                                <input type="password" name="password" required="" placeholder="" class="form-control" id="password" onkeypress="return RestrictSpace()">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="userName">Confirm Password<span class="text-danger">*</span></label>
                                <input type="password" name="password_confirmation" required="" placeholder="" class="form-control" id="password_confirmation" onkeypress="return RestrictSpace()">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group text-left mb-0">
                                <input class="btn btn-primary mr-1" type="submit" style="margin-top: 30px !important;" value="Update Password"/>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane" id="tab7">
                    <form action="{{ url('issuer/updateBankDetails') }}" method="POST" class="form-validation" enctype="multipart/form-data">
                                @csrf
                <div class="row">
                     <?php
$id=Auth::user()->id;
$bankd = \App\IssuerUser::where('user_id',$id)->first();

?>

                        <div class="col-sm-6">
                            <div class="form-group">
                            <label for="userName">Account Holder Name<span class="text-danger">*</span></label>
                            <input type="text" name="accountholdername" title="Please enter only characters" pattern="[a-zA-Z'-'\s]*" parsley-trigger="change" required="" placeholder="" class="form-control" id="accountholdername" value="{{$bankd->accountholdername }}" required>
                             </div>
                             <div class="form-group">
                                <label for="userName">Account Number<span class="text-danger">*</span></label>
                                <input type="password" name="accnumber" parsley-trigger="change" required="" placeholder="" class="form-control" id="accnumber" value="{{$bankd->accno }}" required>
                            </div>
                             <div class="form-group">
                                <label for="userName">IFSC CODE<span class="text-danger">*</span></label>
                                <input type="text" name="ifsccode" parsley-trigger="change" required="" placeholder="" class="form-control" id="ifsccode" value="{{$bankd->ifsccode }}" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label for="userName">Bank Name<span class="text-danger">*</span></label>
                            <input type="text" name="bankname" title="Please enter only characters" pattern="[a-zA-Z'-'\s]*" parsley-trigger="change" required="" placeholder="" class="form-control" id="bankname" value="{{$bankd->bankname }}" required>
                             </div>
                            
                            <div class="form-group">
                                <label for="userName">Retype Account Number<span class="text-danger">*</span></label>
                                <input type="password" name="retypeaccno" parsley-trigger="change" required="" placeholder="" class="form-control" onchange="accountnumber();" id="retypeaccno" value="{{$bankd->accno }}" required>
                            </div>
                            </div>
                        </div>
                        <div class="row next-btn" style="padding:25px 0px;">
                                <div class="col-sm-12">
                                    <div class="form-group text-center mb-0">
                                        <input class="btn btn-primary waves-effect waves-light mr-1" type="submit" value="Update Bank Details" onsubmit="return bankdetails();">
                                      
                                    </div>
                                </div>
                            </div>
                        </form>

                </div>
            </div>
        </div>
    </div>






                            
                            
                        
                        </div>
                    </div>
                </div>
            </div>

        </div>
      
    </div>
<script>
   function accountnumber(){
    var accnum=document.getElementById('accnumber').value;
    var retypeaccno=document.getElementById('retypeaccno').value;
    if(accnum!=retypeaccno){
        alert('Please enter the correct account number.');
    }
   }
</script>

@endsection

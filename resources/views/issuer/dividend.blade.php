@extends('issuer.layout.base')
@section('content')
<style>
    .tooltip {
  position: relative;
  display: inline-block;
 
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 349px;
    background-color: #ebebeb;
    color: #333;
    text-align: center;
    border-radius: 6px;
    padding: 10px 10px;
    position: absolute;
    font-size: 14px;
    font-weight: 500;
    z-index: 1;
    margin-left: -180px;
    margin-top: 20px;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
}</style>
<script>

function checkPhotoFile(target,id) {
    if(target.files[0].size > 8000000) {
        alert("File is too big (max 8 MB)");
        document.getElementById(id).value =1;
        document.getElementById(id).src='';
        return false;
    }   
     else{
        document.getElementById(id).value =0;
    } 
}

</script>
    
     
                <!-- breadcrumb -->
                    <div class="breadcrumb-header justify-content-between">
                        <div class="my-auto">
                            <div class="d-flex">
                                <h4 class="content-title mb-0 my-auto"> Dividend</h4><span class="text-muted mt-1 tx-13 ms-2 mb-0">/ Add New Dividend for Property</span>
                            </div>
                        </div>
                        <div class="d-flex my-xl-auto right-content">
                          
                           
                        </div>
                    </div>
                    <!-- breadcrumb -->

<div class="row">
                        
                        <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    
                                    <form class="form-horizontal" action="{{action('IssuerController@dividendStore')}}" onsubmit="return validation();" method="post" enctype="multipart/form-data">
                                  @csrf
                                <input type="hidden" name="token_type" value="1">
                                   
                                       
                                        <section>
                                          
                                            <p style="color:#000;"><b>Note :</b> <i><span class="tx-danger">*</span> Fields are Mandatory</i></p>
                                            <div class="row row-sm">
                                                <div class="col-md-5 col-lg-4">
                                                    <label class="form-control-label">Property Name: <span class="tx-danger">*</span></label>
                                                   <select name="propertyname" class="states order-alpha form-control select2" id="propertyname">
    <option value="">Select Property</option>
    @foreach($property as $index=>$val)
 <option value="{{$val->id}}">{{$val->propertyName}}</option>
    @endforeach
</select>
                                                    <div class="tx-danger" id="propertyname-form" style="padding-top:10px;"></div>
                                                </div>
                                                
                   <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0">
                                                    <label class="form-control-label">Start Date <span class="tx-danger">*</span></label> 
                                                    <input class="form-control"
                                                    type="text" id="fromdate" name="fromdate"placeholder="">
                                            <div class="tx-danger" id="fromdate-form" style="padding-top:10px;"></div>
                                            </div>  
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0">
                                                    <label class="form-control-label">End Date <span class="tx-danger">*</span></label> 
                                                    <input class="form-control"  type="text" id="enddate" name="enddate" placeholder="">
                                            <div class="tx-danger" id="enddate-form" style="padding-top:10px;"></div>
                                            </div>    

    
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Title: <span class="tx-danger">*</span></label> 
                                                    <input class="form-control" id="title" name="title" placeholder=""  type="text">
                                            <div class="tx-danger" id="title-form" style="padding-top:10px;"></div>
                                            </div>
                                                
                                        <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Description: <span class="tx-danger">*</span></label> 
                                                    <textarea class="form-control" id="description" name="description" placeholder=""></textarea>
                                                <div class="tx-danger" id="description-form" style="padding-top:10px;"></div>
                                                </div>
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Dividend (In FIAT(INR)) : <span class="tx-danger">*</span></label> 
                                                    <input class="form-control" id="dividend" name="dividend" placeholder=""  oninput="this.value = 
 !!this.value && Math.abs(this.value) > 0 ? Math.abs(this.value) : null"  type="number" onkeypress="inpNum(event)" type="number">
                                                <div class="tx-danger" id="dividend-form" style="padding-top:10px;"></div>
                                                </div>
                                              <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                 <label class="form-control-label">Document 1 <span class="tx-danger">*</span></label> 
                                            <input type="file" accept="application/pdf" name="document1" id="document1" onchange="checkPhotoFile(this,'document1valid')" data-height="200" />
                                            <div class="tx-danger" id="documentnew-form" style="padding-top:10px;"></div>
                                             <input type="hidden" id="document1valid" value="">
                                        </div>
                                        <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                 <label class="form-control-label">Document 2 <span class="tx-danger">*</span></label> 
                                            <input type="file" name="document2" id="document2" accept="application/pdf" onchange="checkPhotoFile(this,'document2valid')" data-height="200" />
                                             <div class="tx-danger" id="document2-form" style="padding-top:10px;"></div>
                                               <input type="hidden" id="document2valid" value="">
                                        </div>
                                        <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                 <label class="form-control-label">Document 3 <span class="tx-danger">*</span></label> 
                                            <input type="file" name="document3" id="document3" accept="application/pdf" onchange="checkPhotoFile(this,'document3valid')" data-height="200" />
                                             <div class="tx-danger" id="document3-form" style="padding-top:10px;"></div>
                                             <input type="hidden" id="document3valid" value="">
                                        </div>
                                        <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                 <label class="form-control-label">Document 4 <span class="tx-danger">*</span></label> 
                                            <input type="file" name="document4" id="document4"  accept="application/pdf" onchange="checkPhotoFile(this,'document4valid')" data-height="200" />
                                             <div class="tx-danger" id="document4-form" style="padding-top:10px;"></div>
                                             <input type="hidden" id="document4valid" value="">
                                        </div>
                                        <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                 <label class="form-control-label">Document 5 <span class="tx-danger">*</span></label> 
                                            <input type="file" name="document5" id="document5"  accept="application/pdf" onchange="checkPhotoFile(this,'document5valid')" data-height="200" />
                                             <div class="tx-danger" id="document5-form" style="padding-top:10px;"></div>
                                              <input type="hidden" id="document5valid" value="">
                                        </div>
                                        <div class="col-xs-10 mg-t-20 mg-md-t-30">
                                              <a href="{{route('admin.property.showasset')}}" style="float:right;margin-left:10px;" class="btn btn-danger">@lang('admin.cancel')</a>
                        <button type="submit" class="btn btn-primary" style="float:right;">Submit</button>
                      
                    </div>
                                        </section>
                                      
                                      
                                        
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /row -->

                
                    <!-- row closed -->


            </div>

   <script src="<?php echo url('/'); ?>/public/issuer/assets/plugins/jquery/jquery.min.js"></script>
        <!-- Internal Select2.min js -->
        <script src="<?php echo url('/'); ?>/public/issuer/assets/plugins/select2/js/select2.min.js"></script>        
        <script src="<?php echo url('/'); ?>/public/issuer/assets/js/index.js"></script>       
        <script src="<?php echo url('/'); ?>/public/issuer/assets/js/form-elements.js"></script>
            <script
    type="text/javascript"
    src="//code.jquery.com/jquery-1.9.1.js"
    
  ></script>
    <script type="text/javascript" src="//code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css">

                                            <script>
                                                $(function() {
    $( "#fromdate" ).datepicker({
      changeMonth: true,
      numberOfMonths: 2,
      onClose: function( selectedDate ) {
        $( "#enddate" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#enddate" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 2,
      onClose: function( selectedDate ) {
        $( "#fromdate" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
  });
                                            </script>              
     

<script>
    function inpNum(e) {
  e = e || window.event;
  var charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
  var charStr = String.fromCharCode(charCode);
  if (!charStr.match(/^[0-9]+$/))
    e.preventDefault();
}


function validation(){
   var valid=true;
 
  if(document.getElementById('propertyname').value==''){
    document.getElementById('propertyname-form').innerHTML='It is required';
    valid=false;
  }else{
    document.getElementById('propertyname-form').innerHTML='';
  }

  if(document.getElementById('fromdate').value==''){
    document.getElementById('fromdate-form').innerHTML='It is required';
   valid=false;
  }else{
    document.getElementById('fromdate-form').innerHTML='';
  }

  if(document.getElementById('enddate').value==''){
    document.getElementById('enddate-form').innerHTML='It is required';
     valid=false;
  }else{
    document.getElementById('enddate-form').innerHTML='';
  }

  if(document.getElementById('title').value==''){
    document.getElementById('title-form').innerHTML='It is required';
     valid=false;
  }else{
    document.getElementById('title-form').innerHTML='';
  }


  if(document.getElementById('description').value==''){
    document.getElementById('description-form').innerHTML='It is required';
     valid=false;
  }else{
    document.getElementById('description-form').innerHTML='';
  }


 if(document.getElementById('dividend').value==''){
    document.getElementById('dividend-form').innerHTML='It is required';
     valid=false;
  }else{
    document.getElementById('dividend-form').innerHTML='';
  }

  if(document.getElementById('document1').value==''){
   
    document.getElementById('documentnew-form').innerHTML='It is required';
     valid=false;
  }else{
    document.getElementById('documentnew-form').innerHTML='';
  }

  if(document.getElementById('document1valid').value=='1'){
    document.getElementById('documentnew-form').innerHTML='File is too big (max 8 MB)';
     valid=false;
  }

  if(document.getElementById('document2').value==''){
    document.getElementById('document2-form').innerHTML='It is required';
     valid=false;
  }else{
    document.getElementById('document2-form').innerHTML='';
  }

 if(document.getElementById('document2valid').value=='1'){
    document.getElementById('document2-form').innerHTML='File is too big (max 8 MB)';
     valid=false;
  }

  if(document.getElementById('document3').value==''){
    document.getElementById('document3-form').innerHTML='It is required';
     valid=false;
  }else{
    document.getElementById('document3-form').innerHTML='';
  }

   if(document.getElementById('document3valid').value=='1'){
    document.getElementById('document3-form').innerHTML='File is too big (max 8 MB)';
     valid=false;
  }

  if(document.getElementById('document4').value==''){
    document.getElementById('document4-form').innerHTML='It is required';
     valid=false;
  }else{
    document.getElementById('document4-form').innerHTML='';
  }
 if(document.getElementById('document4valid').value=='1'){
    document.getElementById('document4-form').innerHTML='File is too big (max 8 MB)';
     valid=false;
  }

  if(document.getElementById('document5').value==''){
    document.getElementById('document5-form').innerHTML='It is required';
     valid=false;
  }else{
    document.getElementById('document5-form').innerHTML='';
  }

   if(document.getElementById('document5valid').value=='1'){
    document.getElementById('document5-form').innerHTML='File is too big (max 8 MB)';
     valid=false;
  }
  return valid;
}

</script>

   
@endsection

        

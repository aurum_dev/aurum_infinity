@extends('admin.layout.base')

@section('title', 'Dividend ')

@section('content')
<style>
    #example_length{
margin-top: 5px !important;
    float: right !important;
    margin-right: 266px !important;
}
table.dataTable.dtr-inline.collapsed > tbody > tr > td.dtr-control:before, table.dataTable.dtr-inline.collapsed > tbody > tr > th.dtr-control:before

{
    margin-top: -23px !important;
}
    
</style>
          <!-- breadcrumb -->
                    <div class="breadcrumb-header justify-content-between">
                        <div class="my-auto">
                            <div class="d-flex">
                                <h4 class="content-title mb-0 my-auto"> DIVIDEND DETAILED TOKEN  LIST</h4><span class="text-muted mt-1 tx-13 ms-2 mb-0"></span>
                            </div>
                        </div>
                        <div class="d-flex my-xl-auto right-content">
                            <a class="btn btn-primary" href="{{url('/')}}/issuer/dividendlist">

<i class="fas fa-arrow-circle-left"></i>
                            Back</a>
                            
                        </div>
                    </div>
                    <!-- breadcrumb -->



<div class="row">
<div class="col-xl-12">
                      <div class="card mg-b-20">
                           
                            <div class="card-body">
                                 <p class="content-title mb-0 my-auto"> 

                                    <b style="text-transform:uppercase;">Property Name : </b>

                                    <span>{{$property->propertyName}}</span></p>
                                     <p class="content-title mb-0 my-auto"> 

                                    <b style="text-transform:uppercase;">Issuer Details : </b>

                                    <span>{{$issuer->name}}</span></p>
                                     <p class="content-title mb-0 my-auto"> 

                                    <b style="text-transform:uppercase;">Dividend Title : </b>

                                    <span>{{$tokenDividend->title}}</span></p>

                                     <p class="content-title mb-0 my-auto"> 

                                    <b style="text-transform:uppercase;">Total Bonus Token : </b>

                                    <span>{{$tokenDividend->dividend/$property->tokenValue}}</span></p>
      
        <p class="content-title mb-0 my-auto"> 

                                    <b style="text-transform:uppercase;">Bonus Token Valid From : </b>

                                    <span>{{date("d M, Y",strtotime($tokenDividend->startdate))}}</span>

 <b style="text-transform:uppercase;"> To : </b>
  <span>{{date("d M, Y",strtotime($tokenDividend->enddate))}}</span>
                                </p>
                                

<br><br>
                                <div class="table-responsive">



                                    <table id="example" class="table key-buttons text-md-nowrap">
                                        <thead>
                                            <tr>
                                                <th class="border-bottom-0" style="text-align:left;width:10px;">S.NO</th>
                                                       
                                                <th class="border-bottom-0" style="text-align:left;width:20px;">Investor</th>
                                                
                                                 <th class="border-bottom-0" style="text-align:left;width:10px;">Holding Tokens(Investor)</th>
                                                <th class="border-bottom-0" style="text-align:left;width:10px;">Bonus Tokens</th>
                                                <th class="border-bottom-0" style="text-align:left;width:20px;">Total Holding Days(Investor)</th>
                                              <th class="border-bottom-0" style="text-align:left;width:20px;">Total Holding Days</th>
                                                  
                                                     <th class="border-bottom-0" style="text-align:left;width:20px;">STATUS</th>
                                                     
                                                        <th class="border-bottom-0" style="text-align:left;">TXN HASH</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($tokenbonusdividend as $index => $val)
                                         <tr>
                                             
<td>{{$index+1}}</td>
<td>
<?php
$tmp = \App\User::where('id',$val->investor_id)->first();
echo $tmp->name."<br>".$tmp->email;

?>
   </td>
<td style="text-align: center;">{{$val->total_tokens}}</td>
<td style="text-align: center;">{{$val->dividendtokens}}</td>
<td style="text-align: center;">{{$val->inves_holdingdays}}</td>
<td style="text-align: center;">{{$val->total_holdingdays_req}}</td>

<td>
     @if($val->status == 1) 
                                    <span style="align-items: center;
    border: 1px solid #d6dcec;
    border-radius: 6px;
    display: flex;
    height: 2.5rem;
    justify-content: center;
    margin: 4px;
    text-align: center;
    transform: scale(1);
    transition: .3s ease-in-out;
    width: 2.5rem;"><i class="fa fa-check" aria-hidden="true" style="color:#22c03c;"></i></span> 
                                @else  
                                    <span style="align-items: center;
    border: 1px solid #d6dcec;
    border-radius: 6px;
    display: flex;
    height: 2.5rem;
    justify-content: center;
    margin: 4px;
    text-align: center;
    transform: scale(1);
    transition: .3s ease-in-out;
    width: 2.5rem;"><i class="fa fa-times" aria-hidden="true" style="color:#ff0000;"></i></span> 
                                @endif

</td>

<td>{{$val->txnhash}}</td>
                                         </tr>
                                          @endforeach 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
</div>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
 
      $(".dividendapprove").on("click", function () {
            var walletid  = $(this).attr("data-id");                  
                      
      
     
         var url = "{{ url('/admin/investbonusstoreadmin') }}/" + walletid;

            if (walletid != '') {
                swal({
                         title     : "Are you sure?",
                         html      :"<h4>Loading...</h4>",
                         showSpinner: true,
                         text      : "You want to approve the Bonus Dividend Request",
                         icon      : "info",
                         buttons   : true,
                         dangerMode: false,
                         buttons   : ["No", "Yes"],
                     })
                    .then((willDelete) => {
                        if (willDelete) {
                            swal({
                title: 'Please Wait..!',
                text: 'Is working..',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                onOpen: () => {
                    swal.showLoading()
                }
            })
                            $.ajax({
                                       type   : "GET",
                                       url    : url,                                       
                                       success: function (data) {
                                           if (data.status == 'success') {
                                               swal('Dividend Request Approved Successfully');

                                               location.reload();
                                           }
                                           if (data.status == 'failed') {
                                               swal("We couldn't connect to the server!");
                                             //  location.reload();
                                           }
                                       }
                                   });
                        }
                    });
            }
            return false;
        });
    
    </script>
    
@endsection



@extends('issuer.layout.base')
@section('content')
 

<!-- breadcrumb -->
          <div class="breadcrumb-header justify-content-between">
            <div class="left-content">
              <div>
              <h2 class="main-content-title tx-24 mg-b-1 mg-b-lg-1">Hi {{Auth::user()->name}}, Welcome Back!</h2>
              <p class="mg-b-0">Dashboard</p>
              </div>
            </div>
            
          </div>
          <!-- breadcrumb -->
   <?php
 $id=Auth::user()->id;
$propdet=\App\Property::where('user_id',$id)->get();
$totaltoken=0;
$totaltokenreq=0;
$totaltokenapp=0;
foreach($propdet as $index => $value){
  $totaltoken+=$value->tokenSupply;
   if($value->contract_address==''){
    $totaltokenreq+=$value->tokenSupply;
   }
    if($value->contract_address!=''){
    $totaltokenapp+=$value->tokenSupply;
   }
}
?>

          <!-- row -->
          <div class="row row-sm">
            <div class="col-xl-6 col-lg-6 col-md-6 col-xm-12">
              <div class="card overflow-hidden sales-card bg-primary-gradient">
               <a href="{{ route('property') }}"> <div class="ps-3 pt-3 pe-3 pb-2 pt-0">
                  <div class="">
                    <h6 class="mb-3 tx-20 text-white"><i class="fas fa-coins"></i>&nbsp; TOTAL TOKENS CREATED</h6>
                  </div>
                  <div class="pb-0 mt-0">
                    <div class="d-flex">
                      <div class="">
                        <h4 class="tx-30 fw-bold mb-1 text-white"> {{ $totaltoken }}</h4>
                        
                      </div>
                     
                    </div>
                  </div>
                </div></a>
               
              </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-xm-12">
              <div class="card overflow-hidden sales-card bg-danger-gradient">
               <a href="{{ route('tokenRequest') }}"> <div class="ps-3 pt-3 pe-3 pb-2 pt-0">
                  <div class="">
                    <h6 class="mb-3 tx-20 text-white"><i class="fas fa-coins"></i>&nbsp; REQUESTED TOKEN</h6>
                  </div>
                  <div class="pb-0 mt-0">
                    <div class="d-flex">
                      <div class="">
                        <h4 class="tx-30 fw-bold mb-1 text-white">{{ $totaltokenreq }}</h4>
                        
                      </div>
                     
                    </div>
                  </div>
                </div></a>
              
              </div>
            </div>
           
            <div class="col-xl-6 col-lg-6 col-md-6 col-xm-12">
              <div class="card overflow-hidden sales-card bg-warning-gradient">
                <a href="{{ route('tokenApprove') }}"> <div class="ps-3 pt-3 pe-3 pb-2 pt-0">
                  <div class="">
                    <h6 class="mb-3 tx-20 text-white"><i class="fas fa-coins"></i>&nbsp; APPROVED TOKEN</h6>
                  </div>
                  <div class="pb-0 mt-0">
                    <div class="d-flex">
                      <div class="">
                        <h4 class="tx-30 fw-bold mb-1 text-white">{{$totaltokenapp}}</h4>
                      
                      </div>
                      
                    </div>
                  </div>
                </div></a>
              
              </div>
            </div>
             <div class="col-xl-6 col-lg-6 col-md-6 col-xm-12">
              <div class="card overflow-hidden sales-card bg-success-gradient">
                <a href="{{ route('property') }}"><div class="ps-3 pt-3 pe-3 pb-2 pt-0">
                  <div class="">
                    <h6 class="mb-3 tx-20 text-white"><i class="fas fa-home"></i>&nbsp;NO OF PROPERTIES</h6>
                  </div>
                  <div class="pb-0 mt-0">
                    <div class="d-flex">
                      <div class="">
                        <h4 class="tx-30 fw-bold mb-1 text-white">{{ count($propdet) }}</h4>
                        
                      </div>
                      
                    </div>
                  </div>
                </div></a>
                
              </div>
            </div>
          </div>

@endsection

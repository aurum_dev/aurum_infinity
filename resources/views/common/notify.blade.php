@if (count($errors) > 0)
    <div class="alert alert-danger">
        <button type="button" class="close" data-bs-dismiss="alert">×</button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(Session::has('flash_errorr'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-bs-dismiss="alert">×</button>
        <ul>
           <?php  $ty=Session::get('flash_errorr');
            $tyr=explode(",",$ty);
            ?>
            @foreach ($tyr as $errorr)
                <li>{{ $errorr }}</li>
            @endforeach
        </ul>
    </div>
@endif


@if(Session::has('flash_error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-bs-dismiss="alert">×</button>
        {{ Session::get('flash_error') }}
    </div>
@endif


@if(Session::has('flash_success'))
    <div class="alert alert-success">
        <button type="button" class="close" data-bs-dismiss="alert">×</button>
        {{ Session::get('flash_success') }}
    </div>
@endif

@if(Session::has('flash_warning'))
    <div class="alert alert-warning">
        <button type="button" class="close" data-bs-dismiss="alert">×</button>
        {{ Session::get('flash_warning') }}
    </div>
@endif
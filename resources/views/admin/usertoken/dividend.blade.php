@extends('admin.layout.base')

@section('title', 'Dividend')

@section('content')
<style>
    
#example_length{
margin-top: 5px !important;
    float: right !important;
    margin-right: 82px !important;
}
table.dataTable.dtr-inline.collapsed > tbody > tr > td.dtr-control:before, table.dataTable.dtr-inline.collapsed > tbody > tr > th.dtr-control:before

{
    top: 32% !important;
}
.table>:not(:last-child)>:last-child>*{
    background:#737f9e !important;
    color: #fff;
}

</style>
<div class="breadcrumb-header justify-content-between">
                    <div class="my-auto">
                        <div class="d-flex">
                            <h4 class="content-title mb-0 my-auto"> Divident</h4><span
                                class="text-muted mt-1 tx-13 ms-2 mb-0">/ Divident</span>
                        </div>
                    </div>                    
                </div>
<div class="content-area py-1" style="background:#fff;padding-top: 40px !important;">
    <div class="container-fluid">
        <div class="box box-block bg-white">

            <h5 class="mb-1">Dividend</h5>
            
            <table id="example" class="table table-striped table-hover dt-responsive"  class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>User</th>
                        <th>Token</th>
                        <th>Payment</th>                          
                        <th>Txn Hash</th>                          
                        <th>Payment Amount</th>                          
                        <th>Token Price</th>                          
                        <th>Bonus Value</th>                          
                        <th>Bonus Token</th>                          
                        <th>Number of Token</th>                          
                        <th>Total Token</th>
                    </tr>
                </thead>

                <tbody>
                @foreach($user_token_txn as $index => $token)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $token->user->name }}</td>
                        <td>{{ $token->usercontract->tokenname }} ({{ $token->usercontract->tokensymbol }})</td>
                        <td>{{$token->payment_type}}</td>                        
                        <td>{{$token->txn_hash}}</td>                        
                        <td>{{$token->token_price}}</td>                        
                        <td>{{$token->bonus_value}}</td>                        
                        <td>{{$token->bonus_token}}</td>                        
                        <td>{{$token->number_of_token}}</td>                        
                        <td>{{$token->total_token}}</td>                        
                        <td>
                            @if($token->status==1) 
                                <span style="color: green;">Success</span> 
                            @else 
                                <span style="color: red;">Pending</span> 
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>

                <tfoot>
                    <tr>
                        <th>S.No</th>
                        <th>User</th>
                        <th>Token</th>
                        <th>Payment</th>                          
                        <th>Txn Hash</th>                          
                        <th>Payment Amount</th>                          
                        <th>Token Price</th>                          
                        <th>Bonus Value</th>                          
                        <th>Bonus Token</th>                          
                        <th>Number of Token</th>                          
                        <th>Total Token</th> 
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
@extends('admin.layout.base')

@if($page==1)
    @section('title', 'User Transaction Token')
@else
    @section('title', 'Transaction Token')
@endif

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">

            <h5 class="mb-1">@if($page==1) User @endif Transaction Token</h5>
            
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>User</th>
                        <th>Token</th>
                        <th>Payment</th>                          
                        <th>Txn Hash</th>                          
                        <th>Payment Amount</th>                          
                        <th>Token Price</th>                          
                        <th>Bonus Value</th>                          
                        <th>Bonus Token</th>                          
                        <th>Number of Token</th>                          
                        <th>Total Token</th> 
                        @if($page==0)
                            <th>Action</th> 
                        @endif
                    </tr>
                </thead>

                <tbody>
                @foreach($user_token_txn as $index => $token)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $token->user->name }}</td>
                        <td>{{ $token->usercontract->tokenname }} ({{ $token->usercontract->tokensymbol }})</td>
                        <td>{{$token->payment_type}}</td>                        
                        <td>{{$token->txn_hash}}</td>                        
                        <td>{{$token->token_price}}</td>                        
                        <td>{{$token->bonus_value}}</td>                        
                        <td>{{$token->bonus_token}}</td>                        
                        <td>{{$token->number_of_token}}</td>                        
                        <td>{{$token->total_token}}</td>                        
                        <td>
                            @if($token->status==1) 
                                <span style="color: green;">Success</span> 
                            @else 
                                <span style="color: red;">Pending</span> 
                            @endif
                        </td>
                        
                        @if($page==0)
                            <td>
                                <a href="{{route('admin.usertoken.transactionstatus',$token->id)}}" class="btn btn-info">Change Status</a>
                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>

                <tfoot>
                    <tr>
                        <th>S.No</th>
                        <th>User</th>
                        <th>Token</th>
                        <th>Payment</th>                          
                        <th>Txn Hash</th>                          
                        <th>Payment Amount</th>                          
                        <th>Token Price</th>                          
                        <th>Bonus Value</th>                          
                        <th>Bonus Token</th>                          
                        <th>Number of Token</th>                          
                        <th>Total Token</th> 
                        @if($page==0)
                            <th>Action</th> 
                        @endif
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
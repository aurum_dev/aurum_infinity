@extends('admin.layout.base')

@section('title', 'Update Helpcenter')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">

            <a href="{{ route('admin.helpcenter.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> @lang('admin.back')</a>

			<h5 style="margin-bottom: 2em;">Create helpcenter</h5>

            <form class="form-horizontal" action="{{route('admin.helpcenter.update', $helpcenter->id)}}" method="POST" enctype="multipart/form-data" role="form">            	
            	{{csrf_field()}}
            	 <input type="hidden" name="_method" value="PATCH">

				 <div class="row">
                    <div class="form-group col-md-4">
                    <label for="" class="col-form-label">Type</label>                  
                   
                    <select class="form-control" name="type" required id="type">
                        <option value=""> Select type</option>
                        <option value="email" @if($helpcenter->type=='email') selected @endif> Email</option>
                        <option value="phone"  @if($helpcenter->type=='phone') selected @endif>Phone</option>
                    </select>
                </div> 
                </div>
                 
                <div class="row">

                 <div class="form-group col-md-4">
                    <label for="" class="col-form-label">Title</label>                  
                    <input class="form-control" type="text" name="title" required id="title" value="{{ $helpcenter->title }}" placeholder="Enter title">
                </div>   
                </div>           


             <div class="row">
                <div class="form-group col-md-4">
                    <label for="" class="col-form-label">Description</label>                  
                    <input class="form-control" type="text" name="description" required id="description" value="{{ $helpcenter->description }}" placeholder="Enter description">
                </div>              
             </div>
             <div class="row">
                    <div class="form-group col-md-4">
                    <br>                
                    <br>                
                        <label for="" class="col-xs-2 col-form-label"></label>
                        <div class="col-xs-10">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="{{route('admin.helpcenter.index')}}" class="btn btn-default">@lang('admin.cancel')</a>
                        </div>
                    </div>
             </div>
			</form>
		</div>
    </div>
</div>

@endsection
@extends('admin.layout.base')

@section('title', 'HelpCenter ')

@section('content')
<style>
    
#example_length{
margin-top: 5px !important;
    float: right !important;
    margin-right: 266px !important;
}


</style>
<div class="breadcrumb-header justify-content-between">
                    <div class="my-auto">
                        <div class="d-flex">
                            <h4 class="content-title mb-0 my-auto">HelpCenters</h4><span
                                class="text-muted mt-1 tx-13 ms-2 mb-0">/ Helpcenters List</span>
                        </div>
                    </div>                    
                </div>
<div class="content-area py-1" style="background:#fff;padding-top: 40px !important;">
    <div class="container-fluid">
        <div class="box box-block bg-white">          
         
               
            <table  id="example" class="table table-striped table-hover dt-responsive" id="table-2">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Ticket ID</th>
                        <th>Name</th>
                        <th>Email</th>                        
                        <th>Message</th>
                        <th>Priority</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($helpcenter as $index => $user)
                    <tr>
                        <td>{{$index+1}}</td>
                        <td>{{$user->help_id}}</td>                                         
                        <td>{{$user->name}}</td>                                                
                        <td>{{$user->email}}</td>   
                        <td>{{$user->message}}</td>   
                        <td>{{$user->priority}}</td>   
                        <td>  <a href="<?php echo url('/') ?>/admin/property/createhelp/{{$user->help_id}}" style="margin-left: 1em;margin-bottom: 20px;" class="btn btn-primary pull-right"> Reply</a></td>  
                    </tr>
                    @endforeach
                </tbody>
                
            </table>
        </div>
    </div>
</div>
@endsection
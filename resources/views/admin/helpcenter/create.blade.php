@extends('admin.layout.base')

@section('title', 'Create Help Center')

@section('content')

<div class="breadcrumb-header justify-content-between">
                    <div class="my-auto">
                        <div class="d-flex">
                            <h4 class="content-title mb-0 my-auto">Helpcenter Reply</h4>
                        </div>
                    </div>                    
                </div>
<div class="content-area py-1" style="background:#fff;padding-top: 40px !important;">
    <div class="container-fluid">
        <div class="box box-block bg-white">
   <div class="row">
                    <div class="form-group col-md-12">
            <a href="{{ route('admin.helpcenter.index') }}" style="float:right;" class="btn btn-primary pull-right"><i class="fa fa-angle-left"></i> @lang('admin.back')</a>

           </div></div>
            <form class="form-horizontal" action="{{route('admin.helpcenter.store')}}" method="POST" enctype="multipart/form-data" role="form">              
                {{csrf_field()}}
<div class="form-group col-md-12">
      <div class="row">
        @foreach($helpcenter as $index => $val)

                 <div class="form-group col-md-4">
                    <label for="" class="col-form-label">Ticket Id </label>                  
                    <input class="form-control" type="text" readonly name="ticketid" required id="ticketid" value="{{$val->help_id}}" placeholder="Enter title">
                </div>   
               
                 
                </div>
                 <div class="row">

                 <div class="form-group col-md-12">
                    <label for="" class="col-form-label">Name</label>   
                                   
                    <input class="form-control" readonly value="{{$val->name}}" type="text" name="name" required id="name" placeholder="Enter Name">
                </div>   
                </div>     
                <div class="row">

                 <div class="form-group col-md-12">
                    <label for="" class="col-form-label">Email To</label>   

                    <input class="form-control" readonly value="{{$val->email}}" type="email" name="email" required id="email" placeholder="Enter Email">
                </div>   
                </div>           
  <div class="row">

                 <div class="form-group col-md-12">
                    <label for="" class="col-form-label">Subject</label>                  
                    <input class="form-control" type="text" name="subject" required  placeholder="Enter Subject">
                </div>   
                </div>           


             <div class="row">
                <div class="form-group col-md-12">
                    <label for="" class="col-form-label">Description</label>                  
                   <textarea name="description" id="myeditor"></textarea>
                </div>              
             </div>
             @endforeach
             <div class="row">
                    <div class="form-group col-md-12">
                    <br>                
                    <br>                
                        <div class="col-xs-12">
                           
                            <a href="{{route('admin.helpcenter.index')}}" style="float:right;margin-left:10px;" class="btn btn-secondary">@lang('admin.cancel')</a>
                             <button type="submit" class="btn btn-primary" style="float:right;">Submit</button>
                        </div>
                    </div>
             </div>
         </div>
            </form>
        </div>
    </div>
</div>
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('myeditor');
</script>
@endsection
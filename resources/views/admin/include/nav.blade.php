	<aside class="app-sidebar sidebar-scroll">
				<div class="main-sidebar-header active">
					<a class="desktop-logo logo-light active" href=""><img src="<?php echo url('/'); ?>/public/issuer/assets/img/brand/logo.png" class="main-logo" alt="logo"></a>
					<a class="desktop-logo logo-dark active" href="index.html"><img src="<?php echo url('/'); ?>/public/issuer/assets/img/brand/logo-white.png" class="main-logo dark-theme" alt="logo"></a>
					<a class="logo-icon mobile-logo icon-light active" href=""><img src="<?php echo url('/'); ?>/public/issuer/assets/img/brand/favicon.png" class="logo-icon" alt="logo"></a>
					<a class="logo-icon mobile-logo icon-dark active" href=""><img src="<?php echo url('/'); ?>/public/issuer/assets/img/brand/favicon-white.png" class="logo-icon dark-theme" alt="logo"></a>
				</div>
				<div class="main-sidemenu">
					<div class="app-sidebar__user clearfix">
						<div class="dropdown user-pro-body">
							
							<div class="user-info">
								<h4 class="fw-semibold mt-3 mb-0">{{Auth::user()->name}} </h4>

    <span class="mb-0 text-muted">{{Auth::user()->email}}    </span> 
							</div>
						</div>
					</div>
					<ul class="side-menu">
						<li class="side-item side-item-category">Main</li>
						<li class="slide">
<li class="slide"><a class="side-menu__item" data-bs-toggle="slide" href="{{ route('dashboard') }}"><i class="la la-tachometer" style="font-size:22.5px;color:#5b6e88;padding: 1px 14px 1px 1px;"></i><span class="side-menu__label">Dashboard</span></a>
						</li>
							
						<li class="side-item side-item-category">Users</li>
						<li class="slide"><a class="side-menu__item" data-bs-toggle="slide" href="{{ route('admin.user.index') }}"><i class="fas fa-users" style="font-size:19px;color:#5b6e88;padding: 1px 15px 1px 0px;"></i><span class="side-menu__label"> Users List</span></a>
						</li>
						<li class="side-item side-item-category">LIQUID TOKENIZATION</li>

						
						<!--<li class="slide"><a class="side-menu__item" data-bs-toggle="slide" href="{{ route('token') }}"><i class="cf cf-eth" style="font-size:21.5px;color:#5b6e88;padding: 1px 15px 1px 0px;"></i><span class="side-menu__label">User Token</span><i class="angle fe fe-chevron-down"></i></a>
							<ul class="slide-menu">
		<li><a class="slide-item" href="{{ route('admin.usertoken.index') }}">Token Transaction</a></li>
		<li><a class="slide-item" href="{{ route('admin.dividend') }}">Dividend</a></li>
								
							</ul>
						</li>-->
						<li class="slide"><a class="side-menu__item" data-bs-toggle="slide" href="{{ route('tokenRequest') }}"><i class="cf cf-eth" style="font-size:21.5px;color:#5b6e88;padding: 1px 15px 1px 0px;"></i><span class="side-menu__label"> Requested Token</span><i class="angle fe fe-chevron-down"></i></a>
							<ul class="slide-menu">
								<li><a class="slide-item" href="{{ route('admin.requestedtoken') }}">Requested Token</a></li>
								<li><a class="slide-item" href="{{ route('admin.tokenhistory') }}">Token History</a></li>
								
							</ul>
						</li>
						<li class="side-item side-item-category">PROPERTY</li>
						<li class="slide"><a class="side-menu__item" data-bs-toggle="slide" href="{{ route('property') }}"><i class="fas fa-home" style="font-size:16.5px;color:#5b6e88;padding: 1px 17px 1px 1px;"></i><span class="side-menu__label">Property</span><i class="angle fe fe-chevron-down"></i></a>
							<ul class="slide-menu">
								<li><a class="slide-item" href="{{ route('admin.property.index') }}">Property</a></li>
								<li><a class="slide-item" href="{{ route('admin.property.showasset') }}">Asset Type</a></li>
								<li><a class="slide-item" href="{{ route('admin.dividend') }}">Dividend</a></li>
							</ul>
						</li>
						<li class="side-item side-item-category">TRANSACTION</li>
						<li class="slide"><a class="side-menu__item" data-bs-toggle="slide" href="#"><i class="fas fa-exchange-alt" style="font-size:16.5px;color:#5b6e88;padding: 1px 16px 1px 1px;"></i><span class="side-menu__label">Transaction</span><i class="angle fe fe-chevron-down"></i></a>
							<ul class="slide-menu">
								<li><a class="slide-item" href="{{ url('/admin/wallet') }}">Admin Wallet</a></li>
								<li><a class="slide-item" href="{{ route('admin.tokenizer.showtrans') }}">Crypto Transfer</a></li>
							</ul>
						</li>
						<li class="side-item side-item-category">SETTINGS</li>

						
						<li class="slide"><a class="side-menu__item" data-bs-toggle="slide" href="#"><i class="fas fa-cogs" style="font-size:16.5px;color:#5b6e88;padding: 1px 16px 1px 1px;"></i><span class="side-menu__label">Settings</span><i class="angle fe fe-chevron-down"></i></a>
							<ul class="slide-menu">
								<li><a class="slide-item" href="{{ url('/admin/2fa/urlform') }}?route_url='admin.settings.index'">Site Settings</a></li>
								
							</ul>
						</li>
					   <li class="side-item side-item-category">OTHERS</li>
					   <li class="slide"><a class="side-menu__item" data-bs-toggle="slide" 
					   	href="{{ route('admin.helpcenter.index') }}"><i class="fa fa-user" style="font-size:16.5px;color:#5b6e88;padding: 1px 16px 1px 1px;"></i><span class="side-menu__label">Help Center</span></a>
					   </li>
                       <li class="slide"><a class="side-menu__item" data-bs-toggle="slide" href="{{ route('admin.privacy') }}"><i class="fa fa-unlock-alt" style="font-size:16.5px;color:#5b6e88;padding: 1px 16px 1px 1px;"></i><span class="side-menu__label">Privacy Policy</span></a>
					   </li>
					   	<li class="slide"><a class="side-menu__item" data-bs-toggle="slide" href="{{ route('admin.terms') }}"><i class="fas fa-file-contract" style="font-size:16.5px;color:#5b6e88;padding: 1px 16px 1px 1px;"></i><span class="side-menu__label">Terms & Conditions</span></a>
					   </li>
  						<li class="side-item side-item-category">ACCOUNT</li>
  						<li class="slide"><a class="side-menu__item" data-bs-toggle="slide" href="{{ route('admin.password') }}"><i class="fas fa-user-lock" style="font-size:16.5px;color:#5b6e88;padding: 1px 16px 1px 1px;"></i><span class="side-menu__label">Change Password</span></a>
					   </li>
					   <li class="slide"><a class="side-menu__item" data-bs-toggle="slide" href="{{ route('adminlogout') }}" onclick="event.preventDefault();
						document.getElementById('logout-form').submit();"><i class="fas fa-power-off" style="font-size:16.5px;color:#5b6e88;padding: 1px 16px 1px 1px;"></i><span class="side-menu__label">Logout</span></a>
						   	<form id="logout-form" action="{{ route('adminlogout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
					   </li>
					</ul>
				</div>
			</aside>
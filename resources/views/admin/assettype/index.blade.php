@extends('admin.layout.base')

@section('title', 'Property Asset Types')

@section('content')
<style>
    
#example_length{
margin-top: 5px !important;
    float: right !important;
    margin-right: 266px !important;
}


</style>
<div class="breadcrumb-header justify-content-between">
                    <div class="my-auto">
                        <div class="d-flex">
                            <h4 class="content-title mb-0 my-auto">Assets</h4><span
                                class="text-muted mt-1 tx-13 ms-2 mb-0">/ Assets List</span>
                        </div>
                    </div>                    
                </div>
    <div class="content-area py-1" style="background:#fff;padding-top: 40px !important;">
        <div class="container-fluid">
            
            <div class="box box-block bg-white">
               
                <h5 class="mb-1">Property Asset Types</h5>

                <a href="{{ route('admin.property.createasset') }}" style="margin-left: 1em;float: right;margin-bottom: 20px;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add</a>

                <table  id="example" class="table table-striped table-hover dt-responsive" id="table-2">
                    <thead>
                        <tr>
                            <th>@lang('admin.id')</th>                            
                            <th>Asset Type</th>                            
                            <th>@lang('admin.action')</th>
                        </tr>
                    </thead>
                    <tbody>    
                        @foreach ($assets as $key => $asset)
                            <tr>
                                <td>{{ @$key + 1}}</td>
                                <td>{{ $asset->type }}</td>
                                <td>
                                    <a href="{{ route('admin.property.editasset', $asset->id) }}" class="btn btn-secondary btn-block" style="width:40px;padding:9px 10px;float:left;display:block;margin-top:0;" title="Edit"><i class="fas fa-edit"></i></a>
                                    <a href="{{ route('admin.property.deleteasset', $asset->id) }}"class="btn btn-info btn-block" style="width:40px;padding:9px 10px;float:left;display:block;margin-top:0;margin-left:5px;"> <i class="fa fa-trash" aria-hidden="true"></i> </a>
                                </td>
                            </tr>
                        @endforeach                                            
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>@lang('admin.id')</th>                            
                            <th>Asset Type</th>                            
                            <th>@lang('admin.action')</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection
@extends('admin.layout.auth')

@section('content')


 <div class="container-fluid">
            <div class="row no-gutter">
                <!-- The image half -->
                <div class="col-md-6 col-lg-6 col-xl-7 d-none d-md-flex bg-primary-transparent">
                    <div class="row wd-100p mx-auto text-center">
                        <div class="col-md-12 col-lg-12 col-xl-12 my-auto mx-auto wd-100p">
                            <img src="../public/issuer/assets/img/media/login2.png" class="my-auto ht-xl-80p wd-md-100p wd-xl-50p mx-auto" alt="logo">
                        </div>
                    </div>
                </div>
                <!-- The content half -->
                <div class="col-md-6 col-lg-6 col-xl-5 bg-white">
                    <div class="login d-flex align-items-center py-2">
                        <!-- Demo content-->
                        <div class="container p-0">
                            <div class="row">
                                <div class="col-md-10 col-lg-10 col-xl-9 mx-auto">
                                    <div class="card-sigin">
                                        <div class="mb-5 d-flex">
                                            <a href="index.html"><img src="../public/issuer/assets/img/brand/logo.png" class="sign-favicon-a" alt="logo">
                                          
                                            </a>
                                           
                                        </div>
                                        <div class="card-sigin">
                                            <div class="main-signup-header">
                                                <h2>Welcome Back!</h2>
                                                <h5 class="fw-semibold mb-4">Please sign in to continue.</h5>
                                                <form role="form" method="POST" action="{{ url('/admin/login') }}">
                                                     {{ csrf_field() }}
                                                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                                        <label>Email</label> <input type="email" name="email" required="true" class="form-control" id="email" placeholder="@lang('user.profiles.email')" type="text">
                                                        @if ($errors->has('email'))
                                                            <span class="help-block" style="margin-left: 55px;color: red;">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                                        <label>Password</label> <input type="password" name="password" required="true" class="form-control" id="password" placeholder="@lang('user.profiles.password')">
                                                        @if ($errors->has('password'))
                                                            <span class="help-block" style="margin-left: 55px;color: red;">
                                                                <strong>{{ $errors->first('password') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div><input type="submit" class="btn btn-main-primary btn-block" value="Sign In"> 
                                                   
                                                </form>
                                                <div class="main-signin-footer mt-5">
                                                    <!--  <p><a href="">Forgot password?</a></p>
                                                  <p>Don't have an account? <a href="">Create an Account</a></p>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- End -->
                    </div>
                </div><!-- End -->
            </div>
        </div>





@endsection

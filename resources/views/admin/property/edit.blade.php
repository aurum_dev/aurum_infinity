@extends('admin.layout.base')

@section('title', 'Edit Property')

@section('content')
<!--  -->
<style>
    .tooltip {
  position: relative;
  display: inline-block;
 
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 349px;
    background-color: #ebebeb;
    color: #333;
    text-align: center;
    border-radius: 6px;
    padding: 10px 10px;
    position: absolute;
    font-size: 14px;
    font-weight: 500;
    z-index: 1;
    margin-left: -180px;
    margin-top: 20px;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
}</style>
                <!-- breadcrumb -->
                    <div class="breadcrumb-header justify-content-between">
                        <div class="my-auto">
                            <div class="d-flex">
                                <h4 class="content-title mb-0 my-auto"> Create Token & Property</h4><span class="text-muted mt-1 tx-13 ms-2 mb-0">/ Add New Property</span>
                            </div>
                        </div>
                        <div class="d-flex my-xl-auto right-content">
                          
                           <a href="{{ url('admin/property') }}" style="margin-left: 1em;float: right;margin-bottom: 20px;" class="btn btn-primary pull-right"><i class="fa fa-arrow-left"></i> Back To Property</a>
                        </div>
                    </div>
                    <!-- breadcrumb -->

<div class="row">

                        
                        <div class="col-lg-12 col-md-12">

                            <div class="card">
                                <div class="card-body">

                                   @foreach ($property as $item)  
                                    <form class="form-horizontal" action="{{url('/admin/property/'.$item->id)}}" method="post" enctype="multipart/form-data">
                                 {{csrf_field()}}
                                  {{ method_field("PUT") }}
                                <input type="hidden" name="token_type" value="1">
                                    <div id="wizard2">
                                        <h3>Property Overview Details</h3>
                                     
                                        <section>
                                            <p class="mg-b-20">Try the keyboard navigation by clicking arrow left or right!</p>
                                            <p style="color:#000;"><b>Note :</b> <i><span class="tx-danger">*</span> Fields are Mandatory</i></p>
                                            <div class="row row-sm">
                                               


                                               
 
                                              
                                                <div class="col-md-5 col-lg-4">
                                                    <label class="form-control-label">Property Name: <span class="tx-danger">*</span></label>
                                                     <input class="form-control" id="propertyclient" name="propertyclient" placeholder="" value="{{ $item->user_id}}" required="" type="hidden">
                                                    <input class="form-control" id="propertyname" name="propertyname" placeholder="Enter Property Name" required="" value="{{ $item->propertyName}} " type="text">
                                                    <div class="tx-danger" id="propertyname-form" style="padding-top:10px;"></div>
                                                </div>
                                                
                                                <div class="col-lg-4 mg-t-20 mg-lg-t-0">
                                            <label class="form-control-label">Property State: <span class="tx-danger">*</span></label>
                                          


                                           

                            <input type="hidden" name="country" id="countryId" value="IN"/>
<select name="state" class="states order-alpha form-control select2" id="stateId">
   
   
 <option value="{{ $item->propertyState}}" selected>{{ $item->propertyState}}</option>
 
  
</select>

<div class="tx-danger" id="stateId-form" style="padding-top:10px;"></div>
                                            
                                            <script src="//geodata.solutions/includes/statecity.js"></script>  
                                        </div><!-- col-4 -->
                                        <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0">
                                            <label class="form-control-label">Property City: <span class="tx-danger">*</span></label>
                                        
<select name="city" class="cities order-alpha form-control select2" id="cityId">
    
      <option value="{{ $item->propertyCity}}" selected>{{ $item->propertyCity}}</option>
</select>

<div class="tx-danger" id="cityId-form" style="padding-top:10px;"></div>
                 

                                        </div><!-- col-4 -->
                                        
 
                                      
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Property District/Town: <span class="tx-danger">*</span></label> 

                                                    <input class="form-control" id="propertydistrict" name="propertydistrict" placeholder="Enter Property District/Town" value="{{ $item->propertyDistrict}}" required="" type="text">
                                            <div class="tx-danger" id="propertydistrict-form" style="padding-top:10px;"></div>
                                            </div>
                                                <div class="col-lg-4 mg-t-20 mg-md-t-30">
                                            
                                            <label class="form-control-label">Property Type: <span class="tx-danger">*</span></label>
                                            <select name="propertytype" id="propertytype" class="form-control select2">
                                                <option value="">Select Property Type</option>
                                                 @foreach ($assetType as $value)
                                                <option value="{{ @$value->type }}" @if($item->propertyType == $value->type) selected @endif>{{ @$value->type }}</option>
                                            @endforeach
   
</select>
<div class="tx-danger" id="propertytype-form" style="padding-top:10px;"></div>


                                            
                                            
                                        </div><!-- col-4 -->
                                        <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Token Name: <span class="tx-danger">*</span></label> 
                                                    <input class="form-control" id="tokenname" name="tokenname" value="{{ $item->tokenName}}" placeholder="Enter Token Name" required="" type="text">
                                                <div class="tx-danger" id="tokenname-form" style="padding-top:10px;"></div>
                                                </div>
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Token Symbol : <span class="tx-danger">*</span></label> 
                                                    <input class="form-control" id="tokensymbol" name="tokensymbol" value="{{ $item->tokenSymbol}}" placeholder="Enter Token Symbol" required="" type="text">
                                                <div class="tx-danger" id="tokensymbol-form" style="padding-top:10px;"></div>
                                                </div>
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Token Value (INR): <span class="tx-danger">*</span>

<div class="tooltip" style="color:#ee335e;font-weight:bold;margin-left:10px;font-size:16px;"><i class="fa fa-info-circle" aria-hidden="true"></i>
  <span class="tooltiptext">Token value will be the 1 token value. <br>for e.g. 1 Token=2000</span>
</div>
                                                    </label> 
                                                    <input class="form-control" id="tokenvalue" name="tokenvalue" oninput="this.value = 
 !!this.value && Math.abs(this.value) > 0 ? Math.abs(this.value) : null" value="{{ $item->tokenValue}}" placeholder="Enter Token Value (INR)" required="" type="number" onkeypress="inpNum(event)">
                                                <div class="tx-danger" id="tokenvalue-form" style="padding-top:10px;"></div>
                                                </div>
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Token Supply (INR): <span class="tx-danger">*</span>
<div class="tooltip" style="color:#ee335e;font-weight:bold;margin-left:10px;font-size:16px;"><i class="fa fa-info-circle" aria-hidden="true"></i>
  <span class="tooltiptext">Token supply is the total no of tokens of your property.</span>
</div>
                                                    </label> 
                                                    <input class="form-control" id="tokensupply" name="tokensupply" oninput="this.value = 
 !!this.value && Math.abs(this.value) > 0 ? Math.abs(this.value) : null" value="{{ $item->tokenSupply}}" placeholder="Enter Token Supply (INR)" required="" type="number" onkeypress="inpNum(event)">
                                                <div class="tx-danger" id="tokensupply-form" style="padding-top:10px;"></div>
                                                </div>
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Token Decimal : <span class="tx-danger">*</span></label> 
                                                    <input class="form-control" value="{{ $item->tokenDecimal}}" oninput="this.value = 
 !!this.value && Math.abs(this.value) > 0 ? Math.abs(this.value) : null" id="tokendecimal" name="tokendecimal" placeholder="Enter Token Decimal" required="" type="text">
                                                <div class="tx-danger" id="tokendecimal-form" style="padding-top:10px;"></div>
                                                </div>
                                                 <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30" style="float: left;display: inline-block;margin-top:32px;">
                                      <h3 style="font-size:17px;"><label class="form-control-label">Token Logo: <span class="tx-danger">*</span></label> </h3>
                                     
                                         @if($item->tokenLogo!='')
                                <img style="height: 90px; margin-bottom: 15px;" src="<?php echo url('/')."/" ?>{{ $item->tokenLogo }}">
                            @endif
                                       <input type="hidden" id="tokenlogoold" value="{{ $item->tokenLogo }}" name="tokenlogoold">
                                                        <input type="file" class="" id="tokenlogo" name="tokenlogo" accept="image/jpg, image/jpeg, image/png" style="margin-top:-6px;"/>
                                                        <div class="tx-danger" id="tokenlogo-form" style="padding-top:10px;"></div> 
                                                    </div>
                                                    
                                                 <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Divident : <span class="tx-danger">*</span>
<div class="tooltip" style="color:#ee335e;font-weight:bold;margin-left:10px;font-size:16px;"><i class="fa fa-info-circle" aria-hidden="true"></i>
  <span class="tooltiptext">Is that for bonus token</span>
</div>

                                                    </label> 
                                                    <input class="form-control" id="divident" name="divident" value="{{ $item->Divident}}" placeholder="Enter Divident" required="" type="text">
                                                <div class="tx-danger" id="divident-form" style="padding-top:10px;"></div>
                                                </div>
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Minimum Investment (Tokens): <span class="tx-danger">*</span></label> 
                                                    <input class="form-control" id="minimuminvestment" oninput="this.value = 
 !!this.value && Math.abs(this.value) > 0 ? Math.abs(this.value) : null" value="{{ $item->minimumInvest}}" name="minimuminvestment" placeholder="Enter Minimum Investment (Tokens)" required="" type="text">
                                                <div class="tx-danger" id="minimuminvestment-form" style="padding-top:10px;"></div>
                                                </div>
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Property Size: <span class="tx-danger">*</span></label> 
                                                    <input class="form-control" id="propertysize" name="propertysize" value="{{ $item->propertySize}}" placeholder="Property Size" required="" type="text">
                                                    <div class="tx-danger" id="propertysize-form" style="padding-top:10px;"></div>
                                                </div>
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Target IRR: <span class="tx-danger">*</span></label> 
                                                    <input class="form-control" id="targetirr" name="targetirr" value="{{ $item->targetIrr}}" placeholder="Target IRR" required="" type="text">
                                                    <div class="tx-danger" id="targetirr-form" style="padding-top:10px;"></div>
                                                </div>
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30">
                                                    <label class="form-control-label">Average Yield(%): <span class="tx-danger">*</span></label>
                                                    <input class="form-control" id="averageyield" name="averageyield" value="{{ $item->averageYield}}"  placeholder="Average Yield(%)" required="" type="text">
                                                <div class="tx-danger" id="averageyield-form" style="padding-top:10px;"></div>
                                                </div>
                                                <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-30" style="float: left;display: inline-block;">
                                                 <label class="form-control-label">Property Description: <span class="tx-danger">*</span></label> 
                                                <textarea class="form-control" id="propdesc" name="propdesc" placeholder="" required="" rows="3">{{ $item->propertyDesc}}</textarea>
                                            <div class="tx-danger" id="propdesc-form" style="padding-top:10px;"></div>
                                            </div>
                                            </div>
                                        </section>
                                        <h3>Property Details</h3>
                                        <section>
                                            <p>Please add the property details.</p>
                                            
                                            <p style="color:#000;"><b>Note :</b> <i><span class="tx-danger">*</span> Fields are Mandatory</i></p>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Proponent: <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="proponent" name="proponent" value="{{ $item->Proponent}}" placeholder="Enter Proponent" required="" type="text">
                                            <div class="tx-danger" id="proponent-form" style="padding-top:10px;"></div>
                                            </div>
                                            
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Map Link: please click on the <a href="https://media.star-telegram.com/static/labs/GoogleMapLink2/index.html" target="_blank">Link</a><span class="tx-danger">*</span> 

<div class="tooltip" style="color:#ee335e;font-weight:bold;margin-left:10px;font-size:16px;"><i class="fa fa-info-circle" aria-hidden="true"></i>
  <span class="tooltiptext">Please click on the link and to enter your address it will provide you map link.</span>
</div>
                                                </label> 
                                                <input class="form-control" id="onmap" value="{{ $item->Onmap}}" name="onmap"  onchange="validation()" placeholder="Enter Map Link" required="" type="text">
                                                <div class="tx-danger" id="onmap-form" style="padding-top:10px;"></div>
                                            </div>
                                           <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                 <label class="form-control-label">Address: <span class="tx-danger">*</span></label> 
                                                <textarea class="form-control" id="address" name="address" placeholder="Address" required="" rows="3">{{ $item->Address}}</textarea>
                                            <div class="tx-danger" id="address-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                            <label class="form-control-label">Property Status: <span class="tx-danger">*</span></label>
                                           
<select name="propertystatus" class="form-control select2" id="propertystatus">
   
    @if($item->propertyStatus=="Ready to Move"){
     <option value="Ready to Move" selected="selected">Ready to Move</option>
      <option value="Under Construction">Under Construction</option>
    }@elseif($item->propertyStatus=="Under Construction"){
        <option value="Under Construction" selected="selected">Under Construction</option>
        <option value="Ready to Move">Ready to Move</option>
    }
 @endif
</select>
<div class="tx-danger" id="propertystatus-form" style="padding-top:10px;"></div>
                                        </div><!-- col-4 -->
                                        <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Year of Build: <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="buildyear" name="buildyear" value="{{ $item->buildYear}}" placeholder="Enter Year Of Build" required="" type="text">
                                            <div class="tx-danger" id="buildyear-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">RERA Number: <span class="tx-danger">*</span></label> 
                                                <input class="form-control" id="reranumber" name="reranumber" value="{{ $item->reraNumber}}" placeholder="Enter RERA Number" required="" type="text">
                                            <div class="tx-danger" id="reranumber-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Property Manager: <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="propertymanager" name="propertymanager" value="{{ $item->propertyManager}}" placeholder="Enter Property Manager" required="" type="text">
                                            <div class="tx-danger" id="propertymanager-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Custodian: <span class="tx-danger">*</span></label>
                                                <input class="form-control" value="{{ $item->Custodian}}" id="custodian" name="custodian" placeholder="Enter Custodian Person Name" required="" type="text">
                                            <div class="tx-danger" id="custodian-form" style="padding-top:10px;"></div>
                                            </div>
                                        </section>
                                        <h3>Property Features</h3>
                                        <section>
                                        <p>Please select the property amenities and upload technical specification.</p>
                                        <p style="color:#000;"><b>Note :</b> <i><span class="tx-danger">*</span> Fields are Mandatory</i></p>
                                        
                                        <h3 style="font-size:17px;"><label class="form-control-label">Property Amenities: <span class="tx-danger">*</span></label> </h3>
                                        <div class="tx-danger" id="amenities-form" style="padding-top:10px;padding-bottom:10px;"></div>
                                        
                                        <div class="col-lg-12" style="float: left;display: inline-block;margin-top:10px;">  


                                            <?php 
$check='';

                                            for($i=0;$i<count($amenity);$i++){ 


?>

<div class="col-lg-4" style="float: left;display: inline-block;margin-top:10px;">                                           
                                                <label class="ckbox"><input type="checkbox" name="amenities[]"  value="<?php echo $amenity[$i]['amenity_name'] ?>"><span><?php echo $amenity[$i]['amenity_name'] ?></span></label>
                                            </div>
<?php
    
                                             ?>
                                            
                                           <?php } ?>
                         
    <script language="javascript" type="text/javascript">
    var a = "<?php echo $item->propertyAmenity; ?>";

var a_split=a.split("#");

$('input[name="amenities[]"]').each(function() {
        var chkbox = $(this).val();
               for (var i=0; i < a_split.length; i++) 
               {
                if(a_split[i]==chkbox)
                {
                    $(this).prop('checked', true);
                }
              }
            });
    </script>
                                            
                                            </div>
                                      <div class="col-lg-12" style="float: left;display: inline-block;margin-top:20px;">
                                      <h3 style="font-size:17px;"><label class="form-control-label">Property Technical Specification: <span class="tx-danger">*</span></label> <span style="font-size:13px;">(Tech Specs Sample Format <a href="<?php echo url('/') ?>/public/issuer/assets/pdf/Tech_Specs_Template.xlsx">Download</a>)</span></h3>
                                        <p style="color:#000;"><b>Note :</b> <i>Only PDF Format are allowed</i></p>
                                        
                                        </div>
                                        <div class="col-sm-12 col-md-4" style="float: left;display: inline-block;">
                                                    @if($item->propertyTechSpecs!='')
                                <a href="<?php echo url('/')."/" ?>{{ $item->propertyTechSpecs }}" target="_blank" style="color:#ee335e">Technical Specification Existing File</a>
                            @endif
                                       <input type="hidden" id="techspecsold" value="{{ $item->propertyTechSpecs }}" name="techspecsold">
                                            <input type="file" class="" id="techspecs" name="techspecs" accept="application/pdf"/>
                                            <div class="tx-danger" id="techspecs-form" style="padding-top:10px;"></div>
                                        </div>
                                        
                                        

                                        
                                        
                                        
                                        
                                        </section>
                                        
                                        
                                        <h3>Financials Details</h3>
                                        <section>
                                            <p>Please add the financial details.</p>
                                            <p style="color:#000;"><b>Note :</b> <i><span class="tx-danger">*</span> Fields are Mandatory</i></p>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Cost of Premise: <span class="tx-danger">*</span></label> 
                                                <input class="form-control" value="{{ $item->costOfPremise }}" id="costofpremise" name="costofpremise" placeholder="Enter Cost of Premise" required="" type="text">
                                            <div class="tx-danger" id="costofpremise-form" style="padding-top:10px;"></div>
                                            </div>
                                            
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Stamp Duty Registration: <span class="tx-danger">*</span></label> 
                                                <input class="form-control" id="stampdutyregistration" value="{{ $item->stampDutyReg }}" name="stampdutyregistration" placeholder="Enter Stamp Duty Registration" required="" type="text">
                                        <div class="tx-danger" id="stampdutyregistration-form" style="padding-top:10px;"></div>
                                        </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Acquisition Fee: <span class="tx-danger">*</span></label> 
                                                
                                                <div class="input-group">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">&#8377;</span>
                                                </div><input aria-label="Amount (to the nearest dollar)" value="{{ $item->acquisitionFee }}" class="form-control" id="acquisitionfee" name="acquisitionfee" type="number" onkeypress="inpNum(event)">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">.00</span>
                                                </div>
                                            </div><!-- input-group -->
                                            <div class="tx-danger" id="acquisitionfee-form" style="padding-top:10px;"></div>
                                            </div>
                                            
                                        <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Other Charges: <span class="tx-danger">*</span></label> 
                                            
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">&#8377;</span>
                                                </div><input aria-label="Amount (to the nearest dollar)" value="{{ $item->otherCharges }}" class="form-control" id="othercharges" name="othercharges" type="number" onkeypress="inpNum(event)">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">.00</span>
                                                </div>
                                            </div><!-- input-group -->
                                        <div class="tx-danger" id="othercharges-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Legal Fees: <span class="tx-danger">*</span></label> 
                                            
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">&#8377;</span>
                                                </div><input aria-label="Amount (to the nearest dollar)" value="{{ $item->legalFees }}" class="form-control" name="legalfees" id="legalfees" type="number" onkeypress="inpNum(event)">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">.00</span>
                                                </div>
                                            </div><!-- input-group -->
                                        <div class="tx-danger" id="legalfees-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Maintenance Reserve: <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="maintenancereserve" name="maintenancereserve" value="{{ $item->maintenanceReserve }}" placeholder="Enter Maintenance Reserve" required="" type="text">
                                            <div class="tx-danger" id="maintenancereserve-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Admin Expenses: <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="adminexpenses" name="adminexpenses" value="{{ $item->adminExpenses }}" placeholder="Enter Admin Expenses" required="" type="text">
                                                <div class="tx-danger" id="adminexpenses-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Monthly OPEX: <span class="tx-danger">*</span></label> 
                                                <input class="form-control" value="{{ $item->monthlyOpex }}" id="monthlyopex" name="monthlyopex" placeholder="Enter Monthly OPEX" required="" type="text">
                                                <div class="tx-danger" id="monthlyopex-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Monthly Property Tax: <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="monthlypropertytax" name="monthlypropertytax" placeholder="Enter Monthly Property Tax" value="{{ $item->monthlyPropertyTax }}" required="" type="text">
                                                <div class="tx-danger" id="monthlypropertytax-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Other Monthly Regulatory Charges: <span class="tx-danger">*</span></label>
                                                <input class="form-control" id="monthlyregulatorycharges" name="monthlyregulatorycharges" value="{{ $item->regulatoryCharges }}" placeholder="Enter Other Monthly Regulatory Charges" required="" type="text">
                                                <div class="tx-danger" id="monthlyregulatorycharges-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Total Acquisition cost: <span class="tx-danger">*</span></label> 
                                            
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">&#8377;</span>
                                                </div><input aria-label="Amount (to the nearest dollar)" value="{{ $item->totalAcquisitionCost }}" id="totalacquisitioncost" name="totalacquisitioncost" class="form-control" type="number" onkeypress="inpNum(event)">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">.00</span>
                                                </div>
                                            </div><!-- input-group -->
                                        <div class="tx-danger" id="totalacquisitioncost-form" style="padding-top:10px;"></div>
                                            </div>
                                        </section>
                                        
                                        <h3>Lease Details</h3>
                                        <section>
                                            <p>Please add the Lease details.</p>
                                            <p style="color:#000;"><b>Note :</b> <i><span class="tx-danger">*</span> Fields are Mandatory</i></p>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Carpet Area (sq. ft): <span class="tx-danger">*</span></label> 
                                                <input class="form-control" value="{{ $item->carpetArea }}" id="carpetarea" name="carpetarea" placeholder="Enter Carpet Area" required="" type="text">
                                            <div class="tx-danger" id="carpetarea-form" style="padding-top:10px;"></div>
                                            </div>
                                            
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Built Up Area (sq. ft): <span class="tx-danger">*</span></label> 
                                                <input class="form-control" id="builtuparea" name="builtuparea" value="{{ $item->builtUpArea }}" placeholder="Enter Built Up Area" required="" type="text">
                                                <div class="tx-danger" id="builtuparea-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Monthly Rent: <span class="tx-danger">*</span></label> 
                                                
                                                <div class="input-group">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">&#8377;</span>
                                                </div><input aria-label="Amount (to the nearest dollar)" id="monthlyrent" value="{{ $item->monthlyRent }}" name="monthlyrent" class="form-control" type="number" onkeypress="inpNum(event)">
                                                <div class="input-group-text">
                                                    <span class="input-group-text">.00</span>
                                                </div>
                                            </div><!-- input-group -->
                                            <div class="tx-danger" id="monthlyrent-form" style="padding-top:10px;"></div>
                                            </div>
                                            
                                    
                                        
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Leased Period (Months): <span class="tx-danger">*</span></label>
                                                <input class="form-control" value="{{ $item->leasedPeriod }}" id="leasedperiod" name="leasedperiod" placeholder="Enter Leased Period" required="" type="text">
                                            <div class="tx-danger" id="leasedperiod-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Security Deposit: <span class="tx-danger">*</span></label> 
                                                <input class="form-control" value="{{ $item->securityDeposit }}" id="securitydeposit" name="securitydeposit" placeholder="Enter Security Deposit" required="" type="text">
                                            <div class="tx-danger" id="securitydeposit-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Escalation: <span class="tx-danger">*</span></label> 
                                                <input class="form-control" value="{{ $item->escalation }}" id="escalation" name="escalation" placeholder="Enter Escalation" required="" type="text">
                                            <div class="tx-danger" id="escalation-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Tenants: <span class="tx-danger">*</span></label>
                                                <input class="form-control" value="{{ $item->tenants }}" id="tenants" name="tenants" placeholder="Enter Tenants" required="" type="text">
                                            <div class="tx-danger" id="tenants-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Fitted out : <span class="tx-danger">*</span></label> 
                                                <input class="form-control" value="{{ $item->fittedOut }}" id="fittedout" name="fittedout" placeholder="Enter Fitted out" required="" type="text">
                                            <div class="tx-danger" id="fittedout-form" style="padding-top:10px;"></div>
                                            </div>

                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Monthly Maintenance : <span class="tx-danger">*</span></label>
                                                
                                                <input class="form-control" id="monthlymaintenance" name="monthlymaintenance" 
                                                value="{{ $item->monthlyMaintenance }}" placeholder="Enter Monthly Maintenance" required="" type="text">
                                                <div class="tx-danger" id="monthlymaintenance-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Lease Chart : <span class="tx-danger">*</span></label> 
                                                <input class="form-control" value="{{ $item->leaseChart }}" id="leasechart" name="leasechart" placeholder="Enter Lease Chart" required="" type="text">
                                            <div class="tx-danger" id="leasechart-form" style="padding-top:10px;"></div>
                                            </div>
                                            
                                        </section>
                                        <h3>Return Analysis</h3>
                                        <section>
                                            <p>Please add the Lease details.</p>
                                            <p style="color:#000;"><b>Note :</b> <i><span class="tx-danger">*</span> Fields are Mandatory</i></p>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Gross Yield (%): <span class="tx-danger">*</span></label>
                                                <input class="form-control" value="{{ $item->grossYield }}" id="grossyield" name="grossyield" placeholder="Enter Gross Yield (%)" required="" type="text">
                                            <div class="tx-danger" id="grossyield-form" style="padding-top:10px;"></div>
                                            </div>
                                            
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Expected IRR (%): <span class="tx-danger">*</span></label>
                                                <input class="form-control" value="{{ $item->expectedIr }}" id="expectedirr" name="expectedirr" placeholder="Enter Expected IRR (%)" required="" type="text">
                                            <div class="tx-danger" id="expectedirr-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-0" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Token Lockin Period (months): <span class="tx-danger">*</span></label>
                                                <input class="form-control" value="{{ $item->tokenLockinPeriod }}" id="tokenlockinperiod" name="tokenlockinperiod" placeholder="Enter Token Lockin Period" required="" type="text">
                                            <div class="tx-danger" id="tokenlockinperiod-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Aurum Facilation fees (%): <span class="tx-danger">*</span></label>
                                                <input class="form-control" value="{{ $item->aurumFacilationFees }}" id="aurumfacilationfees" name="aurumfacilationfees" placeholder="Enter Aurum Facilation fees" required="" type="text">
                                            <div class="tx-danger" id="aurumfacilationfees-form" style="padding-top:10px;"></div>
                                            </div>
                                            <div class="col-md-5 col-lg-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                                <label class="form-control-label">Performance Fees: <span class="tx-danger">*</span></label> 
                                                <input class="form-control" value="{{ $item->performanceFees }}" id="performancefees" name="performancefees" placeholder="Enter Performance Fees" required="" type="text">
                                            <div class="tx-danger" id="performancefees-form" style="padding-top:10px;"></div>
                                            </div>
                                        
                                            
                                        </section>
                                        
                                        <h3>Property Reports</h3>
                                        <section>
                                        <p>Please upload the reports and documents.</p>
                                        <p style="color:#000;"><b>Note :</b> <i><span class="tx-danger">*</span> Fields are Mandatory</i></p>
                                    <p style="color:#000;"><b>Note :</b> <i>Only PDF Format are allowed</i></p>
                                        <div class="col-sm-6 col-md-4" style="float: left;display: inline-block;">
                                        
                                        <label class="form-control-label">Investment Deck : <span class="tx-danger">*</span></label>
<br>
   @if($item->propertyInvestmentDeck!='')
                                <a href="<?php echo url('/')."/" ?>{{ $item->propertyInvestmentDeck }}" target="_blank" style="color:#ee335e;margin-bottom: 90px;padding: 10px;border: #ebebeb 1px solid;">Investment Deck OLD File</a><br>
                            @endif
                                       <input type="hidden" id="investmentdeckold" value="{{ $item->propertyInvestmentDeck }}" name="investmentdeckold">


                                            <input type="file" class="" id="investmentdeck" name="investmentdeck" accept="application/pdf" style="margin-top:20px;"/>
                                            <div class="tx-danger" id="investmentdeck-form" style="padding-top:10px;"></div>
                                        </div>
                                        <div class="col-sm-6 col-md-4 " style="float: left;display: inline-block;">
                                        <label class="form-control-label">Title Report : <span class="tx-danger">*</span></label>
                                        <br>
   @if($item->propertytitleReport!='')
                                <a href="<?php echo url('/')."/" ?>{{ $item->propertytitleReport }}" target="_blank" style="color:#ee335e;margin-bottom: 90px;padding: 10px;border: #ebebeb 1px solid;">Property Title OLD File</a><br>
                            @endif
                                       <input type="hidden" id="titlereportold" value="{{ $item->propertytitleReport }}" name="titlereportold">
                                            <input type="file" class="" id="titlereport" name="titlereport"  accept="application/pdf" style="margin-top:20px;"/>
                                            <div class="tx-danger" id="titlereport-form" style="padding-top:10px;"></div>
                                        </div>
                                        <div class="col-sm-6 col-md-4" style="float: left;display: inline-block;">
                                        <label class="form-control-label">Property Due Diligence : <span class="tx-danger">*</span></label>

                                            <br>
   @if($item->propertyDueDiligence!='')
                                <a href="<?php echo url('/')."/" ?>{{ $item->propertyDueDiligence }}" target="_blank" style="color:#ee335e;margin-bottom: 90px;padding: 10px;border: #ebebeb 1px solid;">Property Diligence OLD File</a><br>
                            @endif
                                       <input type="hidden" id="propertyduediligenceold" value="{{ $item->propertyDueDiligence }}" name="propertyduediligenceold">
                                            <input type="file" class="" id="propertyduediligence" name="propertyduediligence" style="margin-top:20px;" accept="application/pdf"/>
                                            <div class="tx-danger" id="propertyduediligence-form" style="padding-top:10px;"></div>
                                        </div>
                                        <div class="col-sm-6 col-md-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                        <label class="form-control-label">Micro market Report : <span class="tx-danger">*</span></label><br>
                @if($item->propertyMicroMarket!='')                         
                                <a href="<?php echo url('/')."/" ?>{{ $item->propertyMicroMarket }}" target="_blank" style="color:#ee335e;margin-bottom: 90px;padding: 10px;border: #ebebeb 1px solid;">Property Diligence OLD File</a><br>
                            @endif
                                       <input type="hidden" id="micromarketreportold" value="{{ $item->propertyMicroMarket }}" name="micromarketreportold">
                                            <input type="file" class="" id="micromarketreport" name="micromarketreport" style="margin-top:20px;" accept="application/pdf"/>
                                            <div class="tx-danger" id="micromarketreport-form" style="padding-top:10px;"></div>
                                        </div>
                                        <div class="col-sm-6 col-md-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                        <label class="form-control-label">Property Management Agreement : <span class="tx-danger">*</span></label>
<br>
       @if($item->propertyMgmtAgrmt!='')                         
                                <a href="<?php echo url('/')."/" ?>{{ $item->propertyMgmtAgrmt }}" target="_blank" style="color:#ee335e;margin-bottom: 90px;padding: 10px;border: #ebebeb 1px solid;">Property Management Agreement OLD File</a><br>
                            @endif
                                       <input type="hidden" id="propertymanagementagreementold" value="{{ $item->propertyMgmtAgrmt }}" name="propertymanagementagreementold">

                                            <input type="file" class="" id="propertymanagementagreement" name="propertymanagementagreement" style="margin-top:20px;" accept="application/pdf"/>
                                            <div class="tx-danger" id="propertymanagementagreement-form" style="padding-top:10px;"></div>
                                        </div>
                                        <div class="col-sm-6 col-md-4 mg-t-20 mg-md-t-40" style="float: left;display: inline-block;">
                                        <label class="form-control-label">Valuation Report : <span class="tx-danger">*</span></label><br>
                            @if($item->propertyValReport!='')                         
                                <a href="<?php echo url('/')."/" ?>{{ $item->propertyValReport }}" target="_blank" style="color:#ee335e;margin-bottom: 90px;padding: 10px;border: #ebebeb 1px solid;">Property Valuation Report OLD File</a><br>
                            @endif
                                       <input type="hidden" id="valuationreportold" value="{{ $item->propertyValReport }}" name="valuationreportold">
                                            <input type="file" class="" id="valuationreport" name="valuationreport" accept="application/pdf" style="margin-top:20px;"/>
                                            <div class="tx-danger" id="valuationreport-form" style="padding-top:10px;"></div>
                                        </div>
                                        
                                        

                                        
                                        
                                        
                                        
                                        </section>
                                        
                                        <h3>Property Gallery</h3>
                                        <section>
                                        <p>Please upload the property images.</p>
<div class="row row-sm">
                                             <script>
      $('.multi-field-wrapper-new').each(function() {
    var $wrapper = $('.multi-fields-new', this);
    $(".add-field", $(this)).click(function(e) {
        $('.multi-field-new1:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
    });
    $('.multi-field-new .remove-field', $wrapper).click(function() {
        if ($('.multi-field-new', $wrapper).length > 1)
            $(this).parent('.multi-field-new').remove();
    });
});
  </script>
                                            
                                           
    <div class="multi-field-wrapper-new col-md-12" style="width:100%;">
      <div class="multi-fields-new">
         <?php 
$gallery=explode("#",$item->galleryImage);
for($j=0;$j<count($gallery);$j++){

                                        ?>
        <div class="multi-field-new" style="width:100%;float:left;display:block">
                                        
                                            
                       <img style="height: 90px; margin-bottom: 15px;" name="gallery[]" accept="image/gif, image/jpg, image/jpeg, image/png" src="<?php echo url('/')."/" ?>{{ $gallery[$j] }}">
           
        <button type="button" class="remove-field btn btn-primary btn-block" style="margin-top: 10px;float: right;width: 129px;"><i class="fas fa-minus"></i>&nbsp;&nbsp;Remove</button>
        </div>

    <?php } ?>
     <div class="multi-field-new1" style="width:100%;float:left;display:block">
                                             <input type="file" class="" id="propgallery" name="gallery[]" accept="image/gif, image/jpg, image/jpeg, image/png"/>
                                            
                       <input type="hidden" value="{{ $item->galleryImage }}" name="galleryimageold">
                        
           
        <button type="button" class="remove-field btn btn-primary btn-block" style="margin-top: 10px;float: right;width: 129px;"><i class="fas fa-minus"></i>&nbsp;&nbsp;Remove</button>
        </div>
      </div>
      <button type="button" class="add-field btn btn-danger btn-block" style="margin-top: 49px;float: right;width: 129px;"><i class="fas fa-plus"></i>&nbsp;&nbsp;Add More</button>
  </div>
 
                                            
                                         
                                            </div>
                                    
                        
<!-- copy of input fields group -->



<!-- jQuery library -->

    
        
                            
                                        
                                        </section>
                                        
                                        
                                   
                                </div></form>   @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- /row -->

                
                    <!-- row closed -->


            </div>
   <script src="<?php echo url('/'); ?>/public/issuer/assets/plugins/jquery/jquery.min.js"></script>
        <!-- Internal Select2.min js -->
        <script src="<?php echo url('/'); ?>/public/issuer/assets/plugins/select2/js/select2.min.js"></script>        
        <script src="<?php echo url('/'); ?>/public/issuer/assets/js/index.js"></script>       
        <script src="<?php echo url('/'); ?>/public/issuer/assets/js/form-elements.js"></script>

<script>
    function inpNum(e) {
  e = e || window.event;
  var charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
  var charStr = String.fromCharCode(charCode);
  if (!charStr.match(/^[0-9]+$/))
    e.preventDefault();
}

</script>
<script>


function validation() {
    
url=document.getElementById("onmap").value;
    Reg = /^https?\:\/\/(www\.)?google\.[a-z]+\/maps\b/;
  var match = url.match(Reg);
 
  if (match){
    //alert("<span>correct</span>");
    }else{
    alert("Please must enter the valid URL.Click on the link you will get Map Link");
    document.getElementById("onmap").value='';
    }
}



</script>
<script>
$(document).ready(function(){
    //group add limit
    var maxGroup = 20;
    
    //add more fields group
    $(".addMore").click(function(){
        if($('body').find('.fieldGroup').length < maxGroup){
            var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
            $('body').find('.fieldGroup:last').after(fieldHTML);
        }else{
            alert('Maximum '+maxGroup+' groups are allowed.');
        }
    });
    
    //remove fields group
    $("body").on("click",".remove",function(){ 
        $(this).parents(".fieldGroup").remove();
    });
});


</script>   
@endsection

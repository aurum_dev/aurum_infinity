@extends('admin.layout.base')

@section('title', 'User Details')

@section('content')
<style>
    table.table-bordered.dataTable th:last-child, table.table-bordered.dataTable th:last-child, table.table-bordered.dataTable td:last-child, table.table-bordered.dataTable td:last-child{
        vertical-align: middle !important;
    }

</style>
<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
            <a href="{{ url('/admin/wallet') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> @lang('admin.back')</a>

			  
<table id="example" class="datatable-full table table-striped table-bordered custom-table-style" style="width: 100% !important">
                <thead>
                    <tr>
                        <th colspan="2">

 <div class="col-md-6" style="float: left;
    display: inline-block;">
                                     
    <h5 style="    margin-top: 1em;
    margin-bottom: 1em">Token Transfer Transaction Details</h5>  </div>

 <div class="col-md-6" style="float: left;
    display: inline-block;">
                                     
    @if($contracts[0]['txnhash'] == '' && $contracts[0]['razorpaytxnhash']!='') 
      <a class="btn btn-info btn-block walletapprove" data-id="{{$contracts[0]['wallet_id']}}" style="width:40px;padding:9px 10px;float:right;display:block;margin-top: 8px;margin-left:5px;" title="Token Approve" href="javascript:void(0);"><i class="fas fa-thumbs-up"></i></a><span style="float:right;padding-top:19px;padding-right:10px;"> Click on button to Approve the Token</span>
@endif   
</div>
    </th>
                       
                        
                     
                    </tr>

                </thead>
                <tbody>
                    <tr>
                        <td><label class="col-xs-12 col-form-label">Wallet ID</label></td>
                        <td>{{$contracts[0]['wallet_id']}}</td>
                    </tr>
                	<tr>
                		<td><label class="col-xs-12 col-form-label">Token Name</label></td>
                		<td>{{$property[0]['tokenName']}}</td>
                	</tr>
                	<tr>
                		<td><label class="col-xs-12 col-form-label">Token Symbol</label></td>
                		<td>{{$property[0]['tokenSymbol']}}</td>
                	</tr>
                	<tr>
                		<td><label class="col-xs-12 col-form-label">Property Name</label></td>
                		<td>{{$property[0]['propertyName']}}</td>
                	</tr>
                    <tr>
                        <td><label class="col-xs-12 col-form-label">Amount (1 Token Value)</label></td>
                        <td><i class="fas fa-rupee-sign"></i>&nbsp;{{$property[0]['tokenValue']}}</td>
                    </tr>
                    <tr>
                        <td><label class="col-xs-12 col-form-label">Property Contract Address</label></td>
    <td><a target="_blank" href="https://mumbai.polygonscan.com/address/{{$property[0]['contract_address']}}">{{$property[0]['contract_address']}}</a></td>
                    </tr>
                	<tr>
                		<td><label class="col-xs-12 col-form-label">Token Icon</label></td>
                		<td><img src="../../{{$property[0]['tokenLogo']}}" style="width:100px;"></td>
                	</tr>
                	<tr>
                		<td><label class="col-xs-12 col-form-label">Issuer Details</label></td>
                		<td>
                			<table>
                				<tr>
                					<td><b>Name</b></td>
                					<td>{{$issuer[0]['name']}}</td>
                				</tr>
                				<tr>
                					<td><b>Email</b></td>
                					<td>{{$issuer[0]['email']}}</td>  
                				</tr>
<tr>
                					<td><b>Ethereum Address</b></td>
                					<td>{{$issuer[0]['eth_address']}}</td>  
                				</tr>

                			</table>

                		</td>
                	</tr>
                	<tr>
                		<td><label class="col-xs-12 col-form-label">Investor Details</label></td>
                		<td>
                			<table>
                				<tr>
                					<td><b>Name</b></td>
                					<td>{{$investor[0]['name']}}</td>
                				</tr>
                				<tr>
                					<td><b>Email</b></td>
                					<td>{{$investor[0]['email']}}</td>  
                				</tr>
<tr>
                					<td><b>Ethereum Address</b></td>
                					<td>{{$investor[0]['eth_address']}}</td>  
                				</tr>
                			</table>

                		</td>
                	</tr>
                	<tr>
                		<td><label class="col-xs-12 col-form-label">Total Tokens<span style="color:#e51212;"> (Transfer From Issuer to Investor)</span></label></td>
                		<td>{{$contracts[0]['total_tokens']}}</td>
                	</tr>
					<tr>
                		<td><label class="col-xs-12 col-form-label">Pay By Investor</label></td>
                		<td><i class="fas fa-rupee-sign"></i>&nbsp;{{$contracts[0]['paybyinvestor']}}</td>
                	</tr>
					<tr>
                		<td><label class="col-xs-12 col-form-label">Pay To Issuer</label></td>
                		<td><i class="fas fa-rupee-sign"></i>&nbsp;{{$contracts[0]['paytoissuer']}}</td>
                	</tr>
                	<tr>
                		<td><label class="col-xs-12 col-form-label">Admin Commission</label></td>
                		<td><i class="fas fa-rupee-sign"></i>&nbsp;{{$contracts[0]['admincommission']}}</td>
                	</tr>
                	<tr>
                		<td><label class="col-xs-12 col-form-label">Token Transaction Hash</label></td>
                		<td><a target="_blank" href="https://mumbai.polygonscan.com/tx/{{$contracts[0]['txnhash']}}">{{$contracts[0]['txnhash']}}</a></td>
                	</tr>
                    <tr>
                        <td><label class="col-xs-12 col-form-label">Transaction ID</label></td>
                        <td>{{$contracts[0]['razorpayhash']}}</td>
                    </tr>
                     <tr>
                        <td><label class="col-xs-12 col-form-label">Transaction Hash</label></td>
                        <td style="width:300px; word-wrap:break-word; display:inline-block;">{{$contracts[0]['razorpaytxnhash']}}</td>
                    </tr>
                     <tr>
                        <td><label class="col-xs-12 col-form-label">Token Purchased Date</label></td>
                        <td>
<?php 
                         echo date("d, M Y h:i:s a",strtotime($contracts[0]['created_at'])); ?></td>
                    </tr>
                     <tr>
                        <td><label class="col-xs-12 col-form-label">Token Approved By </label></td>
                        <td>{{$contracts[0]['approvedby']}}
</td>
                    </tr>
                    <tr>
                        <td><label class="col-xs-12 col-form-label">Token Approved Date</label></td>
                        <td>
@if($contracts[0]['status'] == 1) 
{{ date("d, M Y h:i:s a",strtotime($contracts[0]['updated_at']))}}
 @endif

</td>
                    </tr>
                	<tr>
                		<td><label class="col-xs-12 col-form-label">Status</label></td>
                		<td>
                			 @if($contracts[0]['status'] == 1) 
                                   <a href="javascript:void(0);" style="cursor:default !important;" class="btn btn-success">Approved</a>
                                @else  
                                    <a href="javascript:void(0);" style="cursor:default !important;" class="btn btn-danger">Pending</a>
                                @endif

                		</td>
                	</tr>
                </tbody>
            </table>
            

			
			
						
		</div>
    </div>
</div>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
     
    
      $(".walletapprove").on("click", function () {
            var walletid  = $(this).attr("data-id");                  
            var finalid=walletid;            
          //  var finalid=walletid+'#'+tokentransid+'#'+issuerid+'#'+investorid+'#'+propertyid+'#'+payvalue;
           var url = "{{ url('/admin/investstoreadmin') }}/" + finalid;

             if (walletid != '') {
                swal({
                         title     : "Are you sure?",
                         html      :"<h4>Loading...</h4>",
                         showSpinner: true,
                         text      : "You want to transfer the token from Issuer to investor",
                         icon      : "info",
                         buttons   : true,
                         dangerMode: false,
                         buttons   : ["No", "Yes"],
                     })
                    .then((willDelete) => {
                        if (willDelete) {
                            swal({
                title: 'Please Wait..!',
                text: 'Is working..',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                onOpen: () => {
                    swal.showLoading()
                }
            })
                            $.ajax({
                                       type   : "GET",
                                       url    : url,                                       
                                       success: function (data) {
                                           if (data.status == 'success') {
                                               swal('Token Transferred Successfully');
                                               location.reload();
                                           }
                                           if (data.status == 'failed') {
                                               swal("We couldn't connect to the server!");
                                             //  location.reload();
                                           }
                                       }
                                   });
                        }
                    });
            }
        });
       /*$(document).ready(function(){
       $(".payby, .no-of-token").on("click", function () {
  
              alert('hello');
 
                /*$.ajax({
                    url    : "{{url('/gettokeninvestvalue')}}",
                    type   : "POST",
                    data   : {'token_id': token_id, 'no_of_token': no_of_token, 'payment_id': payment_id, '_token': '{{ csrf_token() }}'},
                    success: function (result) {
                        if (result.status == 1) {
                            $('#amount').val(result.token_equ_value);
                            $('#bonus_token').val(result.bonus_token);
                            $('#total_token').val(result.total_token);
                        } else {
                            $('#amount').val(0);
                        }
                    }
                });
});
       
        });*/
    </script>
@endsection

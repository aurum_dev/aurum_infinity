@extends('issuer.layout.base')
@section('content')
<style>
    table.table-bordered.dataTable th:last-child, table.table-bordered.dataTable th:last-child, table.table-bordered.dataTable td:last-child, table.table-bordered.dataTable td:last-child{
        vertical-align: middle !important;
    }

</style>
<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
            <a href="{{ url('admin/dividend')}}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> @lang('admin.back')</a>

			  
<table id="example" class="datatable-full table table-striped table-bordered custom-table-style" style="width: 100% !important">
                <thead>
                    <tr>
                        <th colspan="2">

 <div class="col-md-6" style="float: left;
    display: inline-block;">
                                     
    <h5 style="    margin-top: 1em;
    margin-bottom: 1em">Dividend Details</h5>  </div>

 
    </th>
                       
                        
                     
                    </tr>

                </thead>
                <tbody>
                    <tr>
                        <td><label class="col-xs-12 col-form-label">Dividend ID</label></td>
                        <td>{{$dividend->dividend_id}}</td>
                    </tr>
                	<tr>
                		<td><label class="col-xs-12 col-form-label">Property Name</label></td>
                		<td>{{$property->propertyName}}</td>
                	</tr>
                	<tr>
                        <td><label class="col-xs-12 col-form-label">Token Value (1 Token Value)</label></td>
                        <td>{{$property->tokenValue}}</td>
                    </tr>
                   
                   
                
                	<tr>
                		<td><label class="col-xs-12 col-form-label">Issuer Details</label></td>
                		<td>
                			<table>
                				<tr>
                					<td><b>Name</b></td>
                					<td>{{$issuer->name}}</td>
                				</tr>
                				<tr>
                					<td><b>Email</b></td>
                					<td>{{$issuer->email}}</td>  
                				</tr>
<tr>
                					<td><b>Crypto Address</b></td>
                					<td>{{$issuer->eth_address}}</td>  
                				</tr>

                			</table>

                		</td>
                	</tr>
                	<tr>
                        <td><label class="col-xs-12 col-form-label">Title</label></td>
                        <td>{{$dividend->title}}</td>
                    </tr>
                	<tr>
                        <td><label class="col-xs-12 col-form-label">Description</label></td>
                        <td>{{$dividend->description}}</td>
                    </tr>
                    <tr>
                        <td><label class="col-xs-12 col-form-label">Dividend</label></td>
                        <td><i class="fas fa-rupee-sign"></i> {{$dividend->dividend}}</td>
                    </tr>
                    <tr>
                        <td><label class="col-xs-12 col-form-label">Bonus Tokens</label></td>
                        <td>{{$dividend->dividend/$property->tokenValue}}</td>
                    </tr>
                      <tr>
                        <td><label class="col-xs-12 col-form-label">Documents</label></td>
                        <td>
                            Document 1 :&nbsp;<a target="_blank" href="<?php echo url('/')."/".$dividend->doc1; ?>"><i class="fas fa-file-pdf" style="font-size:25px;color:#F40F02"></i></a><br>
                            Document 2 :&nbsp;<a target="_blank" href="<?php echo url('/')."/".$dividend->doc2; ?>"><i class="fas fa-file-pdf" style="font-size:25px;color:#F40F02"></i></a><br>
                            Document 3 :&nbsp;<a target="_blank" href="<?php echo url('/')."/".$dividend->doc3; ?>"><i class="fas fa-file-pdf" style="font-size:25px;color:#F40F02"></i></a><br>
                            Document 4 :&nbsp;<a target="_blank" href="<?php echo url('/')."/".$dividend->doc4; ?>"><i class="fas fa-file-pdf" style="font-size:25px;color:#F40F02"></i></a><br>
                            Document 5 :&nbsp;<a target="_blank" href="<?php echo url('/')."/".$dividend->doc5; ?>"><i class="fas fa-file-pdf" style="font-size:25px;color:#F40F02"></i></a><br>
                        </td>
                    </tr>
                	
                </tbody>
            </table>
            

			
			
						
		</div>
    </div>
</div>

@endsection

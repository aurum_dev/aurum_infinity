@extends('admin.layout.base')

@section('title', 'Dividend ')

@section('content')
<style>
    #example_length{
margin-top: 5px !important;
    float: right !important;
    margin-right: 266px !important;
}

    
</style>
          <!-- breadcrumb -->
                    <div class="breadcrumb-header justify-content-between">
                        <div class="my-auto">
                            <div class="d-flex">
                                <h4 class="content-title mb-0 my-auto"> Dividend List</h4><span class="text-muted mt-1 tx-13 ms-2 mb-0">/ Dividend List</span>
                            </div>
                        </div>
                        <div class="d-flex my-xl-auto right-content">
                            
                            
                        </div>
                    </div>
                    <!-- breadcrumb -->



<div class="row">
<div class="col-xl-12">
                        <div class="card mg-b-20">
                           
                            <div class="card-body">
                                <div class="table-responsive">

                                    <table id="example" class="table key-buttons text-md-nowrap">
                                        <thead>
                                            <tr>
                                                <th class="border-bottom-0" style="text-align:left;width:10px;">S.NO</th>
                                                <th class="border-bottom-0" style="text-align:left;width:30px;">Title</th>       
                                                <th class="border-bottom-0" style="text-align:left;width:30px;">Start Date</th>
                                                <th class="border-bottom-0" style="text-align:left;width:30px;">End Date</th>
                                                <th class="border-bottom-0" style="text-align:left;width:30px;">Dividend Amount</th>
                                                 <th class="border-bottom-0" style="text-align:left;width:10px;">Status</th>
                                                  <th class="border-bottom-0" style="text-align:left;width:30px;">Token Detail</th>
                                                <th class="border-bottom-0" style="text-align:left;width:70px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($TokenDividend as $index => $val)
                                          <tr>
                                              <td>{{$index+1}}</td>
                                              <td>{{$val->title}}</td>
                                              <td>{{date("d, M Y",strtotime($val->startdate))}}</td>
                                              <td>{{date("d, M Y",strtotime($val->enddate))}}</td>
                                             
                                              <td><i class="fas fa-rupee-sign"></i>&nbsp;{{$val->dividend}}</td>
                                               <td>
                                                   

                                @if($val->status == 1) 
                                    <span style="align-items: center;
    border: 1px solid #d6dcec;
    border-radius: 6px;
    display: flex;
    height: 2.5rem;
    justify-content: center;
    margin: 4px;
    text-align: center;
    transform: scale(1);
    transition: .3s ease-in-out;
    width: 2.5rem;"><i class="fa fa-check" aria-hidden="true" style="color:#22c03c;"></i></span> 
                                @else  
                                    <span style="align-items: center;
    border: 1px solid #d6dcec;
    border-radius: 6px;
    display: flex;
    height: 2.5rem;
    justify-content: center;
    margin: 4px;
    text-align: center;
    transform: scale(1);
    transition: .3s ease-in-out;
    width: 2.5rem;"><i class="fa fa-times" aria-hidden="true" style="color:#ff0000;"></i></span> 
                                @endif
                          




                                               </td>
                                                <td>
                                                    
 @if($val->status == 1) 
    <a class="btn btn-secondary" href="{{url('/')}}/admin/dividenttokendetail/{{$val->dividend_id}}">Click Here</a>
    @endif

                                                </td>
                                              <td style="text-align:left;">
                                                @if($val->status == 0) 
                                                 <a class="btn btn-secondary btn-block dividendapprove" data-id="{{$val->dividend_id}}" style="width:40px;padding:9px 10px;float:left;display:block;margin-top:0;" title="Approve" href=""><i class="fas fa-thumbs-up"></i></a>
                                                 @endif
  <a class="btn btn-dark btn-block" style="width:40px;padding:9px 10px;display:block;float:left;margin-top:0;margin-left:5px;margin-left:5px;" title="Dividend Detail" href="{{ url('admin/dividentdetail/'.$val->dividend_id) }}"><i class="fas fa-info"></i></a>  
                                              </td>
                                          </tr>
                                          @endforeach 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
</div>
         <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
 
      $(".dividendapprove").on("click", function () {
            var walletid  = $(this).attr("data-id");                  
                      
      
     
         var url = "{{ url('/admin/investordividendnotification') }}/" + walletid;

            if (walletid != '') {
                swal({
                         title     : "Are you sure?",
                         html      :"<h4>Loading...</h4>",
                         showSpinner: true,
                         text      : "You want to approve the Bonus Dividend Request",
                         icon      : "info",
                         buttons   : true,
                         dangerMode: false,
                         buttons   : ["No", "Yes"],
                     })
                    .then((willDelete) => {
                        if (willDelete) {
                            swal({
                title: 'Please Wait..!',
                text: 'Is working..',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                onOpen: () => {
                    swal.showLoading()
                }
            })
                            $.ajax({
                                       type   : "GET",
                                       url    : url,                                       
                                       success: function (data) {
                                           if (data.status == 'success') {
                                               swal('Dividend Request Approved Successfully');

                                               location.reload();
                                           }
                                           if (data.status == 'failed') {
                                               swal("We couldn't connect to the server!");
                                             //  location.reload();
                                           }
                                       }
                                   });
                        }
                    });
            }
            return false;
        });
    
    </script>
@endsection



<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Title -->
    <title>Admin Login</title>


        <link rel="icon" href="{{ Setting::get('site_icon') }}" type="image/x-icon"/>

        <!--- Icons css --->
        <link href="{{asset('public/issuer/assets/plugins/icons/icons.css')}}" rel="stylesheet">

        <!-- Bootstrap css -->
        <link href="{{asset('public/issuer/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

        <!--- Right-sidemenu css --->
        <link href="{{asset('public/issuer/assets/plugins/sidebar/sidebar.css')}}" rel="stylesheet">

        <!-- P-scroll bar css-->
        <link href="{{asset('public/issuer/assets/plugins/perfect-scrollbar/p-scrollbar.css')}}" rel="stylesheet" />

        <!-- Sidemenu css -->
        <link rel="stylesheet" href="{{asset('public/issuer/assets/css/sidemenu.css')}}">

        <!--- Style css --->
        <link href="{{asset('public/issuer/assets/css/style.css')}}" rel="stylesheet">
        <link href="{{asset('public/issuer/assets/css/boxed.css')}}" rel="stylesheet">
        <link href="{{asset('public/issuer/assets/css/dark-boxed.css')}}" rel="stylesheet">
        
        <!---Skinmodes css-->
        <link href="{{asset('public/issuer/assets/css/skin-modes.css')}}" rel="stylesheet">

        <!--- Dark-mode css --->
        <link href="{{asset('public/issuer/assets/css/style-dark.css')}}" rel="stylesheet">

        <!-- Sidemenu-respoansive-tabs css -->
        <link href="{{asset('public/issuer/assets/plugins/sidemenu-responsive-tabs/sidemenu-responsive-tabs.css')}}" rel="stylesheet">
    <!--- Animations css --->
        <link href="{{asset('public/issuer/assets/css/animate.css')}}" rel="stylesheet">

        <!---Switcher css-->
        <link href="{{asset('public/issuer/assets/switcher/css/switcher.css')}}" rel="stylesheet">
        <link href="{{asset('public/issuer/assets/switcher/demo.css')}}" rel="stylesheet">

  
</head>
<body>

  
   <body class="">

        
        <div class="error-page1 main-body bg-light text-dark">      
        <!-- Loader -->
        <div id="global-loader">
            <img src="../public/issuer/assets/img/loader.svg" class="loader-img" alt="Loader">
        </div>
        <!-- /Loader -->

        <!-- Page -->
        <div class="page">
    
    
    @include('common.notify')
    
    @yield('content')

    </div>
</div>

  <!--- JQuery min js --->
        <script src="{{asset('public/issuer/assets/plugins/jquery/jquery.min.js')}}"></script>

        <!--- Bootstrap Bundle js --->
        <script src="{{asset('public/issuer/assets/plugins/bootstrap/js/popper.min.js')}}"></script>
        <script src="{{asset('public/issuer/assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

        <!--- Ionicons js --->
        <script src="{{asset('public/issuer/assets/plugins/ionicons/ionicons.js')}}"></script>

        <!--- Moment js --->
        <script src="{{asset('public/issuer/assets/plugins/moment/moment.js')}}"></script>

        <!-- P-scroll js -->
        <script src="{{asset('public/issuer/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>

        <!--- Eva-icons js --->
        <script src="{{asset('public/issuer/assets/plugins/eva-icons/eva-icons.min.js')}}"></script>

        <!--- Rating js --->
        <script src="{{asset('public/issuer/assets/plugins/rating/jquery.rating-stars.js')}}"></script>
        <script src="{{asset('public/issuer/assets/plugins/rating/jquery.barrating.js')}}"></script>

        <!--- JQuery sparkline js --->
        <script src="{{asset('public/assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>

        
        
        <!--- Custom js --->
        <script src="{{asset('public/issuer/assets/js/custom.js')}}"></script>

        <!-- Switcher js -->
        <script src="{{asset('public/issuer/assets/switcher/js/switcher.js')}}"></script>
    

    
        <!-- Vendor JS -->
       



           @if(Setting::get('demo_mode', 0) == 1)
            <!-- Start of LiveChat (www.livechatinc.com) code -->
            <script type="text/javascript">
                window.__lc = window.__lc || {};
                window.__lc.license = 8256261;
                (function() {
                    var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
                    lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
                })();
            </script>
            <!-- End of LiveChat code -->
        @endif
    </body>
</html>

@section('title', 'Users ')
@extends('admin.layout.base')



@section('content')
<style>
    
#example_length{
margin-top: 5px !important;
    float: right !important;
    margin-right: 250px !important;
}


</style>

<div class="breadcrumb-header justify-content-between">
                    <div class="my-auto">
                        <div class="d-flex">
                            <h4 class="content-title mb-0 my-auto">Users</h4><span
                                class="text-muted mt-1 tx-13 ms-2 mb-0">/ Users List</span>
                        </div>
                    </div>                    
                </div>
<div class="content-area py-1" style="background:#fff;padding-top:40px;">
    <div class="container-fluid">
        <div class="box box-block bg-white">          
            
            <!-- <a href="{{ route('admin.kycmail') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Rejected Kyc Mail Alert</a> -->
            <br><br>
            <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.1.0/css/buttons.dataTables.min.css" type="text/css"  />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" type="text/css" />
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.1.0/css/select.dataTables.min.css" type="text/css" />

<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="https://datatables.net/release-datatables/media/js/jquery.js"></script>
<script type="text/javascript" src="https://datatables.net/release-datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" scr="https://datatables.net/release-datatables/extensions/TableTools/js/dataTables.tableTools.js"></script>

<script src="https://cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.1.0/js/buttons.html5.min.js"></script>

<script src="https://jquery-datatables-column-filter.googlecode.com/svn/trunk/media/js/jquery.dataTables.columnFilter.js" type="text/javascript"></script>
            <table id="example" class="datatable-full table table-striped table-bordered custom-table-style" style="width: 100% !important">
                <thead>
                    <tr>
                        <th style="width:20px;">@lang('admin.id')</th>
                        <th style="width:70px;">Full Name</th>
                        
                        <th style="width:70px;">@lang('admin.email')</th>   
                        <th style="width:50px;">Verify Email/Approved</th>                        
                        <th style="width:40px;">@lang('admin.users.kyc_status')</th>
                        <th style="width:40px;">Bank Details</th>
                        <th style="width:50px;">@lang('admin.type')</th>                  
                        <th style="width:150px;">@lang('admin.action')</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $index => $user)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $user->name }}</td>
                                         
                        <td>{{ $user->email }}</td>
                        <td>
                            <center>
                                @if($user->approved == 1) 
                                    <span style="align-items: center;
    border: 1px solid #d6dcec;
    border-radius: 6px;
    display: flex;
    height: 2.5rem;
    justify-content: center;
    margin: 4px;
    text-align: center;
    transform: scale(1);
    transition: .3s ease-in-out;
    width: 2.5rem;"><i class="fa fa-check" aria-hidden="true" style="color:#22c03c;"></i></span> 
                                @else  
                                    <span style="align-items: center;
    border: 1px solid #d6dcec;
    border-radius: 6px;
    display: flex;
    height: 2.5rem;
    justify-content: center;
    margin: 4px;
    text-align: center;
    transform: scale(1);
    transition: .3s ease-in-out;
    width: 2.5rem;"><i class="fa fa-times" aria-hidden="true" style="color:#ff0000;"></i></span> 
                                @endif
                            </center>
                        </td>                        
                       <td>
                           <center>
                                @if($user->kyc == 1) 
                                    <span style="align-items: center;
    border: 1px solid #d6dcec;
    border-radius: 6px;
    display: flex;
    height: 2.5rem;
    justify-content: center;
    margin: 4px;
    text-align: center;
    transform: scale(1);
    transition: .3s ease-in-out;
    width: 2.5rem;"><i class="fa fa-check" aria-hidden="true" style="color:#22c03c;"></i></span> 
                                @else  
                                    <span style="align-items: center;
    border: 1px solid #d6dcec;
    border-radius: 6px;
    display: flex;
    height: 2.5rem;
    justify-content: center;
    margin: 4px;
    text-align: center;
    transform: scale(1);
    transition: .3s ease-in-out;
    width: 2.5rem;"><i class="fa fa-times" aria-hidden="true" style="color:#ff0000;"></i></span> 
                                @endif
                            </center>

                       </td>
<td>
    
      <?php

foreach($bankdetails as $index2 => $val){
    if($val->user_id==$user->id){
   ?>
 <span style="align-items: center;
    border: 1px solid #d6dcec;
    border-radius: 6px;
    display: flex;
    height: 2.5rem;
    justify-content: center;
    margin: 4px;
    text-align: center;
    transform: scale(1);
    transition: .3s ease-in-out;
    width: 2.5rem;"><i class="fa fa-check" aria-hidden="true" style="color:#22c03c;"></i></span> 
   <?php
    }else{
?>
 <span style="align-items: center;
    border: 1px solid #d6dcec;
    border-radius: 6px;
    display: flex;
    height: 2.5rem;
    justify-content: center;
    margin: 4px;
    text-align: center;
    transform: scale(1);
    transition: .3s ease-in-out;
    width: 2.5rem;"><i class="fa fa-times" aria-hidden="true" style="color:#ff0000;"></i></span> 
<?php

    }
}
?>



</td>
                        <td>
                            @if($user->user_type=='1')
                                <span>Investor</span>
                            @elseif($user->user_type=='2')
                                <span>Issuer </span>
                            @else
                                <span>Agent</span>
                            @endif
                        </td>  

                        <td>                            
                            @if($user->approved == 1) @else @endif
                                <a class="btn btn-secondary btn-block" style="width:40px;padding:9px 10px;float:left;display:block;margin-top:0;" title="Disapprove" href="{{ route('admin.user.userApprovalStatus', [$user->id, 'disapprove'] ) }}"><i class="fas fa-thumbs-down"></i></a>
                               
                                <a class="btn btn-info btn-block" style="width:40px;padding:9px 10px;float:left;display:block;margin-top:0;margin-left:-3px;" title="Approve" href="{{ route('admin.user.userApprovalStatus', [$user->id, 'approve'] ) }}"><i class="fas fa-thumbs-up"></i></a>
                            

                            <a class="btn btn-dark btn-block" style="width:40px;padding:9px 10px;float:left;display:block;margin-top:0;margin-left:5px;" title="User Detail" href="{{ route('admin.details', $user->id ) }}"><i class="fas fa-user"></i></a>
                            @if($user->approved == 1 && $user->kyc==1  && $user->bank_approved==1 && $user->eth_address=='') 
                             <a class="btn btn-dark btn-block addTrusted" data-id="{{$user->id}}" style="width:40px;padding:9px 10px;float:left;display:block;margin-top:5px;" title="CREATE CRYPTO ADDRESS" href="javascript:void(0);"><i class="fas fa-coins"></i></a>
                             @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th style="width:20px;text-transform: uppercase;">@lang('admin.id')</th>
                        <th style="width:70px;text-transform: uppercase;">Full Name</th>
                        
                        <th style="width:70px;text-transform: uppercase;">@lang('admin.email')</th>       
                                  <th style="width:50px;">Verify Email/Approved</th>         
                        <th style="width:40px;text-transform: uppercase;">@lang('admin.users.kyc_status')</th>
                              <th style="width:40px;text-transform: uppercase;">Bank Details</th>
                         <th style="width:70px;text-transform: uppercase;">@lang('admin.type')</th>                  
                        <th style="width:100px;text-transform: uppercase;">@lang('admin.action')</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script>
     $(document).ready(function(){ 
$(".addTrusted").on("click", function () {
  
            var id  = $(this).attr("data-id");
            var url = "{{ url('/admin/propertyEthereumAddress') }}/" + id
          
                swal({
                         title     : "Are you sure?",
                         text      : "You want to create Crypto Address?",
                         icon      : "info",
                         buttons   : true,
                         dangerMode: false,
                         buttons   : ["No", "Yes"],
                          html      :"<h4>Loading...</h4>",
                          showSpinner: true,
                     })

                    .then((willDelete) => {
                        if (willDelete) {

                    swal({
                            title: 'Please Wait..!',
                            text: 'Is working..',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            allowEnterKey: false,
                            onOpen: () => {
                                swal.showLoading()
                            }
                        })
                            $.ajax({
                                       type   : "GET",
                                       url    : url,
                                       success: function (data) {
                                           if (data.status == 'success') {
                                            swal({
                                            title: "Wow!",
                                            text: "Crypto Address Generated Successsfully",
                                            icon: "success"
                                                })
                                               
                                           }else{
                                               swal('Something Went Wrong. Please try again');
                                           }
                                       }
                                   });
                        }else{
                           // $('.addTrusted').prop('checked', false);
                        }
                    });
            
        });
  });
</script>

@endsection
@extends('admin.layout.base')

@section('title', 'User Details')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
            <a href="{{ route('admin.user.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> @lang('admin.back')</a>

			    
<table id="example" class="datatable-full table table-striped table-bordered custom-table-style" style="width: 100% !important">
                <thead>
                    <tr>
                        <th style="width:20px;" colspan="2"><h5 style="margin-bottom: 2em;">User Details</h5>        </th>
                       
                        
                     
                    </tr>
                </thead>
                <tbody>
                	<tr>
                		<td><label class="col-xs-12 col-form-label">Full Name</label></td>
                		<td>{{$user->name}}</td>
                	</tr>
                	
                	<tr>
                		<td><label class="col-xs-12 col-form-label">Email</label></td>
                		<td>{{$user->email}}</td>
                	</tr>
                	<tr>
                		<td><label class="col-xs-12 col-form-label">Ethereum Address</label></td>
                		<td>{{$user->eth_address}}</td>
                	</tr>

			@if($user->user_type==0)
			
				<tr>
					<td><label class="col-xs-12 col-form-label">User Type</label></td>
					<td>
						Individual User
					</td>
				</tr>
				<tr>
					<td><label class="col-xs-12 col-form-label">Address</label></td>
					<td>
						@if(isset($user_detail->address)) {{$user_detail->address}} @endif  @if(isset($user_detail->address_two)) {{$user_detail->address_two}} @endif @if(isset($user_detail->city)) {{$user_detail->city}} @endif @if(isset($user_detail->state)) {{$user_detail->state}} @endif @if(isset($user_detail->postal_code)) {{$user_detail->postal_code}} @endif @if(isset($user_detail->country_id)) {{$user_detail->country->name}} @endif
					</td>
				</tr>

				<tr>
					<td><label class="col-xs-12 col-form-label">Address Proof</label></td>
					<td class="col-xs-10">
						@if(isset($user_detail->address_proof)) 
							<img src="{{img($user_detail->address_proof)}}" /> 
						@endif
					</td>
				</tr>

				<tr>
					<td><label class="col-xs-12 col-form-label">Address Proof Status</label></td>
					<td>
						@if(isset($user_detail->address_proof_status)) 
							@if($user_detail->address_proof_status==0) 
								<span style="color: red;">Not Verified</span>
							@else
								<span style="color: green;">Verified</span>
							@endif
						@endif
					</td>
				</tr>

				<tr>
					<td><label class="col-xs-12 col-form-label">Verify Human</label></td>
					<td>
						@if(isset($user_detail->verify_human)) 
							<img src="{{img($user_detail->verify_human)}}" /> 
						@endif
					</td>
				</tr>

				<tr>
					<td><label class="col-xs-12 col-form-label">Verify Human Status</label></td>
					<td>
						@if(isset($user_detail->verify_human_status)) 
						@if($user_detail->verify_human_status==0) 
							<span style="color: red;">Not Verified</span>
						@else
							<span style="color: green;">Verified</span>
						@endif
						@endif
					</td>
				</tr>		

			@endif



@if($user->user_type==1)
			
				<tr>
					<td colspan="2"><label class="col-xs-12 col-form-label">User Type</label></td>
					
				</tr>

				<tr>
					<td><label class="col-xs-12 col-form-label">Company Name</label></td>
					<td>
						@if(isset($user_detail_company->company_name)) {{$user_detail_company->company_name}} @endif 
					</td>
				</tr>

				<tr>
					<td><label class="col-xs-12 col-form-label">Headquarters</label></td>
					<td>
						@if(isset($user_detail_company->headquarters)) {{$user_detail_company->headquarters}} @endif 
					</td>
				</tr>

				<tr>
					<td><label class="col-xs-12 col-form-label">Date Founded</label></td>
					<td>
						@if(isset($user_detail_company->date_founded)) {{date('d-m-Y',strtotime($user_detail_company->date_founded))}} @endif 
					</td>
				</tr>

				<tr>
					<td><label class="col-xs-12 col-form-label">Team Size</label></td>
					<td>
						@if(isset($user_detail_company->team_size)) {{$user_detail_company->team_size}} @endif 
					</td>
				</tr>

				<tr>
					<td><label class="col-xs-12 col-form-label">Company URL</label></td>
					<td>
						@if(isset($user_detail_company->company_url)) {{$user_detail_company->company_url}} @endif 
					</td>
				</tr>

				<tr>
					<td><label class="col-xs-12 col-form-label">Social Channels</label></td>
					<td>
						@if(isset($user_detail_company->social_channels)) {{$user_detail_company->social_channels}} @endif 
					</td>
				</tr>
				<tr>
					<td><label class="col-xs-12 col-form-label">Incorporation Certificate</label></td>
					<td>
						@if(isset($user_detail_company->incorporation_certificate)) 
							<img src="{{img($user_detail_company->incorporation_certificate)}}" /> 
						@endif
					</td>
				</tr>
				<tr>
					<td><label class="col-xs-12 col-form-label">Incorporation Certificate Status</label></td>
					<td>
						@if(isset($user_detail_company->incorporation_certificate_status)) 
						@if($user_detail_company->incorporation_certificate_status==0) 
							<span style="color: red;">Not Verified</span>
						@else
							<span style="color: green;">Verified</span>
						@endif
						@endif
					</td>
				</tr>

				<tr>
					<td><label class="col-xs-12 col-form-label">Partnership Deed</label></td>
					<td>
						@if(isset($user_detail_company->partnership_deed)) 
							<img src="{{img($user_detail_company->partnership_deed)}}" /> 
						@endif
					</td>
				</tr>

				<tr>
					<td><label class="col-xs-12 col-form-label">Partnership Deed Status</label></td>
					<td>
						@if(isset($user_detail_company->partnership_deed_status)) 
						@if($user_detail_company->partnership_deed_status==0) 
							<span style="color: red;">Not Verified</span>
						@else
							<span style="color: green;">Verified</span>
						@endif
						@endif
					</td>
				</tr>

				<tr>
					<td><label class="col-xs-12 col-form-label">Trust Deed</label></td>
					<td>
						@if(isset($user_detail_company->trust_deed)) 
							<img src="{{img($user_detail_company->partnership_deed)}}" /> 
						@endif
					</td>
				</tr>

				<tr>
					<td><label class="col-xs-12 col-form-label">Trust Deed Status</label></td>
					<td>
						@if(isset($user_detail_company->trust_deed_status)) 
						@if($user_detail_company->partnership_deed_status==0) 
							<span style="color: red;">Not Verified</span>
						@else
							<span style="color: green;">Verified</span>
						@endif
						@endif
					</td>
				</tr>

				<tr>
					<td><label class="col-xs-12 col-form-label">Register Socities</label></td>
					<td>
						@if(isset($user_detail_company->register_socities)) 
							<img src="{{img($user_detail_company->register_socities)}}" /> 
						@endif
					</td>
				</tr>

				<tr>
					<td><label class="col-xs-12 col-form-label">Register Socities Status</label></td>
					<td>
						@if(isset($user_detail_company->register_socities_status)) 
						@if($user_detail_company->register_socities_status==0) 
							<span style="color: red;">Not Verified</span>
						@else
							<span style="color: green;">Verified</span>
						@endif
						@endif
					</td>
				</tr>

				<tr>
					<td><label class="col-xs-12 col-form-label">Signing Authority</label></td>
					<td>
						@if(isset($user_detail_company->signing_authority)) 
							<img src="{{img($user_detail_company->signing_authority)}}" /> 
						@endif
					</td>
				</tr>

				<tr>
					<td><label class="col-xs-12 col-form-label">Signing Authority Status</label></td>
					<td>
						@if(isset($user_detail_company->signing_authority_status)) 
						@if($user_detail_company->signing_authority_status==0) 
							<span style="color: red;">Not Verified</span>
						@else
							<span style="color: green;">Verified</span>
						@endif
						@endif
					</td>
				</tr>
				<tr>
					<td><label class="col-xs-12 col-form-label">Fund Type</label></td>
					<td>
						@if(isset($user_detail_company->fund_type)) 
							@if($user_detail_company->fund_type!=0) 
								{{$user_detail_company->fund->fund_type}}
							@else
								{{$user_detail_company->other_fund_type}}
							@endif
						@endif
					</td>
				</tr>	

										

			
			@endif


                </tbody>
            </table>
            

			

			
						
		</div>
    </div>
</div>

@endsection

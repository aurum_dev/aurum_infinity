@extends('admin.layout.base')

@section('title', 'Create Asset Type')

@section('content')

<div class="content-area py-1" style="background:#fff;padding-top:40px;">
    <div class="container-fluid">
    	<div class="box box-block bg-white">

            <a href="{{ route('admin.tokenizer.showtrans') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> @lang('admin.back')</a>

			<h5 style="margin-bottom: 2em;">Transfer Matic to Issuer/Investor</h5>

            <form class="form-horizontal" action="{{route('admin.tokenizer.storetrans')}}" method="POST" enctype="multipart/form-data" role="form">            	
            	{{csrf_field()}}
				
				<div class="form-group col-md-6">
					<label for="" class="col-form-label">User</label>					
					<select name="username" id="username" class="form-control select2">
                                                <option value="">Select User</option>
                                            @foreach($users as $user)
                                            <option value="{{ @$user->id }}">{{ @$user->email }}</option>
                                            @endforeach
</select>
				</div>
				<div class="form-group col-md-6">
					<label for="" class="col-form-label">Matic</label>					
					<input class="form-control" type="number" maxlength="100" name="maticamt" required id="maticamt" placeholder="Enter Matic">
				</div>				

				<div class="form-group row">
				<br>				
				<br>				
					<label for="" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="{{route('admin.tokenizer.showtrans')}}" class="btn btn-default">@lang('admin.cancel')</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>
  <script src="<?php echo url('/'); ?>/public/issuer/assets/plugins/jquery/jquery.min.js"></script>
        <!-- Internal Select2.min js -->
        <script src="<?php echo url('/'); ?>/public/issuer/assets/plugins/select2/js/select2.min.js"></script>        
        <script src="<?php echo url('/'); ?>/public/issuer/assets/js/index.js"></script>       
        <script src="<?php echo url('/'); ?>/public/issuer/assets/js/form-elements.js"></script>
@endsection
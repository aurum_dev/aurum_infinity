@extends('admin.layout.base')

@section('title', 'Crypto Transfer')

@section('content')
<style>
    
#example_length{
margin-top: 5px !important;
    float: right !important;
    margin-right: 266px !important;
}


</style>
<div class="breadcrumb-header justify-content-between">
                    <div class="my-auto">
                        <div class="d-flex">
                            <h4 class="content-title mb-0 my-auto">Matic Transfer</h4><span
                                class="text-muted mt-1 tx-13 ms-2 mb-0">/ Matic Transfer List</span>
                        </div>
                    </div>                    
                </div>
    <div class="content-area py-1" style="background:#fff;padding-top: 40px !important;">
        <div class="container-fluid">
            
            <div class="box box-block bg-white">
                  <a href="{{ route('admin.tokenizer.createtrans') }}" style="margin-left: 1em;float: right;margin-bottom: 20px;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Transfer Matic</a>

                <table  id="example" class="table table-striped table-hover dt-responsive" id="table-2">
                    <thead>
                        <tr>
                            <th>S.No</th>                            
                            <th>Issuer Details</th>                            
                            <th>Matic</th>
                            <th>Crypto Address For Verify Transaction</th>
                            <th>Transaction Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>    
                        @foreach ($matictransfer as $key => $asset)
                            <tr>
                                <td>{{ @$key + 1}}</td>
                                <td>{{ $asset->name}}</td>
                                <td>{{ $asset->maticamt}}</td>
                                <td><a href='https://mumbai.polygonscan.com/address/{{ $asset->eth_address}}' target="_blank">{{ $asset->eth_address}}</a></td>
                                <td><?php echo date('d, M Y',strtotime($asset->transferdate)); ?></td>
                                <td>Success</td>
                            </tr>
                        @endforeach                                            
                    </tbody>
                    <tfoot>
                        <tr>
                           <th>S.No</th>                            
                           <th>Issuer Details</th>                            
                           <th>Matic</th>
                           <th>Crypto Address For Verify Transaction</th>
                           <th>Transaction Date</th>
                           <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection
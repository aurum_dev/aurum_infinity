@extends('admin.layout.base')

@section('title', 'Token Request')

@section('content')
<style>
    
#example_length{
margin-top: 5px !important;
    float: right !important;
    margin-right: 266px !important;
}
table.dataTable.dtr-inline.collapsed > tbody > tr > td.dtr-control:before, table.dataTable.dtr-inline.collapsed > tbody > tr > th.dtr-control:before

{
    top: 32% !important;
}
.table>:not(:last-child)>:last-child>*{
    background:#737f9e !important;
    color: #fff;
}

</style>
<div class="breadcrumb-header justify-content-between">
                    <div class="my-auto">
                        <div class="d-flex">
                            <h4 class="content-title mb-0 my-auto"> Tokens</h4><span
                                class="text-muted mt-1 tx-13 ms-2 mb-0">/ Tokens History</span>
                        </div>
                    </div>                    
                </div>
<div class="content-area py-1" style="background:#fff;padding-top: 40px !important;">
    <div class="container-fluid">
        <div class="box box-block bg-white">         
                        
            <table id="example" class="table table-striped table-hover dt-responsive" style="width: 100% !important">
                <thead>
                    <tr style="background:#737f9e">
                        <th>@lang('admin.id')</th>                        
                        <th>User</th> 
                        <th>Token Name</th>                         
                        <th>Token Supply</th>                        
                        <th>Token Symbol</th>                        
                        <th>Token Decimal</th>                      
                        <th>Token Value</th> 
                        <th> Status</th>
                        <th>Contract Address</th>
                        
                    
                    </tr>
                </thead>
                <tbody>
                    @foreach($history as $index => $value)
                    <tr>
                        <td>{{ $index + 1 }}</td>                        
                        <td>{{ @(!is_null($value->user->name)) ? $value->user->name : 'Admin'  }}</td>                        
                        <td>{{ @$value->tokenName }}</td>                        
                        <td>{{ @$value->tokenSupply }}</td>   
                        <td>{{ @$value->tokenSymbol }}</td>     
                        <td>{{ @$value->tokenDecimal }}</td>  
                        <td>{{ @$value->tokenValue }}</td>  
                         <td>
                            
<?php
if($value->tokenStatus=="live"){
    ?>
<button class="btn btn-success">Live</button>
    <?php

}else if($value->tokenStatus=="pending"){
 ?>
<button class="btn btn-warning">Pending</button>
    <?php
}else if($value->tokenStatus=="rejected"){
 ?>
<button class="btn btn-danger">Rejected</button>
    <?php
}

?>
                        </td>
                        <td><a target="_blank" href="https://mumbai.polygonscan.com/address/{{ @$value->contract_address }}">{{ @$value->contract_address }}</a></td>
                       
                   
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>@lang('admin.id')</th>                        
                        <th>User</th> 
                        <th>Token Name</th>                         
                        <th>Token Supply</th>                        
                        <th>Token Symbol</th>                        
                        <th>Token Decimal</th>                      
                        <th>Token Value</th> 
                         <th> Status</th>
                        <th>Contract Address</th>
                       
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
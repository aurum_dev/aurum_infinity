
<!-- Footer content -->

<style>
   .subscribe-area {
background-image: linear-gradient(to top, #161f2b 0%, #161f2b 100%);
}
.footer_copyright_item p {
   margin-bottom: 0 !important;
}
.pb-50 {
    padding-bottom: 50px;
}
.pt-70 {
    padding-top: 70px;
}

.mb-15 {
    margin-bottom: 15px;
}

.subscribe-text span {
    font-size: 12px;
    font-weight: 700;
    color: #fff;
    letter-spacing: 5px;
}
.subscribe-text h2 {
    color: #fff;
    font-size: 36px;
    font-weight: 300;
    margin-bottom: 0;
    margin-top: 6px;
}
.subscribe-wrapper {
    overflow: hidden;
}
.mb-15 {
    margin-bottom: 15px;
}
.subscribe-form {
}
.subscribe2-wrapper .subscribe-form input {
    background: none;
    border: 1px solid #fff;
   
    color: #fff;
    display: inline-block;
    font-size: 15px;
    font-weight: 300;
    height: 57px;
    margin-right: 17px;
    padding-left: 35px;
    width: 60%;
    cursor: pointer;
}
 
.subscribe2-wrapper .subscribe-form button {
    background: #05bd8e;
    border: none;
    
    color: #fff;
    display: inline-block;
    font-size: 18px;
    font-weight: 400;
    line-height: 1;
    padding: 18px 46px;
    transition: all 0.3s ease 0s;
}
.subscribe2-wrapper .subscribe-form button i {
    font-size: 18px;
    padding-left: 5px;
}
.wraper_footer.style-six {
    background-color: #3b204e !important;
}
.footer_main_item {
    margin-bottom: 5px !important;
}

</style>
<section class="subscribe-area pb-10 pt-40" style="padding-top:30px;">
<div class="container">
   <div class="row">

               <div class="col-md-5">
                  <div class="subscribe-text mb-15">
                     <p style="color:#fff;font-size:17px;line-height:34px;">Stay up-to-date on the latest industry news, digital products and innovations impacting insurance.</p>
                   
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="subscribe-wrapper subscribe2-wrapper mb-15">
                     <div class="subscribe-form">
                        <form action="#">
                           <input placeholder="Enter your email address" type="email">
                           <button>Subscribe <i class="fas fa-long-arrow-alt-right"></i></button>
                        </form>
                     </div>
                  </div>
               </div>
            </div>

</div>
</section>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <footer class="wraper_footer style-six">
            <!-- wraper_footer_main -->
            <div class="wraper_footer_main" style="background:#232f3e">
               <div class="container" style="padding-top:40px;">
                  <!-- row -->
                  <div class="row footer_main">
                     <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12" style="    color: #fff;
    font-size: 32px;line-height: 40px;
    padding-bottom: 50px;
    font-family: 'Poppins';
    text-align: center;">Democratizing the Real Estate</div>
                     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer_main_item matchHeight">
                           <section id="nav_menu-8" class="widget widget_nav_menu">
                              <h5 class="widget-title" style="font-size:22px;">About Us</h5>
                              <div class="menu-important-links-container">
                                 <ul id="menu-important-links" class="menu rt-mega-menu-transition-slide">
                                 <li id="menu-item-344" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-344"><a href="https://www.aurumproptech.com" target="_blank" style="
    color: #fff;
    font-size: 16px;
    opacity: 0.5;
">- Aurum Premium Listings </a></li>
                                    <li id="menu-item-1012" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-1012"><a target="_blank" href="https://www.aurumventures.in" style="
    color: #fff;
    font-size: 16px;
    opacity: 0.5;
" target="_blank">- Aurum Ventures </a></li>
                                    <li id="menu-item-345" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-345"><a target="_blank" href="https://aurumproptech.in" style="
    color: #fff;
    font-size: 16px;
    opacity: 0.5;
" target="_blank">- Aurum PropTech </a></li>

 <li id="menu-item-345" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-345"><a target="_blank" href="https://auruminfinity.com/" style="
    color: #fff;
    font-size: 16px;
    opacity: 0.5;
" target="_blank">- Aurum Infinity </a></li>
<li id="menu-item-345" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-345"><a target="_blank" href="https://www.aurumproptech.com/CREX/" style="
    color: #fff;
    font-size: 16px;
    opacity: 0.5;
" target="_blank">- Aurum CREX </a></li>

                                                                    </ul>
                              </div>
                           </section>
                        </div>
                     </div>
                  
                     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer_main_item matchHeight">
                           <section id="nav_menu-9" class="widget widget_nav_menu">
                              <h5 class="widget-title" style="font-size:22px;">Our Solutions</h5>
                              <div class="menu-featured-services-container">
                                 <ul id="menu-featured-services" class="menu rt-mega-menu-transition-slide">
                                    <li id="menu-item-344" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-344"><a style="
    color: #fff;
    font-size: 16px;
    opacity: 0.5;
" target="_blank" href="https://www.sell.do/" target="_blank">- Sell.do</a></li>
                                    <li id="menu-item-1012" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-1012"><a style="
    color: #fff;
    font-size: 16px;
    opacity: 0.5;
" target="_blank" href="https://kylas.io/" target="_blank">- Kylas</a></li>
                                    <li id="menu-item-345" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-345"><a style="
    color: #fff;
    font-size: 16px;
    opacity: 0.5;
" target="_blank" href="https://thehousemonk.com/" target="_blank">- TheHouseMonk</a></li>
                                    <li id="menu-item-343" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-343"><a style="
    color: #fff;
    font-size: 16px;
    opacity: 0.5;
" target="_blank" href="http://www.integrowamc.com/" target="_blank">- Integrow AMC</a></li>

  <li id="menu-item-343" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-343"><a style="
    color: #fff;
    font-size: 16px;
    opacity: 0.5;
" target="_blank" href="https://grexter.in/" target="_blank">- Grexter Living</a></li>
 <li id="menu-item-343" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-343"><a style="
    color: #fff;
    font-size: 16px;
    opacity: 0.5;
" target="_blank" href="https://beyondwalls.com/">- Beyondwalls</a></li>


                                 </ul>
                              </div>
                           </section>
                        </div>
                     </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer_main_item matchHeight">
                           <section id="nav_menu-8" class="widget widget_nav_menu">
                              <h5 class="widget-title" style="font-size:22px;">Latest Launch</h5>
                              <div class="menu-important-links-container">
                                 <ul id="menu-important-links" class="menu rt-mega-menu-transition-slide">
                                    <li id="menu-item-338" class="menu-item menu-item-type-post_type menu-item-object-page menu-flyout rt-mega-menu-hover item-338"><a  style="
    color: #fff;
    font-size: 16px;
    opacity: 0.5;
" href="https://seolounge.radiantthemes.com/about-us/" target="_blank">- Aurum Q5</a></li>
                                        </ul>
                              </div>
                           </section>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="footer_main_item matchHeight">
                           <section id="text-5" class="widget widget_text">
                              <h5 class="widget-title" style="font-size:22px;">Contact Us</h5>
                              <div class="textwidget">
                                  <p style="
    color: #fff;
    font-size: 18px;
    opacity: 0.8;margin-bottom: 0;margin-bottom:12px;
">
                                   Aurum Building Q1
                                 </p>
                                 <p style="
    color: #fff;
    font-size: 16px;
    opacity: 0.5;
">
                                   Gen-4/1, TTC Industrial Area, Thane Belapur Road, Ghansoli, Navi Mumbai Thane - 400710
                                 </p>
                                               <p class="widget-title" style="font-size:22px;">Follow Us</p>         
                                   <ul class="social">
                                                           <li class="google-plus"><a href="https://www.instagram.com/aurumproptech" rel="publisher" target="_blank"><i class="fa fa-instagram" style="font-size:30px;"></i></a></li>
                                  <li class="facebook"><a href="https://www.facebook.com/AurumPropTech" target="_blank"><i class="fa fa-facebook-square" style="font-size:30px;"></i></a></li>
                                  <li class="twitter"><a href="https://www.linkedin.com/company/71413509/admin" target="_blank"><i class="fa fa-linkedin-square" style="font-size:30px;"></i></a></li>
                               </ul>
                                                                                                                                                                                                                                                                                                                                                                                                                                <>
                        
                    </div>
                    
                              </div>
                           </section>
                        </div>
                     </div>
                  </div>
                  <!-- row -->
               </div>
            </div>
            <!-- wraper_footer_main -->
            <!-- wraper_footer_copyright -->
            <div class="wraper_footer_copyright" style="background: #161f2b;">
               <div class="container">
                  <!-- row -->
                  <div class="row footer_copyright">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
                    <!-- footer_copyright_item -->
                    <div class="footer_copyright_item" style="text-align:center;">
                        <p style="opacity: .5;"> © 2022 Aurum Softwares & Solutions Private Limited | <a href="privacy" style="color:#fff;">Cookie Policy</a> | <a href="privacy" style="color:#fff;">Privacy Policy</a> | <a href="terms_conditions" style="color:#fff;">Terms & Conditions</a></p>
                    </div>
                    <!-- footer_copyright_item -->
                </div>
           
            </div>
                  <!-- row -->
               </div>
            </div>
            <!-- wraper_footer_copyright -->
         </footer>

           </div>
      <!-- radiantthemes-website-layout -->
      <script type="text/html" id="wpb-modifications"></script> <script type="text/javascript">
         (function () {
            var c = document.body.className;
            c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
            document.body.className = c;
         })();
      </script>
      <link rel='stylesheet' id='radiantthemes-addons-custom-css'  href="{{asset('public/asset/login/wp-content/plugins/radiantthemes-addons/assets/css/radiantthemes-addons-custom.min9f97.css?ver=1638778739')}}" type='text/css' media='all' />
      <style id='radiantthemes-addons-custom-inline-css' type='text/css'>
         .radiantthemes-custom-button.rt810073130 .radiantthemes-custom-button-main{ background:#05bd8e !important;}
         .radiantthemes-custom-button.rt172900896 .radiantthemes-custom-button-main{ background:linear-gradient(to right, #251d59 0%, #4a3259 100%);}
         .radiantthemes-custom-button.rt537002223 .radiantthemes-custom-button-main{ background:linear-gradient(to right, #251d59 0%, #4a3259 100%);}
         .radiantthemes-custom-button.rt636586625 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt2116519540 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt239276426 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt1033353997 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt143729880 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt638223915 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt729187609 .radiantthemes-custom-button-main{ background:linear-gradient(to right, #251d59 0%, #4a3259 100%);}
         .checked { color: orange;}
         .rt-pricing-table.element-nine.rt842858723 > .holder > .spotlight-tag > .spotlight-tag-text,
         .rt-pricing-table.element-nine.spotlight.rt842858723 > .holder > .more .btn{
         background: linear-gradient( to right, #ff7816 0%, #ff590f 100% ) ;
         }
         .rt-pricing-table.element-nine.rt842858723 > .holder > .more .btn{
         border-color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt842858723 > .holder > .more .btn{
         color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1296861313 > .holder > .spotlight-tag > .spotlight-tag-text,
         .rt-pricing-table.element-nine.spotlight.rt1296861313 > .holder > .more .btn{
         background: linear-gradient( to right, #ff7816 0%, #ff590f 100% ) ;
         }
         .rt-pricing-table.element-nine.rt1296861313 > .holder > .more .btn{
         border-color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1296861313 > .holder > .more .btn{
         color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1087964127 > .holder > .spotlight-tag > .spotlight-tag-text,
         .rt-pricing-table.element-nine.spotlight.rt1087964127 > .holder > .more .btn{
         background: linear-gradient( to right, #ff7816 0%, #ff590f 100% ) ;
         }
         .rt-pricing-table.element-nine.rt1087964127 > .holder > .more .btn{
         border-color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1087964127 > .holder > .more .btn{
         color: #ff7816 ;
         }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row, .radiant-contact-form.rt5d04c30e5c56e06 div.wpcf7-response-output{
         }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=submit], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=button],.radiant-contact-form.rt5d04c30e5c56e06 .form-row button[type=submit] {   linear-gradient(to right, #251d59 0%, #4a3259 100%); color:#ffffff; border-top:0px #465579; border-right:0px #465579; border-bottom:0px #465579; border-left:0px #465579; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=submit]:hover, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=button]:hover, .radiant-contact-form.rt5d04c30e5c56e06 .form-row button[type=submit]:hover { background:linear-gradient(to right, #251d59 0%, #4a3259 100%)  }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file], .radiant-contact-form.rt5d04c30e5c56e06 .form-row select, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea {   background-color: #ffffff; padding-top:40px; padding-right:40px; padding-bottom:18px; padding-left:10px; border-radius: 0px 0px 0px 0px; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file], .radiant-contact-form.rt5d04c30e5c56e06 .form-row select, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea {   color  : #b8b8b8; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row select:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea:focus {   color:#252525; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row select:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea:focus {   border-top:0px solid #f5f4f4; border-right:0px solid #f5f4f4; border-bottom:2px solid #f5f4f4; border-left:0px solid #f5f4f4; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file], .radiant-contact-form.rt5d04c30e5c56e06 .form-row select, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea {   border-top:0px solid #f5f4f4; border-right:0px solid #f5f4f4; border-bottom:2px solid #f5f4f4; border-left:0px solid #f5f4f4; }
      </style>
      <link rel='stylesheet' id='vc_animate-css-css'  href="{{asset('public/asset/login/wp-content/plugins/js_composer/assets/lib/bower/animate-css/animate.mine6df.css?ver=6.5.0')}}" type='text/css' media='all' />
      <script type='text/javascript' src="{{asset('public/asset/login/wp-includes/js/dist/vendor/wp-polyfill.min89b1.js?ver=7.4.4')}}" id='wp-polyfill-js'></script>
     <script type='text/javascript' id='wp-polyfill-js-after'>
         ( 'fetch' in window ) || document.write( '<script src="asset/login/wp-includes/js/dist/vendor/wp-polyfill-fetch.min6e0e.js?ver=3.0.0"></scr' + 'ipt>' );( document.contains ) || document.write( '<script src="wp-includes/js/dist/vendor/wp-polyfill-node-contains.min2e00.js?ver=3.42.0"></scr' + 'ipt>' );( window.DOMRect ) || document.write( '<script src="asset/login/wp-includes/js/dist/vendor/wp-polyfill-dom-rect.min2e00.js?ver=3.42.0"></scr' + 'ipt>' );( window.URL && window.URL.prototype && window.URLSearchParams ) || document.write( '<script src="asset/login/wp-includes/js/dist/vendor/wp-polyfill-url.min5aed.js?ver=3.6.4"></scr' + 'ipt>' );( window.FormData && window.FormData.prototype.keys ) || document.write( '<script src="asset/login/wp-includes/js/dist/vendor/wp-polyfill-formdata.mine9bd.js?ver=3.0.12"></scr' + 'ipt>' );( Element.prototype.matches && Element.prototype.closest ) || document.write( '<script src="asset/login/wp-includes/js/dist/vendor/wp-polyfill-element-closest.min4c56.js?ver=2.0.2"></scr' + 'ipt>' );
      </script>
   
      <script type='text/javascript' src="{{asset('public/asset/login/wp-content/plugins/radiantthemes-addons/assets/js/radiantthemes-addons-core.min9f97.js?ver=1638778739')}}" id='radiantthemes-addons-core-js'></script>
      <script type='text/javascript' src="{{asset('public/asset/login/wp-content/plugins/radiantthemes-addons/assets/js/radiantthemes-addons-custom.min9f97.js?ver=1638778739')}}" id='radiantthemes-addons-custom-js'></script>
      <script type='text/javascript' src="{{asset('public/asset/login/wp-content/plugins/radiantthemes-mega-menu/assets/js/rt-megamenu3f90.js?ver=5.6.6')}}" id='rt-megamenu-front-end-js-js'></script>
      <script type='text/javascript' src="{{asset('public/asset/login/wp-content/themes/seolounge/js/radiantthemes-core.min3f90.js?ver=5.6.6')}}" id='radiantthemes-core-js'></script>
      <script type='text/javascript' src="{{asset('public/asset/login/wp-content/plugins/js_composer/assets/lib/bower/isotope/dist/isotope.pkgd.mine6df.js?ver=6.5.0')}}" id='isotope-js'></script>
      <script type='text/javascript' src="{{asset('public/asset/login/wp-content/themes/seolounge/js/radiantthemes-blog-style3f90.js?ver=5.6.6')}}" id='radiantthemes-blog-style-js'></script>
      <script type='text/javascript' src="{{asset('public/asset/login/wp-content/themes/seolounge/js/radiantthemes-custom3f90.js?ver=5.6.6')}}" id='radiantthemes-custom-js'></script>
      <script type='text/javascript' src="{{asset('public/asset/login/wp-includes/js/wp-embed.min3f90.js?ver=5.6.6')}}" id='wp-embed-js'></script>
      <script type='text/javascript' src="{{asset('public/asset/login/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.mine6df.js?ver=6.5.0')}}" id='wpb_composer_front_js-js'></script>
      <script type='text/javascript' src="{{asset('public/asset/login/wp-content/plugins/js_composer/assets/lib/vc_waypoints/vc-waypoints.mine6df.js?ver=6.5.0')}}" id='vc_waypoints-js'></script>
    <script>
   var owl = $('.owl-carousel');
owl.owlCarousel({
    items:3,
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:6000,
    autoplayHoverPause:true
});
$('.play').on('click',function(){
    owl.trigger('play.owl.autoplay',[6000])
})
$('.stop').on('click',function(){
    owl.trigger('stop.owl.autoplay')
})
    </script>
 

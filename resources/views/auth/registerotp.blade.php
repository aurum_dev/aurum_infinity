@extends('layout.auth')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
@section('content')



 <div id="page" class="site">
            <!-- #content -->
            <div id="content" class="site-content">
               <div id="primary" class="content-area">
                  <main id="main" class="site-main">
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                     <div class="container page-container">
                        <article id="post-3351" class="post-3351 page type-page status-publish hentry">
                           <header class="entry-header">
                           </header>
                           <!-- .entry-header -->
                           <div class="entry-content">
                          
                             <section data-vc-full-width="true" style="height:800px;padding-top:0 !important; position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 0; padding-right: 0;opacity:9 !important;" data-vc-full-width-init="true" class="vc_section background-position-center-bottom home-thirteen-pricing-table-responsive vc_custom_1565269117521 vc_section-has-fill">
   <div class="vc_row wpb_row vc_row-fluid vc_custom_1565173495650">
      <div class="vc_col-sm-12 vc_col-lg-12 vc_col-md-12" style="height:800px;background-color: #ffffff; /* For browsers that do not support gradients */
  background-image: linear-gradient(#ffffff, #B9983D12);background:url(<?php echo URL('/'); ?>/public/asset/login/wp-content/uploads/2021/01/backgroundissuer.jpg);background-size: cover;
    background-position: center;background-repeat: no-repeat;">
         <div class="vc_column-inner vc_custom_1565240932947">
            <div class="wpb_wrapper">
              
                <div class="vc_col-sm-6 vc_col-lg-6 vc_col-md-6"></div>  
              
              <div class="vc_col-sm-6 vc_col-lg-6 vc_col-md-6"> 
  <center>

        <div class="vc_column-inner" style='width:90% !important;margin-top:40px !important;'>
        
            <div class="wpb_wrapper">
                      <!-- rt-cf7 -->
               <div class="radiant-contact-form rt5d04c30e5c56e06 element-three home-thirteen-box-shadow vc_custom_1564636361306">
                  <div role="form" class="wpcf7" id="wpcf7-f3578-p3351-o1" lang="en-US" dir="ltr" style="background: #fff;
    box-shadow: 0px 4px 24px -1px rgb(0 0 0 / 10%);padding-bottom:40px;
">
                     <div class="screen-reader-response">
                        <p role="status" aria-live="polite" aria-atomic="true"></p>
                        <ul></ul>
                     </div>
                     <form method="POST" action="{{ route('register') }}"  enctype="multipart/form-data">
                        <div class="get-in_touch" style="padding: 35px 38px 10px 35px;">
                            <div class="row">
                @csrf
                 @include('common.notify')
               
    
                <input type="hidden" name="name" value={{$name}}>
            <input type="hidden" name="email" value={{$email}}>
            <input type="hidden" name="password" value={{$password}}>
            <input type="hidden" name="password_confirmation" value={{$password}}>
           <input type="hidden" name="mobile" value="{{$mobile}}">
          
<input type="hidden" name="user_type" id="user_type" value="{{$user_type}}">
              <div id="otpbox">   <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 no-padding" style="padding:0;">
                                 <div class="form-row">
                    <label for="email" style="font-family:Poppins;color:#251d59;font-size: 15px;font-weight: 500;text-align:left;margin-top:-20px;" class="col-md-8 control-label">Enter OTP</label>

                  <input type="hidden" id="otphidden" value="{{$otpcode}}">
                  <input type="hidden" id="otpstatus" value="0">
                        <input class="form-control" style="margin-top:20px; background-color: #f5f4f4 !important; padding-top: 23px !important;padding-right: 40px !important; padding-bottom: 20px !important; padding-left: 10px !important;border-radius: 4px;" type="number" name="otp" placeholder="Enter 6-Digit OTP" id="otp" autocomplete="off">


                    </div>
                    
                </div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 no-padding" style="margin-top:30px;">
                    
<a href="javascript:void(0);" class="verifymobile"  style="margin-top:30px;color:#080">Resend OTP</a>

                </div>

 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding" style="padding:0;">
  <p><b>Note: </b>Please enter the 6 digit OTP received on your registered mobile no.</p>
</div>
            </div>
                       

                
                <div class="form-button">
                 <div class="radiantthemes-custom-button hover-style-five rt810073130  display-inline-block" data-button-direction="center" data-button-fullwidth="false" style="margin-top:30px;width:100%;">
                <button style="border-radius:5px !important;color: #ffffff; line-height: 20px; font-size: 14px;margin-top:60px;width:100%;border:0;" class="register radiantthemes-custom-button-main  vc_custom_1564647891751" >
                                                   <div class="placeholder" style="font-size:16px;text-transform: uppercase;">Verify</div>
                                              </button>
                                                
 
                                             </div></div>
                
                </div>
            </div>
            </form>
                     
                      
                  </div>
                  <div class="clearfix"></div>
               </div>
               <!-- rt-cf7 -->
            </div>
         </div></center>

              </div>
              
              
              
              
            </div>
         </div>
      </div>
    
   </div>
</section>
                            
                      
                           </div>
                           <!-- .entry-content -->
                        </article>
                        <!-- #post-3351 -->             
                     </div>
                  </main>
                  <!-- #main -->
               </div>
               <!-- #primary -->
            </div>
            <!-- #content -->
         </div>




  <!-- Modal -->


  <!-- Modal -->


  <!-- Modal -->

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


<script type="text/javascript">
     

$(document).ready(function(){

      $(".register").on("click", function () {
        if(document.getElementById("otp").value==''){
            alert("Please must enter OTP.");
            return false;
        }else if(document.getElementById("otphidden").value==document.getElementById("otp").value){
             document.getElementById("otpstatus").value=1;
            return true;
        }else if(document.getElementById("otpstatus").value==0){
            alert("Invalid OTP.Please try again");
            return false;
        }else{
            alert("Invalid OTP.Please try again");
            return false;
        }
         
        });

});
      

  $(document).ready(function(){
      $(".verifymobile").on("click", function () {
  
        if($("#mobile").val() == ''){
            alert("Please Enter Validate Mobile Number");
            return false;
        }else{
            
            document.getElementById("otpbox").style.display="block";
        }
       
       // var amount = $("#payamount").val();
       
                $.ajax({
                    url    : '{{ url("/otpmobile") }}',
                    type   : "POST",
                   data: "_token={{ csrf_token() }}"+"&mobile=7021929014", 
                    success: function (result) {
                        if(result!='0'){
                            $('#otphidden').val(result);
                        }
                       // if (msg['status']=="success") {
                          //  $('#amount').val(result.token_equ_value);
                          //  $('#bonus_token').val(result.bonus_token);
                          //  $('#total_token').val(result.total_token);
                      //  } else {
                         //   $('#amount').val(0);
                      //  }
                    }
                });
});
       
        });


       



       function validateEmail(email) 
    {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }
    </script>



  <style id='radiantthemes-addons-custom-inline-css' type='text/css'>
         .radiantthemes-custom-button.rt810073130 .radiantthemes-custom-button-main{ background:linear-gradient(to right, #251d59 0%, #4a3259 100%) !important;}
         .radiantthemes-custom-button.rt172900896 .radiantthemes-custom-button-main{ background:linear-gradient(to right, #251d59 0%, #4a3259 100%);}
         .radiantthemes-custom-button.rt537002223 .radiantthemes-custom-button-main{ background:linear-gradient(to right, #251d59 0%, #4a3259 100%);}
         .radiantthemes-custom-button.rt636586625 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt2116519540 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt239276426 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt1033353997 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt143729880 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt638223915 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt729187609 .radiantthemes-custom-button-main{ background:linear-gradient(to right, #251d59 0%, #4a3259 100%);}
         .checked { color: orange;}
         .rt-pricing-table.element-nine.rt842858723 > .holder > .spotlight-tag > .spotlight-tag-text,
         .rt-pricing-table.element-nine.spotlight.rt842858723 > .holder > .more .btn{
         background: linear-gradient( to right, #ff7816 0%, #ff590f 100% ) ;
         }
         .rt-pricing-table.element-nine.rt842858723 > .holder > .more .btn{
         border-color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt842858723 > .holder > .more .btn{
         color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1296861313 > .holder > .spotlight-tag > .spotlight-tag-text,
         .rt-pricing-table.element-nine.spotlight.rt1296861313 > .holder > .more .btn{
         background: linear-gradient( to right, #ff7816 0%, #ff590f 100% ) ;
         }
         .rt-pricing-table.element-nine.rt1296861313 > .holder > .more .btn{
         border-color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1296861313 > .holder > .more .btn{
         color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1087964127 > .holder > .spotlight-tag > .spotlight-tag-text,
         .rt-pricing-table.element-nine.spotlight.rt1087964127 > .holder > .more .btn{
         background: linear-gradient( to right, #ff7816 0%, #ff590f 100% ) ;
         }
         .rt-pricing-table.element-nine.rt1087964127 > .holder > .more .btn{
         border-color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1087964127 > .holder > .more .btn{
         color: #ff7816 ;
         }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row, .radiant-contact-form.rt5d04c30e5c56e06 div.wpcf7-response-output{
         }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=submit], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=button],.radiant-contact-form.rt5d04c30e5c56e06 .form-row button[type=submit] {   linear-gradient(to right, #251d59 0%, #4a3259 100%); color:#ffffff; border-top:0px #465579; border-right:0px #465579; border-bottom:0px #465579; border-left:0px #465579; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=submit]:hover, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=button]:hover, .radiant-contact-form.rt5d04c30e5c56e06 .form-row button[type=submit]:hover { background:linear-gradient(to right, #251d59 0%, #4a3259 100%)  }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file], .radiant-contact-form.rt5d04c30e5c56e06 .form-row select, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea {  background-color: #efefef !important; padding-top:23px !important; padding-right:40px !important; padding-bottom:20px !important; padding-left:10px !important; border-radius:4px !important; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file], .radiant-contact-form.rt5d04c30e5c56e06 .form-row select, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea {   color  : #b8b8b8; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row select:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea:focus {   color:#252525; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row select:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea:focus {   border-top:0px solid #f5f4f4; border-right:0px solid #f5f4f4; border-bottom:0px solid #f5f4f4; border-left:0px solid #f5f4f4; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file], .radiant-contact-form.rt5d04c30e5c56e06 .form-row select, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea {   border-top:1px solid #f5f4f4 ! important; border-right:1px solid #f5f4f4 ! important; border-bottom:1px solid #f5f4f4 ! important; border-left:1px solid #f5f4f4 ! important; }
      </style>
@endsection













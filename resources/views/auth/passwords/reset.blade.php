@extends('layout.auth')
@section('content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">


<style>
    
@media only screen and (min-width:275px){
    #mobile{
    display:block;
}
#desktop{
    display:none;
}
#investorlogin{
        height:1800px;
        padding-top:0 !important;
         position: relative; 
         left: 0;
          box-sizing: border-box; 
          width: 400px;          
          opacity:9 !important;
    }
}
@media only screen and (min-width:292px){
    #mobile{
    display:block;
}
#desktop{
    display:none;
}
#investorlogin{
        height:1800px;
        padding-top:0 !important;
         position: relative; 
         left: 0;
          box-sizing: border-box; 
          width: 400px;          
          opacity:9 !important;
    }
}
@media only screen and (min-width:360px){
    #mobile{
    display:block;
}
#desktop{
    display:none;
}
#investorlogin{
         height:1800px;
        padding-top:0 !important;
         position: relative; 
         left: 0;
          box-sizing: border-box; 
          width: 400px;          
          opacity:9 !important;
    }
}
@media only screen and (min-width:375px){
    #mobile{
    display:block;
}
#desktop{
    display:none;
}
#investorlogin{
         height:1800px;
        padding-top:0 !important;
         position: relative; 
         left: 0;
          box-sizing: border-box; 
          width: 400px;          
          opacity:9 !important;
    }
}
@media only screen and (min-width:384px){
    #mobile{
    display:block;
}
#desktop{
    display:none;
}
#investorlogin{
      height:1800px;
        padding-top:61px !important;
         position: relative; 
         left: 0;
          box-sizing: border-box; 
          width: 400px;          
          opacity:9 !important;
    }
}
@media only screen and (min-width:448px){
    #mobile{
    display:none;
}
#desktop{
    display:block;
}
#investorlogin{
        height:1800px;
        padding-top:0 !important;
         position: relative; 
         left: 0;
          box-sizing: border-box; 
          width: 400px;          
          opacity:9 !important;
    }
}


@media only screen and (min-width:975px){
     #mobile{
        display:none;
    }
    #desktop{
        display:block;
    }
    #investorlogin{
        height:800px;
        padding-top:0 !important;
         position: relative; 
         left: -89.5px;
          box-sizing: border-box; 
          width: 1349px; 
          padding-left: 0; 
          padding-right: 0;
          opacity:9 !important;
    }

}
@media only screen and (min-width:995px){
    #mobile{
        display:none;
    }
    #desktop{
        display:block;
    }
#investorlogin{
        height:800px;
        padding-top:0 !important;
         position: relative; 
         left: -89.5px;
          box-sizing: border-box; 
          width: 1349px; 
          padding-left: 0; 
          padding-right: 0;
          opacity:9 !important;
    }

}
@media only screen and (min-width:1100px){
    #mobile{
        display:none;
    }
    #desktop{
        display:block;
    }
    #investorlogin{
        height:800px;
        padding-top:0 !important;
         position: relative; 
         left: -89.5px;
          box-sizing: border-box; 
          width: 1349px; 
          padding-left: 0; 
          padding-right: 0;
          opacity:9 !important;
    }
 
}

</style>


 <div id="page" class="site">
            <!-- #content -->
            <div id="content" class="site-content">
               <div id="primary" class="content-area">
                  <main id="main" class="site-main">
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                     <div class="container page-container">
                        <article id="post-3351" class="post-3351 page type-page status-publish hentry">
                           <header class="entry-header">
                           </header>
                           <!-- .entry-header -->
                           <div class="entry-content">
                          

                             
                             <section id="investorlogin" data-vc-full-width="true" style="" data-vc-full-width-init="true" class="vc_section background-position-center-bottom home-thirteen-pricing-table-responsive vc_custom_1565269117521 vc_section-has-fill">
   <div class="vc_row wpb_row vc_row-fluid vc_custom_1565173495650"><div class="vc_col-sm-12 vc_col-lg-6 vc_col-md-12" style="height:800px;background:url('<?php echo url('/'); ?>/public/asset/login/wp-content/uploads/2021/01/banner_image.jpg');background-size: cover;
    background-position: center;">
         <div class="vc_column-inner vc_custom_1565240932947">
            <div class="wpb_wrapper">
              
              <center>
              <h2 style="font-size: 24px !important;
    color: #fff !important;
    line-height: 40px !important;
    text-align: center !important;
    font-weight: 600 !important;letter-spacing: 0.03px;" class="vc_custom_heading font-weight-regular title-letter-spacing vc_custom_1564573746979">All operations with digital assets<br> in one dashboard</h2>
    <div style="font-size: 17px;color: #fff;line-height: 28px;text-align: center;padding-right:0 !important;" 
    class="vc_custom_heading font-weight-regular visible-lg visible-md visible-sm visible-xs vc_custom_1564998575876">
Aurum Infinity Dashboard is a comprehensive solution to streamline all operations with tokenized securities.
    
    </div>
               <div  class="vc_col-sm-9 vc_col-lg-9 vc_col-md-9" style="float:none;">
     
    <div class="vc_col-sm-12 vc_col-lg-12 vc_col-md-12" style="background: #fff;
    box-shadow:0 8px 16px rgb(32 36 46 / 16%);padding:10px 20px;border-radius:10px;">
            <h2 style="font-size: 18px !important;
    color: #251d59 !important;
    
    text-align: left !important;margin-top:30px;
    font-weight: 600 !important;letter-spacing: 0.03px;margin-bottom:0 !important;" class="vc_custom_heading font-weight-regular title-letter-spacing vc_custom_1564573746979"><i class="fa fa-lock" style="font-size:23px;" ></i>&nbsp;&nbsp;  REGISTER AND GET VERIFIED</h2>
            
            <div style="font-size: 16px;color: #000000;text-align: left;padding-right:0 !important;margin-bottom:15px !important;" 
    class="vc_custom_heading font-weight-regular visible-lg visible-md visible-sm visible-xs vc_custom_1564998575876"      style="">
    Verify your identity to get access to the DS Dashboard and operate security tokens.
    
    </div>
            </div>
    </div>
    
    <div  class="vc_col-sm-9 vc_col-lg-9 vc_col-md-9" style="float:none;">
     
     
    
    <div class="vc_col-sm-12 vc_col-lg-12 vc_col-md-12" style="background: #fff;
    box-shadow:0 8px 16px rgb(32 36 46 / 16%);padding:10px 20px;border-radius:10px;margin-top:30px;">
            <h2 style="font-size: 18px !important;
    color: #251d59 !important; text-align: left !important;
    font-weight: 600 !important;letter-spacing: 0.03px;margin-bottom:0 !important;" class="vc_custom_heading font-weight-regular title-letter-spacing vc_custom_1564573746979"><i class="fas fa-wallet" style="font-size:23px;" ></i>&nbsp;&nbsp;    EASY TO INVEST & TRANSFER</h2>
            
            <div style="font-size: 16px;color: #000000;text-align: left;padding-right:0 !important;margin-bottom:15px !important;" 
    class="vc_custom_heading font-weight-regular visible-lg visible-md visible-sm visible-xs vc_custom_1564998575876"      style="">
    The Dashboard enables users to manage security tokens in a simple and convenient way.
    
    </div>
            </div>
    </div>
    
    
    <div  class="vc_col-sm-9 vc_col-lg-9 vc_col-md-9" style="float:none;">
     
    <div class="vc_col-sm-12 vc_col-lg-12 vc_col-md-12" style="background: #fff;
    box-shadow:0 8px 16px rgb(32 36 46 / 16%);padding:10px 20px;border-radius:10px;margin-top:30px;">
            <h2 style="font-size: 18px !important;
    color: #251d59 !important;
    
    text-align: left !important;
    font-weight: 600 !important;letter-spacing: 0.03px;margin-bottom:0 !important;" class="vc_custom_heading font-weight-regular title-letter-spacing vc_custom_1564573746979"><i class="fa fa-file-text-o" style="font-size:23px;" ></i>&nbsp;&nbsp;   COMPLAINT & LEGALLY APPROVED</h2>
            
            <div style="font-size: 16px;color: #000000;text-align: left;padding-right:0 !important;margin-bottom:15px !important;" 
    class="vc_custom_heading font-weight-regular visible-lg visible-md visible-sm visible-xs vc_custom_1564998575876"      style="">
    Built-in smart contracts ensure compliance of all operations with securities.
    
    </div>
            </div>
    </div>
    
    </center>
              
              
              
              
            </div>
         </div>
      </div>
      <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-12" style="background:#f7f8fa;height:900px;">
        <center>
<h2 style="font-size: 24px !important;
    color: #251d59 !important;
    line-height: 40px !important;
    text-align: center !important;
    font-weight: 600 !important;letter-spacing: 0.03px;margin-top:40px !important;" class="vc_custom_heading font-weight-regular title-letter-spacing vc_custom_1564573746979">WELCOME TO 
    AURUMINFINITY </h2>
    
    
        <div class="vc_column-inner" style='width:80% !important;margin-top:40px !important;'>
        
            <div class="wpb_wrapper">
                      <!-- rt-cf7 -->
               <div class="radiant-contact-form rt5d04c30e5c56e06 element-three home-thirteen-box-shadow vc_custom_1564636361306">
                  <div role="form" class="wpcf7" id="wpcf7-f3578-p3351-o1" lang="en-US" dir="ltr" style="background: #fff;
    box-shadow: 0px 4px 24px -1px rgb(0 0 0 / 10%);padding-bottom:40px;
">
                     <div class="screen-reader-response">
                        <p role="status" aria-live="polite" aria-atomic="true"></p>
                        <ul></ul>
                     </div>
                        <form method="POST" action="{{ route('password.update') }}">
                             <div class="get-in_touch" style="padding:75px 38px 10px 35px;">
                            <div class="row">
                        @csrf
  @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                     <div class="form-row">
                            <label style="font-family:Poppins;color:#251d59;font-size: 16px;font-weight: 500;top:-32px;">{{ __('E-Mail Address') }}</label>

                           
  <span class="wpcf7-form-control-wrap email"> <input id="email" style="margin-top:20px; background-color: #f5f4f4 !important; padding-top: 23px !important;padding-right: 40px !important; padding-bottom: 20px !important; padding-left: 10px !important;border-radius: 4px;" placeholder="Enter your Email ID"  type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}"  required></span>

                             

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            
                        </div></div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                     <div class="form-row">
                            <label style="font-family:Poppins;color:#251d59;font-size: 16px;font-weight: 500;top:-32px;">{{ __('Password') }}</label>

                            
  <span class="wpcf7-form-control-wrap email"> <input style="margin-top:20px; background-color: #f5f4f4 !important; padding-top: 23px !important;padding-right: 40px !important; padding-bottom: 20px !important; padding-left: 10px !important;border-radius: 4px;" id="password" type="password" placeholder="Password"  class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required></span>

                                

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            
                        </div></div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                     <div class="form-row">
                            <label style="font-family:Poppins;color:#251d59;font-size: 16px;font-weight: 500;top:-32px;">{{ __('Confirm Password') }}</label>


<span class="wpcf7-form-control-wrap email"> <input style="margin-top:20px; background-color: #f5f4f4 !important; padding-top: 23px !important;padding-right: 40px !important; padding-bottom: 20px !important; padding-left: 10px !important;border-radius: 4px;" id="password-confirm" type="password" placeholder="Password Confirmation"  class="form-control" name="password_confirmation" required></span>
                            
                               
                            
                        </div></div>


 <div class="form-button">
                    <button style="border-radius:5px !important;color: #ffffff; line-height: 20px; font-size: 14px;margin-top:60px;width:100%;border:0;background: #251d59 !important;" class="radiantthemes-custom-button-main  vc_custom_1564647891751" class="ibtn">
                                                   <div class="placeholder" style="font-size:16px;text-transform: uppercase;">  {{ __('Reset Password') }}</div>
                                              </button>
                   
                    
                </div>
                        



                    </div></div>
                    </form>
                   
                     
                  </div>
                  <div class="clearfix"></div>
               </div>
               <!-- rt-cf7 -->
            </div>
         </div></center>
      </div>
   </div>
</section>
                            
                      
                           </div>
                           <!-- .entry-content -->
                        </article>
                        <!-- #post-3351 -->             
                     </div>
                  </main>
                  <!-- #main -->
               </div>
               <!-- #primary -->
            </div>
            <!-- #content -->
         </div>






   <style id='radiantthemes-addons-custom-inline-css' type='text/css'>
         .radiantthemes-custom-button.rt810073130 .radiantthemes-custom-button-main{ background:linear-gradient(to right, #251d59 0%, #4a3259 100%) !important;}
         .radiantthemes-custom-button.rt172900896 .radiantthemes-custom-button-main{ background:linear-gradient(to right, #251d59 0%, #4a3259 100%);}
         .radiantthemes-custom-button.rt537002223 .radiantthemes-custom-button-main{ background:linear-gradient(to right, #251d59 0%, #4a3259 100%);}
         .radiantthemes-custom-button.rt636586625 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt2116519540 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt239276426 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt1033353997 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt143729880 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt638223915 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt729187609 .radiantthemes-custom-button-main{ background:linear-gradient(to right, #251d59 0%, #4a3259 100%);}
         .checked { color: orange;}
         .rt-pricing-table.element-nine.rt842858723 > .holder > .spotlight-tag > .spotlight-tag-text,
         .rt-pricing-table.element-nine.spotlight.rt842858723 > .holder > .more .btn{
         background: linear-gradient( to right, #ff7816 0%, #ff590f 100% ) ;
         }
         .rt-pricing-table.element-nine.rt842858723 > .holder > .more .btn{
         border-color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt842858723 > .holder > .more .btn{
         color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1296861313 > .holder > .spotlight-tag > .spotlight-tag-text,
         .rt-pricing-table.element-nine.spotlight.rt1296861313 > .holder > .more .btn{
         background: linear-gradient( to right, #ff7816 0%, #ff590f 100% ) ;
         }
         .rt-pricing-table.element-nine.rt1296861313 > .holder > .more .btn{
         border-color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1296861313 > .holder > .more .btn{
         color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1087964127 > .holder > .spotlight-tag > .spotlight-tag-text,
         .rt-pricing-table.element-nine.spotlight.rt1087964127 > .holder > .more .btn{
         background: linear-gradient( to right, #ff7816 0%, #ff590f 100% ) ;
         }
         .rt-pricing-table.element-nine.rt1087964127 > .holder > .more .btn{
         border-color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1087964127 > .holder > .more .btn{
         color: #ff7816 ;
         }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row, .radiant-contact-form.rt5d04c30e5c56e06 div.wpcf7-response-output{
         }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=submit], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=button],.radiant-contact-form.rt5d04c30e5c56e06 .form-row button[type=submit] {   linear-gradient(to right, #251d59 0%, #4a3259 100%); color:#ffffff; border-top:0px #465579; border-right:0px #465579; border-bottom:0px #465579; border-left:0px #465579; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=submit]:hover, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=button]:hover, .radiant-contact-form.rt5d04c30e5c56e06 .form-row button[type=submit]:hover { background:linear-gradient(to right, #251d59 0%, #4a3259 100%)  }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file], .radiant-contact-form.rt5d04c30e5c56e06 .form-row select, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea {  background-color: #efefef !important; padding-top:23px !important; padding-right:40px !important; padding-bottom:20px !important; padding-left:10px !important; border-radius:4px !important; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file], .radiant-contact-form.rt5d04c30e5c56e06 .form-row select, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea {   color  : #b8b8b8; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row select:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea:focus {   color:#252525; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row select:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea:focus {   border-top:0px solid #f5f4f4; border-right:0px solid #f5f4f4; border-bottom:0px solid #f5f4f4; border-left:0px solid #f5f4f4; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file], .radiant-contact-form.rt5d04c30e5c56e06 .form-row select, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea {   border-top:1px solid #f5f4f4 ! important; border-right:1px solid #f5f4f4 ! important; border-bottom:1px solid #f5f4f4 ! important; border-left:1px solid #f5f4f4 ! important; }
      </style>



@endsection

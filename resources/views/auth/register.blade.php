@extends('layout.auth')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
@section('content')



 <div id="page" class="site">
            <!-- #content -->
            <div id="content" class="site-content">
               <div id="primary" class="content-area">
                  <main id="main" class="site-main">
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                     <div class="container page-container">
                        <article id="post-3351" class="post-3351 page type-page status-publish hentry">
                           <header class="entry-header">
                           </header>
                           <!-- .entry-header -->
                           <div class="entry-content">
                          
                             <section data-vc-full-width="true" style="height:800px;padding-top:0 !important; position: relative; left: -89.5px; box-sizing: border-box; width: 1349px; padding-left: 0; padding-right: 0;opacity:9 !important;" data-vc-full-width-init="true" class="vc_section background-position-center-bottom home-thirteen-pricing-table-responsive vc_custom_1565269117521 vc_section-has-fill">
   <div class="vc_row wpb_row vc_row-fluid vc_custom_1565173495650">
      <div class="vc_col-sm-12 vc_col-lg-12 vc_col-md-12" style="height:800px;background-color: #ffffff; /* For browsers that do not support gradients */
  background-image: linear-gradient(#ffffff, #B9983D12);background:url(<?php echo URL('/'); ?>/public/asset/login/wp-content/uploads/2021/01/backgroundissuer.jpg);background-size: cover;
    background-position: center;background-repeat: no-repeat;">
         <div class="vc_column-inner vc_custom_1565240932947">
            <div class="wpb_wrapper">
              <div class="vc_col-sm-6 vc_col-lg-6 vc_col-md-6"></div>  
              
              <div class="vc_col-sm-6 vc_col-lg-6 vc_col-md-6"> 
              <center>
              <style>
    .mainLoginInput{
  height: 40px;
  padding: 0px;
  font-size: 15px;
  margin: 5px 0;
      font-family: 'Public Sans', sans-serif;
}

.mainLoginInput::-webkit-input-placeholder { 
font-family: FontAwesome,'Public Sans', sans-serif;
font-weight: normal;
overflow: visible;
vertical-align: top;
display: inline-block !important;
padding-left: 5px;
padding-top: 2px;
color: #000 !important;
}

.mainLoginInput::-moz-placeholder  { 
font-family: FontAwesome,'Public Sans', sans-serif;
font-weight: normal;
overflow: visible;
vertical-align: top;
display: inline-block !important;
padding-left: 5px;
padding-top: 2px;
color: #000 !important;
}

.mainLoginInput:-ms-input-placeholder  { 
font-family: FontAwesome,'Public Sans', sans-serif;
font-weight: normal;
overflow: visible;
vertical-align: top;
display: inline-block !important;
padding-left: 5px;
padding-top: 2px;
color: #000 !important;
}
</style>    
 <center>

    
        <div class="vc_column-inner" style='width:90% !important;margin-top:40px !important;'>
        
            <div class="wpb_wrapper">
                      <!-- rt-cf7 -->
               <div class="radiant-contact-form rt5d04c30e5c56e06 element-three home-thirteen-box-shadow vc_custom_1564636361306">
                  <div role="form" class="wpcf7" id="wpcf7-f3578-p3351-o1" lang="en-US" dir="ltr" style="background: #fff;
    box-shadow: 0px 4px 24px -1px rgb(0 0 0 / 10%);padding-bottom:40px;
">
                     <div class="screen-reader-response">
                        <p role="status" aria-live="polite" aria-atomic="true"></p>
                        <ul></ul>
                     </div>
                     <form method="POST" action="{{ route('otp') }}"  enctype="multipart/form-data">
                        <div class="get-in_touch" style="padding: 35px 38px 10px 35px;">
                              <h2 style="font-size: 36px !important;
    color: #232F3E !important;
    line-height: 40px !important;margin-bottom: 22px !important;
    text-align: center !important;text-transform: uppercase;
    font-weight: 600 !important;letter-spacing: 0.03px;margin-top:0px !important;font-family: 'Public Sans', sans-serif;
    " class="vc_custom_heading font-weight-regular title-letter-spacing vc_custom_1564573746979">Register Now</h2>
                            <div class="row">
                @csrf
                 @include('common.notify')
               
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding">
                                 <div class="form-row" style="margin-right:10px;">
                   

                   
                         <input class="form-control mainLoginInput" style="background-color: #f5f4f4 !important; padding-top: 23px !important;padding-right: 40px !important; padding-bottom: 20px !important; padding-left: 10px !important;border-radius: 4px;" id="fullname" type="text" name="name" placeholder="&#61447;   Full Name" autocomplete="off">
                         <span id="fullnameerror" style="color:#ff0000;"></span>
                    
                </div>
               </div>
                 
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding">
                                 <div class="form-row">
                      
                             <input class="form-control mainLoginInput" style="background-color: #f5f4f4 !important; padding-top: 23px !important;padding-right: 40px !important; padding-bottom: 20px !important; padding-left: 10px !important;border-radius: 4px;" type="text" id="emailid" name="email" placeholder="&#61443;    E-mail" autocomplete="off">
                              <span id="emailerror" style="color:#ff0000;"></span>
                     </div>
               </div>

               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                 <div class="form-row">
                    
                    
                          <input class="form-control mainLoginInput" style="background-color: #f5f4f4 !important; padding-top: 23px !important;padding-right: 40px !important; padding-bottom: 20px !important; padding-left: 10px !important;border-radius: 4px;" id="password" type="password" name="password" placeholder="&#61475;     Password" autocomplete="off">
                           <span id="passworderror" style="color:#ff0000;"></span>
                     </div>
               </div>

                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                 <div class="form-row">
                  
                  
                        <input class="form-control mainLoginInput" style="background-color: #f5f4f4 !important; padding-top: 23px !important;padding-right: 40px !important; padding-bottom: 20px !important; padding-left: 10px !important;border-radius: 4px;" type="password" id="cpassword" name="password_confirmation" placeholder="&#61475;     Confirm Password" autocomplete="off">
                        <span id="cpassworderror" style="color:#ff0000;"></span>

                    </div>
                </div>
 <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 no-padding">
                                 <div class="form-row">
                    
                  
                        <input class="form-control mainLoginInput" style="background-color: #f5f4f4 !important; padding-top: 23px !important;padding-right: 40px !important; padding-bottom: 20px !important; padding-left: 10px !important;border-radius: 4px;" type="number" name="mobile" placeholder="&#61095; Mobile Number" id="mobile" autocomplete="off">
                        <span id="mobileerror" style="color:#ff0000;"></span>
                    </div>
                </div>
                

              <div id="otpbox" style="display:none;">   <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 no-padding" style="padding:0;">
                                 <div class="form-row">
                   
                  <input type="hidden" id="otphidden">
                   <input type="hidden" id="otpstatus" value="0">
                        <input class="form-control mainLoginInput" style="margin-top:20px; background-color: #f5f4f4 !important; padding-top: 23px !important;padding-right: 40px !important; padding-bottom: 20px !important; padding-left: 10px !important;border-radius: 4px;" type="number" name="otp" placeholder="Enter OTP" id="otp" autocomplete="off">


                    </div>
                  
                </div></div>



                          

                <input type="hidden" value="1" name="user_type" id="user_type">
                <div class="form-button">
                 <div class="radiantthemes-custom-button hover-style-five rt810073130  display-inline-block" data-button-direction="center" data-button-fullwidth="false" style="margin-top:30px;width:100%;">
                                                <button style="border-radius:5px !important;color: #ffffff; line-height: 20px; font-size: 14px;margin-top:60px;width:100%;border:0;" class="register radiantthemes-custom-button-main  vc_custom_1564647891751" >
                                                   <div class="placeholder" style="font-size:16px;text-transform: uppercase;">Register</div>
                                              </button>
                                                
 
                                             </div></div>

                                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding" style="font-size: 18px;
    font-family: 'Public Sans', sans-serif;">
                              Already have an account ? 
                                    <a href="{{ url('/login') }}" style="font-family: 'Public Sans', sans-serif;color:#232F3E !important;text-align:right;text-decoration: underline !important;">Login</a>
                                    
                                
                    </div>
                
                </div>
            </div>
            </form>
                     <div _ngcontent-cld-c53="" class="choice-text" style="padding-bottom:15px;display: none;"><p class="regular-text" style="
    padding-bottom: 14px;
">OR CONTINUE WITH</p>
                    <i class="fa fa-facebook-square" style="font-size:32px;color: #3b5998"></i>&nbsp;&nbsp;
                    <i class="fa fa-twitter-square" style="font-size:32px;color: #1DA1F2"></i>&nbsp;&nbsp;
                    <i class="fa fa-linkedin-square" style="font-size:32px;color: #0077b5"></i>&nbsp;&nbsp;
                     </div> 
                       <!--<button class="btn btn-primary" style="background-color: #3b5998;border-color: #3b5998;font-size:16px;padding-top:4px;"><i class="fa fa-facebook-square" style="font-size:32px;"></i> </button>
                        <button class="btn btn-primary" style="background-color: #0077b5;border-color: #0077b5;font-size:16px;margin-left:10px;"><i class="fa fa-linkedin-square" style="font-size:16px;"></i></button>
                        <button class="btn btn-primary" style="background-color: #1DA1F2;border-color: #1DA1F2;font-size:16px;margin-left:10px;"><i class="fa fa-twitter-square" style="font-size:16px;"></i> </button>
                     -->
                  </div>
                  <div class="clearfix"></div>
               </div>
               <!-- rt-cf7 -->
            </div>
         </div></center>
      </div>
   </div>
</section>

              </center>
              
              </div>
              
            </div>
         </div>
      </div>
    
                        </article>
                        <!-- #post-3351 -->             
                     </div>
                  </main>
                  <!-- #main -->
               </div>
               <!-- #primary -->
            </div>
            <!-- #content -->
         </div>




  <!-- Modal -->


  <!-- Modal -->


  <!-- Modal -->

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


<script type="text/javascript">
     

$(document).ready(function(){

      $(".verifyotp").on("click", function () {
        if(document.getElementById("otphidden").value==document.getElementById("otp").value){
             document.getElementById("otpstatus").value=1;
            return false;
        }else{
            alert("Invalid OTP.Please try again");
            return false;
        }
         
        });
      

});
      




       $(document).ready(function(){

      $(".register").on("click", function () {

         var re = "/\S+@\S+\.\S+/";
         var email= $("#emailid").val();
         var valid = true;
        if($("#fullname").val() == ''){          
            document.getElementById("fullnameerror").innerHTML='It is required';
            valid=false;
        }else{
            document.getElementById("fullnameerror").innerHTML='';
            
        }
    
      if($("#emailid").val()==''){         
           document.getElementById("emailerror").innerHTML='It is required';
            valid=false;
        }else{
            document.getElementById("emailerror").innerHTML='';
         //   return false;
        }
         if($("#emailid").val()!=''){
                if(validateEmail(email)){
                    document.getElementById("emailerror").innerHTML='';
                }else{
                    document.getElementById("emailerror").innerHTML='Please must enter valid Email Id';
                    valid=false;
                }
        }
   
         if($("#password").val() == ''){          
           document.getElementById("passworderror").innerHTML='It is required';
           valid=false;
        }else{
            document.getElementById("passworderror").innerHTML='';
            //return true;
        }

         if($("#cpassword").val() == ''){          
           document.getElementById("cpassworderror").innerHTML='It is required';
            valid=false;
        }else{
            document.getElementById("cpassworderror").innerHTML='';
            //return true;
        }
        if($("#password").val() != $("#cpassword").val()){          
           document.getElementById("cpassworderror").innerHTML='Confirm Password';
           valid=false;
        }
        if($("#mobile").val() == ''){          
           document.getElementById("mobileerror").innerHTML='It is required';
            valid=false;
        }else{
            document.getElementById("mobileerror").innerHTML='';
            //return true;
        }
  
        return valid;
      });
      

});


       function validateEmail(email) 
    {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }
    </script>



  <style id='radiantthemes-addons-custom-inline-css' type='text/css'>
         .radiantthemes-custom-button.rt810073130 .radiantthemes-custom-button-main{ background:linear-gradient(to right, #251d59 0%, #4a3259 100%) !important;}
         .radiantthemes-custom-button.rt172900896 .radiantthemes-custom-button-main{ background:linear-gradient(to right, #251d59 0%, #4a3259 100%);}
         .radiantthemes-custom-button.rt537002223 .radiantthemes-custom-button-main{ background:linear-gradient(to right, #251d59 0%, #4a3259 100%);}
         .radiantthemes-custom-button.rt636586625 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt2116519540 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt239276426 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt1033353997 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt143729880 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt638223915 .radiantthemes-custom-button-main:hover{ background-color:#ffffff !important; color:#251d59 !important;}
         .radiantthemes-custom-button.rt729187609 .radiantthemes-custom-button-main{ background:linear-gradient(to right, #251d59 0%, #4a3259 100%);}
         .checked { color: orange;}
         .rt-pricing-table.element-nine.rt842858723 > .holder > .spotlight-tag > .spotlight-tag-text,
         .rt-pricing-table.element-nine.spotlight.rt842858723 > .holder > .more .btn{
         background: linear-gradient( to right, #ff7816 0%, #ff590f 100% ) ;
         }
         .rt-pricing-table.element-nine.rt842858723 > .holder > .more .btn{
         border-color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt842858723 > .holder > .more .btn{
         color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1296861313 > .holder > .spotlight-tag > .spotlight-tag-text,
         .rt-pricing-table.element-nine.spotlight.rt1296861313 > .holder > .more .btn{
         background: linear-gradient( to right, #ff7816 0%, #ff590f 100% ) ;
         }
         .rt-pricing-table.element-nine.rt1296861313 > .holder > .more .btn{
         border-color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1296861313 > .holder > .more .btn{
         color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1087964127 > .holder > .spotlight-tag > .spotlight-tag-text,
         .rt-pricing-table.element-nine.spotlight.rt1087964127 > .holder > .more .btn{
         background: linear-gradient( to right, #ff7816 0%, #ff590f 100% ) ;
         }
         .rt-pricing-table.element-nine.rt1087964127 > .holder > .more .btn{
         border-color: #ff7816 ;
         }
         .rt-pricing-table.element-nine.rt1087964127 > .holder > .more .btn{
         color: #ff7816 ;
         }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row, .radiant-contact-form.rt5d04c30e5c56e06 div.wpcf7-response-output{
         }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=submit], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=button],.radiant-contact-form.rt5d04c30e5c56e06 .form-row button[type=submit] {   linear-gradient(to right, #251d59 0%, #4a3259 100%); color:#ffffff; border-top:0px #465579; border-right:0px #465579; border-bottom:0px #465579; border-left:0px #465579; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=submit]:hover, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=button]:hover, .radiant-contact-form.rt5d04c30e5c56e06 .form-row button[type=submit]:hover { background:linear-gradient(to right, #251d59 0%, #4a3259 100%)  }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file], .radiant-contact-form.rt5d04c30e5c56e06 .form-row select, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea {  background-color: #efefef !important; padding-top:23px !important; padding-right:40px !important; padding-bottom:20px !important; padding-left:10px !important; border-radius:4px !important; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file], .radiant-contact-form.rt5d04c30e5c56e06 .form-row select, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea {   color  : #b8b8b8; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row select:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea:focus {   color:#252525; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file]:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row select:focus, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea:focus {   border-top:0px solid #f5f4f4; border-right:0px solid #f5f4f4; border-bottom:0px solid #f5f4f4; border-left:0px solid #f5f4f4; }
         .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=text], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=email], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=url], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=tel], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=number], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=password], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=date], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=time], .radiant-contact-form.rt5d04c30e5c56e06 .form-row input[type=file], .radiant-contact-form.rt5d04c30e5c56e06 .form-row select, .radiant-contact-form.rt5d04c30e5c56e06 .form-row textarea {   border-top:1px solid #f5f4f4 ! important; border-right:1px solid #f5f4f4 ! important; border-bottom:1px solid #f5f4f4 ! important; border-left:1px solid #f5f4f4 ! important; }
      </style>
@endsection













<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->truncate();
        DB::table('countries')->insert(array(
  array('id' => '1','code' => 'AF','countrycode' => 'AFG','countryname' => 'Afghanistan'),
  array('id' => '2','code' => 'AX','countrycode' => 'ALA','countryname' => 'Åland'),
  array('id' => '3','code' => 'AL','countrycode' => 'ALB','countryname' => 'Albania'),
  array('id' => '4','code' => 'DZ','countrycode' => 'DZA','countryname' => 'Algeria'),
  array('id' => '5','code' => 'AS','countrycode' => 'ASM','countryname' => 'American Samoa'),
  array('id' => '6','code' => 'AD','countrycode' => 'AND','countryname' => 'Andorra'),
  array('id' => '7','code' => 'AO','countrycode' => 'AGO','countryname' => 'Angola'),
  array('id' => '8','code' => 'AI','countrycode' => 'AIA','countryname' => 'Anguilla'),
  array('id' => '9','code' => 'AQ','countrycode' => 'ATA','countryname' => 'Antarctica'),
  array('id' => '10','code' => 'AG','countrycode' => 'ATG','countryname' => 'Antigua and Barbuda'),
  array('id' => '11','code' => 'AR','countrycode' => 'ARG','countryname' => 'Argentina'),
  array('id' => '12','code' => 'AM','countrycode' => 'ARM','countryname' => 'Armenia'),
  array('id' => '13','code' => 'AW','countrycode' => 'ABW','countryname' => 'Aruba'),
  array('id' => '14','code' => 'AU','countrycode' => 'AUS','countryname' => 'Australia'),
  array('id' => '15','code' => 'AT','countrycode' => 'AUT','countryname' => 'Austria'),
  array('id' => '16','code' => 'AZ','countrycode' => 'AZE','countryname' => 'Azerbaijan'),
  array('id' => '17','code' => 'BS','countrycode' => 'BHS','countryname' => 'Bahamas'),
  array('id' => '18','code' => 'BH','countrycode' => 'BHR','countryname' => 'Bahrain'),
  array('id' => '19','code' => 'BD','countrycode' => 'BGD','countryname' => 'Bangladesh'),
  array('id' => '20','code' => 'BB','countrycode' => 'BRB','countryname' => 'Barbados'),
  array('id' => '21','code' => 'BY','countrycode' => 'BLR','countryname' => 'Belarus'),
  array('id' => '22','code' => 'BE','countrycode' => 'BEL','countryname' => 'Belgium'),
  array('id' => '23','code' => 'BZ','countrycode' => 'BLZ','countryname' => 'Belize'),
  array('id' => '24','code' => 'BJ','countrycode' => 'BEN','countryname' => 'Benin'),
  array('id' => '25','code' => 'BM','countrycode' => 'BMU','countryname' => 'Bermuda'),
  array('id' => '26','code' => 'BT','countrycode' => 'BTN','countryname' => 'Bhutan'),
  array('id' => '27','code' => 'BO','countrycode' => 'BOL','countryname' => 'Bolivia'),
  array('id' => '28','code' => 'BQ','countrycode' => 'BES','countryname' => 'Bonaire'),
  array('id' => '29','code' => 'BA','countrycode' => 'BIH','countryname' => 'Bosnia and Herzegovina'),
  array('id' => '30','code' => 'BW','countrycode' => 'BWA','countryname' => 'Botswana'),
  array('id' => '31','code' => 'BV','countrycode' => 'BVT','countryname' => 'Bouvet Island'),
  array('id' => '32','code' => 'BR','countrycode' => 'BRA','countryname' => 'Brazil'),
  array('id' => '33','code' => 'IO','countrycode' => 'IOT','countryname' => 'British Indian Ocean Territory'),
  array('id' => '34','code' => 'VG','countrycode' => 'VGB','countryname' => 'British Virgin Islands'),
  array('id' => '35','code' => 'BN','countrycode' => 'BRN','countryname' => 'Brunei'),
  array('id' => '36','code' => 'BG','countrycode' => 'BGR','countryname' => 'Bulgaria'),
  array('id' => '37','code' => 'BF','countrycode' => 'BFA','countryname' => 'Burkina Faso'),
  array('id' => '38','code' => 'BI','countrycode' => 'BDI','countryname' => 'Burundi'),
  array('id' => '39','code' => 'KH','countrycode' => 'KHM','countryname' => 'Cambodia'),
  array('id' => '40','code' => 'CM','countrycode' => 'CMR','countryname' => 'Cameroon'),
  array('id' => '41','code' => 'CA','countrycode' => 'CAN','countryname' => 'Canada'),
  array('id' => '42','code' => 'CV','countrycode' => 'CPV','countryname' => 'Cape Verde'),
  array('id' => '43','code' => 'KY','countrycode' => 'CYM','countryname' => 'Cayman Islands'),
  array('id' => '44','code' => 'CF','countrycode' => 'CAF','countryname' => 'Central African Republic'),
  array('id' => '45','code' => 'TD','countrycode' => 'TCD','countryname' => 'Chad'),
  array('id' => '46','code' => 'CL','countrycode' => 'CHL','countryname' => 'Chile'),
  array('id' => '47','code' => 'CN','countrycode' => 'CHN','countryname' => 'China'),
  array('id' => '48','code' => 'CX','countrycode' => 'CXR','countryname' => 'Christmas Island'),
  array('id' => '49','code' => 'CC','countrycode' => 'CCK','countryname' => 'Cocos [Keeling] Islands'),
  array('id' => '50','code' => 'CO','countrycode' => 'COL','countryname' => 'Colombia'),
  array('id' => '51','code' => 'KM','countrycode' => 'COM','countryname' => 'Comoros'),
  array('id' => '52','code' => 'CK','countrycode' => 'COK','countryname' => 'Cook Islands'),
  array('id' => '53','code' => 'CR','countrycode' => 'CRI','countryname' => 'Costa Rica'),
  array('id' => '54','code' => 'HR','countrycode' => 'HRV','countryname' => 'Croatia'),
  array('id' => '55','code' => 'CU','countrycode' => 'CUB','countryname' => 'Cuba'),
  array('id' => '56','code' => 'CW','countrycode' => 'CUW','countryname' => 'Curacao'),
  array('id' => '57','code' => 'CY','countrycode' => 'CYP','countryname' => 'Cyprus'),
  array('id' => '58','code' => 'CZ','countrycode' => 'CZE','countryname' => 'Czech Republic'),
  array('id' => '59','code' => 'CD','countrycode' => 'COD','countryname' => 'Democratic Republic of the Congo'),
  array('id' => '60','code' => 'DK','countrycode' => 'DNK','countryname' => 'Denmark'),
  array('id' => '61','code' => 'DJ','countrycode' => 'DJI','countryname' => 'Djibouti'),
  array('id' => '62','code' => 'DM','countrycode' => 'DMA','countryname' => 'Dominica'),
  array('id' => '63','code' => 'DO','countrycode' => 'DOM','countryname' => 'Dominican Republic'),
  array('id' => '64','code' => 'TL','countrycode' => 'TLS','countryname' => 'East Timor'),
  array('id' => '65','code' => 'EC','countrycode' => 'ECU','countryname' => 'Ecuador'),
  array('id' => '66','code' => 'EG','countrycode' => 'EGY','countryname' => 'Egypt'),
  array('id' => '67','code' => 'SV','countrycode' => 'SLV','countryname' => 'El Salvador'),
  array('id' => '68','code' => 'GQ','countrycode' => 'GNQ','countryname' => 'Equatorial Guinea'),
  array('id' => '69','code' => 'ER','countrycode' => 'ERI','countryname' => 'Eritrea'),
  array('id' => '70','code' => 'EE','countrycode' => 'EST','countryname' => 'Estonia'),
  array('id' => '71','code' => 'ET','countrycode' => 'ETH','countryname' => 'Ethiopia'),
  array('id' => '72','code' => 'FK','countrycode' => 'FLK','countryname' => 'Falkland Islands'),
  array('id' => '73','code' => 'FO','countrycode' => 'FRO','countryname' => 'Faroe Islands'),
  array('id' => '74','code' => 'FJ','countrycode' => 'FJI','countryname' => 'Fiji'),
  array('id' => '75','code' => 'FI','countrycode' => 'FIN','countryname' => 'Finland'),
  array('id' => '76','code' => 'FR','countrycode' => 'FRA','countryname' => 'France'),
  array('id' => '77','code' => 'GF','countrycode' => 'GUF','countryname' => 'French Guiana'),
  array('id' => '78','code' => 'PF','countrycode' => 'PYF','countryname' => 'French Polynesia'),
  array('id' => '79','code' => 'TF','countrycode' => 'ATF','countryname' => 'French Southern Territories'),
  array('id' => '80','code' => 'GA','countrycode' => 'GAB','countryname' => 'Gabon'),
  array('id' => '81','code' => 'GM','countrycode' => 'GMB','countryname' => 'Gambia'),
  array('id' => '82','code' => 'GE','countrycode' => 'GEO','countryname' => 'Georgia'),
  array('id' => '83','code' => 'DE','countrycode' => 'DEU','countryname' => 'Germany'),
  array('id' => '84','code' => 'GH','countrycode' => 'GHA','countryname' => 'Ghana'),
  array('id' => '85','code' => 'GI','countrycode' => 'GIB','countryname' => 'Gibraltar'),
  array('id' => '86','code' => 'GR','countrycode' => 'GRC','countryname' => 'Greece'),
  array('id' => '87','code' => 'GL','countrycode' => 'GRL','countryname' => 'Greenland'),
  array('id' => '88','code' => 'GD','countrycode' => 'GRD','countryname' => 'Grenada'),
  array('id' => '89','code' => 'GP','countrycode' => 'GLP','countryname' => 'Guadeloupe'),
  array('id' => '90','code' => 'GU','countrycode' => 'GUM','countryname' => 'Guam'),
  array('id' => '91','code' => 'GT','countrycode' => 'GTM','countryname' => 'Guatemala'),
  array('id' => '92','code' => 'GG','countrycode' => 'GGY','countryname' => 'Guernsey'),
  array('id' => '93','code' => 'GN','countrycode' => 'GIN','countryname' => 'Guinea'),
  array('id' => '94','code' => 'GW','countrycode' => 'GNB','countryname' => 'Guinea-Bissau'),
  array('id' => '95','code' => 'GY','countrycode' => 'GUY','countryname' => 'Guyana'),
  array('id' => '96','code' => 'HT','countrycode' => 'HTI','countryname' => 'Haiti'),
  array('id' => '97','code' => 'HM','countrycode' => 'HMD','countryname' => 'Heard Island and McDonald Islands'),
  array('id' => '98','code' => 'HN','countrycode' => 'HND','countryname' => 'Honduras'),
  array('id' => '99','code' => 'HK','countrycode' => 'HKG','countryname' => 'Hong Kong'),
  array('id' => '100','code' => 'HU','countrycode' => 'HUN','countryname' => 'Hungary'),
  array('id' => '101','code' => 'IS','countrycode' => 'ISL','countryname' => 'Iceland'),
  array('id' => '102','code' => 'IN','countrycode' => 'IND','countryname' => 'India'),
  array('id' => '103','code' => 'ID','countrycode' => 'IDN','countryname' => 'Indonesia'),
  array('id' => '104','code' => 'IR','countrycode' => 'IRN','countryname' => 'Iran'),
  array('id' => '105','code' => 'IQ','countrycode' => 'IRQ','countryname' => 'Iraq'),
  array('id' => '106','code' => 'IE','countrycode' => 'IRL','countryname' => 'Ireland'),
  array('id' => '107','code' => 'IM','countrycode' => 'IMN','countryname' => 'Isle of Man'),
  array('id' => '108','code' => 'IL','countrycode' => 'ISR','countryname' => 'Israel'),
  array('id' => '109','code' => 'IT','countrycode' => 'ITA','countryname' => 'Italy'),
  array('id' => '110','code' => 'CI','countrycode' => 'CIV','countryname' => 'Ivory Coast'),
  array('id' => '111','code' => 'JM','countrycode' => 'JAM','countryname' => 'Jamaica'),
  array('id' => '112','code' => 'JP','countrycode' => 'JPN','countryname' => 'Japan'),
  array('id' => '113','code' => 'JE','countrycode' => 'JEY','countryname' => 'Jersey'),
  array('id' => '114','code' => 'JO','countrycode' => 'JOR','countryname' => 'Jordan'),
  array('id' => '115','code' => 'KZ','countrycode' => 'KAZ','countryname' => 'Kazakhstan'),
  array('id' => '116','code' => 'KE','countrycode' => 'KEN','countryname' => 'Kenya'),
  array('id' => '117','code' => 'KI','countrycode' => 'KIR','countryname' => 'Kiribati'),
  array('id' => '118','code' => 'XK','countrycode' => 'XKX','countryname' => 'Kosovo'),
  array('id' => '119','code' => 'KW','countrycode' => 'KWT','countryname' => 'Kuwait'),
  array('id' => '120','code' => 'KG','countrycode' => 'KGZ','countryname' => 'Kyrgyzstan'),
  array('id' => '121','code' => 'LA','countrycode' => 'LAO','countryname' => 'Laos'),
  array('id' => '122','code' => 'LV','countrycode' => 'LVA','countryname' => 'Latvia'),
  array('id' => '123','code' => 'LB','countrycode' => 'LBN','countryname' => 'Lebanon'),
  array('id' => '124','code' => 'LS','countrycode' => 'LSO','countryname' => 'Lesotho'),
  array('id' => '125','code' => 'LR','countrycode' => 'LBR','countryname' => 'Liberia'),
  array('id' => '126','code' => 'LY','countrycode' => 'LBY','countryname' => 'Libya'),
  array('id' => '127','code' => 'LI','countrycode' => 'LIE','countryname' => 'Liechtenstein'),
  array('id' => '128','code' => 'LT','countrycode' => 'LTU','countryname' => 'Lithuania'),
  array('id' => '129','code' => 'LU','countrycode' => 'LUX','countryname' => 'Luxembourg'),
  array('id' => '130','code' => 'MO','countrycode' => 'MAC','countryname' => 'Macao'),
  array('id' => '131','code' => 'MK','countrycode' => 'MKD','countryname' => 'Macedonia'),
  array('id' => '132','code' => 'MG','countrycode' => 'MDG','countryname' => 'Madagascar'),
  array('id' => '133','code' => 'MW','countrycode' => 'MWI','countryname' => 'Malawi'),
  array('id' => '134','code' => 'MY','countrycode' => 'MYS','countryname' => 'Malaysia'),
  array('id' => '135','code' => 'MV','countrycode' => 'MDV','countryname' => 'Maldives'),
  array('id' => '136','code' => 'ML','countrycode' => 'MLI','countryname' => 'Mali'),
  array('id' => '137','code' => 'MT','countrycode' => 'MLT','countryname' => 'Malta'),
  array('id' => '138','code' => 'MH','countrycode' => 'MHL','countryname' => 'Marshall Islands'),
  array('id' => '139','code' => 'MQ','countrycode' => 'MTQ','countryname' => 'Martinique'),
  array('id' => '140','code' => 'MR','countrycode' => 'MRT','countryname' => 'Mauritania'),
  array('id' => '141','code' => 'MU','countrycode' => 'MUS','countryname' => 'Mauritius'),
  array('id' => '142','code' => 'YT','countrycode' => 'MYT','countryname' => 'Mayotte'),
  array('id' => '143','code' => 'MX','countrycode' => 'MEX','countryname' => 'Mexico'),
  array('id' => '144','code' => 'FM','countrycode' => 'FSM','countryname' => 'Micronesia'),
  array('id' => '145','code' => 'MD','countrycode' => 'MDA','countryname' => 'Moldova'),
  array('id' => '146','code' => 'MC','countrycode' => 'MCO','countryname' => 'Monaco'),
  array('id' => '147','code' => 'MN','countrycode' => 'MNG','countryname' => 'Mongolia'),
  array('id' => '148','code' => 'ME','countrycode' => 'MNE','countryname' => 'Montenegro'),
  array('id' => '149','code' => 'MS','countrycode' => 'MSR','countryname' => 'Montserrat'),
  array('id' => '150','code' => 'MA','countrycode' => 'MAR','countryname' => 'Morocco'),
  array('id' => '151','code' => 'MZ','countrycode' => 'MOZ','countryname' => 'Mozambique'),
  array('id' => '152','code' => 'MM','countrycode' => 'MMR','countryname' => 'Myanmar [Burma]'),
  array('id' => '153','code' => 'NA','countrycode' => 'NAM','countryname' => 'Namibia'),
  array('id' => '154','code' => 'NR','countrycode' => 'NRU','countryname' => 'Nauru'),
  array('id' => '155','code' => 'NP','countrycode' => 'NPL','countryname' => 'Nepal'),
  array('id' => '156','code' => 'NL','countrycode' => 'NLD','countryname' => 'Netherlands'),
  array('id' => '157','code' => 'NC','countrycode' => 'NCL','countryname' => 'New Caledonia'),
  array('id' => '158','code' => 'NZ','countrycode' => 'NZL','countryname' => 'New Zealand'),
  array('id' => '159','code' => 'NI','countrycode' => 'NIC','countryname' => 'Nicaragua'),
  array('id' => '160','code' => 'NE','countrycode' => 'NER','countryname' => 'Niger'),
  array('id' => '161','code' => 'NG','countrycode' => 'NGA','countryname' => 'Nigeria'),
  array('id' => '162','code' => 'NU','countrycode' => 'NIU','countryname' => 'Niue'),
  array('id' => '163','code' => 'NF','countrycode' => 'NFK','countryname' => 'Norfolk Island'),
  array('id' => '164','code' => 'KP','countrycode' => 'PRK','countryname' => 'North Korea'),
  array('id' => '165','code' => 'MP','countrycode' => 'MNP','countryname' => 'Northern Mariana Islands'),
  array('id' => '166','code' => 'NO','countrycode' => 'NOR','countryname' => 'Norway'),
  array('id' => '167','code' => 'OM','countrycode' => 'OMN','countryname' => 'Oman'),
  array('id' => '168','code' => 'PK','countrycode' => 'PAK','countryname' => 'Pakistan'),
  array('id' => '169','code' => 'PW','countrycode' => 'PLW','countryname' => 'Palau'),
  array('id' => '170','code' => 'PS','countrycode' => 'PSE','countryname' => 'Palestine'),
  array('id' => '171','code' => 'PA','countrycode' => 'PAN','countryname' => 'Panama'),
  array('id' => '172','code' => 'PG','countrycode' => 'PNG','countryname' => 'Papua New Guinea'),
  array('id' => '173','code' => 'PY','countrycode' => 'PRY','countryname' => 'Paraguay'),
  array('id' => '174','code' => 'PE','countrycode' => 'PER','countryname' => 'Peru'),
  array('id' => '175','code' => 'PH','countrycode' => 'PHL','countryname' => 'Philippines'),
  array('id' => '176','code' => 'PN','countrycode' => 'PCN','countryname' => 'Pitcairn Islands'),
  array('id' => '177','code' => 'PL','countrycode' => 'POL','countryname' => 'Poland'),
  array('id' => '178','code' => 'PT','countrycode' => 'PRT','countryname' => 'Portugal'),
  array('id' => '179','code' => 'PR','countrycode' => 'PRI','countryname' => 'Puerto Rico'),
  array('id' => '180','code' => 'QA','countrycode' => 'QAT','countryname' => 'Qatar'),
  array('id' => '181','code' => 'CG','countrycode' => 'COG','countryname' => 'Republic of the Congo'),
  array('id' => '182','code' => 'RE','countrycode' => 'REU','countryname' => 'Réunion'),
  array('id' => '183','code' => 'RO','countrycode' => 'ROU','countryname' => 'Romania'),
  array('id' => '184','code' => 'RU','countrycode' => 'RUS','countryname' => 'Russia'),
  array('id' => '185','code' => 'RW','countrycode' => 'RWA','countryname' => 'Rwanda'),
  array('id' => '186','code' => 'BL','countrycode' => 'BLM','countryname' => 'Saint Barthélemy'),
  array('id' => '187','code' => 'SH','countrycode' => 'SHN','countryname' => 'Saint Helena'),
  array('id' => '188','code' => 'KN','countrycode' => 'KNA','countryname' => 'Saint Kitts and Nevis'),
  array('id' => '189','code' => 'LC','countrycode' => 'LCA','countryname' => 'Saint Lucia'),
  array('id' => '190','code' => 'MF','countrycode' => 'MAF','countryname' => 'Saint Martin'),
  array('id' => '191','code' => 'PM','countrycode' => 'SPM','countryname' => 'Saint Pierre and Miquelon'),
  array('id' => '192','code' => 'VC','countrycode' => 'VCT','countryname' => 'Saint Vincent and the Grenadines'),
  array('id' => '193','code' => 'WS','countrycode' => 'WSM','countryname' => 'Samoa'),
  array('id' => '194','code' => 'SM','countrycode' => 'SMR','countryname' => 'San Marino'),
  array('id' => '195','code' => 'ST','countrycode' => 'STP','countryname' => 'São Tomé and Príncipe'),
  array('id' => '196','code' => 'SA','countrycode' => 'SAU','countryname' => 'Saudi Arabia'),
  array('id' => '197','code' => 'SN','countrycode' => 'SEN','countryname' => 'Senegal'),
  array('id' => '198','code' => 'RS','countrycode' => 'SRB','countryname' => 'Serbia'),
  array('id' => '199','code' => 'SC','countrycode' => 'SYC','countryname' => 'Seychelles'),
  array('id' => '200','code' => 'SL','countrycode' => 'SLE','countryname' => 'Sierra Leone'),
  array('id' => '201','code' => 'SG','countrycode' => 'SGP','countryname' => 'Singapore'),
  array('id' => '202','code' => 'SX','countrycode' => 'SXM','countryname' => 'Sint Maarten'),
  array('id' => '203','code' => 'SK','countrycode' => 'SVK','countryname' => 'Slovakia'),
  array('id' => '204','code' => 'SI','countrycode' => 'SVN','countryname' => 'Slovenia'),
  array('id' => '205','code' => 'SB','countrycode' => 'SLB','countryname' => 'Solomon Islands'),
  array('id' => '206','code' => 'SO','countrycode' => 'SOM','countryname' => 'Somalia'),
  array('id' => '207','code' => 'ZA','countrycode' => 'ZAF','countryname' => 'South Africa'),
  array('id' => '208','code' => 'GS','countrycode' => 'SGS','countryname' => 'South Georgia and the South Sandwich Islands'),
  array('id' => '209','code' => 'KR','countrycode' => 'KOR','countryname' => 'South Korea'),
  array('id' => '210','code' => 'SS','countrycode' => 'SSD','countryname' => 'South Sudan'),
  array('id' => '211','code' => 'ES','countrycode' => 'ESP','countryname' => 'Spain'),
  array('id' => '212','code' => 'LK','countrycode' => 'LKA','countryname' => 'Sri Lanka'),
  array('id' => '213','code' => 'SD','countrycode' => 'SDN','countryname' => 'Sudan'),
  array('id' => '214','code' => 'SR','countrycode' => 'SUR','countryname' => 'Suriname'),
  array('id' => '215','code' => 'SJ','countrycode' => 'SJM','countryname' => 'Svalbard and Jan Mayen'),
  array('id' => '216','code' => 'SZ','countrycode' => 'SWZ','countryname' => 'Swaziland'),
  array('id' => '217','code' => 'SE','countrycode' => 'SWE','countryname' => 'Sweden'),
  array('id' => '218','code' => 'CH','countrycode' => 'CHE','countryname' => 'Switzerland'),
  array('id' => '219','code' => 'SY','countrycode' => 'SYR','countryname' => 'Syria'),
  array('id' => '220','code' => 'TW','countrycode' => 'TWN','countryname' => 'Taiwan'),
  array('id' => '221','code' => 'TJ','countrycode' => 'TJK','countryname' => 'Tajikistan'),
  array('id' => '222','code' => 'TZ','countrycode' => 'TZA','countryname' => 'Tanzania'),
  array('id' => '223','code' => 'TH','countrycode' => 'THA','countryname' => 'Thailand'),
  array('id' => '224','code' => 'TG','countrycode' => 'TGO','countryname' => 'Togo'),
  array('id' => '225','code' => 'TK','countrycode' => 'TKL','countryname' => 'Tokelau'),
  array('id' => '226','code' => 'TO','countrycode' => 'TON','countryname' => 'Tonga'),
  array('id' => '227','code' => 'TT','countrycode' => 'TTO','countryname' => 'Trinidad and Tobago'),
  array('id' => '228','code' => 'TN','countrycode' => 'TUN','countryname' => 'Tunisia'),
  array('id' => '229','code' => 'TR','countrycode' => 'TUR','countryname' => 'Turkey'),
  array('id' => '230','code' => 'TM','countrycode' => 'TKM','countryname' => 'Turkmenistan'),
  array('id' => '231','code' => 'TC','countrycode' => 'TCA','countryname' => 'Turks and Caicos Islands'),
  array('id' => '232','code' => 'TV','countrycode' => 'TUV','countryname' => 'Tuvalu'),
  array('id' => '233','code' => 'UM','countrycode' => 'UMI','countryname' => 'U.S. Minor Outlying Islands'),
  array('id' => '234','code' => 'VI','countrycode' => 'VIR','countryname' => 'U.S. Virgin Islands'),
  array('id' => '235','code' => 'UG','countrycode' => 'UGA','countryname' => 'Uganda'),
  array('id' => '236','code' => 'UA','countrycode' => 'UKR','countryname' => 'Ukraine'),
  array('id' => '237','code' => 'AE','countrycode' => 'ARE','countryname' => 'United Arab Emirates'),
  array('id' => '238','code' => 'GB','countrycode' => 'GBR','countryname' => 'United Kingdom'),
  array('id' => '239','code' => 'US','countrycode' => 'USA','countryname' => 'United States'),
  array('id' => '240','code' => 'UY','countrycode' => 'URY','countryname' => 'Uruguay'),
  array('id' => '241','code' => 'UZ','countrycode' => 'UZB','countryname' => 'Uzbekistan'),
  array('id' => '242','code' => 'VU','countrycode' => 'VUT','countryname' => 'Vanuatu'),
  array('id' => '243','code' => 'VA','countrycode' => 'VAT','countryname' => 'Vatican City'),
  array('id' => '244','code' => 'VE','countrycode' => 'VEN','countryname' => 'Venezuela'),
  array('id' => '245','code' => 'VN','countrycode' => 'VNM','countryname' => 'Vietnam'),
  array('id' => '246','code' => 'WF','countrycode' => 'WLF','countryname' => 'Wallis and Futuna'),
  array('id' => '247','code' => 'EH','countrycode' => 'ESH','countryname' => 'Western Sahara'),
  array('id' => '248','code' => 'YE','countrycode' => 'YEM','countryname' => 'Yemen'),
  array('id' => '249','code' => 'ZM','countrycode' => 'ZMB','countryname' => 'Zambia'),
  array('id' => '250','code' => 'ZW','countrycode' => 'ZWE','countryname' => 'Zimbabwe')
));       
            
    }
}
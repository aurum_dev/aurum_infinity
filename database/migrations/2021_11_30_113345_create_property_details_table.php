<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->default(0);
            $table->integer('user_id')->default(0);
            $table->longText('propertyOverview')->nullable();
            $table->longText('propertyLocationOverview')->nullable();
            $table->string('propertyVideo', 200)->nullable();
            $table->string('geo_location', 200)->nullable();
            $table->string('street_address', 200)->nullable();
            $table->string('floor_number', 200)->nullable();
            $table->string('area_name', 200)->nullable();
            $table->integer('pincode')->nullable();
            $table->integer('yearOfConstruction')->nullable();
            $table->string('community', 200)->nullable();
            $table->integer('no_of_bed')->nullable();
            $table->string('facilities', 200)->nullable();
            $table->double('facilitation_fees')->default(0);
            $table->double('psf_rate')->default(0);
            $table->string('custodian', 200)->nullable();
            $table->double('registration_fees')->default(0);
            $table->double('total_cost_psf')->default(0);
            $table->double('net_operating_yield_sale')->default(0);
            $table->double('net_realizable_yield_sale')->default(0);
            $table->double('net_realizable_yield_purchase')->default(0);
            $table->integer('stamp_duty')->default(0);
            $table->integer('sale_price')->default(0);
            $table->longText('propertyDetailsHighlights')->nullable();
            $table->string('propertyimages', 200)->nullable();
            $table->string('floorplan', 200)->nullable();
            $table->longText('ManagementTeamDescription')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_details');
    }
}

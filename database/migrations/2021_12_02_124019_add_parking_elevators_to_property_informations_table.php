<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParkingElevatorsToPropertyInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_informations', function (Blueprint $table) {
              $table->enum('parking_elevators', ['Yes', 'No'])->default('Yes')->nullable()->after('ventilation_operational_hours');

              $table->enum('parking_lift', ['Yes', 'No'])->default('Yes')->nullable()->after('parking_elevators');

              $table->enum('temp_parking', ['Yes', 'No'])->default('Yes')->nullable()->after('parking_lift');

              $table->enum('parking_washroom', ['Yes', 'No'])->default('Yes')->nullable()->after('temp_parking');

              $table->enum('visitor_management', ['Yes', 'No'])->default('Yes')->nullable()->after('parking_washroom');

              $table->enum('access_control_system', ['Yes', 'No'])->default('Yes')->nullable()->after('visitor_management');

              $table->enum('dedicated_security_control', ['Yes', 'No'])->default('Yes')->nullable()->after('access_control_system');

              $table->enum('fire_emergency', ['Yes', 'No'])->default('Yes')->nullable()->after('dedicated_security_control');
              
              $table->enum('fire_exits', ['Yes', 'No'])->default('Yes')->nullable()->after('fire_emergency');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_informations', function (Blueprint $table) {
            //
        });
    }
}

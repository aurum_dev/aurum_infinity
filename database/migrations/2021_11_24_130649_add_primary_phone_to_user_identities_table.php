<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrimaryPhoneToUserIdentitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_identities', function (Blueprint $table) {
            $table->string('primary_phone')->nullable()->after('country_code');
            $table->string('city_id')->nullable()->after('primary_phone');
            $table->string('eth_address')->nullable()->after('city_id');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_identities', function (Blueprint $table) {
            $table->dropColumn('primary_phone');
            $table->dropColumn('city_id');
            $table->dropColumn('eth_address');
        });
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function () {
    Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('adminlogin');
    Route::get('/issuer/login', 'AdminAuth\LoginController@showLoginIssuerForm')->name('adminIssuerlogin');
    Route::post('/login', 'AdminAuth\LoginController@login');
    Route::post('/logout', 'AdminAuth\LoginController@logout')->name('adminlogout');

    //Route::get('/issuer/register', 'AdminAuth\RegisterController@showIssuerRegistrationForm')->name('Issuerregister');
    //Route::post('/issuer/register', 'AdminAuth\RegisterController@register');

    Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');   
    Route::post('/register', 'AdminAuth\RegisterController@register');


    Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});
Route::post('/otpmobile', 'EpinetController@sendMsg')->name('otpmobile');
Route::post('/otp', 'EpinetController@registerotp')->name('otp');
Route::get('/issuer/login', 'Auth\LoginController@showLoginIssuerForm')->name('adminIssuerlogin');
Route::post('/issuer/login', 'AdminAuth\LoginController@login');

Route::get('/issuer/register', 'Auth\RegisterController@showIssuerRegistrationForm')->name('Issuerregister');
Route::post('/issuer/register', 'Auth\RegisterController@register_issuer');
Route::get('/issuer/dividenttokendetail/{id}', 'IssuerController@dividenttokendetail')->name('dividenttokendetail');


Route::get('/', 'Auth\LoginController@welcome');
Route::get('/investor', 'Auth\LoginController@investor')->name('investor');
Route::get('/blognew', 'Auth\LoginController@blog')->name('blog');
Route::get('/issuer', 'Auth\LoginController@issuer')->name('issuer');

Route::post('getenquiryform', 'Auth\LoginController@getenquiryform')->name('getenquiryform');
Route::post('getenquiryformhome', 'HomeController@getenquiryformhome')->name('getenquiryformhome');
Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify')->name('email_verify');

Route::get('forgot', function () {
    return view('forgot');
});

Route::get('/success', function () {
    return view('success');
});

Route::get('/wyre', 'WyreController@index');
Route::get('/wyretest', 'WyreController@wyretest');

Route::get('fiatpaymentstatus', 'WyreController@fiatpaymentstatus')->name('fiatpaymentstatus');

Auth::routes();

Route::get('/admin/dashboard', 'AdminDashboardController@index')->middleware('auth:admin');

Route::group(['middleware' => 'auth'], function () {
    // ********************** Investor ***********************
    Route::get('/home', 'HomeController@index')->name('home');
    
     Route::get('/notifications', 'HomeController@notifications')->name('notifications');
      Route::get('/bonusdetail/{id}', 'HomeController@notificationsdetail')->name('notificationsdetail');
    Route::get('/dashboard', 'HomeController@investment')->name('dashboard');
    //Route::get('/propertyList', 'HomeController@propertyList')->name('propertyList');
    Route::get('/intel', 'HomeController@intel')->name('intel');
     Route::get('/help', 'HomeController@help')->name('help');
    Route::get('/profile', 'HomeController@profile')->name('profile');
    Route::get('/propertyDetail/{id}', 'HomeController@propertyDetail')->name('propertyDetail');
    Route::get('/applyInvest/{id}', 'HomeController@applyInvest')->name('applyInvest');
Route::get('/applypayment', 'HomeController@applypayment')->name('applypayment');

    Route::post('investstore', 'HomeController@investstore')->name('investstore');


    Route::get('walletBalanceAPI', 'HomeController@wallet_balance_api')->name('walletBalanceAPI');


    Route::get('security', 'UserController@security')->name('security');

    Route::post('/change/password', 'HomeController@update_password')->name('change.password');
    Route::get('participants', function () {
        return view('participants');
    });
    Route::get('prospectus', 'HomeController@prospectus');
    Route::post('prospectus_download', 'HomeController@prospectus_download')->name('prospectus_download');
    Route::get('kyc', 'HomeController@kyc');
    Route::post('personalinfo', 'HomeController@personalinfo');
    Route::post('userdetail', 'HomeController@userdetail')->name('userdetail');
    Route::post('kycstore', 'HomeController@kycstore');
    Route::post('accrediteduserstore', 'HomeController@accrediteduserstore');
    Route::post('accreditedkycstore', 'HomeController@accreditedkycstore');
    Route::post('usercompanydetail', 'HomeController@usercompanydetail')->name('usercompanydetail');
    Route::post('usercompanypersonaldetail', 'HomeController@usercompanypersonaldetail')->name('usercompanypersonaldetail');
    Route::get('wallet', 'HomeController@wallet')->name('wallet');
    Route::get('payment_history', 'HomeController@paymenthistory')->name('paymenthistory');
    Route::get('withdrawETH', 'HomeController@withdrawETHBalance')->name('withdrawETH');
    Route::post('/sendETH', 'HomeController@sendETH')->name('sendETH');
    Route::get('/generate/withdrawOTP', 'HomeController@generateWithdrawOTP')->name('generateWithdrawOTP');
    Route::post('privateKeyPassword', 'HomeController@privateKeyPassword')->name('privateKeyPassword');
    Route::get('privateKeyShow/{id}', 'HomeController@privateKeyShow')->name('privateKeyShow');
    Route::post('requestaddress', 'HomeController@requestaddress')->name('requestaddress');
    Route::post('addtoken', 'HomeController@addtoken')->name('addtoken');
    Route::post('withdraw', 'HomeController@withdraw')->name('withdraw');
    Route::post('storePayment', 'HomeController@storePayment');
    Route::post('payment-gateway', 'HomeController@doPayment');
     Route::post('paymentSuccess', 'HomeController@paymentSuccess');
 Route::get('paymentFailed', 'HomeController@paymentFailed');
    Route::post('payment', 'HomeController@payment');
    Route::get('balance', 'HomeController@balance_of_coin')->name('balance');
    Route::post('getcryptobalance', 'HomeController@getcryptobalance')->name('getcryptobalance');
    Route::post('gettokeninvestvalue', 'HomeController@gettokeninvestvalue')->name('gettokeninvestvalue');
    

    // ---------------- Invest --------------------
    Route::get('invest_token/{id}', 'HomeController@invest_token')->name('invest_token');
    Route::post('investstore', 'HomeController@investstore')->name('investstore');
    //Route::get('fiatpaymentstatus', 'HomeController@fiatpaymentstatus')->name('fiatpaymentstatus');
    // ---------------- Invest -------------------  -

    Route::get('dividend', 'HomeController@dividend');
    Route::get('voting', 'HomeController@showvoting');
    Route::post('votingstore', 'HomeController@votingstore')->name('votingstore');
    Route::get('support', function () {
        return view('support');
    });
    Route::post('supportstore', 'HomeController@supportstore')->name('supportstore');

    Route::get('privacy-policy', function () {
        return view('privacy-policy');
    });
    Route::get('terms-use', function () {
        return view('terms-use');
    });
    // ********************** Investor ***********************

    //Google 2 factor
    Route::get('g2fenablesuccess/{id}', 'UserController@g2fenablesuccess')->name('g2fenablesuccess');
    Route::get('/2fa/enable', 'Google2FAController@enableTwoFactor');
    Route::get('/2fa/disable', 'Google2FAController@disableTwoFactor')->name('disableTwoFactor');;
    // Web check otp
    Route::post('/g2fotpcheckenable', 'Google2FAController@g2fotpcheckenable');

});

Route::get('/2fa/validate', 'Auth\LoginController@getValidateToken');
Route::post('/2fa/validate', ['middleware' => 'throttle:5', 'uses' => 'Auth\LoginController@postValidateToken']);

// API check otp
//Route::post('/gfaenablereal', 'Google2FAController@postValidateToken');
Route::post('/gfavalidateotp', 'Google2FAController@gfavalidateotp');


/*Route Real Estate STO*/

Route::get('/privacy', function(){ return view('privacy');});
Route::get('/terms_conditions', function(){ return view('terms_conditions');});

Route::get('/blog', 'HomeController@blog')->name('blog');
Route::get('/blogDetail', 'HomeController@blogDetail')->name('blogDetail'); //Dynami
//Route::get('/wallet', 'HomeController@wallet')->name('wallet');

Route::get('/investment', 'HomeController@investment')->name('investment');
Route::get('/investDetail', 'HomeController@investDetail')->name('investDetail');
Route::get('/setting', 'HomeController@setting')->name('setting');
Route::get('/activity', 'HomeController@activity')->name('activity');
Route::get('/sellToken', 'HomeController@sellToken')->name('sellToken');
Route::get('/buyToken', 'HomeController@buyToken')->name('buyToken');
Route::post('/profile-identity', 'HomeController@profile_identity')->name('profile.identity');
Route::post('/phelp', 'HomeController@phelp')->name('phelp');
Route::post('/profile-finance', 'HomeController@profile_finance')->name('profile.finance');
Route::post('/profile-background', 'HomeController@profile_background')->name('profile.background');
Route::get('/propertyList', 'HomeController@propertyList')->name('propertyList');

Route::get('/under_construction', function () {
    return view('under_construction');
}
);

Route::get('/propertyList/{type?}', 'HomeController@propertyList')->name('propertyList');
Route::get('/property-asset-list/{type}', 'HomeController@propertyAssetList')->name('propertyAssetList');


Route::get('/propertyDetail/{id}', 'HomeController@propertyDetail')->name('propertyDetail'); // Dynamic

Route::get('/countrycity/{country}', 'UserController@countrycity')->name('countrycity');
Route::group(['prefix' => 'issuer', 'middleware' => 'auth'], function () {
    Route::get('/dashboard', 'IssuerController@dashboard')->name('dashboard');
    Route::get('/kyc', 'IssuerController@kyc')->name('kyc');
    Route::post('/kyc', 'IssuerController@updateKyc')->name('updateKyc');
     Route::post('/updateBankDetails', 'IssuerController@updateBankDetails')->name('updateBankDetails');
    Route::get('/profile', 'IssuerController@profile')->name('profile');
    Route::post('/profile_update', 'IssuerController@profile_update')->name('profile_update');
    Route::get('/property', 'IssuerController@property')->name('property');

    Route::post('/property', 'IssuerController@storeProperty')->name('propertyStore');
    Route::get('/dividendlist', 'IssuerController@dividendlist')->name('dividendlist');
    Route::post('/dividendstore', 'IssuerController@dividendStore')->name('dividendStore');
    Route::get('/withdrawETH', 'IssuerController@withdrawETHBalance')->name('withdrawETH');
    Route::post('/sendETH', 'IssuerController@sendETH')->name('sendETH');
    Route::get('/generate/withdrawOTP', 'IssuerController@generateWithdrawOTP')->name('generateWithdrawOTP');
    Route::get('/token', 'IssuerController@token')->name('token');
    Route::get('/dividend', 'IssuerController@dividend')->name('dividend');
    Route::get('/asset_fund', 'IssuerController@asset_fund')->name('asset_fund');
    Route::get('/propertydetails/{id}', 'IssuerController@propertyDetail')->name('issuer_propertyDetail'); // Dynamic
    Route::get('/tokenList/{id}', 'IssuerController@tokenList')->name('tokenList');
 Route::get('/dividentdetail/{id}', 'IssuerController@dividentdetail')->name('dividentdetail');
 Route::get('/investordividendnotification/{id}', 'IssuerController@investordividendnotification')->name('investordividendnotification');
    Route::get('/tokenRequest', 'IssuerController@tokenRequest')->name('tokenRequest');
    Route::get('/tokenApprove', 'IssuerController@tokenApprove')->name('tokenApprove');
    Route::get('/issuer_paymenthistory', 'IssuerController@wallet')->name('issuer_paymenthistory');
    Route::get('razorpay-payment', 'RazorpayController@index');
    Route::post('razorpay-payment', 'RazorpayController@store')->name('razorpay.payment.store');
    Route::post('storePayment', 'IssuerController@storePayment');
});
